dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: rcugui.m4,v 1.3 2009-02-09 23:11:57 hehi Exp $ 
dnl  
dnl  ROOT generic rcugui framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl ------------------------------------------------------------------
dnl
dnl AC_ROOT_RCUGUI([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUGUI],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcugui-prefix],
        [AC_HELP_STRING([--with-rcugui-prefix],
		[Prefix where Rcugui is installed])],
        rcugui_prefix=$withval, rcugui_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcugui-url],
        [AC_HELP_STRING([--with-rcugui-url],
		[Base URL where the Rcugui dodumentation is installed])],
        rcugui_url=$withval, rcugui_url="")
    if test "x${RCUGUI_CONFIG+set}" != xset ; then 
        if test "x$rcugui_prefix" != "x" ; then 
	    RCUGUI_CONFIG=$rcugui_prefix/bin/rcugui-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUGUI_CONFIG, rcugui-config, no)
    rcugui_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for Rcugui version >= $rcugui_min_version)

    # Check if we got the script
    rcugui_found=no    
    if test "x$RCUGUI_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUGUI_CPPFLAGS=`$RCUGUI_CONFIG --cppflags`
       RCUGUI_INCLUDEDIR=`$RCUGUI_CONFIG --includedir`
       RCUGUI_LIBS=`$RCUGUI_CONFIG --libs`
       RCUGUI_LTLIBS=`$RCUGUI_CONFIG --ltlibs`
       RCUGUI_LIBDIR=`$RCUGUI_CONFIG --libdir`
       RCUGUI_LDFLAGS=`$RCUGUI_CONFIG --ldflags`
       RCUGUI_LTLDFLAGS=`$RCUGUI_CONFIG --ltldflags`
       RCUGUI_PREFIX=`$RCUGUI_CONFIG --prefix`

       # Check the version number is OK.
       rcugui_version=`$RCUGUI_CONFIG -V` 
       rcugui_vers=`echo $rcugui_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcugui_regu=`echo $rcugui_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcugui_vers -ge $rcugui_regu ; then 
            rcugui_found=yes
       fi
    fi
    AC_MSG_RESULT($rcugui_found - is $rcugui_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUGUI, [Whether we have rcugui])


    if test "x$rcugui_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUGUI_LIBDIR $RCUGUI_LIBS $ROOTAUXLIBS $ROOTGLIBS"
    	CPPFLAGS="$CPPFLAGS $RCUGUI_CPPFLAGS $ROOTCFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcugui_main_h=0
        AC_CHECK_HEADER([rcugui/Main.h], [have_rcugui_main_h=1])

        # Check the library. 
        have_librcugui=no
        AC_MSG_CHECKING(for -lrcugui)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcugui/Main.h>],
                        [RcuGui::Main::Instance();])], 
                        [have_librcugui=yes])
        AC_MSG_RESULT($have_librcugui)

        if test $have_rcugui_main_h -gt 0    && \
            test "x$have_librcugui"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUGUI)
        else 
            rcugui_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the Rcugui documentation is installed)
    if test "x$rcugui_url" = "x" && \
	test ! "x$RCUGUI_PREFIX" = "x" ; then 
       RCUGUI_URL=${RCUGUI_PREFIX}/share/doc/rcugui/html
    else 
	RCUGUI_URL=$rcugui_url
    fi	
    AC_MSG_RESULT($RCUGUI_URL)
   
    if test "x$rcugui_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUGUI_URL)
    AC_SUBST(RCUGUI_PREFIX)
    AC_SUBST(RCUGUI_CPPFLAGS)
    AC_SUBST(RCUGUI_INCLUDEDIR)
    AC_SUBST(RCUGUI_LDFLAGS)
    AC_SUBST(RCUGUI_LIBDIR)
    AC_SUBST(RCUGUI_LIBS)
    AC_SUBST(RCUGUI_LTLIBS)
    AC_SUBST(RCUGUI_LTLDFLAGS)
])

dnl
dnl EOF
dnl 

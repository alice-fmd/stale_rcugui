//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "Monitor.h"
#include "MonitorFrame.h"
#include <rcugui/rcuMonitor.xpm>
#include <TSystem.h>
#include <TApplication.h>
#include <iostream>


//====================================================================
RcuGui::Monitor::Monitor()
  : fMain(gClient->GetRoot(), 10, 10, kVerticalFrame), 
    fMenuHints(kLHintsExpandX),
    fMenu(&fMain), 
    fFrameHints(kLHintsExpandX|kLHintsExpandY),
    fFrame(0), 
    fStatusHints(kLHintsExpandX), 
    fStatus(&fMain)
{
  fMain.AddFrame(&fMenu,   &fMenuHints);

  // Add file items 
  fFileMenu = fMenu.AddPopup("&File");
  fFileMenu->AddEntry("&Exit", 1);

  // Set up status fields 
  fStatus.SetParts(3);
  
  // Set up connections 
  fMain.Connect("CloseWindow()", "RcuGui::Monitor", this, "HandleClose()");
  fFileMenu->Connect("Activated(Int_t)", "RcuGui::Monitor", this, 
		     "HandleMenu(int)");  
}

//____________________________________________________________________
void
RcuGui::Monitor::SetFrame(MonitorFrame& frame) 
{
  fFrame = &frame;
  fMain.AddFrame(fFrame,  &fFrameHints);
  fMain.AddFrame(&fStatus, &fStatusHints);
  fFrame->Connect("SourceChanged()","RcuGui::Monitor", this, "HandleSource()");
  fFrame->Connect("Updated()",      "RcuGui::Monitor", this, "HandleUpdate()");
}


//____________________________________________________________________
void
RcuGui::Monitor::HandleClose() 
{
  fFrame->End();
  fMain.DontCallClose();
  fMain.UnmapWindow();
  // if (fFrame) delete fFrame;
  gApplication->Terminate();
}
	      
//____________________________________________________________________
void
RcuGui::Monitor::HandleMenu(Int_t id) 
{
  switch (id) {
  case 1: 
    HandleClose(); 
    break;
  }
}

//____________________________________________________________________
void
RcuGui::Monitor::HandleSource() 
{
  fStatus.SetText(fFrame->Source(), 0);
}
//____________________________________________________________________
void
RcuGui::Monitor::HandleUpdate() 
{
  fStatus.SetText(Form("Last event:  %12ld", fFrame->Last()),    1);
  fStatus.SetText(Form("Events read: %12ld", fFrame->Counter()), 2);
}

//____________________________________________________________________
void
RcuGui::Monitor::Display()
{
  if (!fFrame) {
    std::cerr << "No frame defined" << std::endl;
    exit(1);
  }
  fMain.MapSubwindows();
  fMain.Resize(fMain.GetDefaultSize());
  Int_t w = fMain.GetWidth();
  Int_t h = fMain.GetHeight();
  fMain.SetWMSize(w, h);
  fMain.SetWMSizeHints(0.5*w, 0.5*h, 2*w, 2*h, 2, 2);
  fMain.SetWindowName("RCU Monitor");
  fMain.SetIconName("RCU Monitor");
  fMain.SetClassHints("RcuMonitor", "RcuMonitor");
  fMain.SetMWMHints(kMWMDecorAll      | 
		    kMWMDecorResizeH  | 
		    kMWMDecorMaximize |
		    kMWMDecorMinimize | 
		    kMWMDecorMenu,
		    kMWMFuncAll      | 
		    kMWMFuncResize   | 
		    kMWMFuncMaximize |
		    kMWMFuncMinimize, 
		    kMWMInputModeless);
  const TGPicture* p = 
    gClient->GetPicturePool()->GetPicture("rcu_monitor.xpm", 
					  const_cast<char**>(rcuMonitor_xpm));
  if (p) {
    Pixmap_t pic = p->GetPicture();
    gVirtualX->SetIconPixmap(fMain.GetId(), pic);
  }
  fMain.MapWindow();
}

//____________________________________________________________________
bool 
RcuGui::Monitor::Start(const char* input, long n, size_t skip,
		       bool all, bool wait) 
{
  if (!fFrame) {
    std::cerr << "No frame!" << std::endl;
    return false;
  }
  return fFrame->Start(input, n, skip, all, wait);
}

  
//____________________________________________________________________
//
// EOF
//


  




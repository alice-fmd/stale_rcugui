// -*- mode: C++ -*-
//____________________________________________________________________
//
// $Id: MonitorFrame.h,v 1.4 2009-02-09 23:11:57 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    MonitorFrame.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of MonitorFrame
*/
#ifndef RCUGUI_MONITORFRAME_H
#define RCUGUI_MONITORFRAME_H
#ifndef RCUDATA_READER_H
# include <rcudata/Reader.h>
#endif
#ifndef RCUDATA_PROGRESSMETER_H
# include <rcudata/ProgressMeter.h>
#endif
#ifndef RCUDATA_TYPES_H
# include <rcudata/Types.h>
#endif
#ifndef RCUGUI_LABELEDNUMBER_H
# include <rcugui/LabeledNumber.h>
#endif
#ifndef RCUGUI_MONITORTIMER
# include <rcugui/MonitorTimer.h>
#endif
#ifndef RCUGUI_MONITORTREE_H
# include <rcugui/MonitorTree.h>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif
class TH1;


namespace RcuGui
{
  //____________________________________________________________________
  /** @struct MonitorFrame 
      @brief Frame and visitor 
      @ingroup rcugui_read
  */
  struct MonitorFrame : public TGGroupFrame, public RcuData::ChannelVisitor
  {
    enum {
      kSummed, 
      kSingle, 
      kResume, 
      kPause, 
      kStop,
      kClear,
      kPreMake,
      kFit, 
      kDelete
    };
    /** Contructor.  
	@param f      Frame to put this into. */
    MonitorFrame(TGCompositeFrame& f);
    /** Destructor */
    virtual ~MonitorFrame();
    /** Set the tree */ 
    virtual void SetTree(MonitorTree& t);
    /** Handle buttons */ 
    void HandleButtons(Int_t id);
#if 0
    /** Handle a draw */
    void HandleDraw();
    /** Handle selection changed */
    void HandleSelect();
#endif
    /** Handle fit button */
    void HandleFit();
    /** Handle frequency change of timer */
    void HandleFreq();
    /** Handle frequency change of GUI */
    void HandleUpdate();
    /** Run the monitor. 
	@param input  Input source file 
	@param n      Number of events to read
	@param skip   Number of events to skip
	@param all    Whether to get all events
	@param wait   Whether to wait for data 
	@return @c true on success, false otherwise */
    bool Start(const char* input, long n=-1, size_t skip=0,
	       bool all=false, bool wait=false);
    /** Called at end of run */ 
    void End();
    /** Get the next event */ 
    bool GetNextEvent();
    /** @return End of data flag */
    bool IsEOD() const;
    /** @return number of events processed */ 
    unsigned long Counter() const { return fCounter; }
    /** @return Last event number read */ 
    unsigned long Last() const { return fLast; }
    /** @return The source url */ 
    const char* Source() const { return fSource.c_str(); }
    /** Signal emitted when source is changed */ 
    void SourceChanged() { Emit("SourceChanged()"); }//*SIGNAL*
    /** Signal emitted when updating display */ 
    void Updated() { Emit("Updated()"); }//*SIGNAL*
  protected:
    /** Overwrite the AltroDecoder member function that is called at
	the start of a channel stream.  Here, we find the relevant
	histogram, and make that current.  This is done to speed up
	the processing.  
	@param c Channel object.  */
    bool GotChannel(RcuData::Channel& c, bool);
    /** Overwrite the AltroDecoder member function that is called when
	we get a new timebins data.  this is done so we can process
	the data as soon as possible. 
	@param t   Time bin (note, timebin 14 corresponds to the time
	of the level 1 trigger). 
	@param adc ADC value of time bin @a t */
    bool GotData(RcuData::uint32_t t, RcuData::uint32_t adc);
       
    /** Rcu data reader */ 
    RcuData::Reader* fReader;
    /** Progress meter */ 
    RcuData::ProgressMeter fMeter;
    /** Current channel */ 
    RcuData::Channel* fCurrent;
    
    /** The timer */
    MonitorTimer<MonitorFrame> fTimer;

    /** Hints for mother frame */
    TGLayoutHints        fTopHints;
    /** Main frame */
    TGHorizontalFrame    fTopCont;

    /** Hints for operations frame */
    TGLayoutHints        fOperHints;
    /** Operations */
    TGVerticalFrame      fOper;

    // Operations
    /** Run control */
    TGButtonGroup        fRun;
    /** Resume processing */
    TGTextButton         fResume;
    /** Pause processing */
    TGTextButton         fPause;
    /** Stop processing */
    TGTextButton         fStop;
    /** Clear all histograms */
    TGTextButton         fClear;

    // Misc.
    /** Hints for misc frame */
    TGLayoutHints        fMiscHints;
    /** Run control */
    TGButtonGroup        fMisc;
    /** Resume processing */
    TGTextButton         fPre;
    /** Pause processing */
    TGTextButton         fFit;
    /** Update frequency */
    LabeledIntEntry      fUpdate;
    /** Update frequency */
    LabeledIntEntry      fFreq;
    /** Delete all histograms */
    // TGTextButton         fDelete;

    // Select tree 
    /** Hints for selector */
    // TGLayoutHints        fSelectHints;
    /** Select what to show */
    // TGButtonGroup        fSelect;
    /** Show summed histogram */
    // TGRadioButton        fSummed;
    /** Show inidiviual time-bins or channels */
    // TGRadioButton        fSingle;

    /** Hints for view frame */
    TGLayoutHints        fViewHints;
    /** View frame */
    MonitorTree*         fView;


    /** Current histogram */ 
    // TH1* fCurrentHisto;
    /** Counter of events read */
    unsigned long        fCounter;
    /** Last event number read */ 
    unsigned long        fLast;
    /** The source */
    std::string          fSource;
    /** Other stuff */
    size_t               fUpdateFreq;
    /** Other stuff */
    bool                 fIsStop;
    /** Other stuff */
    bool                 fNeedUpdate;

    ClassDef(MonitorFrame,0);
  };
}
#endif

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_NUMBERDIALOG_H
#define RCUGUI_NUMBERDIALOG_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGMenu
# include <TGMenu.h>
#endif
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif
#ifndef ROOT_TGNumberEntry
# include <TGNumberEntry.h>
#endif
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif
#ifndef ROOT_TGStatusBar
# include <TGStatusBar.h>
#endif

//__________________________________________________________________
namespace RcuGui 
{
  //__________________________________________________________________
  /** @class NumberDialog 
      @brief Transient dialog to input a single integer 
      @ingroup rcugui_basic
  */
  class NumberDialog : public TGTransientFrame 
  {
  public:
    /** Constructor 
	@param p Where to put the transient window
	@param main Where to center the transient window 
	@param title Title of the window 
	@param text Text to put in the dialog 
	@param number On input, the starting value.  On return, the
	value input by the user  */
    NumberDialog(const TGWindow *p, const TGWindow *main,
		 const char *title, const char* text, Int_t& number);
    /** Slot to close down the window */
    void Done();
  private:
    /** Text of the dialog */
    TGLabel  fText;
    /** Input field */
    TGNumberEntry  fNumber;
    /** OK button */
    TGTextButton fOK;
    /** Return value */
    Int_t& fValue;
    ClassDef(NumberDialog,0);
  };
  
}
#endif
//
// EOF
//

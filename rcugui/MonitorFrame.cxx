//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "MonitorFrame.h"
#include "ConnectionDialog.h"
#include "SourceDialog.h"
#include <TF1.h>
#include <TH1.h>
#include <TSystem.h>
#include <rcudata/Channel.h>
#include <iostream>

//====================================================================
RcuGui::MonitorFrame::MonitorFrame(TGCompositeFrame& p)
  : TGGroupFrame(&p, "Monitor", kVerticalFrame),
    fReader(0), 
    fCurrent(0), 
    fTimer(this),
    fTopHints(kLHintsExpandX|kLHintsTop, 3, 3, 3, 3),
    fTopCont(this), 
    // Operations
    fOperHints(kLHintsExpandX|kLHintsExpandY, 0, 0, 0, 0),
    fOper(&fTopCont), 
    fRun(&fOper,       "Run",                         kHorizontalFrame), 
    fResume(&fRun,     "Resume",                      kResume), 
    fPause(&fRun,      "Pause",                       kPause),
    fStop(&fRun,       "Stop",                        kStop),
    fClear(&fRun,      "Clear",                       kClear),
    // Misc. 
    fMiscHints(kLHintsExpandX|kLHintsExpandY, 2, 0, 0, 0),
    fMisc(&fTopCont,   "Misc.",                       kVerticalFrame),
    fPre(&fMisc,       "Pre-allocate histograms ...", kPreMake),
    fFit(&fMisc,       "Fit current spectra",         kFit),
    fUpdate(fMisc,     "Update Freq. (events)", 1, 100000000,kHorizontalFrame),
    fFreq(fMisc,       "Monitor Freq. (ms)", kItimerResolution, 
	  100000000, kHorizontalFrame),
    // Selector 
    // fSelectHints(kLHintsExpandX|kLHintsExpandY, 2, 0, 0, 0),
    // fSelect(&fTopCont, "Display",                     kVerticalFrame), 
    // fSummed(&fSelect,  "Summed",                      kSummed), 
    // fSingle(&fSelect,  "Single",                      kSingle), 
    // The view 
    fViewHints(kLHintsExpandX|kLHintsExpandY, 3, 3, 0, 0),
    // fView(fTop, *this, 800, 600), 
    fView(0),
    // Other stuff 
    // fCurrentHisto(0),
    fCounter(0),
    fLast(0),
    fUpdateFreq(1), 
    fIsStop(false), 
    fNeedUpdate(false)
{
  // fCurrentHisto = fTop.GetSummary();

  // Top Frame 
  AddFrame(&fTopCont, &fTopHints);

  // Control buttons
  fTopCont.AddFrame(&fOper, &fOperHints);
  fOper.AddFrame(&fRun, &fOperHints);
  fPause.SetEnabled(kFALSE);
  fStop.SetEnabled(kFALSE);
  fResume.SetEnabled(kTRUE);
  fClear.SetEnabled(kTRUE);
  fRun.Connect("Clicked(Int_t)", "RcuGui::MonitorFrame", 
	       this, "HandleButtons(int)");

  // Misc buttons 
  fTopCont.AddFrame(&fMisc, &fMiscHints);
  fPre.SetEnabled(kTRUE);
  fFit.SetEnabled(kTRUE);
  fMisc.Connect("Clicked(Int_t)", "RcuGui::MonitorFrame", 
		this, "HandleButtons(int)");

  // Select buttons 
  // fTopCont.AddFrame(&fSelect, &fSelectHints);
  // fSummed.SetDown(kTRUE, kFALSE);
  // fSelect.SetExclusive();
  // fSelect.Connect("Clicked(Int_t)", "RcuGui::MonitorFrame", 
  //                 this, "HandleButtons(int)");

  // Upddate frequncy 
  fUpdate.fView.Connect("ValueSet(Long_t)", "RcuGui::MonitorFrame", 
			this, "HandleUpdate()");  
  fFreq.fView.Connect("ValueSet(Long_t)", "RcuGui::MonitorFrame", 
		      this, "HandleFreq()");  
}

//____________________________________________________________________
void
RcuGui::MonitorFrame::SetTree(MonitorTree& tree) 
{
  fView = &tree;
  // Bottom part
  AddFrame(fView, &fViewHints);
#if 0
  fView->Connect("SelectionChanged()", "RcuGui::MonitorFrame", this, 
		"HandleSelect()");

  // Draw current histogram 
  fView->cd();
  fCurrentHisto->Draw();
#endif
  fView->HandleSelect();
  fFreq.SetValue(fTimer.GetTime());
  fTimer.TurnOff();
}

//____________________________________________________________________
RcuGui::MonitorFrame::~MonitorFrame()
{
  std::cout << std::endl;
}

//____________________________________________________________________
void
RcuGui::MonitorFrame::HandleUpdate()
{
  fUpdateFreq = fUpdate.GetValue();
}

//____________________________________________________________________
void
RcuGui::MonitorFrame::HandleFreq()
{
  fTimer.TurnOff();
  std::cout << "Setting timer frequency to " << fFreq.GetValue() 
	    << " miliseconds" << std::endl;
  fTimer.SetTime(fFreq.GetValue());
  fTimer.TurnOn();
}

//____________________________________________________________________
void
RcuGui::MonitorFrame::HandleButtons(Int_t id) 
{
  switch (id) {
  case kResume:
    if (!fReader) {
      static TString src, out;
      static bool   tree = false, all = false;
      static long   n    = -1;
      static size_t skip = 0;
      src = "";
      int opt = SourceDialog::kOffline | SourceDialog::kOnline;
      new SourceDialog(gClient->GetRoot(), src, out, 
		       n, skip, tree, all, opt);
      if (!src.IsNull()) Start(src.Data(), n, skip, tree, all);
      else return;
    }
    fResume.SetEnabled(kFALSE);
    fPause.SetEnabled(kTRUE);
    fStop.SetEnabled(kTRUE);
    fClear.SetEnabled(kFALSE);
    fPre.SetEnabled(kFALSE);
    fFit.SetEnabled(kFALSE);
    fTimer.TurnOn();
    break;
  case kPause:
    fResume.SetEnabled(kTRUE);
    fPause.SetEnabled(kFALSE);
    fStop.SetEnabled(kTRUE);
    fClear.SetEnabled(kTRUE);
    fPre.SetEnabled(kTRUE);
    fFit.SetEnabled(kTRUE);
    fTimer.TurnOff();
    break;
  case kStop:
    fResume.SetEnabled(kTRUE);
    fPause.SetEnabled(kFALSE);
    fStop.SetEnabled(kFALSE);
    fClear.SetEnabled(kFALSE);
    fPre.SetEnabled(kFALSE);
    fFit.SetEnabled(kFALSE);
    fIsStop = true;
    fTimer.TurnOff();
    if (!fPause.IsEnabled()) End();
    break;
  case kClear:
    if (!fView) return;
    fView->Reset();
    fView->HandleSelect();
    break;
  case kDelete: 
    if (!fView) return;
    fView->Clear();
    fView->ClearList();
    break;
  case kPreMake: 
    {
      TString url;
      new ConnectionDialog(gClient->GetRoot(), url);
      if (!url.IsNull()) fView->PreMakeHistograms(url.Data());
    }
    break;
  case kFit:
    HandleFit();
    break;
  }
}

//____________________________________________________________________
void
RcuGui::MonitorFrame::HandleFit()
{
  if (!fView) return;
  
  TObject* user = fView->Current();
  if (!user || !user->InheritsFrom(TH1::Class())) return;
  TH1* h = static_cast<TH1*>(user);
  
  Double_t mean = h->GetMean();
  Double_t rms  = h->GetRMS();
  
  h->Fit("gaus", "Q+", "", mean - 5 * rms, mean + 5 * rms);
  TF1* gaus = h->GetFunction("gaus");
  if (!gaus) return;
  gaus->SetLineStyle(2);
  
  Double_t pedestal = gaus->GetParameter(1);
  Double_t noise    = gaus->GetParameter(2);

  h->Fit("landau", "Q+", "", pedestal + 5 * noise, 
		h->GetXaxis()->GetXmax());
  TF1* landau = h->GetFunction("landau");
  if (!landau) return;
  
  Double_t signal = landau->GetParameter(1);
  Double_t sigma  = landau->GetParameter(2);
  Double_t chi2   = landau->GetChisquare() / landau->GetNDF();
  
  std::cout << "\n"
	    << "Pedestal:\t" << pedestal << "\n" 
	    << "Noise:\t\t"  << noise    << "\n"
	    << "Signal:\t\t" << signal   << "\n" 
	    << "Sigma:\t\t"  << sigma    << "\n" 
	    << "Chi^2/NDF\t" << chi2     << "\n"
	    << "S/N:\t\t"    << (signal - pedestal) / noise 
	    << std::endl;
  fView->HandleDraw();
}

//____________________________________________________________________
bool
RcuGui::MonitorFrame::Start(const char* input, long n, size_t skip, 
			    bool all, bool wait)
{
  if (!fView) {
    std::cerr << "No view defined - exiting" << std::endl;
    exit(1);
  }
  
  // Close old reader if any
  if (fReader) {
    delete fReader;
    fReader = 0;
  }

  // Make new reader 
  fReader = RcuData::Reader::Create(*this, input, n, skip, false, all, wait);
  if (!fReader) {
    std::cerr << "SpectrumMaker: No reader made for input " 
	      << input << std::endl;
    return false;
  }

  // Reset Progress meter.
  fMeter.Reset(-1, "Event #", 10);

  // Set some names
  fSource  = input;
  fTimer.SetReader(this);
  fIsStop  = false;
  fCounter = 0;
  fLast    = 0;
  Updated();
  SourceChanged();
  return true;
}

//____________________________________________________________________
void
RcuGui::MonitorFrame::End()
{
  // if (!fOutput) return;
  fIsStop = true;
  // RcuData::SpectrumMaker::End();
}

//____________________________________________________________________
bool
RcuGui::MonitorFrame::GotChannel(RcuData::Channel& c, bool)
{
  fCurrent           = 0;
  unsigned int rcu   = c.DDL();
  unsigned int board = c.Board();
  unsigned int chip  = c.Chip();
  unsigned int chan  = c.ChanNo();
  if (!fView->MakeChannel(rcu, board, chip, chan)) return false;
  fCurrent           = &c;
  return true;
}

//____________________________________________________________________
bool
RcuGui::MonitorFrame::GotData(RcuData::uint32_t t, RcuData::uint32_t adc)
{
  if (!fCurrent) return false;
  unsigned int rcu   = fCurrent->DDL();
  unsigned int board = fCurrent->Board();
  unsigned int chip  = fCurrent->Chip();
  unsigned int chan  = fCurrent->ChanNo();
  if (!fView->MakeTimebin(rcu, board, chip, chan, t)) return false;
  fView->Fill(rcu, board, chip, chan, t, adc);
  return true;
}

//____________________________________________________________________
bool
RcuGui::MonitorFrame::IsEOD() const
{
  return (fIsStop || !fReader || fReader->IsEOD());
}

//____________________________________________________________________
bool
RcuGui::MonitorFrame::GetNextEvent()
{
  if (!fReader) return false;
  if (fReader->IsEOD() || fIsStop) {
    fTimer.TurnOff();
    End();
    return false;
  }
  // Update list of entries if needed.
  fView->UpdateList();
  int  ret      = fReader->GetNextEvent();
  fLast         = fReader->GetEventNumber();
  switch (ret) {
  case RcuData::Reader::kData:           break;
  case RcuData::Reader::kSkip:           
  case RcuData::Reader::kNoData:         return true;
  case RcuData::Reader::kEndOfData:      
  case RcuData::Reader::kMaxReached:     return false;
  case RcuData::Reader::kError:          return (fIsStop = true);
  }
  fMeter.Step();
  
  fCounter++;
  if (fReader->GetNumberOfEvents() != int(fMeter.N()))
    fMeter.SetN(fReader->GetNumberOfEvents());

  // Update list of entries if needed.
  // fView->UpdateList();

  // If we're on the next update edge, do it. 
  if (fCounter % fUpdateFreq == 0) {
    fView->HandleSelect(); // Draw(); // HandleDraw();
    Updated();
  }
  return true;
}

//____________________________________________________________________
//
// EOF
//

// -*- mode: C++ -*-
//____________________________________________________________________
//
// $Id: MonitorTree.h,v 1.4 2009-02-09 23:11:57 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    MonitorTree.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of tree for the Monitor
*/
#ifndef RCUGUI_MONITORTREE_H
#define RCUGUI_MONITORTREE_H
#ifndef RCUGUI_LINKEDTREE_H
# include <rcugui/LinkedTree.h>
#endif
#ifndef __LIST__
# include <list>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif


namespace RcuData
{
  namespace Spectra {
    class Rcu;
    class Chan;
  }
}

namespace RcuGui
{
  struct MonitorTree : public LinkedTree 
  {
    /** Constructor 
	@param f Parent frame 
	@param w width
	@param h height */
    MonitorTree(TGCompositeFrame& f, UInt_t w=800, UInt_t h=600, 
		const std::string& topName="top");
    /** Destructor */
    virtual ~MonitorTree() {}
    /** Member function to pre-make some histograms 
	@param url  FEE card URL to connect to */ 
    virtual void PreMakeHistograms(const char* url="fee://localhost/FMD1");
    /** Update list if needed */ 
    virtual void UpdateList();
    /** Select nothing */
    virtual void ClearSelection() { fList.UnselectAll(kFALSE); }

    /** Fill value into appropriate histogram 
	@param board   Board number
	@param chip    Chip number
	@param channel Channel number
	@param t       Time bin 
	@param adc     ADC value */
    virtual void Fill(unsigned int rcu, 
		      unsigned int board, 
		      unsigned int chip, 
		      unsigned int channel, 
		      unsigned int t, 
		      unsigned int adc) { }
    /** Handle new selection */ 
    virtual void HandleSelect() {}
    /** Draw whatever we need to draw */ 
    virtual void HandleDraw() { HandleSelect(); }
    /** Clear it */ 
    virtual void Reset() {}
    /** Clear it */ 
    virtual void Clear() {}
    
    /** Make histograms, etc for a board 
	@param board Board number 
	@return @c true in case of success */
    virtual bool MakeBoard(unsigned int rcu, 
			   unsigned int board) { return true; }
    /** Premake histograms, etc. for a channel.
	@param board   Board number
	@param channel Channel number
	@param minT    Minimum timebin
	@param maxT    Maximum timebin
	@return  @c true in case of success. */
    virtual bool MakeChannel(unsigned int rcu, 
			     unsigned int board, 
			     unsigned int chip,
			     unsigned int channel) { return true; }
    /** Make histograms, etc. for a timebin
	@param board   Board number 
	@param chip    Chip number
	@param channel Channel number 
	@param timebin Timebin number 
	@return @c true on success. */
    virtual bool MakeTimebin(unsigned int rcu, 
			     unsigned int board, 
			     unsigned int chip, 
			     unsigned int channel, 
			     unsigned int timebin) { return true; }
  protected:

    /** Whether we need to update the list */
    Bool_t fNeedUpdate;
    /** Type of sort list */
    typedef std::list<TGListTreeItem*> SortList;
    /** List of branchs to sort */ 
    SortList fNeedSort;
    /** The top entry */
    TGListTreeItem* fTopEntry;
    /** Top entry name */
    std::string fTopName;

    ClassDef(MonitorTree,0);
  };    
}
#endif
//
// EOF
//

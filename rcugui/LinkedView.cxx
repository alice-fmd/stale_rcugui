//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/LinkedView.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <TSystem.h>
#include <RQ_OBJECT.h>

//====================================================================
RcuGui::LinkedView::LinkedView(TGCompositeFrame& parent, UInt_t w, UInt_t h, 
			       UInt_t size, Int_t maxZeros ) 
  : TGTextEdit(&parent, Int_t(.75 * (w - 6)), h - 6), 
    fNumberView(&parent, Int_t(.05 * (w - 6)), h - 6, -1, 
		TGView::kNoVSB, TGFrame::GetDefaultFrameBackground()),
    fSize(size), 
    fMaxZeros(maxZeros)
{
  for (size_t i = 0; i < fSize; i++) {
    TGLongPosition p(0, i);
    fNumberBuffer.InsText(p, Form("%4d", i));
  }
  fNumberView.ChangeOptions(kDoubleBorder);
  parent.AddFrame(&fNumberView, new TGLayoutHints(kLHintsLeft, 0, 0, 0, 0));
  parent.AddFrame(this, new TGLayoutHints(kLHintsLeft|kLHintsExpandX, 
					   0, 0, 0, 0));
  fNumberView.SetText(&fNumberBuffer);
  fText->Clear();
}

//____________________________________________________________________
void 
RcuGui::LinkedView::WriteText(const UInt_t* data) 
{
  fText->Clear();
  Int_t nZeros = 0;
  for (size_t i = 0; i < fSize; i++) {
    if (fMaxZeros > 0 && nZeros >= fMaxZeros) break;
    if (data[i] == 0) nZeros++;
    TGLongPosition p(0, i);
    fText->InsText(p, Form("0x%08x", data[i]));
  }
  Update();
}

//____________________________________________________________________
void 
RcuGui::LinkedView::WriteText(size_t offset, size_t n, const UInt_t* data) 
{
  if (offset >= fSize) return;
  if (offset+n >= fSize) return;
  unsigned int cache[fSize];
  ReadText(cache);
  for (size_t i = offset; i < offset+n; i++) cache[i] = data[i-offset];
  WriteText(cache);
}

//____________________________________________________________________
void 
RcuGui::LinkedView::ReadText(UInt_t* data) 
{
  volatile size_t i;
  for (i = 0; i < size_t(fText->RowCount()) && i < fSize; i++) {
    TGLongPosition p(0, i);
    // Get as much as possible 
    Int_t len = fText->GetLineLength(i);
    Char_t* line = fText->GetLine(p, len);
    if (!line) { 
      std::cerr << "Failed to read line " << i << std::endl;
      continue;
    }
    // std::string str(line, len);
    std::istringstream s(line);
    // s.str(str);
    s >> std::hex >> data[i];
    delete [] line;
  }
}

//____________________________________________________________________
void 
RcuGui::LinkedView::ReadText(size_t offset, size_t n, UInt_t* data) 
{
  if (offset >= fSize) return;
  if (offset+n >= fSize) return;
  volatile size_t i;
  for (i = offset; i < offset+n; i++) {
    if (i >= size_t(fText->RowCount())) data[i-offset] = 0;
    TGLongPosition p(0, i);
    Int_t   len  = fText->GetLineLength(i);
    Char_t* line = fText->GetLine(p, len);
    if (!line) { 
      std::cerr << "Failed to read line " << i << std::endl;
      continue;
    }
    std::istringstream s(line);
    s >> std::hex >> data[i-offset];
    delete [] line;
  }
}

//____________________________________________________________________
void 
RcuGui::LinkedView::ScrollCanvas(Int_t new_top, Int_t direction)
{
  fNumberView.ScrollCanvas(new_top, direction);
  TGTextEdit::ScrollCanvas(new_top, direction);
}

//____________________________________________________________________
void 
RcuGui::LinkedView::Resize(UInt_t w, UInt_t h)
{
  fNumberView.Resize(Int_t(2. / 30 * w), h);
  TGTextEdit::Resize(Int_t(28. / 30 * w), h);
}

//____________________________________________________________________
//
// EOF
// 

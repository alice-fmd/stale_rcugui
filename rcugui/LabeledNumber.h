// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_LABELEDNUMBER_H
#define RCUGUI_LABELEDNUMBER_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif
#ifndef ROOT_TMath
# include <TMath.h>
#endif
#ifndef TGNumberEntry
# include <TGNumberEntry.h>
#endif
#ifndef TString
# include <TString.h>
#endif

class TGLayoutHints;

namespace RcuGui 
{
  enum {
    kHex     = BIT(15), 
    kNormal  = BIT(16), 
    kReal    = BIT(17), 
    kInteger = BIT(18)
  };
  
  //====================================================================
  /** @struct LabeledNumber 
      @brief base class for number entries and views that has a label
      attached. 
      @ingroup rcugui_basic
   */
  struct LabeledNumber : public TQObject
  {
    /** Constructor
	@param f       Parent frame
	@param label   Label (text)
	@param option  Frame options
	@param lmargin Left margin
	@param rmargin Right margin
	@param tmargin Top margin
	@param bmargin Bottom margin  */
    LabeledNumber(TGCompositeFrame& f, 
		  const char*       label, 
		  Int_t             option =kHorizontalFrame, 
		  Int_t             lmargin=3, 
		  Int_t             rmargin=0, 
		  Int_t             tmargin=0, 
		  Int_t             bmargin=3);
    /** Destructor */
    virtual ~LabeledNumber() {}
    /** Set the display to be in hex format
	@param ishex Show as hex numbers if @c true */
    virtual void SetHexFormat(Bool_t ishex);
    /** Add the view
	@param view View to use */
    void         AddView(TGFrame& view);
    /** Signal emitted when value is changed */
    void ValueChanged() { Emit("ValueChanged()"); } //*SIGNAL*
    /** Frame hints */
    TGLayoutHints      fLHFrame;
    /** Label hints */
    TGLayoutHints      fLHLabel;
    /** Prefix hints */
    TGLayoutHints      fLHPrefix;
    /** View hints */
    TGLayoutHints      fLHView;
    /** Frame */
    TGCompositeFrame   fFrame;
    /** Prefix */
    TGLabel            fPrefix;
    /** Label */
    TGLabel            fLabel;
    ClassDef(LabeledNumber,0) // A Labeled number 
    
  };

  //====================================================================
  /** @struct LabeledIntView
      @ingroup rcugui_basic
   */
  struct LabeledIntView : public LabeledNumber
  {
    /** Constructor
	@param f        Parent frame
	@param label    Label (text)
	@param min      Minimum allowed value
	@param max      Maximum allowed value 
	@param option   Frame options
	@param lmargin 	Left margin  
	@param rmargin 	Right margin 
	@param tmargin 	Top margin   
	@param bmargin 	Bottom margin */
    LabeledIntView(TGCompositeFrame& f, 
		   const char*       label, 
		   Long_t            min, 
		   Long_t            max, 
		   Int_t             option=kHorizontalFrame|kHex,
		   Int_t             lmargin=3, 
		   Int_t             rmargin=0, 
		   Int_t             tmargin=0, 
		   Int_t             bmargin=3);
    /** @return Get the value  */
    Long_t GetValue() const { return fView.GetIntNumber(); }
    /** @param val Set value to @a val */
    void SetValue(Long_t val) { fView.SetIntNumber(val);  }
    /** Set the display to be in hex format
	@param ishex Show as hex numbers if @c true */
    void SetHexFormat(Bool_t ishex);
    /** @param t Tool tip text */
    void SetToolTipText(const char* t);
    /** @param what Enable (if @c true) or disable (if @c false) */
    void SetEnabled(Bool_t what) { fView.SetEnabled(what); }
    /** Handle a change of value */ 
    void HandleChange();
    /** View */
    TGNumberEntryField      fView;
  };


  //====================================================================
  /** @struct LabeledIntEntry
      @ingroup rcugui_basic
   */
  struct LabeledIntEntry : public LabeledNumber 
  {
    /** Constructor
	@param f        Parent frame
	@param label    Label (text)
	@param min      Minimum allowed value
	@param max      Maximum allowed value 
	@param option   Frame options
	@param lmargin 	Left margin  
	@param rmargin 	Right margin 
	@param tmargin 	Top margin   
	@param bmargin 	Bottom margin */
    LabeledIntEntry(TGCompositeFrame& f, 
		    const char*       label, 
		    Long_t            min, 
		    Long_t            max, 
		    Int_t             option=kHorizontalFrame|kHex,
		    Int_t             lmargin=3, 
		    Int_t             rmargin=0, 
		    Int_t             tmargin=0, 
		    Int_t             bmargin=3);
    /** @return Get the value */
    Long_t GetValue() const { return fView.GetIntNumber(); }
    /** @param val Set value to @a val */
    void SetValue(Long_t val) { fView.SetIntNumber(val);  }
    /** @param what Enable (if @c true) or disable (if @c false) */
    void SetEnabled(Bool_t what) { fView.GetNumberEntry()->SetEnabled(what); }
    /** Set the display to be in hex format
	@param ishex Show as hex numbers if @c true */
    void SetHexFormat(Bool_t ishex);
    /** @param t Tool tip text */
    void SetToolTipText(const char* t);
    /** Handle a change of value */ 
    void HandleChange();
    /** The view */
    TGNumberEntry     fView;
  };

  //====================================================================
  /** @struct LabeledFloatView
      @ingroup rcugui_basic
   */
  struct LabeledFloatView : public LabeledNumber
  {
    /** Constructor
	@param f        Parent frame
	@param label    Label (text)
	@param min      Minimum allowed value
	@param max      Maximum allowed value 
	@param option   Frame options
	@param lmargin 	Left margin  
	@param rmargin 	Right margin 
	@param tmargin 	Top margin   
	@param bmargin 	Bottom margin */
    LabeledFloatView(TGCompositeFrame& f, 
		     const char*       label, 
		     Float_t           min, 
		     Float_t           max, 
		     Int_t             option=kHorizontalFrame,
		     Int_t             lmargin=3, 
		     Int_t             rmargin=0, 
		     Int_t             tmargin=0, 
		     Int_t             bmargin=3);
    /** @return Get the value */
    Float_t GetValue() const { return fView.GetNumber(); }
    /** @param val Set value to @a val */
    void SetValue(Float_t val) { fView.SetNumber(val);  }
    /** @param t Tool tip text */
    void SetToolTipText(const char* t);
    /** @param what Enable (if @c true) or disable (if @c false) */
    void SetEnabled(Bool_t what) { fView.SetEnabled(what); }
    /** Handle a change of value */ 
    void HandleChange();
    /** The view */
    TGNumberEntryField      fView;
  };


  //====================================================================
  /** @struct LabeledFloatEntry
      @ingroup rcugui_basic
   */
  struct LabeledFloatEntry : public LabeledNumber 
  {
    /** Constructor
	@param f        Parent frame
	@param label    Label (text)
	@param min      Minimum allowed value
	@param max      Maximum allowed value 
	@param option   Frame options
	@param lmargin 	Left margin  
	@param rmargin 	Right margin 
	@param tmargin 	Top margin   
	@param bmargin 	Bottom margin */
    LabeledFloatEntry(TGCompositeFrame& f, 
		      const char*       label, 
		      Float_t           min, 
		      Float_t           max, 
		      Int_t             option=kHorizontalFrame,
		      Int_t             lmargin=3, 
		      Int_t             rmargin=0, 
		      Int_t             tmargin=0, 
		      Int_t             bmargin=3);
    /** @return Get the value */
    Float_t GetValue() const { return fView.GetNumber(); }
    /** @param val Set value to @a val */
    void SetValue(Float_t val) { fView.SetNumber(val);  }
    /** @param what Enable (if @c true) or disable (if @c false) */
    void SetEnabled(Bool_t what) { fView.GetNumberEntry()->SetEnabled(what); }
    /** @param t Tool tip text */
    void SetToolTipText(const char* t);
    /** Handle a change of value */ 
    void HandleChange();
    /** The view*/
    TGNumberEntry     fView;
  };
}
#endif
//
// EOF
//

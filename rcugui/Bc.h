// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BC_H
#define RCUGUI_BC_H
#ifndef RCUGUI_SCROLLABLE_H
# include <rcugui/Scrollable.h>
#endif
#ifndef RCUGUI_LABELEDNUMBER_H
# include <rcugui/LabeledNumber.h>
#endif
#ifndef RCUGUI_ERRORBIT_H
# include <rcugui/ErrorBit.h>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif 
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGNumberEntry
# include <TGNumberEntry.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef RCUGUI_REGSITER_H
# include <rcugui/Register.h>
#endif
#ifndef RCUXX_BC_H
# include <rcuxx/Bc.h>
#endif


namespace RcuGui
{
  /** @class Bc 
      @brief GUI interface to Board controller registers 
      @ingroup rcugui_low
  */
  class Bc : public Scrollable 
  {
  public:
    /** Constructor 
	@param tab Parent
	@param low Low-level interface  */
    Bc(TGTab& tab, Rcuxx::Bc& low);
    /** Destructor */
    virtual ~Bc();
    /** Handle broadcast button */
    virtual void HandleBroadcast();
    /** Handle address change */
    virtual void HandleAddress();
    /** Handle tries 
	@param v Not used */
    virtual void HandleTries(Long_t v);
    /** Handle update all */
    virtual void HandleUpdate();
    /** Handle all buttons */ 
    virtual void HandleAll(Int_t v);
    /** Update all. */
    virtual void Update();
    /** Set debug */
    virtual void SetDebug(Bool_t debug);
  protected:
    /** Low level */
    Rcuxx::Bc& fLow;
    /** Re-used layout hints */
    TGLayoutHints  fHints;
    /** Tabs */
    TGTab          fTab;
    /** Address group */
    TGGroupFrame  fAddressGroup;
    /** Layout hint for broadcast */
    TGLayoutHints fBroadcastHints;
    /** Broad cast option */
    TGCheckButton fBroadcast;
    /** Board number */
    LabeledIntEntry fBoardAddress;
    /** Layout hints for all buttons */
    TGLayoutHints fAllButtonsHints;
    /** Commit, or update all group */ 
    TGButtonGroup fAllButtons;
    /** Commit all button */ 
    TGTextButton fCommitAll;
    /** Update all button */ 
    TGTextButton fUpdateAll;
    /** I2C check box */ 
    TGCheckButton fUseI2C;
    
    /** Status */
    TGCompositeFrame* fConfigStatus;
    /** Command row */
    TGGroupFrame fCSRCommands;
    /** Reset altros */
    Command* fALRST;
    /** Reset board controller */
    Command* fBCRST;    
    /** Configuration register 0 and 1 */
    Register* fCSR01;
    /** Configuration register 2 */
    Register* fCSR2;
    /** Configuration register 3 */
    Register* fCSR3;
    /** Firmware version */
    Register* fVersion;


  
    /** Status */
    TGCompositeFrame* fCounters;
    /** Command row */
    TGGroupFrame fCounterCommands;
    /** Latch counters */
    Command* fCNTLAT;
    /** Latch counters */
    Command* fCNTCLR;
    /** Counters */
    Register* fL1CNT;
    /** Counters */
    Register* fL2CNT;
    /** Counters */
    Register* fDSTBCNT;
    /** Counters */
    Register* fSCLKCNT;

    /** Status */
    TGCompositeFrame* fTestMode;
    /** Command row */
    TGGroupFrame fTSMCommands;
    /** Scan length read-out */
    Command* fSCEVL;
    /** Event length read-out */
    Command* fEVLRDO;
    /** Test mode start */
    Command* fSTTSM;
    /** Test mode start */
    Command* fACQRDO;
    /** Test mode word */
    Register* fTSMWORD;
    /** Test mode undersampling ratio */
    Register* fUSRATIO;

    /** Status */
    TGCompositeFrame* fMonitors;
    /** Command row */
    TGGroupFrame fMonitorCommands;
    /** Update all registers */
    TGTextButton fUpdateMons;
    /** Number of tries */
    LabeledIntEntry fTries;
    /** Re-used layout hints */
    TGLayoutHints  fLowerHints;
    /** Containers */
    TGHorizontalFrame fMonLower;
    /** Sub */
    TGVerticalFrame fMonLeft;
    /** Sub */
    TGVerticalFrame fMonRight;
    
    /** Start conversion of monitered things */
    Command* fSTCNV;
    /** Temperature */
    Register* fTemp;
    /** Analogue voltage */
    Register* fAV;
    /** Analogue current */
    Register* fAC;
    /** Digital voltage */
    Register* fDV;
    /** Digital currrent */
    Register* fDC;

  };
}

#endif

//____________________________________________________________________
//
// EOF
//


// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_CONNECTION_H
#define RCUGUI_CONNECTION_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif
#ifndef ROOT_TGLayout
# include <TGLayout.h>
#endif
#ifndef ROOT_TGTableLayout
# include <TGTableLayout.h>
#endif
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif
// class TGTableLayoutHints;
class TGTextButton;
class TGGroupFrame;
// class TGHButtonGroup;
// class TGLayoutHints;
class TString;

namespace RcuGui
{
  //__________________________________________________________________
  /** @struct ConnectionType 
      @brief  Utility type used by the ConnectionDialog. 
      @ingroup rcugui_basic
  */
  struct ConnectionType 
  {
    /** Constructor 
	@param p 
	@param title 
	@param i   */
    ConnectionType(TGCompositeFrame& p, const char* title, int i);
    /** Copy constructor 
	@param other */
    ConnectionType(const ConnectionType& other);
    /** Destructor */
    virtual ~ConnectionType() {}
    /** Is it enabled */
    virtual bool   IsEnabled() const { return fSelect.IsDown(); }
    /** Enable */
    virtual void   Enable() { fSelect.SetDown(); }
    /** Disable */
    virtual void   Disable() { fSelect.SetDown(kFALSE); }
    /** @param url Get the url */
    virtual void   Url(TString& url) { url = ""; }
    /** Connect button to handler 
	@param rClass Reciever class 
	@param r Pointer to receiver 
	@param rHandler Receiver handler member function name */
    virtual void   Connect(const char* rClass, void* r, const char* rHandler);
    /** Handle a signal */
    virtual void   Handle() { Enable(); }
  protected:
    /** Layout hints */
    TGTableLayoutHints fSelectHints;
    /** Select button */
    TGRadioButton      fSelect;
  };    

  //__________________________________________________________________
  /** @struct ConnectionDialog
      @brief  A dialog that askes for connection details
      @ingroup rcugui_basic
  */
  //____________________________________________________________________
  struct ConnectionDialog : public TGTransientFrame
  {
    /** Types */
    enum {
      /** USB to Front-end*/
      kU2F    =  1, 
      /** Fee server client */
      kFee    =  2, 
      /** DDL */
      kRorc   =  4, 
      /** Via stdin/stdout */
      kPipe   =  8,
      /** Decline */
      kCancel = 16, 
      /** Accept */
      kOk     = 32
    };
  
    /** Constructor
	@param p     Parent window
	@param flags What to enable
	@param ret   On return, contains the connection URI
	@return  */
    ConnectionDialog(const TGWindow* p, TString& ret, 
		     int flags=(kU2F|kFee|kRorc|kPipe));
    /** Destructor */
    virtual ~ConnectionDialog();

    /** Handle the OK and cancel buttons. 
	@param i Which button */
    void HandleButtons(Int_t i);
    /** Enable the U2F line */
    void EnableU2F();
    /** Enable the Fee line */
    void EnableFee();
    /** Enable the Rorc line */
    void EnableRorc();
    /** Enable the Rorc line */
    void EnablePipe();
  protected:
    /** Reference to return value */
    TString&          fReturn;
    /** Layout hints for the selection frame */
    TGLayoutHints     fSelectHints;
    /** Selection frame */
    TGGroupFrame      fSelect;
    /** U2F Line */
    ConnectionType*   fU2F;
    /** Fee Line */
    ConnectionType*   fFee;
    /** RORC Line */
    ConnectionType*   fRorc;
    /** RORC Line */
    ConnectionType*   fPipe;
    /** Hints for button group */
    TGLayoutHints     fButtonsHints;
    /** Hints for buttons */
    TGLayoutHints     fButtonHints;
    /** Button group */
    TGHButtonGroup    fButtons;
    /** Cancel button */
    TGTextButton      fCancel;
    /** OK button */
    TGTextButton      fOK;
  };
}

#endif
//
// EOF
//

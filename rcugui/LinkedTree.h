// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    LinkedTree.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup rcugui_util
*/
#ifndef RCUGUI_LINKEDTREE
#define RCUGUI_LINKEDTREE
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGListTree
# include <TGListTree.h>
#endif
#ifndef ROOT_TRootEmbeddedCanvas
# include <TRootEmbeddedCanvas.h>
#endif
#ifndef ROOT_TCanvas
# include <TCanvas.h>
#endif
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif
class TVirtualPad;
class TDirectory;

namespace RcuGui
{
  /** Class to show histograms in a file.  On the left side, a tree
      list of the histograms and graphs in the file is shown.  On the
      right hand side is a canvas.  */
  class LinkedTree : public TGGroupFrame
  {
  public:
    /** Constructor 
	@param parent  Parent frame 
	@param w       Width 
	@param h       Height
	@param options Options  */
    LinkedTree(TGCompositeFrame& parent, Int_t w=800, Int_t h=600, 
	       UInt_t options=kHorizontalFrame);
    /** Destructor */ 
    virtual ~LinkedTree() {}
    /** Handle entries 
	@param e  selected entry, if any 
	@param id Id of entry */
    virtual void HandleEntry(TGListTreeItem* e, Int_t id);
    /** Handle key strokes 
	@param f      Item selected, if any 
	@param keysym Key symbol 
	@param mask   Modifier mask */
    virtual void HandleKey(TGListTreeItem* f, UInt_t keysym, UInt_t mask);
    /** Handle Return 
	@param f Selected item, if any */
    virtual void HandleReturn(TGListTreeItem* f);
    /** Utility function to scan directory for things to put in list
	@param dir Directory to scan
	@return  @c kTRUE on success, @c kFALSE otherwise */
    virtual Bool_t ScanDirectory(TDirectory* dir);
    /** Return the currently selected entry */ 
    TGListTreeItem* CurrentEntry() const { return fCurrentEntry; }
    /** @return the currently selected user data (possibly 0) */
    TObject* Current() const;
    /** Selection changed signal */
    void SelectionChanged() { Emit("SelectionChanged()"); }//*SIGNAL*
    /** Change into the canvas for draws 
	@param subpad Sub pad number or 0 for canvas */ 
    TVirtualPad* cd(Int_t subpad=0);
    /** @return Reference to the canvas */
    TCanvas& Canvas() { return fCanvas; }
    /** Clear the list */
    virtual void ClearList();
    /** Clear the canvas */ 
    virtual void ClearCanvas();
    /** Update canvas */ 
    virtual void UpdateCanvas();
    /** Update canvas */ 
    virtual void UpdateList();
  protected:
    /** Utility function to scan directory for things to put in list
	@param dir Directory to scan
	@return  @c kTRUE on success, @c kFALSE otherwise */
    virtual Bool_t DoScanDirectory(TDirectory* dir);

    /** Layout for container */
    TGLayoutHints       fContainerHints;
    /** Container */
    TGCanvas            fContainer;
    /** List */
    TGListTree          fList;
    /** Layout for canvas */
    TGLayoutHints       fCanvasHints;
    /** Embedded canvas */
    TRootEmbeddedCanvas fEmCanvas;
    /** Canvas */
    TCanvas             fCanvas;
    /** 1D Histogram Icon */
    const TGPicture*    fHist1DIcon;
    /** 2D Histogram Icon */
    const TGPicture*    fHist2DIcon;
    /** 3D Histogram Icon */
    const TGPicture*    fHist3DIcon;
    /** Graph Icon */
    const TGPicture*    fGraphIcon;
    /** Current list entry */
    TGListTreeItem*     fCurrentEntry;

    ClassDef(LinkedTree,0);
  };
}
#endif
//
// EOF
//


  
    
    

// -*- mode: C++ -*-
//____________________________________________________________________
//
// $Id: Monitor.h,v 1.14 2009-02-09 23:11:57 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    Monitor.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of DateMonitor
*/
#ifndef RCUGUI_DATEMONITOR_H
#define RCUGUI_DATEMONITOR_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGMenu
# include <TGMenu.h>
#endif
#ifndef ROOT_TGStatusBar
# include <TGStatusBar.h>
#endif
#ifndef __STRING__
# include <string>
#endif
class TH1;
class TGStatusBar;
class TGCanvas;
class TGPicture;

namespace RcuData
{
  class RawVisitor;
}

namespace RcuGui
{
  // Forward declaration. 
  struct MonitorFrame;
  
  /** @defgroup rcugui_read GUI classes for reading raw data 
      @ingroup rcugui
  */
  //____________________________________________________________________
  /** @struct Monitor 
      @brief Monitor online events. This contains the main widget as
      well as the timer. 
      @ingroup rcugui_read
  */
  struct Monitor 
  {
    /** Constructor */
    Monitor();
    /** Destructor */
    virtual ~Monitor() {}
    /** Set the frame */ 
    void SetFrame(MonitorFrame& frame);
    /** Handle menus */
    void HandleMenu(Int_t id);
    /** Handle close */
    void HandleClose();
    /** Handle update */ 
    void HandleUpdate();
    /** Handle source */ 
    void HandleSource(); 
    /** Get the frame */ 
    TGCompositeFrame* Frame() { return &fMain; }
    /** Display GUI */
    void Display();
    /** Run the monitor. 
	@param input  Input source file 
	@param n      Number of events to read
	@param skip   Number of events to skip
	@param all    Whether to get all events
	@param wait   Whether to wait for data 
	@return @c true on success, false otherwise */
    bool Start(const char* input, long n=-1, size_t skip=0,
	       bool all=false, bool wait=false);
  protected:
    /** Main frame */
    TGMainFrame fMain;
    /** Layout hints for the status bar */ 
    TGLayoutHints fMenuHints;
    /** Menu bar */
    TGMenuBar fMenu;
    /** File menu  */
    TGPopupMenu* fFileMenu;
    /** Layout hints for the status bar */ 
    TGLayoutHints fFrameHints;
    /** Monitor frame */ 
    MonitorFrame* fFrame;
    /** Layout hints for the status bar */ 
    TGLayoutHints fStatusHints;
    /** Status bar */
    TGStatusBar fStatus;
  };
}

#endif
//
// EOF
//

  

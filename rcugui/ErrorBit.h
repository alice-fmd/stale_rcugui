// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ERRORBIT_H
#define RCUGUI_ERRORBIT_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif
#ifndef ROOT_TMath
# include <TMath.h>
#endif
#ifndef TGNumberEntry
# include <TGNumberEntry.h>
#endif
#ifndef TString
# include <TString.h>
#endif

class TGLayoutHints;

namespace RcuGui 
{

  //====================================================================
  /** @struct ErrorBit 
      @brief Bit to be toggled on errors.   Cannot be set.
      @ingroup rcugui_basic
   */
  struct ErrorBit 
  {
    /** Constructor 
	@param p 
	@param name 
	@param desc 
	@param okColour 
	@param badColour  */
    ErrorBit(TGCompositeFrame& p, const Char_t* name, const Char_t* desc, 
	     Int_t okColour=0x00aa00, Int_t badColour=0xdd0000);
    /** Destructor */
    virtual ~ErrorBit() {}
    
    /** Handle button */
    virtual void HandleButton();
    /** @param ok New state  */
    void SetState(Bool_t ok);
    /** Set the state 
	@param ok New state 
	@param errstr Error string  */
    void SetState(Bool_t ok, TString& errstr);
    /** @param what Disable or enable error bit */
    virtual void SetEnabled(Bool_t what) {   fState.SetEnabled(what); }

    /** Top Layout hints */
    TGLayoutHints    fTopLH;
    /** Name Layout hints */
    TGLayoutHints    fNameLH;
    /** State Layout hints */
    TGLayoutHints    fStateLH;
    /** Frame */
    TGVerticalFrame  fTop;
    /** Name */
    TGLabel          fName;
    /** State */
    TGTextButton     fState;
    /** Description */
    TString          fDescription;
    /** State */
    Bool_t           fGood;
    /** Colour when OK */
    Int_t            fOkColour;
    /** Colour when bad */
    Int_t            fBadColour;
  };
  
}
#endif
//
// EOF
//

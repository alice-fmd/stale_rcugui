//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    LinkedTree.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup rcugui
*/
#include "rcugui/LinkedTree.h"
#include <TROOT.h>
#include <TKey.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TGraph.h>
#include <TF1.h>
#include <TGClient.h>
#include <KeySymbols.h>
#include <iostream>
#include <rcudata/ProgressMeter.h>
#include <TStyle.h>
#include <TClass.h>
#include <TDirectory.h>

//____________________________________________________________________
RcuGui::LinkedTree::LinkedTree(TGCompositeFrame& parent, 
			       Int_t w, Int_t h, UInt_t options) 
  : TGGroupFrame(&parent, "Display",options),
    fContainerHints(kLHintsExpandY, 
		    0, UInt_t(options & kHorizontalFrame ? 3 : 0), 
		    0, UInt_t(options & kHorizontalFrame ? 0 : 3)),
    fContainer(this, 
	       UInt_t(options & kHorizontalFrame ? w - h - 3 : w), 
	       UInt_t(options & kHorizontalFrame ? h         : h - w - 3)),
    fList(&fContainer, kHorizontalFrame), 
    fCanvasHints(kLHintsExpandX|kLHintsExpandY),
    fEmCanvas(0, this,
	      UInt_t(options & kHorizontalFrame ? h : w), 
	      UInt_t(options & kHorizontalFrame ? h : w)),
    fCanvas("canvas", 10, 10, fEmCanvas.GetCanvasWindowId()),
    fHist1DIcon(gClient->GetPicture("h1_t.xpm")),
    fHist2DIcon(gClient->GetPicture("h2_t.xpm")),
    fHist3DIcon(gClient->GetPicture("h3_t.xpm")),
    fGraphIcon(gClient->GetPicture("graph.xpm")),
    fCurrentEntry(0)
{
  // Add the frames 
  AddFrame(&fContainer, &fContainerHints);
  fContainer.AddFrame(&fList);
  AddFrame(&fEmCanvas, &fCanvasHints);
  fEmCanvas.AdoptCanvas(&fCanvas);
  
  // Set some canvas attributes 
  fCanvas.SetBorderMode(0);
  fCanvas.SetBorderSize(0);
  fCanvas.SetFillColor(0);
  fCanvas.SetBottomMargin(0.10);
  fCanvas.SetLeftMargin(0.10);
  fCanvas.SetTopMargin(0.05);
  fCanvas.SetRightMargin(0.05);
  gStyle->SetOptFit(111);
  gStyle->SetOptStat(1110);
  gStyle->SetStatColor(0);
  gStyle->SetStatBorderSize(1);
  gStyle->SetStatY(.95);
  gStyle->SetStatX(.95);
  gStyle->SetStatW(.2);
  // gStyle->SetStatH(.2);
  gStyle->SetTitleFillColor(0);
  gStyle->SetTitleBorderSize(0);
  gStyle->SetTitleY(.995);
  gStyle->SetTitleX(.10);
  gStyle->SetTitleH(.04);
  
  // Connect signals to slots
  fList.Connect("Clicked(TGListTreeItem*,Int_t)", "RcuGui::LinkedTree",
		this, "HandleEntry(TGListTreeItem*,Int_t)");
#if 0
  fList.Connect("KeyPressed(TGFame*,ULong_t,ULong_t)", 
		"RcuGui::LinkedTree", this, 
		"HandleKey(TGListTreeItem*,UInt_t,UInt_t)");
#endif
  fList.Connect("KeyPressed(TGListTreeItem*,UInt_t,UInt_t)", 
		"RcuGui::LinkedTree", this, 
		"HandleKey(TGListTreeItem*,UInt_t,UInt_t)");
  fList.Connect("ReturnPressed(TGListTreeItem*)", 
		"RcuGui::LinkedTree", this, 
		"HandleReturn(TGListTreeItem*)");
}

namespace 
{
  RcuData::ProgressMeter fgMeter;
}

//____________________________________________________________________
Bool_t
RcuGui::LinkedTree::ScanDirectory(TDirectory* dir) 
{
  fgMeter.Reset(-1, "Reading directories", 1);
  Bool_t ret = DoScanDirectory(dir);
  std::cout << std::endl;
  return ret;
}
  
//____________________________________________________________________
Bool_t
RcuGui::LinkedTree::DoScanDirectory(TDirectory* dir) 
{
  static Bool_t first = kTRUE;
  if (first) {
    first = kFALSE;
  }
  TGListTreeItem* savEntry = fCurrentEntry;
  fCurrentEntry = fList.AddItem(fCurrentEntry, dir->GetName());
  TList* list = dir->GetListOfKeys();
  if (!list || list->GetEntries() < 1) return kTRUE;
  TIter       next(list);
  TKey*       k   = 0;
  Bool_t      ret = kTRUE;
  std::string name(dir->GetName());
  while ((k = static_cast<TKey*>(next()))) {
    TObject* o = 0;
    fgMeter.Step();
    if (name == k->GetName()) {
      // Set the user data for the directory 
      o = k->ReadObj();
      fCurrentEntry->SetUserData(o);
      if (o) fCurrentEntry->SetTipText(o->GetTitle());
      continue;
    }
    TClass* cl  = gROOT->GetClass(k->GetClassName());
    if (!cl) continue;
    const TGPicture* pic = 0;
    if   (cl->InheritsFrom(TDirectory::Class())) {
      ret = DoScanDirectory(static_cast<TDirectory*>(k->ReadObj()));
      continue;
    }
    else if (cl->InheritsFrom(TH3::Class()))    pic = fHist3DIcon;
    else if (cl->InheritsFrom(TH2::Class()))    pic = fHist2DIcon;
    else if (cl->InheritsFrom(TH1::Class()))    pic = fHist1DIcon;
    else if (cl->InheritsFrom(TGraph::Class())) pic = fGraphIcon;
    
    if (!pic) continue;
    o = k->ReadObj();
    TGListTreeItem* item = fList.AddItem(fCurrentEntry, k->GetName(), pic,pic);
    item->SetUserData(o);
    if (o) fCurrentEntry->SetTipText(o->GetTitle());

  }
  fCurrentEntry = savEntry;
  return ret;
}

//____________________________________________________________________
void
RcuGui::LinkedTree::ClearCanvas()
{
  fCanvas.Clear();
}

//____________________________________________________________________
void
RcuGui::LinkedTree::ClearList()
{
  fList.DeleteItem(fList.GetFirstItem());
  UpdateList();
}

  
//____________________________________________________________________
void
RcuGui::LinkedTree::HandleReturn(TGListTreeItem * f)
{

  if (!f) { 
    fList.UnselectAll(kFALSE);
    fList.SetSelected(0);
    return;
  }
  fList.ToggleItem(f);
  UpdateList();
}

  
//____________________________________________________________________
void
RcuGui::LinkedTree::HandleKey(TGListTreeItem * f, UInt_t keysym, UInt_t mask)
{
  if (!f) { 
    fList.UnselectAll(kFALSE);
    fList.SetSelected(0);
    return;
  }
  TGListTreeItem* next = 0;
  switch (keysym) {
  case kKey_Up:
    next = f->GetPrevSibling();
    if (!next) { 
      next = f->GetParent();
      if (next) fList.CloseItem(next);
    }
    break;
  case kKey_Down:
    next = f->GetNextSibling();
    if (!next && f->GetParent()) {
      next = f->GetParent()->GetNextSibling();
      fList.CloseItem(f->GetParent());
    }
    break;
  case kKey_Left:
    next = f->GetParent();
    if (next) fList.CloseItem(next);
    break;
  case kKey_Right:
    next = f->GetFirstChild();
    if (next) fList.OpenItem(f);
    break;
  case kKey_PageUp:
    fList.PageUp(kTRUE);
    next = fList.GetSelected();
    break;
  case kKey_PageDown:
    fList.PageDown(kTRUE);
    next = fList.GetSelected();
    break;
  }
  if (next) gClient->NeedRedraw(&fList);
  if (next && next != f) {
    fList.ClearHighlighted();
    fList.SetSelected(next);
    HandleEntry(next,0);
  }
}

//____________________________________________________________________
void
RcuGui::LinkedTree::HandleEntry(TGListTreeItem* entry, Int_t id) 
{
  TGListTreeItem* old = fCurrentEntry;
  if (entry) {
    if (!entry->GetUserData()) return;
    fCurrentEntry = entry;
  }
  else {
    fCurrentEntry = 0;
    ClearCanvas();
  }
  if (old != fCurrentEntry) fCanvas.cd();
  SelectionChanged();
}

//____________________________________________________________________
void
RcuGui::LinkedTree::UpdateList() 
{
  gClient->NeedRedraw(&fList);
}

//____________________________________________________________________
void
RcuGui::LinkedTree::UpdateCanvas() 
{
  fCanvas.Modified();
  fCanvas.Update();
  fCanvas.cd();
}

//____________________________________________________________________
TVirtualPad*
RcuGui::LinkedTree::cd(Int_t subpad) 
{
  return fCanvas.cd(subpad);
}

//____________________________________________________________________
TObject*
RcuGui::LinkedTree::Current() const
{
  if (!fCurrentEntry) return 0;
  if (!fCurrentEntry->GetUserData()) return 0;
  return static_cast<TObject*>(fCurrentEntry->GetUserData());
}

  
//____________________________________________________________________
//
// EOF
//

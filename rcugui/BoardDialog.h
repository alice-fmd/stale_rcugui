// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BOARDDIALOG_H
#define RCUGUI_BOARDDIALOG_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGMenu
# include <TGMenu.h>
#endif
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif
#ifndef ROOT_TGNumberEntry
# include <TGNumberEntry.h>
#endif
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif
#ifndef ROOT_TGStatusBar
# include <TGStatusBar.h>
#endif

//__________________________________________________________________
namespace RcuGui 
{
  //____________________________________________________________________
  /** @class BoardDialog
      @ingroup rcugui_low
   */
  struct BoardDialog : public TGTransientFrame
  {
    /** Data */
    const unsigned int* fMask;
    /** Data */
    unsigned int*       fData;
    /** Option */
    Bool_t*             fAny;
    /** Option */
    Bool_t*             fCanceled;
    /** Hints */
    TGLayoutHints       fLH1;
    /** Hints */
    TGLayoutHints       fLH2;
    /** Hints */
    TGLayoutHints       fLH3;
    /** Hints */
    TGLayoutHints       fLH4;
    /** Title */
    TGLabel             fTitle;
    /** Table */
    TGCompositeFrame    fTable;
    /** Label */
    TGLabel             fChannel;
    /** Label */
    TGLabel             fChip;
    /** Labels */
    TGLabel*            fChannelAxis[17];
    /** Labels */
    TGLabel*            fChipAxis[9];
    /** Row */
    TGCheckButton*      fAllChip[8];
    /** Column */
    TGCheckButton*      fAllChannel[16];
    /** Individual */
    TGCheckButton*      fEnable[8 * 16];
    /** Buttons */
    TGButtonGroup       fButtons;
    /** Accept */
    TGTextButton        fOk;
    /** Decline */
    TGTextButton        fCancel;
    /** short cut */
    TGTextButton        fAll;
    /** short cut */
    TGTextButton        fNone;
    /** Constructor 
	@param p       Parent 
	@param branch  Branch name 
	@param board   Board number 
	@param mask    Mask of valid channels 
	@param data    On entry, the channels, on exit new value
	@param any     On exit, true if any channels are on
	@param cancel  On exit, true if we get a cancel */
    BoardDialog(const TGWindow *p, Char_t branch, Int_t board,
		const unsigned int* mask, unsigned int* data, 
		Bool_t* any, Bool_t* cancel);
    /** Destructor */
    ~BoardDialog();
    /** Handle buttons */
    void HandleBut(Int_t id);
    /** Handle chip */
    void HandleChip();
    /** Handle channel */
    void HandleChannel();
    ClassDef(BoardDialog,0);
  };
}
  
#endif
//
// EOF
//

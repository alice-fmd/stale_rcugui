//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    DisplayHists.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup rcugui_util
*/
#ifndef RCUGUI_DISPLAYHISTS
#define RCUGUI_DISPLAYHISTS
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGMenu
# include <TGMenu.h>
#endif
#ifndef RCUGUI_LINKEDTREE
# include <rcugui/LinkedTree.h>
#endif
#ifndef ROOT_TGStatusBar
# include <TGStatusBar.h>
#endif
class TFile;


namespace RcuGui
{
  /** Class to show histograms in a file.  On the left side, a tree
      list of the histograms and graphs in the file is shown.  On the
      right hand side is a canvas.  */
  class DisplayHists : public TGMainFrame
  {
  public:
    /** Ids */
    enum {
      kMenuOpen = 1, 
      kMenuClose, 
      kMenuSaveAs,
      kMenuQuit 
    };
    /** Constructor 
	@param file Name of file to open */
    DisplayHists(const char* file=0);
    /** Open a file 
	@param file Name of file to open 
	@return  @c kTRUE on success, @c kFALSE otherwise */
    Bool_t Open(const char* file);
    /** Handle menu items 
	@param id Menu id */
    void HandleMenu(Int_t id);
    /** Handle close */
    void HandleClose();
    /** Handle draw */
    void HandleDraw();
  protected:
    /** Layout for menu */
    TGLayoutHints       fMenuHints;
    /** Menu bar */
    TGMenuBar           fMenu;
    /** File menu */
    TGPopupMenu*        fFileMenu;
    /** Layout for view */
    TGLayoutHints       fViewHints;
    /** Main container */
    LinkedTree          fView;
    /** Layout for status bar */
    TGLayoutHints       fStatusHints;
    /** Status bar */
    TGStatusBar         fStatus;
    /** File to read from */
    TFile* fFile;
  };
}
#endif
//
// EOF
//


  
    
    

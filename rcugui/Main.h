// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_MAIN_H
#define RCUGUI_MAIN_H
# include <TGFrame.h>
# include <TGMenu.h>
# include <TGTab.h>
# include <TGFrame.h>
# include <TGButton.h>
# include <TGNumberEntry.h>
# include <TGLabel.h>
# include <TGStatusBar.h>
# if 0
#  ifndef COMPILER_H
#   include <Compiler.h>
#  endif
#  ifndef ACQUISITION_H
#   include <Acquisition.h>
#  endif
# endif
# include <map>
# ifndef SCALE
#  define SCALE(X) (RcuGui::Main::Instance()->Scale()*(X))
# endif
# ifndef ISCALE
#  define ISCALE(X) int(RcuGui::Main::Instance()->Scale()*(X))
# endif

class TTimer;

namespace Rcuxx
{
  class Rcu;
  class Altro;
  class Bc;
  class Acq;
}

//__________________________________________________________________
/** @defgroup rcugui RCU Graphical User Interface 
 */
//__________________________________________________________________
/** @defgroup rcugui_low Interface to low level RCU
    @ingroup rcugui
 */
//__________________________________________________________________
/** @namespace RcuGui RCU ROOT based Graphical User Interface 
    @ingroup rcugui
 */
namespace RcuGui 
{
  class Altro;
  class Bc;
  class Rcu;
  class Acquisition;
  class Compiler;


  //__________________________________________________________________
  /** @class Main 
      @brief Main interface class 
      @ingroup rcugui_low
  */
  class Main 
  {
  public:
    /** Operations */
    enum {
      /** Close it */
      kClose, 
      /** Quit */
      kQuit, 
      /** Update all */
      kUpdate, 
      /** Choose program */
      kChoose
    };
    /** Type of program list */
    typedef std::map<std::string, unsigned> ProgramList;
    /** Singleton */ 
    static Main* Instance();
    Float_t Scale() const { return fFontScale; }
    /** Destructor */
    virtual ~Main() { Destroy(); }
    /** Singleton access interface */
    // static Main* Instance();
    /** Display the window */
    virtual void Display();
    /** Destoy the window */
    virtual void Destroy();
    /** Close the window */
    virtual void Close();
    /** Handle file menu 
	@param i Entry to handle */
    virtual void HandleFile(Int_t i);
    /** Handle file menu 
	@param i Entry to handle */
    virtual void HandleOption(Int_t i);
    /** Set status bar information 
	@param text Text to put in the status bar 
	@param noupdate Do not check error status if true */
    virtual void SetStatus(const char* text, Bool_t noupdate=kFALSE);
    /** Set status bar information 
	@param abort If @c true, try to do an abort 
	@param altro If @c true, try to do an FECRST
	@param text Text to put in the status bar */
    virtual void SetError(const char* text, Bool_t abort=kFALSE, 
			  Bool_t altro=kFALSE);
    /** Update the error and status register display */
    virtual void Update();
    /** Set data to commit on acquisition from GUI
	@param mask Mask of what to set  */
    virtual void Set4Acq(Int_t mask);
    /** Pop a dialog on errors 
	@param err Error code */
    virtual UInt_t Error(UInt_t err);
    /** Normal startup */
    void SetInteractive(Bool_t interactive=kTRUE) 
    {
      fInteractive = interactive;
    }
    /** Make debug message 
	@param lvl 
	@param where 
	@param format 
    */
    void DebugMsg(Int_t lvl, const Char_t* where, const char* format, ...);
    /** Get debug flag */ 
    bool IsDebug() const;
    /** Add an acquisition tab 
	@param acq Pointer to low-level Acq object */
    virtual void AddAcq(Rcuxx::Acq& acq);
    /** Add an RCU tab 
	@param rcu Pointer to low-level Rcu object 
	@param maxFEC Bit mask of allowed active FECs.
	@param maxALTRO Maximum number of ALTRO's per FEC. */
    virtual void AddRcu(Rcuxx::Rcu& rcu, Int_t maxFEC=0xFFFFFFFF, 
			const UInt_t* mask=0);
    /** Add an BC tab 
	@param bc Pointer to low-level Bc object */
    virtual void AddBc(Rcuxx::Bc& bc);
    /** Add an ALTRO tab 
	@param altro Pointer to low-level Altro object */
    virtual void AddAltro(Rcuxx::Altro& altro);
    /** Add an Compiler tab */
    virtual void AddCompiler();
  protected:
    /** Constructor */
    Main();
    /** Singleton */
    static Main*   fgInstance;
    /** Are we interactive */
    Bool_t fInteractive;
    /** Main window */
    TGMainFrame   fMain;
    /** Layout hints */
    TGLayoutHints fMenuHints;
    /** Menu bar */
    TGMenuBar     fMenuBar;
    /** Layoug hints for menu entries */
    TGLayoutHints fMenuItemLayout;
    /** File menu */
    TGPopupMenu   fFileMenu;
    /** File menu */
    TGPopupMenu   fActionMenu;
    /** File menu */
    TGPopupMenu   fOptionMenu;
    /**  Tab hints */
    TGLayoutHints fTabHints;
    /** Tabs */
    TGTab         fTab;
    /**  Status bar hints */
    TGLayoutHints fStatusHints;
    /** Status bar */
    TGStatusBar   fStatusBar;
    /** DAQ tab */
    Acquisition*  fAcquisition;
    /** RCU tab */
    Rcu* 	  fRcu;
    /** Altro interface */
    Bc* 	  fBc;
    /** Altro interface */
    Altro* 	  fAltro;
    /** Compiler interface tab */
    Compiler*     fCompiler;
    /** Font scale factor */
    Float_t       fFontScale;
  };
}

#endif
//
// EOF
//

  
  

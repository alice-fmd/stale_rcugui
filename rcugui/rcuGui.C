//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef __CINT__
# ifndef RCUGUI_MAIN_H
#  include <rcugui/Main.h>
# endif
# ifndef RCUGUI_CONNECTIONDIALOG_H
#  include <rcugui/ConnectionDialog.h>
# endif
# ifndef RCUXX_RCU_H
#  include <rcuxx/Rcu.h>
# endif
# ifndef RCUXX_BC_H
#  include <rcuxx/Bc.h>
# endif
# ifndef RCUXX_ALTRO_H
#  include <rcuxx/Altro.h>
# endif
# ifndef RCUXX_ACQ_H
#  include <rcuxx/Acq.h>
# endif
# ifndef RCUDATA_ACQ_H
#  include <rcudata/Acq.h>
# endif
# ifndef __IOSTREAM__
#  include <iostream>
# endif
# ifndef __SSTREAM__
#  include <sstream>
# endif
# ifndef __STDEXCEPT__
#  include <stdexcept>
# endif
# ifndef ROOT_TSystem
#  include <TSystem.h>
# endif
# ifndef ROOT_TApplication
#  include <TApplication.h>
# endif
#include "config.h"
#include <rcudata/Options.h>
#endif

RcuGui::Main*
rcugui(char* url, bool emul, bool inter, bool debug) 
{
#ifdef __CINT__
  gSystem->Load("librcugui.so");
#endif
  TString ret;
  if (!url || url[0] == '\0') {
    new RcuGui::ConnectionDialog(gClient->GetRoot(), ret);
    url = const_cast<char*>(ret.Data());
  }
  Rcuxx::Rcu*   rcu     = Rcuxx::Rcu::Open(url, emul, debug);
  if (!rcu) return 0;
  Rcuxx::Bc     bc(*rcu);
  Rcuxx::Altro  altro(*rcu);
  
  UInt_t mask[] = { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
		    0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF };
  RcuGui::Main* mainGUI = RcuGui::Main::Instance();
  mainGUI->SetInteractive(inter);
  mainGUI->AddRcu(*rcu, 0xFFFFFFFF, mask);
  mainGUI->AddBc(bc);
  mainGUI->AddAltro(altro);
  if (rcu->EVWORD()) {
    Rcuxx::Acq* acq = new RcuData::Acq(*rcu);
    mainGUI->AddAcq(*acq);
  }
  mainGUI->AddCompiler();
  mainGUI->Display();

  if (!inter) {
    gApplication->Run();
    return 0;
  }
  return mainGUI;
}


#ifndef __CINT__
int
main(int argc, char** argv) 
{
  Option<bool> hOpt('h', "help",    "Show this help", false, false);
  Option<bool> vOpt('v', "version", "Show version", false, false);
  Option<bool> eOpt('e', "emulation", "Emulation mode", false, false);
  Option<bool> dOpt('d', "debug",     "Show debug messages", false, false);
  CommandLine cl("SOURCE");
  cl.Add(hOpt);
  cl.Add(vOpt);
  cl.Add(eOpt);
  cl.Add(dOpt);

  if (!cl.Process(argc, argv)) return 1;
  if (hOpt) {
    cl.Help();
    Rcuxx::Rcu::PrintHelp(std::cout);
    return 0;
  }
  if (vOpt) {
    std::cout << "rcugui version " << VERSION << std::endl;
    return 0;
  }
  std::string device = (cl.Remain().size() > 0 ? cl.Remain()[0] : 
			"/dev/altro0");
  TApplication app("rcugui", 0, 0);
  try {
    rcugui(const_cast<char*>(device.c_str()), eOpt, false, dOpt);
  }
  catch (unsigned int& r) {
    std::cerr << "Error: " << r << std::endl;
    return 1;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

#endif

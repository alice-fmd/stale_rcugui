//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include "NumberDialog.h"

//____________________________________________________________________
RcuGui::NumberDialog::NumberDialog(const TGWindow *p, const TGWindow *main,
				   const char *title, const char* text, 
				   Int_t& number) 
  : TGTransientFrame(p, main, 100, 40, kVerticalFrame), 
    fText(this, text), 
    fNumber(this, number, 3, -1, TGNumberFormat::kNESInteger, 
	    TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMin, 0),
    fOK(this, "OK"),
    fValue(number)
{
  AddFrame(&fText, new TGLayoutHints(kLHintsCenterX, 3, 3, 3, 0));
  AddFrame(&fNumber, new TGLayoutHints(kLHintsExpandX, 6, 6, 3, 3));  
  AddFrame(&fOK, new TGLayoutHints(kLHintsRight, 6, 6, 3, 3));
  fOK.Connect("Clicked()", "RcuGui::NumberDialog", this, "Done()");
  
  MapSubwindows();
  
  Resize(GetDefaultSize());
  
  // position relative to the parent's window
  CenterOnParent();
  SetWindowName(title);
  SetIconName(title);
  SetClassHints("MsgBox", "MsgBox");
  
  MapRaised();
  fClient->WaitFor(this);
}

//____________________________________________________________________
void
RcuGui::NumberDialog::Done() 
{
  fValue = fNumber.GetIntNumber();
  DeleteWindow();
}

//____________________________________________________________________
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_LINKEDVIEW_H
#define RCUGUI_LINKEDVIEW_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif 
#ifndef ROOT_TGTextEdit
# include <TGTextEdit.h>
#endif 
#ifndef ROOT_TGTableLayout
# include <TGTableLayout.h>
#endif 
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif

namespace RcuGui
{
  //____________________________________________________________________
  /** @class LinkedView 
      @brief Link line numbers to display of text. 
      @ingroup rcugui_basic
   */
  class LinkedView : public TGTextEdit 
  {
  public:
    /** Constuctor 
	@param parent 
	@param w 
	@param h 
	@param size 
	@param maxZeros 
	@return  */
    LinkedView(TGCompositeFrame& parent, UInt_t w, UInt_t h, UInt_t size, 
	       Int_t maxZeros=3);
    /** Read contents of display into buffer */ 
    void ReadText(UInt_t* data);
    /** Read contents of display into buffer */ 
    void ReadText(size_t offset, size_t n, UInt_t* data);
    /** Write buffer into display */
    void WriteText(const UInt_t* data);
    /** Write buffer into display */
    void WriteText(size_t offset, size_t n, const UInt_t* data);
    /** Row count */
    UInt_t RowCount() const { return fText->RowCount();  }
    /** Clear it */ 
    void Clear() { fText->Clear(); }
    /** Load a file */ 
    void Load(const Char_t* name) { fText->Load(name); }
    /** Save to a file */ 
    void Save(const Char_t* name) { fText->Save(name); }
    /** Scroll this and number canvas */
    void ScrollCanvas(Int_t new_top, Int_t direction);
    /** Resize */ 
    void Resize(UInt_t w, UInt_t h);
    /** Get the number view */ 
    TGTextView& Numbers() { return fNumberView; }
    /** Get the number buffer */ 
    TGText& NumbersBuffer() { return fNumberBuffer; }
  protected:
    /** Number text buffer */ 
    TGText      fNumberBuffer;
    /** Number view frame */ 
    TGTextView  fNumberView;
    /** Number of lines */
    UInt_t fSize;
    /** Maximum numbers of zero's */
    Int_t fMaxZeros;
  };
}
#endif
//
// EOF
//

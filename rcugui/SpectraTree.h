// -*- mode: C++ -*-
//____________________________________________________________________
//
// $Id: SpectraTree.h,v 1.4 2009-02-09 23:11:57 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    MonitorTree.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of tree for the Monitor
*/
#ifndef RCUGUI_SPECTRATREE_H
#define RCUGUI_SPECTRATREE_H
#ifndef RCUGUI_MONITORTREE_H
# include <rcugui/MonitorTree.h>
#endif
#ifndef RCUDATA_SPECTRA_H
# include <rcudata/Spectra.h>
#endif

namespace RcuGui
{
  //____________________________________________________________________
  /** @struct SpectraTree 
      @brief Special LinkedTree class for the monitor 
      @ingroup rcugui_read 
  */
  struct SpectraTree : public MonitorTree 
  {
    /** Constructor 
	@param f   Parent frame 
	@param w   Width
	@param h   Height  */
    SpectraTree(TGCompositeFrame& f, UInt_t w=800, UInt_t h=600);
    /** Fill value into appropriate histogram 
	@param board   Board number
	@param chip    Chip number
	@param channel Channel number
	@param t       Time bin 
	@param adc     ADC value */
    void Fill(unsigned int rcu, 
	      unsigned int board, 
	      unsigned int chip, 
	      unsigned int channel, 
	      unsigned int t, 
	      unsigned int adc);
    /** Clear it */
    void Clear();
    /** Reset it */
    void Reset();
    /** Handle selection */
    void HandleSelect();

    /** Check if we have all the stuff we need for this channel (that
	is, a cache object and an entry in the tree).  If not, we make
	it (recursively).
	@param board   Board number
	@param chip    Chip number
	@param channel Channel number
	@return @c true */
    bool MakeChannel(unsigned int rcu, 
		     unsigned int board, 
		     unsigned int chip, 
		     unsigned int channel);
    /** Check if we have everything we need for this time bin (that
	is, a cache object and an entry in the tree).  If not we make
	the needed stuff. 
	@param board   Board number  
	@param chip    Chip number   
	@param channel Channel number
	@param t       Timebin
	@return @c true on success, @c false otherwise */
    bool MakeTimebin(unsigned int rcu, 
		     unsigned int board, 
		     unsigned int chip, 
		     unsigned int channel, 
		     unsigned int t);
  protected:
    /** Reference to cache */ 
    RcuData::Spectra::Top   fTop;
    /** Pointer to current channel information */
    RcuData::Spectra::Chan* fCurrent;
    /** Cached entry */ 
    TGListTreeItem* fCachedEntry;
  };
}
#endif
//
// EOF
//

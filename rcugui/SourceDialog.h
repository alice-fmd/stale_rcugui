// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_SOURCE_H
#define RCUGUI_SOURCE_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif
#ifndef ROOT_TGTextEntry
# include <TGTextEntry.h>
#endif
#ifndef ROOT_TGLayout
# include <TGLayout.h>
#endif
#ifndef ROOT_TGTableLayout
# include <TGTableLayout.h>
#endif
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif
#ifndef RCUGUI_LABELEDNUMBER
# include <rcugui/LabeledNumber.h>
#endif
// class TGTableLayoutHints;
class TGGroupFrame;
// class TGHButtonGroup;
// class TGLayoutHints;
class TString;

namespace RcuGui
{
  //__________________________________________________________________
  /** @struct SourceType 
      @brief  Utility type used by the SourceDialog. 
      @ingroup rcugui_basic
  */
  struct SourceType 
  {
    /** Constructor 
	@param p 
	@param title 
	@param i 
	@return  */
    SourceType(TGCompositeFrame& p, const char* title, int i);
    /** Copy constructor 
	@param other 
	@return  */
    SourceType(const SourceType& other);
    /** Destructor */
    virtual ~SourceType() {}
    /** IS enableed? */
    virtual bool   IsEnabled() const { return fSelect.IsDown(); }
    /** Enable */
    virtual void   Enable() { fSelect.SetDown(); }
    /** Disable */
    virtual void   Disable() { fSelect.SetDown(kFALSE); }
    /** @param url Get URL */
    virtual void   Url(TString& url) const { url = ""; }
    /** Connect handler 
	@param rClass 
	@param r 
	@param rHandler  */
    virtual void   Connect(const char* rClass, void* r, const char* rHandler);
    /** Handle button */
    virtual void   Handle() { Enable(); }
    /** Handle browse */
    virtual void   HandleBrowse() {}
  protected:
    /** Hints */
    TGTableLayoutHints fSelectHints;
    /** Button */
    TGRadioButton      fSelect;
  };    

  //__________________________________________________________________
  /** @struct SourceDialog
      @brief  A dialog that askes for source details
      @ingroup rcugui_basic
  */
  struct SourceDialog : public TGTransientFrame
  {
    /** Types */
    enum {
      /** Online */
      kOnline  =  1, 
      /** Offline */
      kOffline =  2, 
      /** Cancel */
      kCancel  =  4, 
      /** OK */
      kOk      =  8, 
      /** Browse */
      kBrowse  = 16,
      /** Tree */
      kTree    = 32,
      /** All */
      kAll     = 64
    };
  
    /** Constructor
	@param p     Parent window
	@param ret   On return, contains the source URI
	@param out   On return, contains optional output file
	@param n     On return, contains number of events to read 
	@param skip  On return, contains number of events to skip 
	@param tree  On return, flags whether to make a data tree or not 
	@param all   On return, flags whether we should get all events. 
	@param flags Bit pattern of features to enable in dialog  */
    SourceDialog(const TGWindow* p, 
		 TString& ret,
		 TString& out, 
		 long&    n, 
		 size_t&  skip,
		 bool&    tree, 
		 bool&    all,
		 int flags=(kOnline|kOffline));
    /** Destructor */
    virtual ~SourceDialog();

    /** Handle the OK and cancel buttons. 
	@param i Which button */
    void HandleButtons(Int_t i);
    /** Handle browse button */
    void HandleBrowse();
    /** Enable the U2F line */
    void EnableOnline();
    /** Enable Offline line */
    void EnableOffline();
  protected:
    /** Reference to return value */
    TString&          fReturn;
    /** Reference to return value */
    TString&          fOut;
    /** Reference to return value */
    long&             fN;
    /** Reference to return value */
    size_t&           fSkip;
    /** Reference to return value */
    bool&             fAll;
    /** Reference to return value */
    bool&             fTree;
    /** Layout hints for the selection frame */
    TGLayoutHints     fSelectHints;
    /** Selection frame */
    TGGroupFrame      fSelect;
    /** Hints for option group */
    TGLayoutHints     fOptionsHints;
    /** Hints for options */
    TGLayoutHints     fOptionHints;
    /** Option group */
    TGGroupFrame      fOptions;
    /** Frame for output options */
    TGHorizontalFrame fOutputFrame;
    /** Output file entry */
    TGLabel           fOutputLabel;
    /** Output file entry */
    TGTextEntry       fOutputEntry;
    /** Output file browse button */ 
    TGTextButton      fOutputBrowse;
    /** Max events */ 
    LabeledIntEntry   fMaxEvents;
    /** Skip events */ 
    LabeledIntEntry   fSkipEvents;
    /** Frame for misc options */
    TGHorizontalFrame fMiscFrame;
    /** Tree check button */ 
    TGCheckButton     fTreeOpt;
    /** All check button */ 
    TGCheckButton     fAllOpt;
    /** Hints for button group */
    TGLayoutHints     fButtonsHints;
    /** Hints for buttons */
    TGLayoutHints     fButtonHints;
    /** Button group */
    TGHButtonGroup    fButtons;
    /** Cancel button */
    TGTextButton      fCancel;
    /** OK button */
    TGTextButton      fOK;
    /** On-line dialog */
    SourceType*       fOnline;
    /** Off-line dialog */
    SourceType*       fOffline;
  };
}

#endif
//
// EOF
//

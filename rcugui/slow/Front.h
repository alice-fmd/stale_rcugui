// -*- mode: C++ -*-
#ifndef RCUGUI_SLOW_RCU_H
#define RCUGUI_SLOW_RCU_H
#include <TGFrame.h>
// #include <TGTab.h>
#include <rcugui/ErrorBit.h>
#include <rcugui/Scrollable.h>
#include <string>
#include <vector>

namespace RcuGui
{
  namespace Slow 
  {
    //__________________________________________________________________
    class Errst;
    class Actfec;
    class State;
    class FecFactory;
    class Fec;

    //__________________________________________________________________
    /** RCU slow monitor frame 
	@ingroup slowmon */
    class Front : public Scrollable
    {
    public:
      /** ctor */
      Front(const std::string& name, TGTab& parent);
      /** Set FEC factory */
      void SetFactory(FecFactory* factory) { fFactory = factory; }
      /** Update list of FECs */
      void UpdateFecs(unsigned int actfec);
    private:
      /** Check error and status register */
      void CheckErrst();
      /** Start time */
      time_t          fStart;
      /** Server name */
      std::string     fName;
      /** Parent */
      TGTab&          fTabs;
      /** Active FECs */
      Actfec*         fActfec;
      /** State */
      State*          fState;
      /** Error and status register */
      Errst*          fErrst;
      /** Factory of FECs */
      FecFactory*     fFactory;
      /** List of FECs */
      typedef std::vector<Fec*> FecList;
      /** List of FECs */
      FecList         fFecs;
    };
  }
}
#endif
//
// EOF
//

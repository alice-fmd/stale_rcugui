#include "slow/Graph.h"
#include "slow/Canvas.h"
#include "slow/Table.h"
#include <dim/dic.hxx>
#include <TLegend.h>
#include <TH1.h>

//====================================================================
RcuGui::Slow::Graph::Graph(const std::string& name, 
			   const std::string& title,
			   Canvas&           canvas, 
			   Table&            table,
			   Int_t             col, 
			   TF1*              conv)
  : Service(name, title),
    fMin((conv ? conv->Eval(0) : 0)),
    fMax((conv ? conv->Eval(1024) : 1024)),
    fStart(0),
    fLast(fStart), 
    fADC(0),
    fConv(conv), 
    fCanvas(canvas), 
    fTable(table),
    fRealValue(&table.Inner(), "Unknown"),
    fADCValue(&table.Inner(), "Unknown")
{  
  fGraph.SetName(Form("g_%s", name.c_str()));
  fGraph.SetTitle(name.c_str());
  fGraph.SetLineColor(col);
  fGraph.SetMarkerColor(col);
  fGraph.SetMarkerStyle(20);
  fGraph.SetMarkerSize(1);
  fGraph.GetHistogram()->GetYaxis()->SetTitle(title.c_str());
  fGraph.GetHistogram()->SetMaximum(1.05*fMax);
  fGraph.GetHistogram()->SetMinimum(0.95*fMin);
  fGraph.GetHistogram()->SetStats(0);
  fGraph.SetPoint(0, 0, fMin);
  
  fRealValue.SetBackgroundColor(NOLINK_COLOR);
  fADCValue.SetBackgroundColor(NOLINK_COLOR);
}

//____________________________________________________________________
RcuGui::Slow::Graph::~Graph()
{
}

//____________________________________________________________________
void
RcuGui::Slow::Graph::Init()
{
  fCanvas.AddGraph(*this);
  fTable.AddGraph(*this);
}

//____________________________________________________________________
void
RcuGui::Slow::Graph::Draw(Option_t* option)
{
  TString opt(option);
  opt.Append(" same");
  fGraph.Draw(opt.Data());
}

//____________________________________________________________________
void
RcuGui::Slow::Graph::MakeLegendEntry(TLegend& l)
{
  l.AddEntry(&fGraph, fTitle.c_str(), "pl");
}

//____________________________________________________________________
void
RcuGui::Slow::Graph::NoLink()
{
  fRealValue.SetText("No link");
  fADCValue.SetText("No link");
  fRealValue.SetBackgroundColor(NOLINK_COLOR);
  fADCValue.SetBackgroundColor(NOLINK_COLOR);
}

//____________________________________________________________________
void
RcuGui::Slow::Graph::DoClear()
{
  int n = fGraph.GetN();
  for (int i = n - 1; i >= 0; i--) fGraph.RemovePoint(i);
  fGraph.SetPoint(0, 0, fMin);
  fClear = false;
}

//____________________________________________________________________
void
RcuGui::Slow::Graph::Handle(DimStampedInfo* info)
{
  // This is executed by a timer to not block the GUI in the DIM call back 
  fRealValue.SetBackgroundColor(LINK_COLOR);
  fADCValue.SetBackgroundColor(LINK_COLOR);

  int t = info->getTimestamp();
  int i = fGraph.GetN();

  if (fStart == 0) { 
    fStart = t;
    i      = 0;
  }

  if (fClear) { DoClear(); i = 0; }
  if (fPause) return;

  double rv  = 0;
  int    adc = info->getInt();
  if (!fConv) rv = adc;
  else        rv = fConv->Eval(adc);
  fLast = rv;
  fADC  = adc;

  // std::cout << Name() << " " << t << "->" << rv << std::endl;
  fGraph.SetPoint(i, t, rv);

  fRealValue.SetText(Form("%6.2f", rv));
  fADCValue.SetText(Form("0x%x", adc));
  UpdateClients(t);
}

//____________________________________________________________________
void
RcuGui::Slow::Graph::UpdateClients(int t)
{
  fCanvas.Update(t);
  fTable.Update();
}  
//
// EOF
//

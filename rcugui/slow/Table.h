// -*- mode: c++ -*-
#ifndef RCUGUI_SLOW_TABLE_H
#define RCUGUI_SLOW_TABLE_H
#include <TGFrame.h>
#include <TGLabel.h>
#include <string>
#include <vector>
class TGButtonGroup;

namespace RcuGui
{
  namespace Slow 
  {
    class Graph;

    //__________________________________________________________________
    class Table : public TGGroupFrame 
    {
    public:
      Table(TGCompositeFrame& p, 
	    const std::vector<std::string>& headings,
	    const std::vector<std::string>& labels,
	    bool horiz=true);
      void Update();
      void AddGraph(Graph& g);
      void HandleButton(Int_t id);
      TGCompositeFrame& Inner() { return fTable; }
    protected:
      TGCompositeFrame              fTable;
      TGLabel                       fTopLeft;
      typedef std::vector<Graph*>   GraphList;
      GraphList                     fGraphs;
      typedef std::vector<TGLabel*> CellList;
      CellList                      fHeaders;
      CellList                      fLabels;
      typedef std::vector<TGButtonGroup*> ButtonList;
      ButtonList                    fButtons;
      TGLayoutHints                 fButtonHints;
      Bool_t                        fHorizontal;
    };
  }
}
#endif
//
// EOF
// 

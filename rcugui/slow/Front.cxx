#include <TGClient.h>
#include "slow/Service.h"
#include "slow/Fec.h"
#include "TTimer.h"
#include "slow/Front.h"
#include "dim/dic.hxx"
#include <iostream>
#include <iomanip>

//====================================================================
class RcuGui::Slow::Errst : public TGGroupFrame, public DimInfoHandler
{
public:
  Errst(RcuGui::Slow::Front& p, const std::string& name) 
    : TGGroupFrame(&(p.Container()), 
		   "Error and status register", kHorizontalFrame),
      fErrst(0),
      fPattern(*this,"Pattern","Error in PMREAD instruction",
	       0xc0c0c0, 0xdd0000),
      fAbort(*this,"Abort","Abort execution of instructions", 
	     0xc0c0c0, 0xdd0000),
      fTimeout(*this,"Timeout", 
	       "FEC did not respond within 32 clock cycles",
	       0xc0c0c0, 0xdd0000),
      fAltro(*this, "Altro", "Error from BC/ALTRO's",
	     0xc0c0c0, 0xdd0000),
      fHWAdd(*this, "Address","Channel mismatch in readout",
	     0xc0c0c0, 0xdd0000),
      fBusy(*this, "Busy", "Bus interface is busy",
	    0xc0c0c0, 0xdd0000)
  {
    p.Container().AddFrame(this, new TGLayoutHints(kLHintsExpandX));
    fErrst  = new DimStampedInfo(Form("%s_ERRST", name.c_str()), 0, -1, this);
  }
  void SetNoLink(bool nolink)
  {
    fPattern.SetEnabled(!nolink);
    fAbort.  SetEnabled(!nolink);
    fTimeout.SetEnabled(!nolink);
    fAltro.  SetEnabled(!nolink);
    fHWAdd.  SetEnabled(!nolink);
    fBusy.   SetEnabled(!nolink);
  }
  void SetValues(unsigned int errst)
  {
    fPattern.SetState(!(errst & (1 << 0)));
    fAbort.  SetState(!(errst & (1 << 1)));
    fTimeout.SetState(!(errst & (1 << 2)));
    fAltro.  SetState(!(errst & (1 << 3)));
    fHWAdd.  SetState(!(errst & (1 << 4)));
    fBusy.   SetState(!(errst & (1 << 31)));
  }
  void infoHandler()
  {
    DimInfo* info = getInfo();
    if (!info || info != fErrst) return;
    std::cout << "ERRST updated" << std::endl;
    
    void*    data   = info->getData();
    int      size   = info->getSize();
    bool     nolink =  (!data || size <= 0);
    SetNoLink(nolink);
    if (nolink) return;
    
    unsigned int errst;
    memcpy(&errst, data, sizeof(unsigned int));
    SetValues(errst);
  }
protected:
  DimStampedInfo* fErrst;
  ErrorBit        fPattern;
  ErrorBit        fAbort;
  ErrorBit        fTimeout;
  ErrorBit        fAltro;
  ErrorBit        fHWAdd;
  ErrorBit        fBusy;
};

//====================================================================
class RcuGui::Slow::Actfec : public TGGroupFrame, public DimInfoHandler
{
public:
  struct UpdateTimer : public TTimer 
  {
    UpdateTimer(RcuGui::Slow::Front& p) 
      : TTimer(100, kTRUE), fMain(p) {}
    void SetActfec(unsigned int actfec) { fActfec = actfec; }
    Bool_t Notify() { 
      Reset(); 
      fMain.UpdateFecs(fActfec); 
      TurnOff();
      return kTRUE; 
    }
    RcuGui::Slow::Front& fMain;
    unsigned int fActfec;
  };
  
  Actfec(RcuGui::Slow::Front& p, const std::string& name) 
    : TGGroupFrame(&(p.Container()), 
		   "Active front-end cards", kHorizontalFrame),
      fActfec(Form("%s_AFL",   name.c_str()), 60, 0, 0, this),
      fActfecA(this, "Branch A", kHorizontalFrame),
      fActfecB(this, "Branch B", kHorizontalFrame), 
      fTimer(p)
  {
    p.Container().AddFrame(this, new TGLayoutHints(kLHintsExpandX));
    AddFrame(&fActfecA, new TGLayoutHints(kLHintsExpandX));
    AddFrame(&fActfecB, new TGLayoutHints(kLHintsExpandX));

    using namespace RcuGui;
    
    for (size_t i = 0; i < 16; i++) {
      ErrorBit* actbit = new ErrorBit(fActfecA, Form("%d", i),
				      Form("FEC @ 0x%02d", i), 
				      0x00aa00, 0xc0c0c0);
      actbit->SetState(kButtonUp);
      fActive.push_back(actbit);
    }
    for (size_t i = 16; i < 32; i++) {
      ErrorBit* actbit = new ErrorBit(fActfecB, Form("%d", i-16),
				      Form("FEC @ 0x%02d", i), 
				      0x00aa00, 0xc0c0c0);
      actbit->SetState(kButtonUp);
      fActive.push_back(actbit);
    }
  }
  void SetNoLink(bool nolink)
  {
    SetTextColor(nolink ? NOLINK_COLOR : LINK_COLOR);
    for (Actives::iterator i = fActive.begin(); i != fActive.end(); ++i) 
      (*i)->SetEnabled(!nolink);
  }
  bool SetValues(unsigned int actfec)
  {
    SetTextColor(LINK_COLOR);
    bool changed = false;
    for (size_t i = 0; i < 32; i++) { 
      Bool_t on  = TESTBIT(actfec, i);
      Bool_t old = fActive[i]->fGood;
      fActive[i]->SetState(on);
      if (on != old) changed = true;
    }
    return changed;
  }
  void infoHandler()
  {
    DimInfo* info = getInfo();
    if (!info || info != &fActfec) return;
    
    void*    data   = info->getData();
    int      size   = info->getSize();
    bool     nolink =  (size <= 0 || !data);
    std::cout << "ACTFEC updated (" << data << "," << size << ")" << std::endl;
    SetNoLink(nolink);
    if (nolink) return;
    
    unsigned int actfec;
    memcpy(&actfec, data, sizeof(unsigned int));
    for (size_t i = 0; i < 32; i++) {
      if (i % 16 == 0) std::cout << "  " << std::flush;
      std::cout << (TESTBIT(actfec,i) ? '*' : '-') << std::flush;
    }
    std::cout << std::endl;
    if (SetValues(actfec)) { 
      fTimer.SetActfec(actfec);
      fTimer.TurnOn();
    }
  }
protected:
  DimStampedInfo  fActfec;
  TGGroupFrame    fActfecA;
  TGGroupFrame    fActfecB;
  typedef std::vector<RcuGui::ErrorBit*> Actives;
  Actives         fActive;
  UpdateTimer     fTimer;
};

//====================================================================
class RcuGui::Slow::State : public TGGroupFrame, public DimInfoHandler
{
public:
  State(RcuGui::Slow::Front& p, const std::string& name) 
    : TGGroupFrame(&(p.Container()), "State", kHorizontalFrame),
      // Update every 10 seconds
      fState(Form("%s_RCU_STATE", name.c_str()), 10, 0, 0, this),
      fButton(this, "No link")
  {
    p.Container().AddFrame(this, new TGLayoutHints(kLHintsExpandX));
    AddFrame(&fButton, new TGLayoutHints(kLHintsExpandX));
    fButton.SetEnabled(false);
    fButton.SetToolTipText("Current state of the RCU");
    Set(0);
  }
  enum { 
    kIdle = 1, 
    kStandby,
    kDownloading,
    kStandbyConf,
    kRunning,
    kError
  };
  const char* State2Text(int state)
  {
    switch (state) {
    case kIdle:        return "Idle"; 
    case kStandby:     return "Standby"; 
    case kDownloading: return "Downloading"; 
    case kStandbyConf: return "Ready"; 
    case kRunning:     return "Running"; 
    case kError:       return "Error"; 
    }
    return "No link";
  }
  Pixel_t State2Color(int state)
  {
    switch (state) {
    case kIdle:        return 0xc6c6c6; 
    case kStandby:     return 0x4791ff; 
    case kDownloading: return 0xffff00; 
    case kStandbyConf: return 0x4791ff; 
    case kRunning:     return 0x00ff00; 
    case kError:       return 0xff000; 
    }
    return 0xFF9900;
  }
  void Set(unsigned int state)
  {
    fButton.SetText(State2Text(state));
    fButton.ChangeBackground(State2Color(state));
  }
  void infoHandler()
  {
    DimInfo* info = getInfo();
    if (!info || info != &fState) return;
    
    void*    data   = info->getData();
    int      size   = info->getSize();
    bool     nolink =  (!data || size <= 0);
    Set(0);
    SetTextColor(nolink ? NOLINK_COLOR : LINK_COLOR);
    if (nolink) return;
    
    int state;
    memcpy(&state, data, sizeof(int));
    Set(state);
  }
protected:
  DimStampedInfo  fState;
  TGTextButton    fButton;
};

//====================================================================
RcuGui::Slow::Front::Front(const std::string& name, TGTab& tabs)
  : Scrollable("RCU", tabs),
    fStart(time(NULL)),
    fName(name),
    fErrst(0), 
    fTabs(tabs),
    fState(new RcuGui::Slow::State(*this, fName)),
    fActfec(new RcuGui::Slow::Actfec(*this, fName)),
    fFactory(0), 
    fFecs(32)
{
  CheckErrst();
}

//____________________________________________________________________
void
RcuGui::Slow::Front::CheckErrst()
{
  DimBrowser br;
  if (br.getServices(Form("%s_ERRST", fName.c_str())) <= 0) return;
  fErrst = new RcuGui::Slow::Errst(*this, fName);
}    
//____________________________________________________________________
void
RcuGui::Slow::Front::UpdateFecs(unsigned int actfec)
{
  std::cout << "Updating fecs: 0x" << std::hex << std::setfill('0') 
	    << std::setw(8) << actfec << std::dec << std::setfill(' ')
	    << std::endl;
  DISABLE_AST
  Bool_t changed = kFALSE;
  for (size_t i = 0; i < 32; i++) { 
    Bool_t on = TESTBIT(actfec, i);
    if (on && fFactory && !fFecs[i]) {
      fFecs[i] = fFactory->MakeFec(fName, (unsigned char)i, fTabs);
      changed = kTRUE;
    }
    else if (!on && fFecs[i]) { 
      for (size_t j = 0; j < fTabs.GetNumberOfTabs(); j++) { 
	if (fTabs.GetTabContainer(j) != &(fFecs[i]->Tab())) continue;
	
	fTabs.RemoveTab(j);
	delete fFecs[i];
	fFecs[i] = 0;
	changed = kTRUE;
	break;
      }
    }
  }
  if (changed) {
    fTabs.SetTab("RCU", kFALSE);
    fTabs.Resize(fTabs.GetDefaultSize());
    fTabs.MapSubwindows();
    fTabs.Layout();
    gClient->NeedRedraw(&fTabs, kTRUE);
    gClient->NeedRedraw(const_cast<TGWindow*>(fTabs.GetParent()), kTRUE);
  }
  ENABLE_AST
}
//____________________________________________________________________
//
// EOF
// 

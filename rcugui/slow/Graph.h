// -*- mode: C++ -*- 
#ifndef RCUGUI_SLOW_GRAPH_H
#define RCUGUI_SLOW_GRAPH_H
#include <rcugui/slow/Service.h>
#include <rcugui/LabeledNumber.h>
#include <TRootEmbeddedCanvas.h>
#include <TGraph.h>
#include <TGButton.h>
#include <TGButtonGroup.h>
#include <TTimer.h>
#include <TGButton.h>
#include <TF1.h>
#ifndef __CINT__
#include <dim/dic.hxx>
#else
// Forward decl 
class DimBrowser;
class DimInfo;
class DimStampedInfo;
class DimInfoHandler;
#endif
class TLegend;

namespace RcuGui 
{

  namespace Slow 
  {
    class Canvas;
    class Table;

    //__________________________________________________________________
    /** Trend graph 
	@ingroup slowmon */
    class Graph : public Service
    {
    public:
      /** CTOR */
      Graph(const std::string& name, 
	    const std::string& title, 
	    Canvas&            canvas, 
	    Table&             table,
	    Int_t              col, 
	    TF1*               conv);
      /** DTOR */
      virtual ~Graph();
      /** Handle update */
      virtual void Handle(DimStampedInfo* info);
      /** IF no access */
      virtual void NoLink();
      /** Draw this */
      virtual void Draw(Option_t* option="pl");
      /** Draw this */
      virtual void MakeLegendEntry(TLegend& l);
      /** Get the graph */
      TGraph& GetGraph() { return fGraph; }
      /** Pause display */
      virtual void Pause() { fPause = true; }
      /** Resume display */
      virtual void Resume() { fPause = false; }
      /** Clear graph */
      virtual void Clear() { fClear = true; }
      /** Get the ith cell */
      virtual TGLabel& Cell(int i) { return i == 0 ? fRealValue : fADCValue; }
      virtual void Init();
  protected:
      /** Clear graph */
      virtual void DoClear();
      /** Ask table and canvas to update */
      virtual void UpdateClients(int t);
      /** Minimum value */
      Double_t             fMin;
      /** Maximum value */
      Double_t             fMax;
      /** Graph */
      TGraph               fGraph;
      /** Start time */
      int                  fStart;
      /** Last value */
      double               fLast;
      unsigned int         fADC;
      /** Conversion function */
      TF1*                 fConv;
      /** Reference to canvas */
      Canvas&              fCanvas;
      /** Reference to canvas */
      Table&               fTable;
      /** Table cell value */
      TGLabel              fRealValue;
      /** Table cell value */
      TGLabel              fADCValue;
      /** Pause */
      bool                 fPause;
      /** Clear */
      bool                 fClear;
    };
  }
}
#endif
//
// EOf
//

// -*- mode: C++ -*-
#ifndef RCUGUI_SLOW_SERVICE_H
#define RCUGUI_SLOW_SERVICE_H
#include <TGFrame.h>
#include <string>
#define NOLINK_COLOR  0xaa6600
#define LINK_COLOR    0x00aa00
class DimBrowser;
class DimInfo;
class DimStampedInfo;
class DimInfoHandler;

namespace RcuGui
{
  /** @defgroup slowmon Slow Monitor code */
  /** namespace for slow monitor code 
      @ingroup slowmon 
  */
  namespace Slow 
  {
    //__________________________________________________________________
    /** Base class for slow monitor services 
      @ingroup slowmon 
    */
    class Service 
    {
    public:
      virtual ~Service() {}
      virtual void Handle(DimStampedInfo* info) = 0;
      virtual void NoLink() = 0;
      virtual void Init() {}
      const std::string& Name() const { return fName; }
      const std::string& Title() const { return fTitle; }
      void SetInfo(DimStampedInfo* info) { fInfo = info; }
    protected:
      Service(const std::string& name, 
	      const std::string& title) 
	: fName(name), fTitle(title), fInfo(0)
      {}
      DimStampedInfo* fInfo;
      std::string fName;
      std::string fTitle;
    };
  }
}
#endif
//
// EOF
//


#include "slow/Top.h"
#include "rcuGui.xpm"
#include <TApplication.h>

//====================================================================
RcuGui::Slow::Top::Top(const std::string& name)
  : TGMainFrame(gClient->GetRoot(), 1, 1, kVerticalFrame),
    fName(name),
    fMenuHints(kLHintsTop|kLHintsExpandX),
    fMenuBar(this, 100, 5),
    fMenuItemLayout(kLHintsTop|kLHintsLeft, 0, 4, 0, 0),
    fFileMenu(gClient->GetRoot(), 10, 10, kHorizontalFrame),
    fTabHints(kLHintsTop|kLHintsExpandX|kLHintsExpandY),
    // fSplitterHints(kLHintsTop|kLHintsExpandX),
    // fSplitter(this),
    fTab(this, 800, 300 /*, kFixedWidth*/), 
    fRcu(name, fTab),
    fMsg(name.c_str(), *this),
    fMsgHints(kLHintsExpandX, 3, 3, 3, 3)
{
  AddFrame(&fMenuBar, &fMenuHints);
  AddFrame(&fTab, &fTabHints);
  // AddFrame(&fSplitter, &fSplitterHints);
  AddFrame(&fMsg, &fMsgHints);
  // fSplitter.SetFrame(&fTab, kTRUE);
  // fSplitter.SetFrame(&fMsg, kFALSE);
  

  fFileMenu.AddEntry("&Quit", 0);
  fFileMenu.Connect("Activated(Int_t)", "RcuGui::Slow::Top", this, 
		    "HandleMenu(Int_t)");
  fMenuBar.AddPopup("&File", &fFileMenu, &fMenuItemLayout);

  Connect("CloseWindow()", "RcuGui::Slow::Top", this, "Close()");

  // SHow it 
  MapSubwindows();
  Resize(GetDefaultSize());
  Int_t w = int(GetWidth());
  Int_t h = int(GetHeight());
  // Resize(w,h);
  SetWMSize(w, h);
  SetWMSizeHints(int(w*.5), int(h*.8*.5), 2*w, 2*h, 2, 2);
  std::string title(Form("RCU slow monitor - fee://%s/%s", 
			 DimClient::getDnsNode(), name.c_str()));
  SetWindowName(title.c_str());
  SetIconName(title.c_str());
  SetClassHints("RcuMain", "RcuMain");
  const TGPicture* p = 
    gClient->GetPicturePool()->GetPicture("rcu_gui.xpm", 
					  const_cast<char**>(rcuGui_xpm));
  if (p) {
    Pixmap_t pic = p->GetPicture();
    gVirtualX->SetIconPixmap(GetId(), pic);
  }
  // SetIconPixmap("rcu_gui.xpm");
  SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
  MapWindow();
}


//____________________________________________________________________
void
RcuGui::Slow::Top::HandleMenu(Int_t id)
{
  switch (id) {
  case 0: Close(); break;
  }
}

//____________________________________________________________________
void 
RcuGui::Slow::Top::Close() 
{
  DontCallClose();
  // UnmapWindow();
  gApplication->Terminate();
}


//____________________________________________________________________
//
// EOF
//

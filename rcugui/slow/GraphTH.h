// -*- mode: C++ -*- 
#ifndef RCUGUI_SLOW_GRAPHTH_H
#define RCUGUI_SLOW_GRAPHTH_H
#include <rcugui/slow/Graph.h>

namespace RcuGui 
{
  namespace Slow 
  {
    class GraphTH : public Graph
    {
    public:
      GraphTH(const std::string&    name, 
	      const std::string&    title, 
	      Canvas&               canvas, 
	      Table&                table,
	      Int_t                 col, 
	      TF1*                  conv,
	      bool                  upper=true);
      void UpdateClients(int t);
      TGLabel& Cell(int i);
    protected:
      DimInfo* fThresholdService;
      TGLabel  fRealThreshold;
      TGLabel  fADCThreshold;
      TGraph   fGThres;
      bool     fUpperBound;
    };
  }
}
#endif

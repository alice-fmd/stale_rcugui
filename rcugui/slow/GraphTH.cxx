#include "slow/GraphTH.h"
#include "slow/Table.h"
#include "slow/Canvas.h"

//____________________________________________________________________
RcuGui::Slow::GraphTH::GraphTH(const std::string&    name, 
			       const std::string&    title, 
			       Canvas&               canvas, 
			       Table&                table,
			       Int_t                 col, 
			       TF1*                  conv,
			       bool                  upper) 
  : Graph(name, title, canvas, table, col, conv), 
    fGThres(2),
    fRealThreshold(&table.Inner(), ""),
    fADCThreshold(&table.Inner(), ""),
    fUpperBound(upper)
{
  fGThres.SetName(Form("%s_TH", name.c_str()));
  fGThres.SetTitle("Threshold");
  fGThres.SetLineStyle(2);
  fGThres.SetLineColor(col);
  // fGThres.SetLineWidth((upper ? 1 : -1) * 2002); // Exclussion zone
  // fGThres.SetFillStyle(3001);
  // fGThres.SetFillColor(kGray);
  fGThres.SetPoint(0, 0, fMin);
  canvas.cd();
  fGThres.Draw("l same");

  fThresholdService = new DimInfo(Form("%s_TH", Name().c_str()), 0, 0);
}

//____________________________________________________________________
void
RcuGui::Slow::GraphTH::UpdateClients(int t)
{
  if (fThresholdService->getSize() > 0) { 
    int   adcThreshold  = fThresholdService->getInt();
    float realThreshold = fConv->Eval(adcThreshold);
    fRealThreshold.SetText(Form("%5.2f", realThreshold));
    fADCThreshold.SetText(Form("0x%x", adcThreshold));
    
    fGThres.Clear();
    fGThres.SetPoint(0, fStart, realThreshold);
    fGThres.SetPoint(1, t,      realThreshold);

    bool within = false;
    if (fUpperBound) 
      within = (fLast <= realThreshold || fADC == adcThreshold);
    else 
      within = (fLast >= realThreshold || fADC == adcThreshold);
    Pixel_t col = (within ? 0x77FF77 : 0xFF3333);
    if (!within)
      std::cout << Name() << " " << fLast << " (" << fADC << ") " 
		<< (fUpperBound ? ">" : "<") 
		<< realThreshold << " (" << adcThreshold << ")" << std::endl;
    fRealValue.SetBackgroundColor(col);
    fADCValue.SetBackgroundColor(col);
  }
  RcuGui::Slow::Graph::UpdateClients(t);
}


//____________________________________________________________________
TGLabel&
RcuGui::Slow::GraphTH::Cell(int i)
{
  switch (i) { 
  case 0:
  case 1: return RcuGui::Slow::Graph::Cell(i);  break;
  case 2: return fRealThreshold; break;
  case 3: return fADCThreshold; break;
  }
  return RcuGui::Slow::Graph::Cell(i);
}
//____________________________________________________________________
//
// EOF
//

// -*- mode: c++ -*-
#ifndef RCUGUI_SLOW_CANVAS_H
#define RCUGUI_SLOW_CANVAS_H
#include <TGFrame.h>
#include <TH1F.h>
#include <TRootEmbeddedCanvas.h>
#include <TGButtonGroup.h>
#include <TGButton.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <string>
#include <vector>
#include <TTimer.h>

namespace RcuGui
{
  namespace Slow 
  {
    class Graph;

    //__________________________________________________________________
    class Canvas : public TGCompositeFrame 
    {
    public:

      Canvas(TGCompositeFrame& p, 
	     const char* name, 
	     const char* ytitle,
	     int         w, 
	     int         h, 
	     Float_t     min, 
	     Float_t     max);
      void Update(int t);
      void AddGraph(Graph& g);
      void cd() { fEmCanvas.GetCanvas()->cd(); }
    protected:
      void DoUpdate();
      /** An update timer */
      struct UpdateTimer : public TTimer 
      {
	/** CTOR */
	UpdateTimer(Canvas& c) : TTimer(100, kTRUE), fCanvas(c) {}
	/** Called on time-out */
	Bool_t Notify() { 
	  fCanvas.DoUpdate();
	  TurnOff();
	  return kTRUE;
	}
	/** Graph */
	Canvas& fCanvas;
      } fTimer;

      TRootEmbeddedCanvas fEmCanvas;
      TH1F                fFrame;
      Int_t               fStart;
      Int_t               fT;
      typedef std::vector<Graph*> GraphList;
      GraphList           fGraphs;
      TLegend             fLegend;
    };
  }
}
#endif
//
// EOF
// 

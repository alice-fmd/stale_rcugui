// -*- mode: C++ -*- 
#ifndef RCUGUI_SLOW_MESSAGE_H
#define RCUGUI_SLOW_MESSAGE_H
#include <TGListView.h>
#include <TGFrame.h>
#include <string>
#include <vector>
#include <map>
#ifndef __CINT__
#include <dim/dic.hxx>
#else
// Forward decl 
class DimBrowser;
class DimInfo;
class DimStampedInfo;
class DimInfoHandler;
#endif


namespace RcuGui
{
  namespace Slow
  {
    //__________________________________________________________________
    /** Message structure */
    struct Msg
    {
      int type;
      char detector[4];
      char source[256];
      char description[256];
      char date[20];
    };

    //__________________________________________________________________
    class Message : public TGGroupFrame, public DimInfoHandler
    {
    public:
      /** CTOR */
      Message(const std::string& name, TGFrame& p);
    private:
      /** Handle updates */
      void infoHandler();
      /** Info */
      DimStampedInfo* fMsgChannel;
      /** Messages list */
      TGListView    fMsg;
      /** Container of messages */
      TGLVContainer fMsgCont;
      /** The messages */
      typedef std::vector<TGLVEntry*> Msgs;
      /** The messages */
      Msgs fMsgs;
      /**  Status bar hints */
      TGLayoutHints fStatusHints;
      /** Last update */
      int fLast;
      /** Last date string */
      std::string fLastDate;
      /** Picture set */ 
      typedef std::pair<const TGPicture*,const TGPicture*> PicSet;
      /** Big Info icon */
      PicSet fInfo;
      /** Big Warning icon */
      PicSet fWarning;
      /** Big Error icon */
      PicSet fError;
      /** Big Debug icon */
      PicSet fDebug;
      /** Colors */
      Pixel_t fColors[8];
    };
  }
}
#endif
//
// EOF
//

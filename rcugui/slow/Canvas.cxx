#include "slow/Canvas.h"
#include "slow/Graph.h"
#include <TCanvas.h>
#include <TH1F.h>

//__________________________________________________________________
RcuGui::Slow::Canvas::Canvas(TGCompositeFrame& p, 
			     const char* name, 
			     const char* ytitle, 
			     int         width, 
			     int         height,
			     Float_t     min, 
			     Float_t     max)
  : TGCompositeFrame(&p, width+30, height, kHorizontalFrame), 
    fTimer(*this),
    fEmCanvas(0, this, width, height), 
    fFrame(name, ytitle, 20, 0, 1), 
    fLegend(0.75, 0.2, 0.97, 0.94)
{
  p.AddFrame(this, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

  Int_t cid = fEmCanvas.GetCanvasWindowId();
  TCanvas* c = new TCanvas(Form("c_%s", name), width, height, cid);
  c->SetFillColor(0);
  c->SetBorderMode(0);
  c->SetBorderSize(0);
  c->SetFrameFillColor(0);
  c->SetFrameBorderSize(1);
  c->SetFrameBorderMode(0);
  c->SetTopMargin(0.05);
  c->SetBottomMargin(0.12);
  c->SetRightMargin(0.25);
  c->SetLeftMargin(float(height)/8./width);
  
  fEmCanvas.AdoptCanvas(c);
  fEmCanvas.SetAutoFit(kTRUE);
  AddFrame(&fEmCanvas, new TGLayoutHints(kLHintsLeft|kLHintsExpandX));

  fFrame.SetStats(0);

  fFrame.GetXaxis()->SetTimeDisplay(1);
  fFrame.GetXaxis()->SetTitle("t [s]");
  fFrame.GetXaxis()->SetTimeFormat("%H:%M:%S");

  fFrame.GetXaxis()->SetTitleFont(132);
  fFrame.GetXaxis()->SetTitleSize(0.06);
  fFrame.GetXaxis()->SetTitleOffset(float(height)/width*1.2);
  fFrame.GetXaxis()->SetLabelFont(132);
  fFrame.GetXaxis()->SetLabelSize(0.06);
  fFrame.GetXaxis()->SetNdivisions(5);

  fFrame.GetYaxis()->SetTitleFont(132);
  fFrame.GetYaxis()->SetTitleSize(0.06);
  fFrame.GetYaxis()->SetTitleOffset(0.25);
  fFrame.GetYaxis()->SetLabelFont(132);
  fFrame.GetYaxis()->SetLabelSize(0.06);
  fFrame.GetYaxis()->SetTitle(ytitle);

  fFrame.SetMinimum(min);
  fFrame.SetMaximum(max);

  fLegend.SetBorderSize(0);
  fLegend.SetFillColor(0);


  fFrame.Draw("");
  fLegend.Draw();

  c->Modified();
  c->Update();
  c->cd();
}

//__________________________________________________________________
void
RcuGui::Slow::Canvas::AddGraph(Graph& g)
{
  fGraphs.push_back(&g);
  TCanvas* c = fEmCanvas.GetCanvas();
  c->cd();

  g.Draw();
  g.MakeLegendEntry(fLegend);
}


//__________________________________________________________________
void
RcuGui::Slow::Canvas::Update(int t)
{
  fT = t;
  fTimer.TurnOn();
}

//__________________________________________________________________
void
RcuGui::Slow::Canvas::DoUpdate()
{
  if (fStart == 0) { 
    fStart = fT;
  }
  
  TCanvas* c = fEmCanvas.GetCanvas();
  c->cd();

  if (fT == fStart) fT++;
  fFrame.SetBins(20, fStart, fT);
  c->Modified();
  c->Update();
  c->cd();
}

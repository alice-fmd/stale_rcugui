// -*- mode: C++ -*- 
#ifndef RCUGUI_SLOW_MAIN_H
#define RCUGUI_SLOW_MAIN_H
#include <rcugui/slow/Message.h>
#include <rcugui/slow/Front.h>
#include <TGMenu.h>
#include <TGStatusBar.h>
#include <TGTab.h>
#ifndef __CINT__
#include <dim/dic.hxx>
#else
// Forward decl 
class DimBrowser;
class DimInfo;
class DimStampedInfo;
class DimInfoHandler;
#endif


namespace RcuGui
{
  namespace Slow
  {
    //__________________________________________________________________
    class Top : public TGMainFrame 
    {
    public:
      Top(const std::string& name);
      void HandleMenu(Int_t i);
      void Close();
      Front& Rcu() { return fRcu; }
    private:
      /** Name */
      std::string fName;
      /** Layout hints */
      TGLayoutHints fMenuHints;
      /** Menu bar */
      TGMenuBar     fMenuBar;
      /** Layoug hints for menu entries */
      TGLayoutHints fMenuItemLayout;
      /** File menu */
      TGPopupMenu   fFileMenu;
      /** File menu */
      TGPopupMenu   fActionMenu;
      /** File menu */
      TGPopupMenu   fOptionMenu;
      /** Splitter hints */
      // TGLayoutHints fSplitterHints;
      /** Splitter */
      // TGVSplitter fSplitter;
      /**  Tab hints */
      TGLayoutHints fTabHints;
      /** Tabs */
      TGTab         fTab;
      /** Message hints */
      TGLayoutHints fMsgHints;
      /** Message log */
      RcuGui::Slow::Message fMsg;
      /** RCU services */
      Front fRcu;
  };
  }
}
#endif
//
// EOF
//

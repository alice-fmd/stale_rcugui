// -*- mode: c++ -*-
#ifndef RCUGUI_SLOW_FEC_H
#define RCUGUI_SLOW_FEC_H
#include <TGFrame.h>
// #include <TGTab.h>
#include <rcugui/ErrorBit.h>
#include <rcugui/Scrollable.h>
// #include <rcugui/slow/Service.h>
#include <string>
#include <map>
#include <vector>
#ifndef __CINT__
#include <dim/dic.hxx>
#else
// Forward decl 
class DimBrowser;
class DimInfo;
class DimStampedInfo;
class DimInfoHandler;
#endif
class TGTab;

namespace RcuGui
{
  namespace Slow 
  {
    class Service;
    //__________________________________________________________________
    class FecBits : public TGGroupFrame
    {
    public:
      FecBits(TGCompositeFrame& p, bool horiz=true);
      void UpdateLink(bool linked);
      void UpdateBits(unsigned int bits);
      void UpdateMask(unsigned int mask);
    protected:
      void UpdateDisplay();
      bool         fLinked;
      unsigned int fBits;
      unsigned int fMask;

      /** @{ 
	  @name Gui elements for CSR1 errors */
      TGLayoutHints fHints;
      TGGroupFrame  fErrors;
      ErrorBit      fError;
      ErrorBit      fSlowControl;
      ErrorBit      fAltroError;
      ErrorBit      fParity;
      ErrorBit      fInstruction;
      /** @} */

      /** @{ 
	  @name Gui elements for CSR1 interrupts */
      TGGroupFrame  fInterrupts;
      ErrorBit      fInterrupt;
      ErrorBit      fMissedSclk;
      ErrorBit      fAlps;
      ErrorBit      fPaps;
      ErrorBit      fDcOverTh;
      ErrorBit      fDvOverTh;
      ErrorBit      fAcOverTh;
      ErrorBit      fAvOverTh;
      ErrorBit      fTempOverTh;
      /** @} */
    };

    //__________________________________________________________________
    /** Base class for FECs
	@ingroup slowmon */
    class Fec : public Scrollable, public DimInfoHandler
    {
    public:
      /** CTOR */
      Fec(const std::string& name, unsigned char addr, TGTab& parent,
	  bool horiz);
      /** DTOR */
      virtual ~Fec();
      /** Register a service */
      virtual void Register(Service* m);
      /** Constuct service name */
      std::string MakeName(const std::string& name) const;
      /** Get a pointer to the container */
      TGCompositeFrame& Inner() { return fInner; }
    protected:
      /** Handle updates */
      virtual void infoHandler();
      /** Number of FEC */
      unsigned int    fNum;
      /** Server name */
      std::string     fServer;
      /** the Config/Status register 0 service */
      DimStampedInfo* fCsr0;
      /** the Config/Status register 1 service */
      DimStampedInfo* fCsr1;
      /** Display of status bits */
      FecBits fBits;
      TGCompositeFrame fInner;

      /** Map from information to service */
      typedef std::map<DimStampedInfo*,Service*> InfoMap;
      /** Map from information to service */
      InfoMap fInfos;
    };

    //__________________________________________________________________
    /** Factory of fec objects 
	@ingroup slowmon */
    class FecFactory
    {
    public:
      virtual ~FecFactory() {}
      virtual Fec* MakeFec(const std::string& name, 
			   unsigned char num, 
			   TGTab& tabs) 
      {
	return new Fec(name, num, tabs, false);
      }
    };
  }
}
#endif

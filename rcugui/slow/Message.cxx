#include "slow/Message.h"
#include "slow/Service.h"
#include <TGClient.h>
#include <TGApplication.h>
#include <TGPicture.h>
#include <dim/dic.hxx>
#include <iostream>

//====================================================================
RcuGui::Slow::Message::Message(const std::string& name, 
			       TGFrame& p)
  : TGGroupFrame(&p, "Messages", kHorizontalFrame /*|kFixedWidth*/),
    fMsg(this, 700, 100),
    fMsgCont(&fMsg),
    fStatusHints(kLHintsExpandX, 3, 3, 3, 3),
    fLast(-1),
    fLastDate("")
{
  AddFrame(&fMsg, &fStatusHints);

  fMsg.SetHeaders(5);
  fMsg.SetHeader("Event",       0, 0, 0);
  fMsg.SetHeader("Date",        0, 0, 1);
  fMsg.SetHeader("Detector",    0, 0, 2);
  fMsg.SetHeader("Source",      0, 0, 3);
  fMsg.SetHeader("Description", 0, 0, 4);
  fMsg.SetViewMode(kLVDetails);
  fMsg.AdjustHeaders();

  UInt_t small = 16;
  TGPicturePool* pool = gClient->GetPicturePool();
  fInfo.first     = pool->GetPicture("mb_asterisk_s.xpm");
  fInfo.second    = pool->GetPicture("mb_asterisk_s.xpm", small, small);
  fWarning.first  = pool->GetPicture("mb_exclamation_s.xpm");
  fWarning.second = pool->GetPicture("mb_exclamation_s.xpm", small, small);
  fError.first    = pool->GetPicture("mb_stop_s.xpm");
  fError.second   = pool->GetPicture("mb_stop_s.xpm", small, small);
  fDebug.first    = pool->GetPicture("mb_question_s.xpm");
  fDebug.second   = pool->GetPicture("mb_question_s.xpm", small, small);
  
  if (!gClient->GetColorByName("black",  fColors[0])) // Info
    std::cerr << "black pixel not found" << std::endl; 
  if (!gClient->GetColorByName("blue",   fColors[1])) // Warning
    std::cerr << "blue pixel not found" << std::endl;
  gClient->GetColorByName("red",    fColors[2]); // Error
  gClient->GetColorByName("orange", fColors[3]); // Audit failure
  gClient->GetColorByName("black",  fColors[4]); // Audit succes
  gClient->GetColorByName("green",  fColors[5]); // Debug
  gClient->GetColorByName("red",    fColors[6]); // Alarm
  gClient->GetColorByName("black",  fColors[7]); // Unknown
  

  // Subscribe 
  fMsgChannel = new DimStampedInfo(Form("%s_Message", name.c_str()), 
				   3, 0, 0, this);
}
//____________________________________________________________________
void
RcuGui::Slow::Message::infoHandler()
{
  DimStampedInfo* info = static_cast<DimStampedInfo*>(getInfo());
  if (!info) return;
  void*    data = info->getData();
  int      size = info->getSize();
  int      t    = info->getTimestamp();
  bool     nolink = (!data || size <= 0);
  
  if (nolink) 
    std::cout << "Service " << info->getName() << " has no link" 
	      << std::endl;

  SetTextColor(nolink ? NOLINK_COLOR : LINK_COLOR);
  if (info == fMsgChannel) {
    // Avoid repeated messages
    if (fLast == t) return;
    fLast = t;

    // Check that we got valid data 
    if (size != sizeof(Msg)) return;

    Msg msg;
    memcpy(&msg, data, sizeof(msg));
    std::string date(msg.date);

    // Avoid repeated messages 
    if (date == fLastDate) return;
    fLastDate = date;
    
    // Avoid too long a back-log
    if (fMsgCont.GetList()->GetEntries() > 10) fMsgCont.RemoveAll();
    PicSet* picset = 0;
    Pixel_t c = 0;
    std::string type;
    switch (msg.type) {
    case 1:  type = "Info";          picset = &fInfo;    c = fColors[0]; break;
    case 2:  type = "Warning";       picset = &fWarning; c = fColors[1];break;
    case 4:  type = "Error";         picset = &fError;   c = fColors[2];break;
    case 8:  type = "Audit failure"; picset = &fWarning; c = fColors[3];break;
    case 16: type = "Audit success"; picset = &fDebug;   c = fColors[4];break;
    case 32: type = "Debug";         picset = &fDebug;   c = fColors[5];break;
    case 64: type = "Alarm";         picset = &fError;   c = fColors[6];break;
    default: type = "Unknown";       c = fColors[7]; break;
    }

    TGLVEntry* entry = new TGLVEntry(&fMsgCont, type.c_str(), msg.date,
				     0, kChildFrame, 
				     GetDefaultFrameBackground());
    entry->SetSubnames(msg.date, 
		       msg.detector,
		       msg.source, 
		       msg.description);
    entry->SetForegroundColor(c);
    if (picset && picset->first && picset->second) 
      entry->SetPictures(picset->first, picset->second);

    fMsgCont.AddItem(entry);
    fMsg.Layout();
    gClient->NeedRedraw(&fMsg);
  }
}

//
// EOF
//

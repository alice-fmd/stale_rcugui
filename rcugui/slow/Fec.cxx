#include "slow/Fec.h"
#include "slow/Service.h"
#include "slow/Graph.h"
#include "dim/dic.h"
#include <iostream>
#include <iomanip>

//====================================================================
RcuGui::Slow::FecBits::FecBits(TGCompositeFrame& p, bool horiz)
  : TGGroupFrame(&p, "Status bits", horiz ? kHorizontalFrame : kVerticalFrame),
    fBits(0x0),
    fMask(0x0),
    fErrors(this, "Errors", kHorizontalFrame),
    fError(fErrors, "Error", "Error recvied"),
    fSlowControl(fErrors, "SC", "Slow control instruction error"),
    fAltroError(fErrors, "ALTRO", "ALTRO Error"),
    fParity(fErrors, "Parity", "Parity error"),
    fInstruction(fErrors, "Instr", "Instruction error"),
    fInterrupts(this, "Interrupts", kHorizontalFrame),
    fInterrupt(fInterrupts, "Interrupt","Interrupt recvied"),
    fMissedSclk(fInterrupts, "SCLK", "Missed sample clocks"),
    fAlps(fInterrupts, "Alps", "ALTRO power supply error"),
    fPaps(fInterrupts, "Paps", "PASA power supply error"),
    fDcOverTh(fInterrupts, "DC", "Digital current over threshold"),
    fDvOverTh(fInterrupts, "DV", "Digital voltage over threshold"),
    fAcOverTh(fInterrupts, "AC", "Analog current over threshold"),
    fAvOverTh(fInterrupts, "AV", "Analog voltage over threshold"),
    fTempOverTh(fInterrupts, "T", "Temperture over threshold")
{
  p.AddFrame(this, new TGLayoutHints(kLHintsExpandX));
  AddFrame(&fErrors, new TGLayoutHints(kLHintsExpandX));
  AddFrame(&fInterrupts, new TGLayoutHints(kLHintsExpandX));
}
//____________________________________________________________________
void
RcuGui::Slow::FecBits::UpdateBits(unsigned int bits)
{
  fBits = bits;
  UpdateDisplay();
}
//____________________________________________________________________
void
RcuGui::Slow::FecBits::UpdateMask(unsigned int mask)
{
  fMask = mask;
  UpdateDisplay();
}
//____________________________________________________________________
void
RcuGui::Slow::FecBits::UpdateLink(bool linked)
{
  fLinked = linked;
  UpdateDisplay();
}

//____________________________________________________________________
void
RcuGui::Slow::FecBits::UpdateDisplay()
{
  SetTextColor(!fLinked ? NOLINK_COLOR : LINK_COLOR);
  fInterrupt.	SetEnabled(fLinked);
  fError.	SetEnabled(fLinked);
  fSlowControl.	SetEnabled(fLinked);
  fAltroError. 	SetEnabled(fLinked);
  fInstruction.	SetEnabled(fLinked);
  fParity.     	SetEnabled(fLinked);
  fMissedSclk. 	SetEnabled(fLinked);
  fAlps.	SetEnabled(fLinked);
  fPaps.	SetEnabled(fLinked);
  fDcOverTh.	SetEnabled(fLinked);
  fDvOverTh.	SetEnabled(fLinked);
  fAcOverTh.	SetEnabled(fLinked);
  fAvOverTh.	SetEnabled(fLinked);
  fTempOverTh. SetEnabled(fLinked);
  if (!fLinked) { 
    std::cout << "no link" << std::endl;
    return;
  }
    
  unsigned int csr1 = (fBits & fMask);
  fInterrupt.	 SetState(!(csr1 & (1 << 13)));
  fError.	 SetState(!(csr1 & (1 << 12)));
  fSlowControl.  SetState(!(csr1 & (1 << 11)));
  fAltroError.   SetState(!(csr1 & (1 << 10)));
  fInstruction.  SetState(!(csr1 & (1 << 9)));
  fParity.	 SetState(!(csr1 & (1 << 8)));
  fMissedSclk.   SetState(!(csr1 & (1 << 7)));
  fAlps.	 SetState(!(csr1 & (1 << 6)));
  fPaps.	 SetState(!(csr1 & (1 << 5)));
  fDcOverTh.	 SetState(!(csr1 & (1 << 4)));
  fDvOverTh.	 SetState(!(csr1 & (1 << 3)));
  fAcOverTh.	 SetState(!(csr1 & (1 << 2)));
  fAvOverTh.	 SetState(!(csr1 & (1 << 1)));
  fTempOverTh.   SetState(!(csr1 & (1 << 0)));
}

  
//====================================================================
RcuGui::Slow::Fec::Fec(const std::string& name, 
		       unsigned char num, TGTab& tabs, 
		       bool horiz)
  : Scrollable(Form("Fec %2d", int(num)), tabs),
    fNum(num), 
    fServer(name),
    fCsr0(0),
    fCsr1(0),
    fBits(fCont, true), 
    fInner(&fCont, 1, 1, (horiz ? kHorizontalFrame : kVerticalFrame))
{
  std::cout << "Fec for " << name << "/" << int(num)
	    << std::endl;
  fCsr0  = new DimStampedInfo(Form("%s_%02d_CSR0", name.c_str(), int(num)), 
			      600, 0, 0, this);
  fCsr1  = new DimStampedInfo(Form("%s_%02d_CSR1", name.c_str(), int(num)), 
			      600, 0, 0, this);
  fCont.AddFrame(&fInner, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
}
//____________________________________________________________________
RcuGui::Slow::Fec::~Fec()
{
  for (InfoMap::iterator i = fInfos.begin(); i != fInfos.end(); i++) { 
    delete i->first;
    delete i->second;
  }
}

//____________________________________________________________________
void
RcuGui::Slow::Fec::infoHandler()
{
  DimInfo* info = getInfo();
  if (!info) return;
  void*    data   = info->getData();
  int      size   = info->getSize();
  bool     nolink = (size <= 0 || !data);
  if (nolink) 
    std::cout << "Service " << info->getName() << " has no link" 
	      << std::endl;

  if (info == fCsr1) {
    fBits.UpdateLink(!nolink);
    if (nolink) return;

    unsigned int csr1;
    memcpy(&csr1, data, sizeof(unsigned int));
    fBits.UpdateBits(csr1);
  }
  else if (info == fCsr0) {
    if (nolink) return;
    unsigned int csr0;
    memcpy(&csr0, data, sizeof(unsigned int));
    fBits.UpdateMask(csr0);
  }
  else {
    InfoMap::iterator i = fInfos.find(static_cast<DimStampedInfo*>(info));
    if (i == fInfos.end()) return;
    if (nolink) i->second->NoLink();
    else        i->second->Handle(i->first);
  }
}
//____________________________________________________________________
std::string
RcuGui::Slow::Fec::MakeName(const std::string& name) const
{
  return std::string(Form("%s_%02d_%s",fServer.c_str(),
			  int(fNum),name.c_str()));
}
//____________________________________________________________________
void
RcuGui::Slow::Fec::Register(Service* m)
{
  if (!m) return;

  const std::string& n = m->Name();
  DimStampedInfo* info = 0;
  for (InfoMap::const_iterator i = fInfos.begin(); 
       i != fInfos.end(); i++) { 
    info = i->first;
    if (!info) continue;
    if (n == info->getName()) break;
    info = 0;
  }
  if (info) { 
    std::cerr << "Info " << n << " already registered (" 
	      << info->getName() << ")" << std::endl;
    return;
  }
  info = new DimStampedInfo(n.c_str(), 0, 0, 0, this);
  fInfos[info] = m;
  m->SetInfo(info);
  m->Init();
  // fCont.AddFrame(m, &fHints);
}



//__________________________________________________________________
//
// EOF
//

#include "slow/Table.h"
#include "slow/Graph.h"
#include <TGTableLayout.h>

struct ButtonGroup : public TGButtonGroup
{
  ButtonGroup(const TGWindow* p) 
    : TGButtonGroup(p, "", kHorizontalFrame),
      fHints(kLHintsExpandX,0,0,0,0)
  {
    fBorderWidth = 0;
    SetBorderDrawn(kFALSE);
    // SetBackgroundColor(kWhite);
    SetLayoutHints(&fHints);
  }
  TGLayoutHints fHints;
};

//__________________________________________________________________
RcuGui::Slow::Table::Table(TGCompositeFrame& p, 
			   const std::vector<std::string>& headings,
			   const std::vector<std::string>& labels,
			   bool horiz)
  : TGGroupFrame(&p, "Summary", kHorizontalFrame),
    fTable(this,1,1,kSunkenFrame),
    fTopLeft(&fTable, ""),
    fButtonHints(kLHintsExpandX, 0, 0, 0, 0),
    fHorizontal(horiz)
{
  AddFrame(&fTable); // , new TGLayoutHints(kLHintsExpandX));
  fTable.SetBackgroundColor(0x0);
  int nhead = headings.size();
  int ndisp = labels.size();
  int nrows = (horiz ? nhead+1 : ndisp+2);
  int ncols = (horiz ? ndisp+2 : nhead+1);
  fTable.SetLayoutManager(new TGTableLayout(&fTable, ncols, nrows,
					    kFALSE, 1));
  UInt_t opts = kLHintsFillX|kLHintsFillY;
  Pixel_t headCol = 0x7777ff;
  fTopLeft.SetBackgroundColor(headCol);
  fTable.AddFrame(&fTopLeft,new TGTableLayoutHints(0,1,0,1,opts,0, 0, 0, 0));

  const TGPicture* clearPic = gClient->GetPicture("bld_delete.xpm", 16, 16);
  const TGPicture* pausePic = gClient->GetPicture("pause.png", 16, 16);
  
  for (size_t i = 0; i < nhead; i++) {
    TGLabel* header = new TGLabel(&fTable, headings[i].c_str());
    header->SetBackgroundColor(headCol);
    header->SetTextJustify(kTextCenterX|kTextCenterY);
    int col = (horiz ? i + 1 : 0);
    int row = (horiz ? 0     : i+1);
    fTable.AddFrame(header,new TGTableLayoutHints(col, col+1, row, row+1, opts,
						   0, 0, 0, 0));
    fHeaders.push_back(header);

    TGButtonGroup*   buttons = new ButtonGroup(&fTable);
    TGPictureButton* pause   = new TGPictureButton(buttons, pausePic, 2*i);
    TGPictureButton* clear   = new TGPictureButton(buttons, clearPic, 2*i+1);
    // buttons->SetBorderDrawn(kFALSE);
    // buttons->SetBackgroundColor(kBlue);
    // buttons->SetBorderWidth(0);
    // buttons->SetLayoutHints(&fButtonHints);
    buttons->Connect("Clicked(Int_t)", "RcuGui::Slow::Table", this, 
		     "HandleButton(Int_t)");
    fButtons.push_back(buttons);
    pause->AllowStayDown(kTRUE);
    clear->AllowStayDown(kTRUE);
    col = (horiz ? col     : ndisp+1);
    row = (horiz ? ndisp+1 : row);
    fTable.AddFrame(buttons, new TGTableLayoutHints(col,col+1,row,row+1,
						    opts|kLHintsCenterX,
						    0, 0, 0, 0));
  }
  Pixel_t labelCol = 0x77ddff;
  for (size_t i = 0; i < ndisp; i++) {
    TGLabel* label = new TGLabel(&fTable, labels[i].c_str());
    label->SetBackgroundColor(labelCol);
    label->SetTextJustify(kTextCenterX|kTextCenterY);
    int col = (horiz ? 0   : i + 1);
    int row = (horiz ? i+1 : 0);
    fTable.AddFrame(label, new TGTableLayoutHints(col, col+1, row, row+1, opts,
						  0, 0, 0, 0));
    fLabels.push_back(label);
  }
  TGLabel* oper = new TGLabel(&fTable, "Operations");
  int col = (horiz ? 0       : ndisp + 1);
  int row = (horiz ? ndisp+1 : 0);
  fTable.AddFrame(oper, new TGTableLayoutHints(col, col+1, row, row+1, opts,
						0, 0, 0, 0));
  fLabels.push_back(oper);
  
  p.AddFrame(this, new TGLayoutHints(horiz ? kLHintsExpandX : kLHintsExpandY));
}
    
//__________________________________________________________________
void
RcuGui::Slow::Table::AddGraph(Graph& g)
{
  int n   = fGraphs.size();
  std::cout << "Adding graph # " << n << ": " << g.Name() << std::endl;
  fGraphs.push_back(&g);
  UInt_t opts = kLHintsFillX|kLHintsFillY;
  for (size_t i = 0; i < fLabels.size()-1; i++) { 
    int col = (fHorizontal ? n+1 : i+1);
    int row = (fHorizontal ? i+1 : n+1);
    TGLabel& c = g.Cell(i);
    c.SetBackgroundColor(0xFFFFFF);
    c.SetTextJustify(kTextRight|kTextCenterY);
    c.SetMargins(0, 2, 0, 0);
    fTable.AddFrame(&c, new TGTableLayoutHints(col, col+1, row, row+1, 
					       opts, 0, 0, 0, 0));
  }
}

//__________________________________________________________________
void
RcuGui::Slow::Table::HandleButton(Int_t id)
{
  int  graphNo = id / 2;
  bool isClear = id % 2;

  if (graphNo < 0 || graphNo >= fGraphs.size() || graphNo >= fButtons.size()) 
    return;
  Graph*           graph   = fGraphs[graphNo];
  TGButtonGroup*   buttons = fButtons[graphNo];
  
  if (!graph || !buttons) return;

  if (isClear) { 
    graph->Clear();
    buttons->Find(id)->SetDown(kFALSE);
  }
  else { 
    bool paused = buttons->Find(id)->IsDown();
    if(paused) graph->Pause();
    else       graph->Resume();
  }
}

//__________________________________________________________________
void
RcuGui::Slow::Table::Update()
{
  gClient->NeedRedraw(&fTable, kTRUE);
}

//__________________________________________________________________
//
// EOF
//


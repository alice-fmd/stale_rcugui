// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_INSTRUCTIONMEMORY_H
#define RCUGUI_INSTRUCTIONMEMORY_H
# include <TGFrame.h>
# include <TGTab.h>
# include <rcugui/rcu/Memory.h>
# include <rcugui/LabeledNumber.h>
# include <rcuxx/rcu/RcuIMEM.h>
# include <rcuxx/rcu/RcuIRADD.h>
# include <rcuxx/rcu/RcuIRDAT.h>
# include <rcuxx/rcu/RcuCommand.h>

namespace RcuGui
{
  //==================================================================
  /** @class InstructionMemory
      @ingroup rcugui_low
      @brief specialisation for Pedestal memory.  Adds a button to fill 
      with and incremental pattern. */
  class InstructionMemory : public Memory 
  {
  public:
    /** Constructor 
	@param tabs 
	@param imem 
	@param iradd 
	@param irdat 
	@param exec   */
    InstructionMemory(TGTab& tabs, 
		      Rcuxx::RcuIMEM& imem, 
		      Rcuxx::RcuIRADD& iradd, 
		      Rcuxx::RcuIRDAT& irdat,
		      Rcuxx::RcuCommand& exec, 
		      Rcuxx::RcuCommand& abort);
    /** Handle address change */
    void HandleAddr();
    /** Set address */
    void SetAddr(Int_t addr);
  protected:
    /** Command */
    Rcuxx::RcuCommand& fCmd;
    /** hints */
    TGLayoutHints fInstrHints;
    /** instructions */
    TGGroupFrame fInstr;
    /** Address */
    LabeledIntEntry* fAddr;
    /** Execute */
    Command fEXEC;
    /** Execute */
    Command fABORT;
    /** Last */
    class IRADD : public RcuGui::Register
    {
    private:
      /** Last address */
      Rcuxx::RcuIRADD&   fLowAdd;
      /** Last data */
      Rcuxx::RcuIRDAT&   fLowDat;
      /** Last address */
      RcuGui::LabeledIntView fIRADD;
      /** Last data */
      RcuGui::LabeledIntView fIRDAT;
    public:
      /** Constructor 
	  @param f The frame to put the interface in 
	  @param lowAdd Low-level access to address
	  @param lowDat Low-level access to data */
      IRADD(TGCompositeFrame& f, 
	    Rcuxx::RcuIRADD& lowAdd, 
	    Rcuxx::RcuIRDAT& lowDat) 
	: RcuGui::Register(f, lowAdd), 
	  fLowAdd(lowAdd), fLowDat(lowDat),
	  fIRADD(fFields,"Last ALTRO Instruction", 0, 0xfffff),
	  fIRDAT(fFields, "Last ALTRO Data", 0, 0xfffff)
      {
	fGroup.SetTitle("Last instruction address & data");
	fIRADD.SetToolTipText("Last Bus instruction");
	fIRDAT.SetToolTipText("Last Bus data");
	Get();
      }
      /** Update it */
      Bool_t Update() 
      {
	if (fLowDat.Update()) return kFALSE;
	return RcuGui::Register::Update();
      }    
      /** Decode values */
      void Get()
      {
	fIRADD.SetValue(fLowAdd.IRADD());
	fIRDAT.SetValue(fLowDat.IRDAT());
      }
    };
    IRADD* fLast;    
  };
}

//====================================================================
inline
RcuGui::InstructionMemory::InstructionMemory(TGTab& tabs, Rcuxx::RcuIMEM& imem,
					     Rcuxx::RcuIRADD& iradd, 
					     Rcuxx::RcuIRDAT& irdat, 
					     Rcuxx::RcuCommand& exec,
					     Rcuxx::RcuCommand& abort) 
  : Memory(tabs, imem, 256, kHorizontalFrame, 6, 100),
    fCmd(exec),
    fInstrHints(kLHintsExpandX|kLHintsTop, ISCALE(3), ISCALE(3), 0, ISCALE(5)),
    fInstr(fTop, "Execute", kHorizontalFrame),
    fAddr(exec.HasArgument() ? 
	  new LabeledIntEntry(fInstr,"Address", 0, imem.Size()) : 0),
    fEXEC(fInstr, exec, "EXECute instructions"),
    fABORT(fInstr, abort, "ABORT execution of instruction memory"),
    fLast(&iradd && &irdat ? new IRADD(*fTop, iradd, irdat) : 0)
{
  fTop->SetLayoutManager(new TGVerticalLayout(fTop));
  fTop->AddFrame(&fInstr, &fInstrHints);
  if (fAddr) 
    fAddr->fView.Connect("ValueSet(Long_t)", "RcuGui::InstructionMemory", 
			 this, "HandleAddr()");
  //fView.Resize(fView.GetWidth(), fView.GetHeight() - 160);
  // fButtons.Resize(fButtons.GetWidth(), fButtons.GetHeight() - 160);
  //fCont.Resize(fCont.GetWidth(), fCont.GetHeight() - 150);
}

//____________________________________________________________________
inline void 
RcuGui::InstructionMemory::HandleAddr()
{
  if (!fAddr) return;
  unsigned int addr = fAddr->GetValue();
  fCmd.SetArgument(addr);
}

//____________________________________________________________________
inline void 
RcuGui::InstructionMemory::SetAddr(Int_t offset)
{
  if (!fAddr) return;
  fAddr->SetValue(offset);
  fCmd.SetArgument(offset);
}

#endif

//
// EOF
//

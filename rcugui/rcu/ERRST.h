// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ERRST_H
#define RCUGUI_ERRST_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuERRST.h>


namespace RcuGui
{
  //__________________________________________________________________
  /** @class ERRST
      @brief Interface to the ERRor and STatus register
      @ingroup rcugui
  */
  class ERRST : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    ERRST(TGCompositeFrame& f, Rcuxx::RcuERRST& low) 
      : Register(f, low), 
	fLow(low),
	fPattern(fFields,"Pattern","Error in PMREAD instruction",
		 0xc0c0c0, 0xdd0000),
	fAbort(fFields,"Abort","Abort execution of instructions", 
	       0xc0c0c0, 0xdd0000),
	fTimeout(fFields,"Timeout", 
		 "FEC did not respond within 32 clock cycles",
		 0xc0c0c0, 0xdd0000),
	fAltro(fFields, "Altro", "Error from BC/ALTRO's",
	       0xc0c0c0, 0xdd0000),
	fHWAdd(fFields, "Address","Channel mismatch in readout",
	       0xc0c0c0, 0xdd0000),
	fBusy(fFields, "Busy", "Bus interface is busy",
	      0xc0c0c0, 0xdd0000),
	fWhere(fFields,"Where", 0, 0x3ff, kHorizontalFrame, 
	       ISCALE(3), ISCALE(3), ISCALE(12), ISCALE(2))
    {
      fGroup.SetTitle("Error and status");
      Get();
    }
    Bool_t Update()
    {
      UInt_t ret = fLow.Update();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      Get();
      return kTRUE;
    }
    void Clear() 
    {
      if (!fClear) return;
      UInt_t ret = fLow.Clear();
      if (ret) {
	Main::Instance()->Error(ret);
	return;
      }
      Get();
    }  
    void Get() 
    {
      fPattern.SetState(!fLow.IsPattern());
      fAbort.SetState(!fLow.IsAbort());
      fTimeout.SetState(!fLow.IsTimeout());
      fAltro.SetState(!fLow.IsAltro());
      fHWAdd.SetState(!fLow.IsHWAdd());
      fBusy.SetState(!fLow.IsBusy());
      fWhere.SetValue(fLow.Where());
      Main::Instance()->SetError("");
      if (/* fLow->IsPattern() || */
	  /* fLow.IsAbort()   ||  */
	  fLow.IsTimeout() || 
	  fLow.IsAltro()   || 
	  fLow.IsHWAdd()   || 
	  fLow.IsBusy()) {
	std::stringstream s;
	if (fLow.IsPattern()) s << fPattern.fDescription  << "\n";
	if (fLow.IsAbort())   s << fAbort.fDescription    << "\n";
	if (fLow.IsTimeout()) s << fTimeout.fDescription  << "\n";
	if (fLow.IsAltro())   s << fAltro.fDescription    << "\n";
	if (fLow.IsHWAdd())   s << fHWAdd.fDescription    << "\n";
	if (fLow.IsBusy())    s << fBusy.fDescription     << "\n";
	s << " at 0x" << std::hex << fWhere.GetValue();
	RcuGui::Main::Instance()->SetError(s.str().c_str(),
					   fLow.IsBusy(), 
					   fLow.IsAltro());
      }
    }
  private:
    Rcuxx::RcuERRST& fLow;
    ErrorBit fPattern;
    ErrorBit fAbort;
    ErrorBit fTimeout;
    ErrorBit fAltro;
    ErrorBit fHWAdd;
    ErrorBit fBusy;
    LabeledIntView fWhere;
  };
}
#endif
//
// EOF
//

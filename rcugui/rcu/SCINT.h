// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_SCINT_H
#define RCUGUI_SCINT_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuSCADD.h>
# include <rcuxx/rcu/RcuSCDAT.h>
# include <rcuxx/rcu/RcuCommand.h>

namespace RcuGui
{
  //====================================================================
  /** @class SCINT
      @brief Interface to the ACTive FrontEnd Card  
      @ingroup rcugui
  */
  class SCINT : public Register
  {
  public:
    SCINT(TGCompositeFrame& f, 
	  Rcuxx::RcuSCADD& add, 
	  Rcuxx::RcuSCDAT& dat,
	  Rcuxx::RcuCommand& exec) 
      : Register(f, add), fLowAdd(add), fLowDat(dat),
	fAddress(&fFields),
	fFec(fAddress,"Address", 0, 0x1F),
	fReg(fAddress,"Instruction", 0, 0xFF),
	fData(fAddress,"Data", 0, 0xffffffff), 
	fOptions(&fFields),
	fRead(&fOptions, "Read"),
	fBCast(&fOptions, "Broadcast"),
	fExec(exec)
    {
      fOptions.AddFrame(&fRead,  new TGLayoutHints(kLHintsLeft,0,3,0,0));
      fOptions.AddFrame(&fBCast,  new TGLayoutHints(kLHintsLeft,0,3,0,0));
      fFields.AddFrame(&fAddress, 
		       new TGLayoutHints(kLHintsLeft|kLHintsExpandX,0,3,00));
      fFields.AddFrame(&fOptions, new TGLayoutHints(kLHintsRight|10,3,00));
      fGroup.SetTitle("Slow control execution");
      Get();
    }
    Bool_t Update() 
    {
      unsigned int ret = fLowDat.Update();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      return Register::Update();
    }
    void Get() 
    {
      fFec.SetValue(fLowAdd.Fec());
      fReg.SetValue(fLowAdd.Instruction());
      fRead.SetDown(fLowAdd.IsRead());
      fBCast.SetDown(fLowAdd.IsBroadcast());
      fData.SetValue(fLowDat.Data());
    }
    void Set()
    {
      fLowAdd.SetRead(fRead.IsDown());
      fLowAdd.SetBroadcast(fBCast.IsDown());
      fLowAdd.SetFec(fFec.GetValue());
      fLowAdd.SetInstruction(fFec.GetValue());
      fLowDat.SetData(fData.GetValue());
    }
    Bool_t Commit() 
    {
      Set();
      fLowAdd.Commit();
      fLowDat.Commit();
      return fExec.Commit();
    }
  private:
    Rcuxx::RcuSCADD&   fLowAdd;
    Rcuxx::RcuSCDAT&   fLowDat;
    Rcuxx::RcuCommand& fExec;
    TGVerticalFrame  fAddress;
    // TGHorizontalFrame  fDummy;
    TGVerticalFrame    fOptions;
    TGCheckButton      fRead;
    TGCheckButton      fBCast;
    LabeledIntEntry    fFec;
    LabeledIntEntry    fReg;
    LabeledIntEntry fData;
  };
}
#endif
//
// EOF
//

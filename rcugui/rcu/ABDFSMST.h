// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ABDFSMST_H
#define RCUGUI_ABDFSMST_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/rcu/StateDisplay.h>
# include <rcuxx/rcu/RcuABDFSMST.h>
# include <climits>


namespace RcuGui
{
  //====================================================================
  /** @class ABDFSMST
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class ABDFSMST : public Register
  {
  private:
    Rcuxx::RcuABDFSMST&  fLow;
    StateDisplay fRDO_B;
    StateDisplay fRD_B;
    StateDisplay fWR_B;
    StateDisplay fRDO_A;
    StateDisplay fRD_A;
    StateDisplay fWR_A;
  public:
    ABDFSMST(TGCompositeFrame& f, Rcuxx::RcuABDFSMST& low) 
      : Register(f, low), fLow(low),
	fRDO_B(fFields,fLow.RDO_B()), 	
	fRD_B(fFields,fLow.RD_B()), 	
	fWR_B(fFields,fLow.WR_B()), 	
	fRDO_A(fFields,fLow.RDO_A()), 	
	fRD_A(fFields,fLow.RD_A()), 	
	fWR_A(fFields,fLow.WR_A())
    {
      // "Arbitrator FSM"
      Get();
    }
    void Get() 
    {
      fRDO_B.Update();
      fRD_B.Update();
      fWR_B.Update();
      fRDO_A.Update();
      fRD_A.Update();
      fWR_A.Update();
    }
  };
}
#endif
//
// EOF
//

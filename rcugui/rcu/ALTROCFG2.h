// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTROCFG2_H
#define RCUGUI_ALTROCFG2_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/rcu/ZeroSuppression.h>
# include <rcuxx/rcu/RcuALTROCFG2.h>


namespace RcuGui
{
  //====================================================================
  /** @class ALTROCFG21
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class ALTROCFG2 : public Register
  {
  private:
    Rcuxx::RcuALTROCFG2&   fLow;
    LabeledIntEntry        fPreTrigger;
  public:
    ALTROCFG2(TGCompositeFrame& f, Rcuxx::RcuALTROCFG2& low) 
      : Register(f, low), fLow(low),
	fPreTrigger(fFields, "Pre-L1 samples", 0, 0xf, kVerticalFrame)
    {
      fGroup.SetTitle("ALTRO configuration 2");
      Get();
    }
    void Get() 
    {
      fPreTrigger.SetValue(fLow.PreSamples());
    }
    void Set() 
    {
      fLow.SetPreSamples(fPreTrigger.GetValue());
    }
  };
}
#endif
//
// EOF
//

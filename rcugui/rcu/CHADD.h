// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_CHADD_H
#define RCUGUI_CHADD_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuCHADD.h>


namespace RcuGui
{
  //====================================================================
  /** @class CHADD
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class CHADD : public Register
  {
  private:
    Rcuxx::RcuCHADD&  fLow;
    LabeledIntEntry fAddress1;
    LabeledIntEntry fAddress2;
  public:
    CHADD(TGCompositeFrame& f, Rcuxx::RcuCHADD& low) 
      : Register(f, low), fLow(low),
	fAddress1(fFields, "Address1", 0, 0xffff),
	fAddress2(fFields, "Address2", 0, 0xffff)
    {
      fGroup.SetTitle("Channel address");
      Get();
    }
    void Get() 
    {
      fAddress1.SetValue(fLow.Address1());
      fAddress2.SetValue(fLow.Address2());
    }
  };
}
#endif
//
// EOF
//

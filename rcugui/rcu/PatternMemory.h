// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_PATTERNMEMORY_H
#define RCUGUI_PATTERNMEMORY_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/rcu/PedestalConfig.h>
# include <rcugui/rcu/PedestalConfig.h>
# include <rcugui/rcu/Memory.h>
# include <rcuxx/rcu/RcuPMEM.h>
# include <rcuxx/rcu/RcuPMCFG.h>
# include <rcuxx/rcu/RcuIMEM.h>
# include <rcuxx/rcu/RcuERRST.h>

namespace RcuGui
{
  //==================================================================
  /** @class PatternMemory
      @ingroup rcugui_low
      @brief specialisation for Pedestal memory.  Adds a button to fill 
      with and incremental pattern. */
  class PatternMemory : public Memory 
  {
  public:
    /** Type of pattern */
    enum { 
      /** Incremental */
      kInc, 
      /** Fixed */ 
      kFixed
    };
    /** Constructor 
	@param tabs Where to add the tab to.   
	@param maxZeros Maximum number of zeros to show
	@param low Pointer to low-level driver 
	@param pmcfg Pattern memory block config 
	@param imem Instruction memory 
	@param errst Error and status register */
    PatternMemory(TGTab& tabs, Rcuxx::RcuMemory& low, 
		  Rcuxx::RcuPMCFG& pmcfg,
		  Rcuxx::RcuIMEM&  imem, 
		  Rcuxx::RcuERRST& errst, 
		  Int_t maxZeros=3);
    /** Fill with an incremental pattern */ 
    void Fill();
    /** Handle the mode */
    void HandleMode(Int_t);
  protected:
    /** Configuration */
    PedestalConfig      fConfig;
    /** Filling */
    TGTextButton        fFill;
    /** Filling */
    TGLayoutHints       fModeHints;
    /** Filling */
    TGButtonGroup       fMode;
    /** Filling */
    TGRadioButton       fIncremental;
    /** Filling */
    TGRadioButton       fFixed;
    /** Filling */
    LabeledIntEntry     fMinValue;
    /** Filling */
    LabeledIntEntry     fMaxValue;
  };
}

//============================================================
inline
RcuGui::PatternMemory::PatternMemory(TGTab&            tabs,		       
				     Rcuxx::RcuMemory& low,
				     Rcuxx::RcuPMCFG&  pmcfg,
				     Rcuxx::RcuIMEM&   imem, 
				     Rcuxx::RcuERRST&  errst,
				     Int_t             maxZeros) 
  : Memory(tabs, low, maxZeros, kHorizontalFrame, ISCALE(6), ISCALE(130)),
    fConfig(*fTop,pmcfg,imem,errst),
    fFill(&fButtons, "Fill", 4), 
    fModeHints(kLHintsExpandX),
    fMode(&fButtons),
    fIncremental(&fMode, "Incremental", kInc),
    fFixed(&fMode, "Fixed",       kFixed),
    fMinValue(fMode, "Min value", 0, 1023, kVerticalFrame),
    fMaxValue(fMode, "Max value", 0, 1023, kVerticalFrame)
{
  fTop->SetLayoutManager(new TGVerticalLayout(fTop));
  fFill.Connect("Clicked()", "RcuGui::PatternMemory", this, "Fill()"); 
  fButtons.AddFrame(&fMode, &fModeHints);
  fMode.Connect("Clicked(Int_t)" ,"RcuGui::PatternMemory", this, 
		"HandleMode(Int_t)");
  fMaxValue.SetValue(1023);
  fMode.SetExclusive(kFALSE);
  fIncremental.SetDown();
  // fView.Resize(fView.GetWidth(), fView.GetHeight() - 150);
  // fButtons.Resize(fButtons.GetWidth(), fButtons.GetHeight() - 150);
  // fCont.Resize(fCont.GetWidth(), fCont.GetHeight() - 140);
  // HandleMode(kInc);
} 

//____________________________________________________________________
inline void 
RcuGui::PatternMemory::HandleMode(Int_t mode) 
{
  fMaxValue.SetEnabled(mode == kInc);
}

//____________________________________________________________________
inline void 
RcuGui::PatternMemory::Fill() 
{
  std::vector<unsigned int> data(fLow.Size());
  if (fIncremental.IsDown()) {
    unsigned int min = fMinValue.GetValue();
    unsigned int max = fMaxValue.GetValue();
    unsigned int dv  = std::max(unsigned((max - min) / data.size()),1u);
    data[0] = min;
    for (size_t i = 1; i < data.size(); i++) { 
      if (data[i-1] + dv > 1023) data[i] = min;
      else                       data[i] = data[i-1] + dv;
    }
  }
  else {
    for (size_t i = 0; i < data.size(); i++) data[i] = fMinValue.GetValue();
  }
  fView.WriteText(&(data[0]));
}
#endif
//
// EOF
//

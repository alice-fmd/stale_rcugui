// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_RESREG_H
#define RCUGUI_RESREG_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuRESREG.h>


namespace RcuGui
{
  //====================================================================
  /** @class RESREG
      @brief Interface to the RESult REGister 
      @ingroup rcugui
  */
  class RESREG : public Register
  {
  private:
    Rcuxx::RcuRESREG&  fLow;
    LabeledIntView fAddress;
    LabeledIntView fResult;
  public:
    RESREG(TGCompositeFrame& f, Rcuxx::RcuRESREG& low) 
      : Register(f, low), fLow(low),
	fAddress(fFields,"Address", 0, 0x1f),
	fResult(fFields,"Result", 0, 0xffff)
    {
      fGroup.SetTitle("Monitor result register");
      Get();
    }
    void Get() 
    {
      fAddress.SetValue(fLow.Address());
      fResult.SetValue(fLow.Result());
    }
    void Set()
    {
      // fLow.SetAddress(fAddress.GetValue());
      // fLow.SetResult(fResult.GetValue());
    }
  };

}
#endif
//
// EOF
//

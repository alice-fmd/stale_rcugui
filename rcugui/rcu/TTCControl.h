// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_TTCControl_H
#define RCUGUI_TTCControl_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuTTCControl.h>


namespace RcuGui
{
  //__________________________________________________________________
  /** @class TTCControl
      @brief Interface to the ERRor and STatus register
      @ingroup rcugui
  */
  class TTCControl : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    TTCControl(TGCompositeFrame& f, Rcuxx::RcuTTCControl& low) 
      : Register(f, low), 
	fLow(low),
	fHints(kLHintsExpandX, 3, 3, 0, 0),
	fConfig(&fFields, "Configuration", kVerticalFrame),
        fSerialB(&fConfig, "SerialB"),
        fDisableError(&fConfig, "Disable Error checking"),
        fEnableRoi(&fConfig, "Enable Region-of-Interest"),
        fL0Support(&fConfig, "L0 Support"),
        fL2aFIFO(&fConfig, "L2 accept FIFO"),
        fL2rFIFO(&fConfig, "L2 reject FIFO"),
        fL2tFIFO(&fConfig, "L2 t FIFO"),
        fL1MsgMask(&fConfig, "L1 Message Mask"),
        fTrgMask(&fConfig, "Trigger mask"),
	fInfo(&fFields, "Information", kVerticalFrame),
	fBCOverflow(fInfo, "BCOverlow", "Bunch-crossing counter overflow", 
		    0xdd0000, 0xc0c0c0),
	fRunActive(fInfo, "RunActive", "Run is active", 0x00dd00, 0xc0c0c0),
	fBusy(fInfo, "Busy", "TTC interface busy", 0xeeee00, 0xc0c0c0),
	fCDHVersion(fInfo, "CDH Version", 0x0, 0xF)
    {
      fGroup.SetTitle("TTC control");
      fFields.AddFrame(&fConfig,        &fHints);
      fFields.AddFrame(&fInfo,          &fHints);
      fConfig.AddFrame(&fSerialB,       &fHints);
      fConfig.AddFrame(&fDisableError,  &fHints);
      fConfig.AddFrame(&fEnableRoi,     &fHints);
      fConfig.AddFrame(&fL0Support,     &fHints);
      fConfig.AddFrame(&fL2aFIFO,       &fHints);
      fConfig.AddFrame(&fL2rFIFO,       &fHints);
      fConfig.AddFrame(&fL2tFIFO,       &fHints);
      fConfig.AddFrame(&fL1MsgMask,     &fHints);
      fConfig.AddFrame(&fTrgMask,       &fHints);
      Get();
    }
    void Get() 
    {
      fSerialB.SetDown(fLow.SerialB());
      fDisableError.SetDown(fLow.DisableError());
      fEnableRoi.SetDown(fLow.EnableRoi());
      fL0Support.SetDown(fLow.L0Support());
      fL2aFIFO.SetDown(fLow.L2aFIFO());
      fL2rFIFO.SetDown(fLow.L2rFIFO());
      fL2tFIFO.SetDown(fLow.L2tFIFO());
      fL1MsgMask.SetDown(fLow.L1MsgMask());
      fTrgMask.SetDown(fLow.TrgMask());
      fBCOverflow.SetState(fLow.BCOverflow());
      fRunActive.SetState(fLow.RunActive());
      fBusy.SetState(fLow.Busy());
      fCDHVersion.SetValue(fLow.CDHVersion());
    }
    void Set() 
    {
      fLow.SetSerialB(fSerialB.IsDown());
      fLow.SetDisableError(fDisableError.IsDown());
      fLow.SetEnableRoi(fEnableRoi.IsDown());
      fLow.SetL0Support(fL0Support.IsDown());
      fLow.SetL2aFIFO(fL2aFIFO.IsDown());
      fLow.SetL2rFIFO(fL2rFIFO.IsDown());
      fLow.SetL2tFIFO(fL2tFIFO.IsDown());
      fLow.SetL1MsgMask(fL1MsgMask.IsDown());
      fLow.SetTrgMask(fTrgMask.IsDown());
    }
  private:
    Rcuxx::RcuTTCControl& fLow;
    TGLayoutHints fHints;
    /** Group */ 
    TGGroupFrame fConfig;
    /** SerialB */
    TGCheckButton fSerialB;
    /** DisableError */
    TGCheckButton fDisableError;
    /** EnableRoi */
    TGCheckButton fEnableRoi;
    /** L0Support */
    TGCheckButton fL0Support;
    /** L2aFIFO */
    TGCheckButton fL2aFIFO;
    /** L2rFIFO */
    TGCheckButton fL2rFIFO;
    /** L2tFIFO */
    TGCheckButton fL2tFIFO;
    /** L1MsgMask */
    TGCheckButton fL1MsgMask;
    /** Trigger mask */
    TGCheckButton fTrgMask;
    /** Group */ 
    TGGroupFrame fInfo;
    /** BCOverflow */
    ErrorBit fBCOverflow;
    /** RunActive */
    ErrorBit fRunActive;
    /** Busy */
    ErrorBit fBusy;
    /** Common data header version */
    LabeledIntView fCDHVersion;
  };
}
#endif
//
// EOF
//

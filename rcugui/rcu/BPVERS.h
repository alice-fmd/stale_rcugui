// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BPVERS_H
#define RCUGUI_BPVERS_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuBPVERS.h>


namespace RcuGui
{
  //====================================================================
  /** @class BPVERS
      @brief Interface to the INTerrupt MODe
      @ingroup rcugui
  */
  class BPVERS : public Register
  {
  private:
    Rcuxx::RcuBPVERS&  fLow;
    TGLayoutHints fTips;
    TGCheckButton fPHOS;
  public:
    BPVERS(TGCompositeFrame& f, Rcuxx::RcuBPVERS& low) 
      : Register(f, low), 
	fLow(low),
	fTips(kLHintsExpandX, ISCALE(2), ISCALE(2)),
	fPHOS(&fFields, "PHOS version")
    {
      fGroup.SetTitle("Back-plane version");
      fPHOS.SetToolTipText("Enable for PHOS backplane");
      fFields.AddFrame(&fPHOS, &fTips);
    }
    void Get() 
    {
      fPHOS.SetDown(fLow.IsPHOS());
    }
    void Set() 
    {
      fLow.SetPHOS(fPHOS.IsDown());
    }
  };
}
#endif
//
// EOF
//

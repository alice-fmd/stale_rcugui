// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_Counters_H
#define RCUGUI_Counters_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuCounter.h>
# include <map>
# include <climits>

namespace RcuGui
{
  struct CounterContainer : public Rcuxx::Register
  {
    CounterContainer(Rcuxx::RcuCounter** counters)
      : Rcuxx::Register("Counters", "Counters", false)
    {
      Rcuxx::RcuCounter** p = counters;
      while (*p) { 
	fCounters.push_back(*p);
	p++;
      }
    }
    virtual bool IsClearable() const { return true; }
    unsigned int Update() { 
      unsigned int ret = 0;
      for (CounterList::iterator i = fCounters.begin(); 
	   i != fCounters.end(); ++i) {
	int ret2 = (*i)->Update();
	if (ret == 0 && ret2 != 0) ret = ret2;
	sleep(1);
      }
      return ret;
    }
    unsigned int Clear() { 
      unsigned int ret = 0;
      for (CounterList::iterator i = fCounters.begin(); 
	   i != fCounters.end(); ++i) 
	if ((ret = (*i)->Clear())) break;
      return ret;
    }
    void Print() const { 
      std::cout << "Counters:" << std::endl;
      for (CounterList::const_iterator i = fCounters.begin(); 
	   i != fCounters.end(); ++i) 
	std::cout << "\t\t" << (*i)->Name() << ":\t\t" << (*i)->Counts()
		  << std::endl;
    }
    typedef std::vector<Rcuxx::RcuCounter*> CounterList;
    CounterList fCounters;
  };
  
  //====================================================================
  /** @class Counters
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class Counters : public Register
  {
  public:
    Counters(TGCompositeFrame& f, 
	     CounterContainer* c, 
	     const char** names) 
      : Register(f, *c), 
	fFrame(&fFields),
	fContainer(c),
	fViews(0)
    {
      fFields.AddFrame(&fFrame, 
		       new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
      Setup(names);
    }
    Counters(TGCompositeFrame& f, 
	     Rcuxx::RcuCounter** counters, 
 	     const char**        names)
      : Register(f, *(fContainer = new CounterContainer(counters))), 
	fFrame(&fFields), 
	fViews(0)
    {
      fFields.AddFrame(&fFrame, 
		       new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
      Setup(names);
    }
    void Setup(const char** names) { 
      size_t j = 0;
      for (CounterContainer::CounterList::iterator i = 
	     fContainer->fCounters.begin();
	   i != fContainer->fCounters.end(); ++i, ++j) { 
	LabeledIntView* v = new LabeledIntView(fFrame, 
					       (names && names[j] ? names[j] : 
						(*i)->Name().c_str()), 
					       0x0, LONG_MAX);
	v->SetHexFormat(kFALSE);
	fViews.push_back(v);
      }
      Get();
    }
    void Get() 
    {
      size_t j = 0;
      for (CounterContainer::CounterList::iterator i = 
	     fContainer->fCounters.begin();
	   i != fContainer->fCounters.end(); ++i, ++j) { 
	LabeledIntView* v = fViews[j];
	v->SetValue((*i)->Counts());
      }
    }
  private:
    typedef std::vector<LabeledIntView*>  ViewList;
    TGVerticalFrame fFrame;
    ViewList fViews;
    CounterContainer* fContainer;
  };
}
#endif
//
// EOF
//

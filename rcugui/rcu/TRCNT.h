// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_TRCNT_H
#define RCUGUI_TRCNT_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/rcu/Memory.h>
# include <rcuxx/rcu/RcuTRCNT.h>

namespace RcuGui
{
  //====================================================================
  /** @class TRCNT 
      @brief Display of the TRigger CouNTers
      @ingroup rcugui 
  */
  class TRCNT : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    TRCNT(TGCompositeFrame& f, Rcuxx::RcuTRCNT& low) 
      : RcuGui::Register(f, low), 
	fLow(low),
	fReceived(fFields, "Received", 0,0xffff, kHorizontalFrame),
	fAccepted(fFields, "Accepted",0,0xffff, kHorizontalFrame)
    {
      fGroup.SetTitle("Trigger counters");
      fAccepted.SetToolTipText("Accepted number of L1 triggers");
      fAccepted.SetToolTipText("Received number of L1 triggers");
      Get();
    }
    void Get()
    {
      fAccepted.SetValue(fLow.Accepted());
      fReceived.SetValue(fLow.Received());
    }
  private:
    Rcuxx::RcuTRCNT&   fLow;
    RcuGui::LabeledIntView fReceived;
    RcuGui::LabeledIntView fAccepted;
  };
}
#endif
//
// EOF
//

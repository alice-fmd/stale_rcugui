// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ROIConfig_H
#define RCUGUI_ROIConfig_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuROIConfig1.h>
# include <rcuxx/rcu/RcuROIConfig2.h>


namespace RcuGui
{
  //====================================================================

  /** @class ROIConfig1
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class ROIConfig : public Register
  {
  private:
    Rcuxx::RcuROIConfig1&   fLow1;
    Rcuxx::RcuROIConfig2&   fLow2;
    LabeledIntEntry         fRegion1;
    LabeledIntEntry         fRegion2;
  public:
    ROIConfig(TGCompositeFrame& f, 
	       Rcuxx::RcuROIConfig1& low1,
	       Rcuxx::RcuROIConfig2& low2) 
      : Register(f, low1), 
	fLow1(low1),
	fLow2(low2),
	fRegion1(fFields, "Region", 0, 0xffffffff, kHorizontalFrame), 
	fRegion2(fFields, "Region", 0, 0xffffffff, kHorizontalFrame)
    {
      fGroup.SetTitle("Region of Interest");
      Get();
      // fRegion.SetHexFormat(kFALSE);
    }
    void Get() 
    {
      fRegion1.SetValue(fLow1.Region());
      fRegion2.SetValue(fLow2.Region());
    }
    void Set() 
    {
      fLow1.SetRegion(fRegion1.GetValue());
      fLow2.SetRegion(fRegion2.GetValue());
    }
    Bool_t Update() 
    {
      unsigned int ret = fLow2.Update();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      return Register::Update();
    }
    Bool_t Commit() 
    {
      Set();
      unsigned int ret = fLow2.Commit();
      if (ret) { 
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      return Register::Commit();
    }

  };
}
#endif
//
// EOF
//

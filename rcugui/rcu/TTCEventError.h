// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_TTCEventError_H
#define RCUGUI_TTCEventError_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuTTCEventError.h>


namespace RcuGui
{
  //====================================================================
  /** @class TTCEventError
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class TTCEventError : public Register
  {
  public:
    TTCEventError(TGCompositeFrame& f, Rcuxx::RcuTTCEventError& low) 
      : Register(f, low), 
	fLow(low),
	fRowHints(kLHintsExpandX, 0, 0, 0, 0),
	fColumn(&fFields),
	fRow1(&fColumn),
	fSerialBStop(fRow1, "SerialBStop", "Serial B stop bit error",
		     0xee0000, 0xc0c0c0),
	f1BitAddress(fRow1, "1BitAddress","Single bit hamming address error",
		     0xee0000, 0xc0c0c0),
	f2BitAddress(fRow1, "2BitAddress","Double bit hamming address error",
		     0xee0000, 0xc0c0c0),
	f1BitBroadcast(fRow1, "1BitBroadcast",
		       "Single bit hamming broadcast error",
		       0xee0000, 0xc0c0c0),
	f2BitBroadcast(fRow1, "2BitBroadcast", 
		       "Double bit hamming broadcast error", 
		       0xee0000, 0xc0c0c0),
	fUnknownAddress(fRow1, "UnknownAddress", "Unknown message address",
			0xee0000, 0xc0c0c0),
	fL1MsgIncomplete(fRow1, "L1MsgIncomplete", "Incomplete L1 message",
			 0xee0000, 0xc0c0c0),
	fRow2(&fColumn),
	fL2aMsgIncomplete(fRow2, "L2aMsgIncomplete", "Incomplete L2a message",
			  0xee0000, 0xc0c0c0),
	fROIMsgIncomplete(fRow2, "ROIMsgIncomplete", "Incomplete ROI message",
			  0xee0000, 0xc0c0c0),
	fTTCAddress(fRow2, "TTCAddress", "TTCrx address",
		    0xee0000, 0xc0c0c0),
	fSporiusL0(fRow2, "SporiusL0", "Spurious L0",
		   0xee0000, 0xc0c0c0),
	fMissingL0(fRow2, "MissingL0", "Missing L0",
		   0xee0000, 0xc0c0c0),
	fSporiusL1(fRow2, "SporiusL1", "Spurious L1",
		   0xee0000, 0xc0c0c0),
	fBoundaryL1(fRow2, "BoundaryL1", "Boundary L1",
		    0xee0000, 0xc0c0c0),
	fRow3(&fColumn),
	fMissingL1(fRow3, "MissingL1", "Missing L1",
		   0xee0000, 0xc0c0c0),
	fL1MsgOut(fRow3, "L1MsgOut", "Late/early L1Msg",
		  0xee0000, 0xc0c0c0),
	fL1MsgMissing(fRow3, "L1MsgMissing", "Missing L1Msg",
		      0xee0000, 0xc0c0c0),
	fL2MsgOut(fRow3, "L2MsgOut", "Late/early L2Msg",
		  0xee0000, 0xc0c0c0),
	fL2MsgMissing(fRow3, "L2MsgMissing", "Missing L2Msg",
		      0xee0000, 0xc0c0c0),
	fROIMsgOut(fRow3, "ROIMsgOut", "Late/early ROIMsg",
		   0xee0000, 0xc0c0c0),
	fRow4(&fColumn),
	fROIMsgMissing(fRow4, "ROIMsgMissing", "Missing ROIMsg",
		       0xee0000, 0xc0c0c0),
	fPrePulse(fRow4, "PrePulse", "Prepulse error",
		  0xee0000, 0xc0c0c0),
	fL1MsgContent(fRow4, "L1MsgContent", "L1Msg content error",
		      0xee0000, 0xc0c0c0),
	fL2MsgContent(fRow4, "L2MsgContent", "L2Msg content error",
		      0xee0000, 0xc0c0c0),
	fROIMsgContent(fRow4, "ROIMsgContent", "ROIMsg content error", 
		       0xee0000, 0xc0c0c0)
    {
      fFields.AddFrame(&fColumn, &fRowHints);
      fColumn.AddFrame(&fRow1, &fRowHints);
      fColumn.AddFrame(&fRow2, &fRowHints);
      fColumn.AddFrame(&fRow3, &fRowHints);
      fColumn.AddFrame(&fRow4, &fRowHints);
      fGroup.SetTitle("Event errors");
      Get();
    }
    void Get() 
    {
      fSerialBStop.SetState(fLow.SerialBStop());	
      f1BitAddress.SetState(fLow.SingleBitAddress());	
      f2BitAddress.SetState(fLow.DoubleBitAddress());	
      f1BitBroadcast.SetState(fLow.SingleBitBroadcast());	
      f2BitBroadcast.SetState(fLow.DoubleBitBroadcast());	
      fUnknownAddress.SetState(fLow.UnknownAddress());	
      fL1MsgIncomplete.SetState(fLow.L1MsgIncomplete());	
      fL2aMsgIncomplete.SetState(fLow.L2aMsgIncomplete());	
      fROIMsgIncomplete.SetState(fLow.ROIMsgIncomplete());	
      fTTCAddress.SetState(fLow.TTCAddress());	
      fSporiusL0.SetState(fLow.SporiusL0());	
      fMissingL0.SetState(fLow.MissingL0());	
      fSporiusL1.SetState(fLow.SporiusL1());	
      fBoundaryL1.SetState(fLow.BoundaryL1());	
      fMissingL1.SetState(fLow.MissingL1());	
      fL1MsgOut.SetState(fLow.L1MsgOut());	
      fL1MsgMissing.SetState(fLow.L1MsgMissing());	
      fL2MsgOut.SetState(fLow.L2MsgOut());	
      fL2MsgMissing.SetState(fLow.L2MsgMissing());	
      fROIMsgOut.SetState(fLow.ROIMsgOut());	
      fROIMsgMissing.SetState(fLow.ROIMsgMissing());	
      fPrePulse.SetState(fLow.PrePulse());	
      fL1MsgContent.SetState(fLow.L1MsgContent());	
      fL2MsgContent.SetState(fLow.L2MsgContent());	
      fROIMsgContent.SetState(fLow.ROIMsgContent());	
    }
  private:
    Rcuxx::RcuTTCEventError&  fLow;
    TGLayoutHints fRowHints;
    TGVerticalFrame   fColumn;
    TGHorizontalFrame fRow1;
    TGHorizontalFrame fRow2;
    TGHorizontalFrame fRow3;
    TGHorizontalFrame fRow4;
    /** Serial B stop bit error */
    ErrorBit fSerialBStop;
    /** Single bit hamming address error */
    ErrorBit f1BitAddress;
    /** Double bit hamming address error */
    ErrorBit f2BitAddress;
    /** Single bit hamming broadcast error */
    ErrorBit f1BitBroadcast;
    /** Double bit hamming broadcast error */
    ErrorBit f2BitBroadcast;
    /** Unknown message address */
    ErrorBit fUnknownAddress;
    /** Incomplete L1 message */
    ErrorBit fL1MsgIncomplete;
    /** Incomplete L2a message */
    ErrorBit fL2aMsgIncomplete;
    /** Incomplete ROI message */
    ErrorBit fROIMsgIncomplete;
    /** TTCrx address */
    ErrorBit fTTCAddress;
    /** Spurious L0 */
    ErrorBit fSporiusL0;
    /** Missing L0 */
    ErrorBit fMissingL0;
    /** Spurious L1 */
    ErrorBit fSporiusL1;
    /** Boundary L1 */
    ErrorBit fBoundaryL1;
    /** Missing L1 */
    ErrorBit fMissingL1;
    /** Late/early L1Msg */
    ErrorBit fL1MsgOut;
    /** Missing L1Msg */
    ErrorBit fL1MsgMissing;
    /** Late/early L2Msg */
    ErrorBit fL2MsgOut;
    /** Missing L2Msg */
    ErrorBit fL2MsgMissing;
    /** Late/early ROIMsg */
    ErrorBit fROIMsgOut;
    /** Missing ROIMsg */
    ErrorBit fROIMsgMissing;
    /** Prepulse error */
    ErrorBit fPrePulse;
    /** L1Msg content error */
    ErrorBit fL1MsgContent;
    /** L2Msg content error */
    ErrorBit fL2MsgContent;
    /** ROIMsg content error */
    ErrorBit fROIMsgContent;
  };
}
#endif
//
// EOF
//

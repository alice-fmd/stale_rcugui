// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_FMIREG_H
#define RCUGUI_FMIREG_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuFMIREG.h>
# include <rcuxx/rcu/RcuFMOREG.h>

namespace RcuGui
{
  //====================================================================
  /** @class FMIREG
      @brief Interface to the ACTive FrontEnd Card  
      @ingroup rcugui
  */
  class FMIREG : public Register
  {
  public:
    FMIREG(TGCompositeFrame& f, 
	   Rcuxx::RcuFMIREG& lowI, 
	   Rcuxx::RcuFMOREG& lowO) 
      : Register(f, lowI), fLowI(lowI), fLowO(lowO),
	fInput(fFields,"Input", 0, 0xff),
	fOutput(fFields,"Output", 0, 0xff)
    {
      fGroup.SetTitle("Firmware input/output");
      Get();
    }
    Bool_t Update() 
    {
      unsigned int ret = fLowO.Update();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      return Register::Update();
    }
    void Get() 
    {
      fInput.SetValue(fLowI.Data());
      fOutput.SetValue(fLowO.Data());
    }
    void Set()
    {
      fLowI.SetData(fInput.GetValue());
      fLowO.SetData(fOutput.GetValue());
    }
    Bool_t Commit() 
    {
      Set();
      fLowO.Commit();
      return Register::Commit();
    }
  private:
    Rcuxx::RcuFMIREG& fLowI;
    Rcuxx::RcuFMOREG& fLowO;
    LabeledIntEntry fInput;
    LabeledIntEntry fOutput;
  };
}
#endif
//
// EOF
//

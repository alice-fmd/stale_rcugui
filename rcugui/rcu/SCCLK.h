// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_SCCLK_H
#define RCUGUI_SCCLK_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
// # include <rcugui/LabeledNumber.h>
# include <rcuxx/rcu/RcuSCCLK.h>


namespace RcuGui
{
  //====================================================================
  /** @class RESREG
      @brief Interface to the RESult REGister 
      @ingroup rcugui
  */
  class SCCLK : public Register
  {
  private:
    Rcuxx::RcuSCCLK&  fLow;
    TGHButtonGroup    fChoices;
    TGRadioButton     f5000kHz;
    TGRadioButton     f2500kHz;
    TGRadioButton     f1250kHz;
    TGRadioButton     f0625kHz;
    
  public:
    SCCLK(TGCompositeFrame& f, Rcuxx::RcuSCCLK& low) 
      : Register(f, low), fLow(low),
	fChoices(&fFields, "Clock speed"),
	f5000kHz(&fChoices, "5MHz"),
	f2500kHz(&fChoices, "2.5MHz"),
	f1250kHz(&fChoices, "1.25MHz"),
	f0625kHz(&fChoices, "625kHz")
    {
      fGroup.SetTitle("I2C clock speed");
      fFields.AddFrame(&fChoices,
		       new TGLayoutHints(kLHintsExpandX, 3, 3, 0, 0));
      Get();
    }
    void Get() 
    {
      Rcuxx::RcuSCCLK::Speed_t speed = fLow.Speed();
      switch (speed) { 
      case Rcuxx::RcuSCCLK::k5MHz:     f5000kHz.SetState(kButtonDown); break;
      case Rcuxx::RcuSCCLK::k2500kHz:  f2500kHz.SetState(kButtonDown); break;
      case Rcuxx::RcuSCCLK::k1250kHz:  f1250kHz.SetState(kButtonDown); break;
      case Rcuxx::RcuSCCLK::k625kHz:   f0625kHz.SetState(kButtonDown); break;
      default:                         f5000kHz.SetState(kButtonDown); break;
      }
    }
    void Set()
    {
      if      (f5000kHz.IsDown()) fLow.SetSpeed(Rcuxx::RcuSCCLK::k5MHz);
      else if (f2500kHz.IsDown()) fLow.SetSpeed(Rcuxx::RcuSCCLK::k2500kHz);
      else if (f1250kHz.IsDown()) fLow.SetSpeed(Rcuxx::RcuSCCLK::k1250kHz);
      else if (f0625kHz.IsDown()) fLow.SetSpeed(Rcuxx::RcuSCCLK::k625kHz);
      else                        fLow.SetSpeed(Rcuxx::RcuSCCLK::k5MHz);
    }
  };

}
#endif
//
// EOF
//

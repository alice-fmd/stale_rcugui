// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_L2Timeout_H
#define RCUGUI_L2Timeout_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuL2Timeout.h>


namespace RcuGui
{
  //====================================================================

  /** @class L2Timeout1
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class L2Timeout : public Register
  {
  private:
    Rcuxx::RcuL2Timeout&   fLow;
    LabeledIntEntry        fMin;
    LabeledIntEntry        fMax;
  public:
    L2Timeout(TGCompositeFrame& f, Rcuxx::RcuL2Timeout& low) 
      : Register(f, low), fLow(low),
	fMin(fFields, "Min", 0, 0xffff, kHorizontalFrame), 
	fMax(fFields, "Max", 0, 0xffff, kHorizontalFrame)
    {
      fMin.SetHexFormat(kFALSE);
      fMax.SetHexFormat(kFALSE);
      Get(); 
   }
    void Get() 
    {
      fMin.SetValue(fLow.Min());
      fMax.SetValue(fLow.Max());
    }
    void Set() 
    {
      fLow.SetMin(fMin.GetValue());
      fLow.SetMax(fMax.GetValue());
    }
  };
}
#endif
//
// EOF
//

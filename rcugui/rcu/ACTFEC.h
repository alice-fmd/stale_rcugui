// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ACTFEC_H
#define RCUGUI_ACTFEC_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/rcu/ActiveBit.h>
# include <rcugui/rcu/Memory.h>
# include <rcuxx/rcu/RcuACTFEC.h>
# include <rcugui/rcu/ActiveChannels.h>

namespace RcuGui
{
  
  //====================================================================
  /** @class ACTFEC 
      @brief Interface to the ACTive FrontEnd Card  
      @ingroup rcugui
  */
  class ACTFEC : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    ACTFEC(TGCompositeFrame& f, Rcuxx::RcuACTFEC& low, ActiveChannels& acl, 
	   UInt_t maxFEC, const UInt_t* mask) 
      : RcuGui::Register(f, low), 
	fLow(low),
	fACL(acl),
	fLH(kLHintsExpandX,0,0,0,0),
	fOptHints(kLHintsExpandX,0,ISCALE(3),0,0),
	fButHints(kLHintsNormal,ISCALE(3),ISCALE(3), 0, 0),
	fBranches(&fFields, 1, 1, kHorizontalFrame),
	fAFrame(&fBranches, "Branch A", kHorizontalFrame),
	fBFrame(&fBranches, "Branch B", kHorizontalFrame),
	fOpt(&fFields, "Options", kHorizontalFrame),
	fToMatch(&fOpt, "Match ACL"),
	fAll(&fOpt, "All"),
	fNone(&fOpt, "None")
    {
      for (size_t i = 0; i < 8; i++) fMask[i] = mask[i];
      fFields.SetLayoutManager(new TGVerticalLayout(&fFields));
      fGroup.SetTitle("Active Front-End Cards");
      fFields.AddFrame(&fBranches, &fOptHints);

      // Branch A
      fBranches.AddFrame(&fAFrame, &fLH);
      for (size_t i = 0; i < 16; i++) {
	fCheck[i] = new RcuGui::ActiveBit(fAFrame, 'A', i, fACL, 
					  fMask, kTRUE);
	fCheck[i]->SetActive((maxFEC & (1 << i)) > 0);
      }
      

      // Branch B
      fBranches.AddFrame(&fBFrame, &fLH);
      for (size_t i = 16; i < 32; i++) {
	fCheck[i] = new RcuGui::ActiveBit(fBFrame, 'B', i-16, fACL, 
					  fMask, kTRUE);
	fCheck[i]->SetActive((maxFEC & (1 << i)) > 0);
      }

      // Options 
      fOpt.AddFrame(&fToMatch);
      fOpt.AddFrame(&fAll, &fButHints);
      fOpt.AddFrame(&fNone, &fButHints);
      fFields.AddFrame(&fOpt, &fOptHints);

      for (size_t i = 0; i < 32; i++) {
	fToMatch.Connect("Toggled(Bool_t)", "RcuGui::ActiveBit", fCheck[i],
			  "SetSync(Bool_t)");
	fAll.Connect("Clicked()", "RcuGui::ActiveBit", fCheck[i], "SetAll()");
	fNone.Connect("Clicked()", "RcuGui::ActiveBit", fCheck[i],"SetNone()");
      }
      fToMatch.SetDown(kFALSE);
      Get();
      fToMatch.SetDown(kTRUE);
    }
    void Get()
    {
      // Int_t val = fLow.Value();
      for (size_t i = 0; i < 32; i++) {
	fCheck[i]->SetState(fLow.IsOn(i));
	if (fToMatch.IsDown()) fCheck[i]->Get();
      }
    }
    void Set() 
    {
      // Int_t val = 0;
      for (size_t i = 0; i < 32; i++) 
	fLow.SetOn(i, fCheck[i]->fGood);
      if (!fToMatch.IsDown()) return;
      fACL.Commit();
    }
  private:
    /** Low-level */ 
    Rcuxx::RcuACTFEC&   fLow;
    ActiveChannels&     fACL;
    /** Layout hints */
    TGLayoutHints       fLH;  
    TGLayoutHints       fOptHints;  
    TGLayoutHints       fButHints;  

    TGCompositeFrame    fBranches;
    TGGroupFrame        fAFrame;
    TGGroupFrame        fBFrame;
    /** Check marks */ 
    ActiveBit*  	fCheck[32];
    TGGroupFrame        fOpt;
    TGCheckButton       fToMatch;
    TGTextButton        fAll;
    TGTextButton        fNone;
    unsigned int        fMask[8];
    /** The display of the value */
    // TGNumberEntry    fValue;
  };
}
#endif
//
// EOF
//

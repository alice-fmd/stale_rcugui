// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_EVMNGST_H
#define RCUGUI_EVMNGST_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/rcu/StateDisplay.h>
# include <rcuxx/rcu/RcuEVMNGST.h>
# include <climits>


namespace RcuGui
{
  //====================================================================
  /** @class EVMNGST
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class EVMNGST : public Register
  {
  private:
    Rcuxx::RcuEVMNGST&  fLow;
    StateDisplay fTST_TRG;
    StateDisplay fTTC_TRG;
    StateDisplay fEV;
    StateDisplay fINSSEQ;
  public:
    EVMNGST(TGCompositeFrame& f, Rcuxx::RcuEVMNGST& low) 
      : Register(f, low), fLow(low),
	fTST_TRG(fFields,fLow.TST_TRG()), 	
	fTTC_TRG(fFields,fLow.TTC_TRG()), 	
	fEV(fFields,fLow.EV()), 	
	fINSSEQ(fFields,fLow.INSSEQ())
    {
      // "Arbitrator FSM"
      Get();
    }
    void Get() 
    {
      fTST_TRG.Update();
      fTTC_TRG.Update();
      fEV.Update();
      fINSSEQ.Update();
    }
  };
}
#endif
//
// EOF
//

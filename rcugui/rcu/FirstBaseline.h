// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_FIRSTBASELINE_H
#define RCUGUI_FIRSTBASELINE_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>

namespace RcuGui
{
  struct FirstBaseline : public TGGroupFrame 
  {
  FirstBaseline(TGCompositeFrame& p, bool pol) 
    : TGGroupFrame(&p, "1st baseline", kVerticalFrame),
      fHints(kLHintsExpandX|kLHintsExpandY),
      fPolHints(kLHintsExpandX, 3, 0,10, 3),
      fPol(pol ? new TGCheckButton(this, "1's compliment", 0) : 0),
      fModeHints(kLHintsExpandX,3,0,3, 3),
      fMode(this, kVerticalFrame)
    {
      p.AddFrame(this, &fHints);
      if (fPol) AddFrame(fPol, &fPolHints);
      fMode.SetExclusive(kTRUE);
      AddFrame(&fMode, &fModeHints);
      const char* modes[] = { "din-fpd", 
			      "din-f(t)", 
			      "din-f(din)", 
			      "din-f(din-vpd)", 
			      "din-vpd-fpd", 
			      "din-vpd-f(t)", 
			      "din-vpd-f(din)", 
			      "din-vpd-f(din-vpd)", 
			      "f(din)-fpd", 
			      "f(din-vpd)-fpd", 
			      "f(t)-fpd", 
			      "f(t)-f(t)", 
			      "f(din)-f(din)", 
			      "f(din-vpd)-f(din-vpd)", 
			      "din-fpd", 
			      "din-fpd" };
      for (size_t i = 0; i < 16; i++)
	fSelect[i] = new TGRadioButton(&fMode, modes[i], i);
    }
    virtual ~FirstBaseline()
    {
    }
    
    void Get(unsigned short mode, bool pol)
    {
      fMode.SetButton(mode, kTRUE);
      if (fPol) fPol->SetState(pol ? kButtonDown : kButtonUp);
    }
    void Set(unsigned short& mode, bool& pol)
    {
      mode = 0;
      for (size_t i = 0; i < 16; i++) {
	if (fSelect[i]->IsDown()) { mode = i; break; }
      }
      if (fPol) pol = fPol->IsOn();
    }
  protected:
    TGLayoutHints      fHints;
    TGLayoutHints      fModeHints;
    TGRadioButton*     fSelect[16];
    TGLayoutHints      fPolHints;
    TGCheckButton*     fPol;
    TGButtonGroup      fMode;
  };
}
#endif
//
// EOF
//

    
      
      
      

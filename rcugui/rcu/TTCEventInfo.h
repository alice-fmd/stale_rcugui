// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_TTCEventInfo_H
#define RCUGUI_TTCEventInfo_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuTTCEventInfo.h>


namespace RcuGui
{
  //====================================================================
  /** @class TTCEventInfo
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class TTCEventInfo : public Register
  {
  public:
    TTCEventInfo(TGCompositeFrame& f, Rcuxx::RcuTTCEventInfo& low) 
      : Register(f, low), fLow(low),
	fRowHints(kLHintsExpandX, 0, 0, 0, 0),
	fColumn(&fFields),
	fRow1(&fColumn),
	fROIEnabled(fRow1, "ROIEnabled", "ROI enabled",
		    0x00ee00, 0xc0c0c0),
	fROIAnnounced(fRow1, "ROIAnnounced", "ROI announced",
		      0x00ee00, 0xc0c0c0),
	fROIRecieved(fRow1, "ROIRecieved", "ROI recieved",
		     0x00ee00, 0xc0c0c0),
	fWithinROI(fRow1, "WithinROI", "Within ROI",
		   0x00ee00, 0xc0c0c0),
	fSwTrg(fRow1, "SwTrg", "Software trigger",
	       0x00ee00, 0xc0c0c0),
	fCalTrg(fRow1, "CalTrg", "Calibration trigger",
		0x00ee00, 0xc0c0c0),
	fHasL2R(fRow1, "HasL2R", "Has L2 reject trigger",
		0x00ee00, 0xc0c0c0),
	fHasL2A(fRow1, "HasL2A", "Has L2 accept trigger",
		0x00ee00, 0xc0c0c0),
	fHasPayload(fRow2, "HasPayload", "Has payload",
		    0x00ee00, 0xc0c0c0),
	fRow2(&fColumn),
	fSwCalTrgType(fRow2, "SW/Cal trigger type", 0, 0xffffffff),
	fSCLKPhase(fRow2, "SCLK phase", 0, 0xffffffff)
    {
      fGroup.SetTitle("Event information");
      fFields.AddFrame(&fColumn, &fRowHints);
      fColumn.AddFrame(&fRow1, &fRowHints);
      fColumn.AddFrame(&fRow2, &fRowHints);
      Get();
    }
    void Get() 
    {
      fROIEnabled.SetState(fLow.ROIEnabled());
      fROIAnnounced.SetState(fLow.ROIAnnounced());
      fROIRecieved.SetState(fLow.ROIRecieved());
      fWithinROI.SetState(fLow.WithinROI());
      fSwCalTrgType.SetValue(fLow.SwCalTrgType());
      fSwTrg.SetState(fLow.SwTrg());
      fCalTrg.SetState(fLow.CalTrg());
      fHasL2R.SetState(fLow.HasL2R());
      fHasL2A.SetState(fLow.HasL2A());
      fHasPayload.SetState(fLow.HasPayload());
      fSCLKPhase.SetValue(fLow.SCLKPhase());
    }
  private:
    Rcuxx::RcuTTCEventInfo&  fLow;
    TGLayoutHints fRowHints;
    TGVerticalFrame   fColumn;
    TGHorizontalFrame fRow1;
    TGHorizontalFrame fRow2;
    /** ROI enabled */
    ErrorBit fROIEnabled;
    /** ROI announced */
    ErrorBit fROIAnnounced;
    /** ROI recieved */
    ErrorBit fROIRecieved;
    /** Within ROI */
    ErrorBit fWithinROI;
    /** Calibration/software trigger type */
    LabeledIntView fSwCalTrgType;
    /** Software trigger */
    ErrorBit fSwTrg;
    /** Calibration trigger */
    ErrorBit fCalTrg;
    /** Has L2 reject trigger */
    ErrorBit fHasL2R;
    /** Has L2 accept trigger */
    ErrorBit fHasL2A;
    /** Has payload */
    ErrorBit fHasPayload;
    /** Slow clock phase at L0/L1 */
    LabeledIntView fSCLKPhase;
  };
}
#endif
//
// EOF
//

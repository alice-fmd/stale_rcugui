// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_INTMOD_H
#define RCUGUI_INTMOD_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuINTMOD.h>


namespace RcuGui
{
  //====================================================================
  /** @class INTMOD
      @brief Interface to the INTerrupt MODe
      @ingroup rcugui
  */
  class INTMOD : public Register
  {
  private:
    Rcuxx::RcuINTMOD&  fLow;
    TGLayoutHints fTips;
    TGCheckButton fBranchA;
    TGCheckButton fBranchB;
  public:
    INTMOD(TGCompositeFrame& f, Rcuxx::RcuINTMOD& low) 
      : Register(f, low), 
	fLow(low),
	fTips(kLHintsExpandX, ISCALE(2), ISCALE(2)),
	fBranchA(&fFields, "Branch A"),
	fBranchB(&fFields, "Branch B")
    {
      fGroup.SetTitle("Interrupt mode");
      fBranchA.SetToolTipText("Enable interrupts for branch A");
      fBranchB.SetToolTipText("Enable interrupts for branch B");
      fFields.AddFrame(&fBranchA, &fTips);
      fFields.AddFrame(&fBranchB, &fTips);
    }
    void Get() 
    {
      fBranchA.SetDown(fLow.BranchA());
      fBranchB.SetDown(fLow.BranchB());
    }
    void Set() 
    {
      fLow.SetBranchA(fBranchA.IsDown());
      fLow.SetBranchB(fBranchB.IsDown());
    }
  };
}
#endif
//
// EOF
//

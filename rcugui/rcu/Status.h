// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_STATUS_H
#define RCUGUI_STATUS_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <TGCanvas.h>
# include <rcugui/Scrollable.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/Register.h>
# include <rcugui/rcu/StatusEntry.h>
# include <rcuxx/rcu/RcuSTATUS.h>
# include <vector>

namespace RcuGui
{
  //==================================================================
  /** @class Status
      @ingroup rcugui_low
   */
  class Status // : public Scrollable 
  {
  public:
    /** Signals */
    enum {
      /** pdate */
      kUpdate,
      /** Print */
      kPrint
    };
    /** Create the register interface tab 
	@param tabs Where to put the tab 
	@param low Low-level interface  */
    Status(TGTab& tabs, Rcuxx::RcuSTATUS& low);
    /** Handle buttons 
	@param id of button to handle */
    void Handle(Int_t id);
    /** Update (some of) the register displays */
    Bool_t Update();
    /** Update (some of) the register displays */
    void Print() const;
    /** Write the data buffer into the display */ 
    void WriteText();
  protected:
    /** Low */
    Rcuxx::RcuSTATUS& fLow;
    /** ELement */
    TGLayoutHints fButtonHints;
    /** ELement */
    TGLayoutHints fViewHints;
    /** ELement */
    TGLayoutHints fTableHints;
    /** ELement */
    TGLayoutHints fLineHints;
    
    /** Composite frame - owned by tabs */
    TGCompositeFrame* fTop;
    /** Entry view */
    LabeledIntView fEntries;
    /** Container */
    TGHorizontalFrame fBottom;
    /** Canvas */
    TGCanvas fCanvas;
    /** Container */
    TGVerticalFrame fCont;
    /** Button group */
    TGButtonGroup fButtons;
    /** Update button */
    TGTextButton fUpdate;    
    /** Update button */
    TGTextButton fPrint;    
    /** ELement */
    std::vector<StatusEntry*> fList;
  };
}

//==================================================================
inline 
RcuGui::Status::Status(TGTab& tabs, Rcuxx::RcuSTATUS& low)
  : fLow(low), 
    fButtonHints(kLHintsExpandY,ISCALE(3), ISCALE(3), ISCALE(3), ISCALE(3)),
    fViewHints(kLHintsExpandX,0, 0, 0, 0),
    fTableHints(kLHintsExpandY|kLHintsExpandX, ISCALE(3), ISCALE(3), 
		ISCALE(3), ISCALE(3)),
    fLineHints(kLHintsExpandX),
    fTop(tabs.AddTab("Status")),
    fEntries(*fTop,"Entries", 0, 0x1f, kHorizontalFrame, 
	     ISCALE(3), ISCALE(3), ISCALE(3), ISCALE(3)),
    fBottom(fTop),
    fCanvas(&fBottom, ISCALE(fTop->GetWidth()-300), 
	    ISCALE(fTop->GetHeight()-100),  kChildFrame|kSunkenFrame),
    fCont(fCanvas.GetViewPort()),
    fButtons(&fBottom, "Operations", kChildFrame|kVerticalFrame),
    fUpdate(&fButtons, "Update", 0),
    fPrint(&fButtons, "Print", 1),
    fList(low.Size()-1)
{
  fTop->SetLayoutManager(new TGVerticalLayout(fTop));
  fTop->AddFrame(&fBottom, &fTableHints);
  fBottom.AddFrame(&fCanvas, &fViewHints);
  fBottom.AddFrame(&fButtons, &fButtonHints);
  fCanvas.SetContainer(&fCont);
  // fTable.SetLayoutManager(new TGMatrixLayout(fTable, fLow.Size(), 12));
  for (size_t i = 0; i < fLow.Size()-1; i++) {
    TGHorizontalFrame* line = new TGHorizontalFrame(&fCont);
    fCont.AddFrame(line, &fLineHints);
    fList[i] = new StatusEntry(*line);
  }
  fButtons.Connect("Clicked(Int_t)", "RcuGui::Status", this, "Handle(Int_t)");
  WriteText();
}

//____________________________________________________________________
inline void
RcuGui::Status::Handle(Int_t oper) 
{
  switch (oper) {
  case kUpdate:  Update(); break;
  case kPrint:   Print();  break;
  }
}

//____________________________________________________________________
inline Bool_t 
RcuGui::Status::Update() 
{
  UInt_t ret = fLow.Update();
  if (ret) {
    Main::Instance()->Error(ret);
    return kFALSE;
  }
  Main::Instance()->SetStatus(Form("Read Status"));
  WriteText();
  return kTRUE;
}

//____________________________________________________________________
inline void 
RcuGui::Status::Print() const
{
  size_t n = fLow.Entries();
  std::cout << "Contents of RCU status register\n" 
	    << "\tEntries:\t" << n << std::endl;
  for (size_t i = 0; i < fLow.Size(); i++) {
    std::cout << "\t" << std::setw(3) << i << ": 0x" 
	      << std::setfill('0') << std::hex << std::setw(4) 
	      << fLow.Data()[i] << std::setfill(' ') << std::dec 
	      << std::endl;
  }
  for (size_t i = 0; i < n && i < fLow.Size(); i++) {
    try {
      const Rcuxx::RcuStatusEntry& e = fLow.Entry(i);
      
      if (e.IsIdentified()) 
	std::cout << "\t 0x" << std::hex << std::setfill('0') 
		  << std::setw(2) << e.Where();
      else 
	std::cout << "\tBranch " << (e.Where() >> 4 ? 'B' : 'A');
      std::cout << ":";
      if (e.IsSoft())       std::cout << " soft ";
      if (e.IsAnswering())  std::cout << " answering";
      if (e.IsMissedSclk()) std::cout << " SCLK";
      if (e.IsAlps())       std::cout << " ALPS";
      if (e.IsPaps())       std::cout << " PAPS";
      if (e.IsDcOverTh())   std::cout << " DC";
      if (e.IsDvOverTh())   std::cout << " DV";
      if (e.IsAcOverTh())   std::cout << " AC";
      if (e.IsAvOverTh())   std::cout << " AV";
      if (e.IsTempOverTh()) std::cout << "T";
      std::cout << std::endl;
    } 
    
    catch (std::exception& e) {
      std::cout << "\tNo entry at " << i << std::endl;
      continue;
    }
  }
}
//____________________________________________________________________
inline void 
RcuGui::Status::WriteText()
{
  size_t i, n = fLow.Entries();
  if (n > 32) {
    std::cerr << "Argh! too many entries: " << n << "(0x" << std::hex 
	      << n << ")" << std::endl;
    return;
  }
  fEntries.SetValue(n);
  try {
    for (i = 0; i < n && i < fLow.Size(); i++) {
      if (!fList[i]) continue;
      fList[i]->Get(fLow.Entry(i));
    }
    for (; i < fList.size(); i++) { 
      if (!fList[i]) continue;
      fList[i]->Clear();
    }
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }


}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_Counter_H
#define RCUGUI_Counter_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuCounter.h>
# include <climits>


namespace RcuGui
{
  //====================================================================
  /** @class Counter
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class Counter : public Register
  {
  private:
    Rcuxx::RcuCounter&  fLow;
    LabeledIntView fCounts;
  public:
    Counter(TGCompositeFrame& f, Rcuxx::RcuCounter& low) 
      : Register(f, low), fLow(low),
	fCounts(fFields, "Counts", 0, LONG_MAX)
    {
      Get();
    }
    Counter(TGCompositeFrame& f, Rcuxx::RcuCounter& low, 
	    const char* name) 
      : Register(f, low), fLow(low),
	fCounts(fFields, "Counts", 0,  LONG_MAX)
    {
      fGroup.SetTitle(name);
      Get();
    }
    void Get() 
    {
      fCounts.SetValue(fLow.Counts());
    }
  };
}
#endif
//
// EOF
//

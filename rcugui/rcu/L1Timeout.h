// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_L1Timeout_H
#define RCUGUI_L1Timeout_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuL1Timeout.h>


namespace RcuGui
{
  //====================================================================

  /** @class L1Timeout1
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class L1Timeout : public Register
  {
  private:
    Rcuxx::RcuL1Timeout&   fLow;
    LabeledIntEntry        fTimeout;
    LabeledIntEntry        fWindow;
  public:
    L1Timeout(TGCompositeFrame& f, Rcuxx::RcuL1Timeout& low) 
      : Register(f, low), fLow(low),
	fTimeout(fFields, "Timeout", 0, 0xfff, kHorizontalFrame), 
	fWindow(fFields, "Window", 0, 0xf, kHorizontalFrame)
    {
      Get();
      fTimeout.SetHexFormat(kFALSE);
      fWindow.SetHexFormat(kFALSE);
    }
    void Get() 
    {
      fTimeout.SetValue(fLow.Timeout());
      fWindow.SetValue(fLow.Window());
    }
    void Set() 
    {
      fLow.SetTimeout(fTimeout.GetValue());
      fLow.SetWindow(fWindow.GetValue());
    }
  };
}
#endif
//
// EOF
//

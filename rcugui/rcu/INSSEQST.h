// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_INSSEQST_H
#define RCUGUI_INSSEQST_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/rcu/StateDisplay.h>
# include <rcuxx/rcu/RcuINSSEQST.h>
# include <climits>


namespace RcuGui
{
  //====================================================================
  /** @class INSSEQST
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class INSSEQST : public Register
  {
  private:
    Rcuxx::RcuINSSEQST&  fLow;
    StateDisplay fINSTSEQ;
    StateDisplay fINSTLOOP;
    StateDisplay fRU;
  public:
    INSSEQST(TGCompositeFrame& f, Rcuxx::RcuINSSEQST& low) 
      : Register(f, low), fLow(low),
	fINSTSEQ(fFields,fLow.INSTSEQ()), 	
	fINSTLOOP(fFields,fLow.INSTLOOP()), 	
	fRU(fFields,fLow.RU())
    {
      // "Arbitrator FSM"
      Get();
    }
    void Get() 
    {
      fINSTSEQ.Update();
      fINSTLOOP.Update();
      fRU.Update();
    }
  };
}
#endif
//
// EOF
//

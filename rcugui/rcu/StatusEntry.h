// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_STATUSENTRY_H
#define RCUGUI_STATUSENTRY_H
# include <TGFrame.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuStatusEntry.h>

namespace RcuGui
{
  //____________________________________________________________________
  /** @class StatusEntry
      @brief Interface to the RCU memory banks 
      @ingroup rcugui_low
  */
  class StatusEntry 
  {
  public:
    /** Constructor 
	@param p 
    */
    StatusEntry(TGCompositeFrame& p);
    /** @param e Get values */
    void Get(const Rcuxx::RcuStatusEntry& e);
    /** Clear it */
    void Clear();
  protected:
    /** Layout hints for where container */
    TGLayoutHints fWHints;
    /** Contain of where information */
    TGHorizontalFrame fW;
    /** Identified bit */
    ErrorBit       fIsIdentified;
    /** Location field */
    LabeledIntView fWhere;
    /** Layout hints for bits container */
    TGLayoutHints fBHints;
    /** Container of error bits */
    TGHorizontalFrame fB;
    /** Whether this is a soft interrupt */
    ErrorBit fIsSoft;
    /** Whether the FEC answers. */
    ErrorBit fIsAnswering;
    /** SCLK - Missed sample clocks */
    ErrorBit fMissedSclk;
    /** Alps - ALTRO power supply error */
    ErrorBit fAlps;
    /** Paps - PASA power supply error */
    ErrorBit fPaps;
    /** DC - Digital current over threshold */
    ErrorBit fDcOverTh;
    /** DV - Digital voltage over threshold */
    ErrorBit fDvOverTh;
    /** AC - Analog current over threshold */
    ErrorBit fAcOverTh;
    /** AV - Analog voltage over threshold */
    ErrorBit fAvOverTh;
    /** T - Temperture over threshold */
    ErrorBit fTempOverTh;
  };
}

//======================================================================
inline 
RcuGui::StatusEntry::StatusEntry(TGCompositeFrame& p)
  : fWHints(kLHintsLeft|kLHintsExpandX|kLHintsExpandY, 
	    0, ISCALE(3), 0, ISCALE(3)), 
    fW(&p),
    fIsIdentified(fW,"Id", "Valid address",              0x00aa00, 0xc0c0c0),
    fWhere(fW,"Address", 0, 0x1F, kHorizontalFrame, 
	   ISCALE(3), ISCALE(3), ISCALE(15), ISCALE(2)),
    fBHints(kLHintsRight|kLHintsExpandY||kLHintsExpandX, 
	    ISCALE(3), ISCALE(3), 0, ISCALE(3)), 
    fB(&p),
    fIsSoft(fB,"Soft", "Non-critical",                   0x00aa00, 0xc0c0c0),
    fIsAnswering(fB,"Answer","Hardware  is replying",    0x00aa00, 0xc0c0c0),
    fMissedSclk(fB,"SCLK","Missed slow clocks",          0xaa0000, 0xc0c0c0),
    fAlps(fB,"Alps",   "ALTRO power supply error",       0xaa0000, 0xc0c0c0),
    fPaps(fB,"Paps",   "Pasa power supply error",        0xaa0000, 0xc0c0c0),
    fDcOverTh(fB,"DC", "Digital current over threshold", 0xaa0000, 0xc0c0c0),
    fDvOverTh(fB,"DV", "Digital voltage over threshold", 0xaa0000, 0xc0c0c0),
    fAcOverTh(fB,"AC", "Analog current over threshold",  0xaa0000, 0xc0c0c0),
    fAvOverTh(fB,"AV", "Analog voltage over threshold",  0xaa0000, 0xc0c0c0),
    fTempOverTh(fB,"T","Temperature over threshold",     0xaa0000, 0xc0c0c0)
{
  p.AddFrame(&fW, &fWHints);
  p.AddFrame(&fB, &fBHints);
  Clear();
}

//____________________________________________________________________
inline void 
RcuGui::StatusEntry::Get(const Rcuxx::RcuStatusEntry& e) 
{
  Clear();
  Bool_t isId = e.IsIdentified();
  fIsIdentified.SetState(isId);
  if (isId) {
    fIsSoft.SetState(e.IsSoft());
    fIsAnswering.SetState(e.IsAnswering());
    fMissedSclk.SetState(e.IsMissedSclk());
    fAlps.SetState(e.IsAlps());
    fPaps.SetState(e.IsPaps());
    fDcOverTh.SetState(e.IsDcOverTh());
    fDvOverTh.SetState(e.IsDvOverTh());
    fAcOverTh.SetState(e.IsAcOverTh());
    fAvOverTh.SetState(e.IsAvOverTh());
    fTempOverTh.SetState(e.IsTempOverTh());
  }
  fWhere.SetValue(e.Where());
}

//____________________________________________________________________
inline void 
RcuGui::StatusEntry::Clear() 
{
  fIsIdentified.SetState(kFALSE);
  fIsSoft.SetState(kFALSE);
  fIsAnswering.SetState(kFALSE);
  fMissedSclk.SetState(kFALSE);
  fAlps.SetState(kFALSE);
  fPaps.SetState(kFALSE);
  fDcOverTh.SetState(kFALSE);
  fDvOverTh.SetState(kFALSE);
  fAcOverTh.SetState(kFALSE);
  fAvOverTh.SetState(kFALSE);
  fTempOverTh.SetState(kFALSE);
  fWhere.SetValue(0);
  fWhere.SetEnabled(kFALSE);
}

#endif
//
// EOF
//

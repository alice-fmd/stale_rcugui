// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_FECERR_H
#define RCUGUI_FECERR_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Main.h>
# include <rcuxx/rcu/RcuFECERR.h>


namespace RcuGui
{
  //====================================================================
  /** @class FECERR
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class FECERR : public Register
  {
  private:
    Rcuxx::RcuFECERR&  fLow;
    LabeledIntView fValue;
  public:
    FECERR(TGCompositeFrame& f, Rcuxx::RcuFECERR& low) 
      : Register(f, low), fLow(low),
	fValue(fFields, "Errors", 0, 0xffffffff)
    {
      fGroup.SetTitle(Form("Front-end errors - Branch %c)", low.Name()[6]));
      Get();
    }
    void Get() 
    {
      fValue.SetValue(fLow.Data());
      if (fLow.Data() == 0) return;
      Main::Instance()->SetError(Form("Error in instructions for branch %c",
				      fLow.Name()[6]));
    }
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_RDOFEC_H
#define RCUGUI_RDOFEC_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuRDOFEC.h>


namespace RcuGui
{
  //====================================================================
  /** @class RDOFEC 
      @brief Interface to the ACTive FrontEnd Card  
      @ingroup rcugui
  */
  class RDOFEC : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    RDOFEC(TGCompositeFrame& f, Rcuxx::RcuRDOFEC& low) 
      : Register(f, low), fLow(low),
	fLH(kLHintsExpandX,0,0,0,0),
	fOptHints(kLHintsExpandX,0,ISCALE(3),0,0),
	fBranches(&fFields),
	fAFrame(&fBranches, "Branch A", kHorizontalFrame),
	fBFrame(&fBranches, "Branch B", kHorizontalFrame)
    {
      fGroup.SetTitle("Read-out Front-End Cards");
      fFields.AddFrame(&fBranches, &fOptHints);
      fBranches.AddFrame(&fAFrame, &fLH);
      for (size_t i = 0; i < 16; i++) 
	fCheck[i] = new RcuGui::ErrorBit(fAFrame, Form("%0d", i), 
					 Form("Read-out of board A-%d", i),
					 0x00aa00, 0xc0c0c0);
      fBranches.AddFrame(&fBFrame, &fLH);
      for (size_t i = 16; i < 32; i++) 
	fCheck[i] = new RcuGui::ErrorBit(fBFrame, Form("%0d", i-16), 
					 Form("Read-out of board B-%d", i-16),
					 0x00aa00, 0xc0c0c0);
      Get();
    }
    void Get()
    {
      // Int_t val = fLow.Value();
      for (size_t i = 0; i < 32; i++) 
	fCheck[i]->SetState(fLow.IsOn(i));
    }
  private:
    /** Low-level */ 
    Rcuxx::RcuRDOFEC& fLow;
    /** Layout hints */
    TGLayoutHints    fLH;  
    /** Layout hints */
    TGLayoutHints    fOptHints;  
    /** Branches */
    TGHorizontalFrame       fBranches;
    /** Value label */
    TGGroupFrame          fAFrame;
    /** Value label */
    TGGroupFrame          fBFrame;
    /** Check marks */ 
    ErrorBit*   fCheck[32];
  };
}
#endif
//
// EOF
//

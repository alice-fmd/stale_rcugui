// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_EVWORD_H
#define RCUGUI_EVWORD_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuEVWORD.h>


namespace RcuGui
{
  //====================================================================
  /** @class EVWORD
      @brief Interface to the EVent WORD register
      @ingroup rcugui
  */
  class EVWORD : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    EVWORD(TGCompositeFrame& f, Rcuxx::RcuEVWORD& low) 
      : Register(f, low), 
	fLow(low),
	fView1(fFields, "Event word 1", 0, 0xffff),
	f1Trigger(fFields, "Trigger", "Trigger was rececieved", 
		  0x00aa00, 0xc0c0c0),
	f1Start(fFields, "Start", "Start of read-out", 0x00aa00, 0xc0c0c0),
	f1End(fFields, "End", "End of read-out", 0x00aa00, 0xc0c0c0),
	f1DM1(fFields, "DM1", "Data in memory 1", 0x00aa00, 0xc0c0c0),
	f1DM2(fFields, "DM2", "Data in memory 2", 0x00aa00, 0xc0c0c0),	
	fView2(fFields, "Event word 2", 0, 0xffff),
	f2Trigger(fFields, "Trigger", "Trigger was rececieved", 
		  0x00aa00, 0xc0c0c0),
	f2Start(fFields, "Start", "Start of read-out", 0x00aa00, 0xc0c0c0),
	f2End(fFields, "End", "End of read-out", 0x00aa00, 0xc0c0c0),
	f2DM1(fFields, "DM1", "Data in memory 1",0x00aa00, 0xc0c0c0),
	f2DM2(fFields, "DM2", "Data in memory 2",0x00aa00, 0xc0c0c0)
    {
      fGroup.SetTitle("Trigger word");

      fView1.SetHexFormat(kTRUE);
      fView2.SetHexFormat(kTRUE);
      fView1.SetToolTipText("Current event word - "
			     "Bit mask  0/1: data in DMEM2/1 "
			     "2: no more data "
			     "3: start of event "
			     "4: trigger");
      fView2.SetToolTipText("Current event word - "
			     "Bit mask  0/1: data in DMEM2/1 "
			     "2: no more data "
			     "3: start of event "
			     "4: trigger");
      Get();
    }  
    void Get()
    {
      fView1.SetValue(fLow.View1() & 0xffff);
      f1Trigger.SetState(fLow.HasTrigger() ? kButtonDown : kButtonUp);
      f1Start.SetState(fLow.IsStart() ? kButtonDown : kButtonUp);
      f1End.SetState(fLow.IsEnd() ? kButtonDown : kButtonUp);
      f1DM1.SetState(fLow.HasData(1) ? kButtonDown : kButtonUp);
      f1DM2.SetState(fLow.HasData(2) ? kButtonDown : kButtonUp);
      fView2.SetValue(fLow.View2() & 0xffff);
      f2Trigger.SetState(fLow.HasTrigger() ? kButtonDown : kButtonUp);
      f2Start.SetState(fLow.IsStart() ? kButtonDown : kButtonUp);
      f2End.SetState(fLow.IsEnd() ? kButtonDown : kButtonUp);
      f2DM1.SetState(fLow.HasData(1) ? kButtonDown : kButtonUp);
      f2DM2.SetState(fLow.HasData(2) ? kButtonDown : kButtonUp);
    }
  private:
    Rcuxx::RcuEVWORD&  fLow;
    RcuGui::LabeledIntView fView1;
    RcuGui::ErrorBit  f1Trigger;
    RcuGui::ErrorBit  f1Start;
    RcuGui::ErrorBit  f1End;
    RcuGui::ErrorBit  f1DM1;
    RcuGui::ErrorBit  f1DM2;
    RcuGui::LabeledIntView fView2;
    RcuGui::ErrorBit  f2Trigger;
    RcuGui::ErrorBit  f2Start;
    RcuGui::ErrorBit  f2End;
    RcuGui::ErrorBit  f2DM1;
    RcuGui::ErrorBit  f2DM2;
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_STATE_H
#define RCUGUI_STATE_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#include <rcuxx/rcu/RcuState_t.h>
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef TGTextEntry
# include <TGTextEntry.h>
#endif
#ifndef TString
# include <TString.h>
#endif

class TGLayoutHints;

namespace RcuGui 
{

  //====================================================================
  /** @struct StateDisplay 
      @brief Bit to be toggled on errors.   Cannot be set.
      @ingroup rcugui_basic
   */
  struct StateDisplay : public TGVerticalFrame
  {
    /** Constructor 
	@param p 
	@param name 
	@param desc 
	@param okColour 
	@param badColour  */
    StateDisplay(TGCompositeFrame& p, const Rcuxx::RcuState_t& low)
      : TGVerticalFrame(&p), 
	fLow(low), 
	fHints(kLHintsExpandX, 2, 2, 1, 1),
	fName(this, low.Name().c_str()), 
	fDisplay(this, "")
    {
      AddFrame(&fName, &fHints);
      AddFrame(&fDisplay, &fHints);
      fDisplay.SetEnabled(kFALSE);
      p.AddFrame(this, &fHints);
    }
    /** Destructor */
    virtual ~StateDisplay() {}
    
    /** @param ok New state  */
    void Update() { 
      // std::stringstream s;
      // s << "state " << fLow.State();
      fDisplay.SetText(fLow.AsString());
    }
  protected:
    const Rcuxx::RcuState_t& fLow;
    /** Name Layout hints */
    TGLayoutHints    fHints;
    /** Name */
    TGLabel          fName;
    /** StateDisplay */
    TGTextEntry      fDisplay;
  };
  
}
#endif
//
// EOF
//

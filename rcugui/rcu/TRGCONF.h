// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_TRGCONF_H
#define RCUGUI_TRGCONF_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuTRGCONF.h>


namespace RcuGui
{
  //____________________________________________________________________
  /** @class TRGCONF 
      @brief Interface to the TRigger ConFiG register 
      @ingroup rcugui
  */
  class TRGCONF : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    TRGCONF(TGCompositeFrame& f, Rcuxx::RcuTRGCONF& low) 
      : RcuGui::Register(f, low), 
	fLow(low),
	fLH(kLHintsExpandX,0,ISCALE(6),0,0),
	fMapping(&fFields, "Trigger mapping"), 
	fL0ToL1(&fMapping, "L0 (input) -> L1 (fec)"),
	fL1ToL1(&fMapping, "L1 (input) -> L1 (fec)"),
	fSources(&fFields, "Trigger source"), 
	fSoftware(&fSources, "Software"),
	fHardware(&fSources, "Hardware"),
	fTTC(&fSources, "TTC"),
	fL2Latency(fFields, "L2 latency", 0, 0xFFF)
    {
      fGroup.SetTitle("Trigger configuration");
      fFields.AddFrame(&fMapping, &fLH);
      fFields.AddFrame(&fSources, &fLH);
      fSources.Connect("Clicked(Int_t)", "RcuGui::LabeledIntEntry", 
		       &fL2Latency, "SetEnabled(Bool_t)");
      fL2Latency.SetHexFormat(kFALSE);
      Get();
    }
    void Get() 
    {
      fSoftware.SetState(fLow.IsSoftwareTriggers() ? kButtonDown : kButtonUp);
      fHardware.SetState(fLow.IsHardwareTriggers() ? kButtonDown : kButtonUp);
      fTTC.SetState(fLow.IsTTCTriggers() ? kButtonDown : kButtonUp);
      fL2Latency.SetValue(fLow.L2Latency());
      fL0ToL1.SetDown(fLow.IsL0ToL1());
      fL1ToL1.SetDown(fLow.IsL1ToL1());
    }
    void Set()
    {
      fLow.EnableSoftwareTriggers(fSoftware.IsDown());
      fLow.EnableHardwareTriggers(fHardware.IsDown());
      fLow.EnableTTCTriggers(fTTC.IsDown());
      fLow.SetL2Latency(fL2Latency.GetValue());
      fLow.SetMapping(fL0ToL1.IsOn() ? 
		      Rcuxx::RcuTRGCONF::kL0ToL1 : 
		      Rcuxx::RcuTRGCONF::kL1ToL1);
    }
  private:
    /** Low-level interface */ 
    Rcuxx::RcuTRGCONF& fLow;
    /** Layout hints */
    TGLayoutHints  fLH;
    /** Mapping */
    TGVButtonGroup fMapping;
    /** L0->L1 */
    TGRadioButton fL0ToL1;
    /** L1->L1 */
    TGRadioButton fL1ToL1;
    /** Source buttons */
    TGVButtonGroup fSources;
    /** Software trigger button */
    TGCheckButton  fSoftware;
    /** Hardware trigger button */
    TGCheckButton  fHardware;
    /** TTC trigger button */
    TGCheckButton  fTTC;
    /** Latency */
    LabeledIntEntry fL2Latency;
  };
}
#endif
//
// EOF
//

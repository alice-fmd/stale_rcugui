// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ACTIVEBIT_H
#define RCUGUI_ACTIVEBIT_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/BoardDialog.h>
# include <rcugui/rcu/ActiveChannels.h>

namespace RcuGui
{
  //____________________________________________________________________
  /** @class ActiveBit
      @ingroup rcugui_low
   */
  struct ActiveBit : public ErrorBit 
  {
  public:
    /** Constructor  
	@param p      Parent
	@param branch Branch number
	@param board  Board number
	@param acl    Pointer to ACL
	@param mask   Mask of valid channels
	@param sync   Sync to ACL  */
    ActiveBit(TGCompositeFrame& p, Char_t branch, Int_t board, 
	      ActiveChannels& acl, const UInt_t* mask, Bool_t sync);
    /** @param on Turn on/off */
    void SetActive(Bool_t on=kTRUE);
    /** @param sync Sync to ACL or not */
    void SetSync(Bool_t sync);
    /** Turn on all */
    void SetAll();
    /** Turn off all */
    void SetNone();
    /** Decode */
    void Get();
    /** Encode */
    void Set();
    /** Handle buttons */
    void HandleButton();
  protected:
    /** Board # */
    Int_t           fBoard;
    /** Branch */
    Char_t          fBranch;
    /** Data */
    unsigned int    fData[8];
    /** ACL */
    ActiveChannels& fACL;
    /** Option */
    Bool_t          fSync;
    /** Count */
    const UInt_t*   fMask;
    /** Flag */
    Bool_t          fActive;
    ClassDef(ActiveBit,0);
  };
}
//====================================================================
inline
RcuGui::ActiveBit::ActiveBit(TGCompositeFrame& p, Char_t branch, Int_t board, 
			     ActiveChannels& acl, const UInt_t* mask, 
			     Bool_t sync)
  : ErrorBit(p, Form("%2d", board), Form("%2d", board), 0x00aa00, 0xc0c0c0), 
    fBoard(board), 
    fBranch(branch),
    fACL(acl), 
    fSync(sync), 
    fMask(mask),
    fActive(kTRUE)
{
  fState.SetEnabled(kTRUE);
  SetActive();
}

//____________________________________________________________________
inline void 
RcuGui::ActiveBit::SetActive(Bool_t on)
{
  SetEnabled(on);
  if (!on) SetNone();
  fActive = on;
}

//____________________________________________________________________
inline void 
RcuGui::ActiveBit::Get() 
{
  if (!fActive) return;
  fGood = kFALSE;
  unsigned int board = (fBranch == 'B' ? 16 : 0)+fBoard;
  for (unsigned int chip = 0; chip < 8; chip++) { 
    for (unsigned int channel = 0; channel < 16; channel++) { 
      if (fACL.ChannelEnabled(board, chip, channel)) { 
	fData[chip] |= (1 << channel);
	fGood = kTRUE;
      }
    }
  }
}

//____________________________________________________________________
inline void 
RcuGui::ActiveBit::SetSync(Bool_t sync) 
{
  // if (sync) std::cout << "Syncronising ACTFEC to ACL" << std::endl;
  if (!fActive) return;
  fSync = sync;
}

//____________________________________________________________________
inline void 
RcuGui::ActiveBit::Set() 
{
  if (!fActive) return;
  fACL.EnableBoard((fBranch=='B'?16:0)+fBoard,fData,8);
  fGood = kFALSE;
  for (size_t i = 0; i < 8; i++) if (fData[i] != 0) fGood = kTRUE;
  SetState(fGood);
}

//____________________________________________________________________
inline void 
RcuGui::ActiveBit::SetAll() 
{
  if (!fActive) return;
  for (Int_t i = 0; i < 8; i++) fData[i] = 0xffff;
  if (!fSync) return;
  Set();
}

//____________________________________________________________________
inline void 
RcuGui::ActiveBit::SetNone() 
{
  if (!fActive) return;
  for (Int_t i = 0; i < 8; i++) fData[i] = 0;
  if (!fSync) return;
  Set();
}

//____________________________________________________________________
inline void 
RcuGui::ActiveBit::HandleButton() 
{
  if (!fActive) return;
  fGood = !fGood;
  SetState(fGood);
  if (fSync) {
    if (fGood) {
      Bool_t any, cancel;
      new RcuGui::BoardDialog(&fTop,fBranch,fBoard,fMask,fData,&any,&cancel);
      if (!any || cancel) fGood = kFALSE;
      SetState(fGood);
      if (cancel) return;
      Set();
    }
    else {
      unsigned int data[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
      fACL.DisableBoard((fBranch=='B'?16:0)+fBoard);
    }
  }
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_MEMORY_H
#define RCUGUI_MEMORY_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGCanvas.h>
# include <TGButton.h>
# include <TString.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/LinkedView.h>
# include <rcuxx/rcu/RcuMemory.h>

#define SCALE(X) (RcuGui::Main::Instance()->Scale()*(X))
#define ISCALE(X) int(RcuGui::Main::Instance()->Scale()*(X))

namespace RcuGui
{

  //____________________________________________________________________
  /** @class Memory
      @brief Interface to the RCU memory banks 
      @ingroup rcugui_low
  */
  class Memory // : public Scrollable
  {
  public:
    /** Constructor 
	@param tabs Where to add the tab to.   
	@param maxZeros Maximum number of zeros to show
	@param low Pointer to low-level driver 
	@param options Frame options
	@param woff Width offset 
	@param hoff Height offset */
    Memory(TGTab&            tabs, 
	   Rcuxx::RcuMemory& low, 
	   Int_t             maxZeros=3, 
	   Int_t             options=kHorizontalFrame, 
	   UInt_t            woff=6,
	   UInt_t            hoff=6);
    /** Destructor */
    virtual ~Memory();
    /** Update the diplay */
    virtual Bool_t Update();
    /** Commit to the RCU */
    virtual Bool_t Commit();
    /** Zero the memory */
    virtual void Zero();
    /** Get the size */
    UInt_t Size() const { return fLow.Size(); }
    /** Get the data */
    // UInt_t* Data() const { return fData; }
    /** Read the contents of the display into the data buffer */ 
    void ReadText();
    /** Write the data buffer into the display */ 
    void WriteText();
    /** Set the data */
    /** @param option Not used */
    void Print(Option_t* option="") const;
    /** @return The linked view */
    LinkedView* View() { return &fView; }
  protected:
    /** Pointer to low-level */ 
    Rcuxx::RcuMemory& fLow;
    /** Name */
    TString fName;
    /** Top */
    TGCompositeFrame* fTop;
    /** Container  */
    TGHorizontalFrame fCont;
    /** Width */
    UInt_t fCanvasW;
    /** Height */
    UInt_t fCanvasH;
    /** Linked view */
    LinkedView fView;
    /** Layout */
    TGLayoutHints fButtonHints;
    /** Button group */
    TGButtonGroup fButtons;
    /** Update button */
    TGTextButton fUpdate;
    /** Commit button */
    TGTextButton* fCommit;
    /** Zero button */
    TGTextButton* fZero;
    /** Zero the rest of the data buffer */
    void ZeroRest();  
  };
}
//====================================================================
inline 
RcuGui::Memory::Memory(TGTab&            tabs,		       
		       Rcuxx::RcuMemory& low,
		       Int_t             maxZeros, 
		       Int_t             options,
		       UInt_t            woff, 
		       UInt_t            hoff) 
  : fLow(low),
    fName(low.Name().c_str()),
    fTop(tabs.AddTab(low.Name().c_str())),
    fCont(fTop),
    fCanvasW(fTop->GetWidth()-woff),
    fCanvasH(fTop->GetHeight()-hoff),
    fView(fCont, fCanvasW, fCanvasH, low.Size(), maxZeros),
    fButtonHints(kLHintsExpandY|kLHintsExpandX,ISCALE(3), 0, 0, 0),
    fButtons(&fCont, "Operations", kChildFrame|kVerticalFrame),
    fUpdate(&fButtons, "Update", 0),
    fCommit(0), 
    fZero(0)
{
  fTop->SetLayoutManager(new TGVerticalLayout(fTop));
  fTop->AddFrame(&fCont, 
		 new TGLayoutHints(kLHintsExpandX|kLHintsExpandY|kLHintsBottom,
				   ISCALE(3), ISCALE(3), ISCALE(3),ISCALE(3)));
  fUpdate.Connect("Clicked()", "RcuGui::Memory", this, "Update()");
  if (fLow.IsWriteable()) {
    fCommit = new TGTextButton(&fButtons, "Commit", 1);
    fCommit->Connect("Clicked()", "RcuGui::Memory", this, "Commit()");
    fZero = new TGTextButton(&fButtons, "Zero", 3);
    fZero->Connect("Clicked()", "RcuGui::Memory", this, "Zero()");
  }
  fCont.AddFrame(&fButtons, &fButtonHints);
  WriteText();
}

//____________________________________________________________________
inline 
RcuGui::Memory::~Memory()
{
  if (fCommit) delete fCommit;
  if (fZero)   delete fZero;
}

//____________________________________________________________________
inline void 
RcuGui::Memory::WriteText() 
{
  fView.WriteText(&(fLow.Data()[0]));
}

//____________________________________________________________________
inline void 
RcuGui::Memory::ReadText() 
{
#if 0
  if (!fView) {
    std::cerr << "No view" << std::endl;
    return;
  }
#endif
  std::vector<unsigned int> d(fLow.Data().size());
  fView.ReadText(&(d[0]));
  fLow.Set(0, d.size(), &(d[0]));
  ZeroRest();
}

//____________________________________________________________________
inline Bool_t
RcuGui::Memory::Update() 
{
  UInt_t ret = fLow.Update();
  if (ret) {
    Main::Instance()->Error(ret);
    return kFALSE;
  }
  Main::Instance()->SetStatus(Form("Read %s", fName.Data()));
  WriteText();
  return kTRUE;
}

//____________________________________________________________________
inline Bool_t
RcuGui::Memory::Commit() 
{
  if (!fLow.IsWriteable()) return kTRUE;
  ReadText();
  UInt_t ret = fLow.Commit();
  if (ret) {
    Main::Instance()->Error(ret);
    return kFALSE;
  }
  Update();
  Main::Instance()->SetStatus(Form("Wrote %s", fName.Data()));
  return kTRUE;
}

//____________________________________________________________________
inline void 
RcuGui::Memory::Zero()
{
  fView.SendMessage(&fView, MK_MSG(kC_COMMAND, kCM_MENU), 
		     TGTextEdit::kM_FILE_NEW, 0);
  if (fLow.IsClearable()) fLow.Clear();
  else                    fLow.Zero();
  Update();
}

//____________________________________________________________________
inline void 
RcuGui::Memory::ZeroRest() 
{
  for (size_t i = fView.RowCount(); i < fLow.Size(); i++) fLow.Data()[i] = 0;
}

//____________________________________________________________________
inline void 
RcuGui::Memory::Print(Option_t* option) const
{
  std::cout << "Contents of " << fName << std::endl;
  fLow.Print();
}

#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTROIF_H
#define RCUGUI_ALTROIF_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuALTROIF.h>


namespace RcuGui
{
  //====================================================================
  /** @class ALTROIF
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class ALTROIF : public Register
  {
  public:
    ALTROIF(TGCompositeFrame& f, Rcuxx::RcuALTROIF& low) 
      : Register(f, low), fLow(low),
	fSamplesPerChannel(fFields, "Samples/channel", 0, 0x3ff),
	fSamplingRatio(&fFields, "Tsclk/Tlhc"),
	f20MHz(&fSamplingRatio, "20MHz"),
	f10MHz(&fSamplingRatio, "10MHz"),
	f5MHz(&fSamplingRatio, "5MHz"),
	f2500kHz(&fSamplingRatio, "2.5MHz"),
	fCSTBDelay(fFields, "CSTB delay", 0, 0x3),
	fBcCheck(&fFields, "BC instruction check"),
	fNoCheck(&fBcCheck, "No checks"),
	fTPCBC(&fBcCheck, "TPC (5bits)"),
	fPHOSBC(&fBcCheck, "PHOS (8bits)"),
	fFMDBC(&fBcCheck, "FMD (7bits)")
    {
      fGroup.SetTitle("ALTRO bus interface");
      fFields.AddFrame(&fSamplingRatio);
      fFields.AddFrame(&fBcCheck);
      fCSTBDelay.SetHexFormat(kFALSE);
      fSamplesPerChannel.SetHexFormat(kFALSE);
      Get();
    }
    void Get() 
    {
      fSamplesPerChannel.SetValue(fLow.SamplesPerChannel());
      Rcuxx::RcuALTROIF::SampleSpeed_t speed = fLow.SamplingRatio();
      switch (speed) { 
      case Rcuxx::RcuALTROIF::k20MHz:   f20MHz.SetState(kButtonDown); break;
      case Rcuxx::RcuALTROIF::k10MHz:   f10MHz.SetState(kButtonDown); break;
      case Rcuxx::RcuALTROIF::k5MHz:    f5MHz.SetState(kButtonDown); break;
      case Rcuxx::RcuALTROIF::k2500kHz: f2500kHz.SetState(kButtonDown); break;
      }
      fCSTBDelay.SetValue(fLow.CSTBDelay());
      Rcuxx::RcuALTROIF::BcInstructionCheck_t bcchk = fLow.BcCheck();
      switch (bcchk) { 
      case Rcuxx::RcuALTROIF::kNoCheck: fNoCheck.SetState(kButtonDown); break;
      case Rcuxx::RcuALTROIF::kTPCBC:   fTPCBC.SetState(kButtonDown); break;
      case Rcuxx::RcuALTROIF::kPHOSBC:  fPHOSBC.SetState(kButtonDown); break;
      case Rcuxx::RcuALTROIF::kFMDBC:   fFMDBC.SetState(kButtonDown); break;
      }
    }
    void Set()
    {
      fLow.SetSamplesPerChannel(fSamplesPerChannel.GetValue());
      Rcuxx::RcuALTROIF::SampleSpeed_t speed = Rcuxx::RcuALTROIF::k10MHz;
      if (f20MHz.IsDown())   speed = Rcuxx::RcuALTROIF::k20MHz;
      if (f10MHz.IsDown())   speed = Rcuxx::RcuALTROIF::k10MHz;
      if (f5MHz.IsDown())    speed = Rcuxx::RcuALTROIF::k5MHz;
      if (f2500kHz.IsDown()) speed = Rcuxx::RcuALTROIF::k2500kHz;
      fLow.SetSamplingRatio(speed);
      fLow.SetCSTBDelay(fCSTBDelay.GetValue());
      Rcuxx::RcuALTROIF::BcInstructionCheck_t bcchk = Rcuxx::RcuALTROIF::kNoCheck;
      if (fNoCheck.IsDown()) bcchk = Rcuxx::RcuALTROIF::kNoCheck;
      if (fTPCBC.IsDown())   bcchk = Rcuxx::RcuALTROIF::kTPCBC;
      if (fPHOSBC.IsDown())  bcchk = Rcuxx::RcuALTROIF::kPHOSBC;
      if (fFMDBC.IsDown())   bcchk = Rcuxx::RcuALTROIF::kFMDBC;
      fLow.SetBcCheck(bcchk);
    }
  private:
    Rcuxx::RcuALTROIF&  fLow;
    LabeledIntEntry fSamplesPerChannel;
    TGVButtonGroup  fSamplingRatio;
    TGRadioButton   f20MHz;
    TGRadioButton   f10MHz;
    TGRadioButton   f5MHz;
    TGRadioButton   f2500kHz;
    LabeledIntEntry fCSTBDelay;
    TGVButtonGroup  fBcCheck;
    TGRadioButton   fNoCheck;
    TGRadioButton   fTPCBC;
    TGRadioButton   fPHOSBC;
    TGRadioButton   fFMDBC;
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTROCFG1_H
#define RCUGUI_ALTROCFG1_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/rcu/FirstBaseline.h>
# include <rcugui/rcu/SecondBaseline.h>
# include <rcugui/rcu/ZeroSuppression.h>
# include <rcuxx/rcu/RcuALTROCFG1.h>


namespace RcuGui
{
  //====================================================================
  /** @class ALTROCFG11
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class ALTROCFG1 : public Register
  {
  private:
    Rcuxx::RcuALTROCFG1&   fLow;
    FirstBaseline          f1st;
    SecondBaseline         f2nd;
    ZeroSuppression        fZS;
  public:
    ALTROCFG1(TGCompositeFrame& f, Rcuxx::RcuALTROCFG1& low) 
      : Register(f, low), fLow(low),
	f1st(fFields, false), 
	f2nd(fFields), 
	fZS(fFields)
    {
      fGroup.SetTitle("ALTRO configuration 1");
      Get();
    }
    void Get() 
    {
      f1st.Get(fLow.FirstBMode(), false);
      f2nd.Get(fLow.IsSecondBEnable(), fLow.SecondBPre(), fLow.SecondBPost());
      fZS.Get(fLow.IsZSEnable(), fLow.ZSPre(), fLow.ZSPost(), fLow.ZSGlitch());
    }
    void Set() 
    {
      unsigned short mode, pre, post;
      bool enable;
      f1st.Set(mode, enable);
      fLow.SetFirstBMode(mode);
      
      f2nd.Set(enable, pre, post);
      fLow.SetSecondBEnable(enable);
      fLow.SetSecondBPre(pre);
      fLow.SetSecondBPost(post);

      fZS.Set(enable, pre, post, mode);
      fLow.SetZSEnable(enable);
      fLow.SetZSPre(pre);
      fLow.SetZSPost(post);
      fLow.SetZSMode(mode);
      // fLow.SetZSOffset(fZSOffset.GetValue());
    }
  };
}
#endif
//
// EOF
//

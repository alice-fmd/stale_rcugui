// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ERRREG_H
#define RCUGUI_ERRREG_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuERRREG.h>


namespace RcuGui
{
  //====================================================================
  /** @class ERRREG
      @brief Interface to the RESult REGister 
      @ingroup rcugui
  */
  class ERRREG : public RcuGui::Register
  {
  private:
    Rcuxx::RcuERRREG&  fLow;
    ErrorBit 	       fNoAck;
    ErrorBit 	       fNotAct;
  public:
    ERRREG(TGCompositeFrame& f, Rcuxx::RcuERRREG& low) 
      : RcuGui::Register(f, low), 
	fLow(low),
	fNoAck(fFields, "NOACK", "No acknowledge", 0xc0c0c0, 0xff0000),
	fNotAct(fFields, "NOTACT", "Not active", 0xc0c0c0, 0xff0000)
    {
      fGroup.SetTitle("Monitor error register");
      Get();
    }
    void Get() 
    {
      fNoAck.SetState(!fLow.IsNoAcknowledge());
      fNotAct.SetState(!fLow.IsNotActive());
    }
  };
}
#endif
//
// EOF
//

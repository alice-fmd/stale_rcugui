// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_TRCFG1_H
#define RCUGUI_TRCFG1_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuTRCFG1.h>


namespace RcuGui
{
  //____________________________________________________________________
  /** @class TRCFG1 
      @brief Interface to the TRigger ConFiG register 
      @ingroup rcugui
  */
  class TRCFG1 : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    TRCFG1(TGCompositeFrame& f, Rcuxx::RcuTRCFG1& low) 
      : RcuGui::Register(f, low), 
	fLow(low),
	fLH(kLHintsExpandX,0,ISCALE(6),0,0),
	fFoo(&fFields),
	fOptions(&fFoo),
	fOpt(&fOptions, "Optimize R/O"),
	fPop(&fOptions, "Popped R/O"),
	fBMD(&fFoo),
	fBMD4(&fBMD, "4 Buffers"),
	fBMD8(&fBMD, "8 Buffers"),
	fMode(&fFields),
	fMode0(&fMode, "Software trigger", 0),
	fMode2(&fMode, "Ext. L1, internal L2", 1),
	fMode3(&fMode, "Ext. L1 and L2", 0),
	fTwv(fMode, "Trigger wait", 0, 0x3fff, kHorizontalFrame),
	fPointers(&fFields),
	fWptr(fPointers,"Write pointer", 0, 8),
	fRptr(fPointers,"Read pointer", 0, 8),
	fRemb(fPointers,"Free buffers", 0, 16, kHorizontalFrame),
	fCongLH(kLHintsRight, ISCALE(3), ISCALE(3), ISCALE(3), ISCALE(3)),
	fCongestion(&fPointers),
	fFull(fCongestion, "Full", "Data buffers full", 0xcc0000, 0xc0c0c0),
	fEmpty(fCongestion, "Empty", "Data buffers empty", 0x00aa00, 0xc0c0c0)
    {
      fGroup.SetTitle("Trigger configuration 1");
      using namespace RcuGui;
  
      fFields.AddFrame(&fFoo);
  
      fOpt.SetToolTipText("Use BC to optimize read out");
      fPop.SetToolTipText("Pop data from e.g., U2F");
      fPop.SetDown();
      fFoo.AddFrame(&fOptions, &fLH);
  
      fBMD4.SetDown();
      fFoo.AddFrame(&fBMD, &fLH);
  
      fMode2.SetDown();
      fTwv.SetValue(4096);
      fTwv.SetToolTipText("# clock cycles (25ns) between L1 and L2");
      fTwv.SetHexFormat(kFALSE);
      fFields.AddFrame(&fMode, &fLH);
      fMode.Connect("Clicked(Int_t)", "RcuGui::LabeledIntEntry", &fTwv, 
		    "SetEnabled(Bool_t)");

      //________________________________________________________________
      fFields.AddFrame(&fPointers, &fLH); 
      fPointers.AddFrame(&fCongestion, &fCongLH);
      Get();
    }
    void Get() 
    {
      fTwv.SetValue(fLow.Twv()); // IntNumber(cfg.tw);
      fBMD8.SetDown(fLow.BMD() == Rcuxx::RcuTRCFG1::k8Buffers);
      if (fLow.Mode() == Rcuxx::RcuTRCFG1::kInternal)  fMode0.SetDown(kTRUE);
      if (fLow.Mode() == Rcuxx::RcuTRCFG1::kDerivedL2) fMode2.SetDown(kTRUE);
      if (fLow.Mode() == Rcuxx::RcuTRCFG1::kExternal)  fMode3.SetDown(kTRUE);
      fOpt.SetDown(fLow.IsOpt());
      fPop.SetDown(fLow.IsPop());
      fRemb.SetValue(fLow.Remb());
      fEmpty.SetState(fLow.IsEmpty());
      fFull.SetState(fLow.IsFull());
      fRptr.SetValue(fLow.Rptr());
      fWptr.SetValue(fLow.Wptr());
    }
    void Set()
    {
      using Rcuxx::RcuTRCFG1;
      if (fMode0.GetState() == kButtonDown)fLow.SetMode(RcuTRCFG1::kInternal);
      if (fMode2.GetState() == kButtonDown)fLow.SetMode(RcuTRCFG1::kDerivedL2);
      if (fMode3.GetState() == kButtonDown)fLow.SetMode(RcuTRCFG1::kExternal);
      fLow.SetOpt(fOpt.GetState() == kButtonDown);
      fLow.SetPop(fPop.GetState() == kButtonDown);
      fLow.SetBMD((fBMD8.GetState() == kButtonDown ? 
		    RcuTRCFG1::k8Buffers : RcuTRCFG1::k4Buffers));
      fLow.SetTwv(fTwv.GetValue()); // IntNumber())
    }
  private:
    /** Low-level interface */ 
    Rcuxx::RcuTRCFG1& fLow;
    /** Layout hints */
    TGLayoutHints fLH;
    /** Options */
    TGVerticalFrame fFoo;
    TGButtonGroup   fOptions;
    TGCheckButton   fOpt;
    TGCheckButton   fPop;

    /** Buffer MoDe input/output */
    TGButtonGroup fBMD;
    TGRadioButton fBMD4;
    TGRadioButton fBMD8;

    /** Mode buttons */
    TGButtonGroup           fMode;
    TGRadioButton           fMode0;
    TGRadioButton           fMode2;
    TGRadioButton           fMode3;
    RcuGui::LabeledIntEntry fTwv;
    /** Display */
    TGVerticalFrame        fPointers;
    RcuGui::LabeledIntView fWptr;
    RcuGui::LabeledIntView fRptr;
    RcuGui::LabeledIntView fRemb;

    // TGHorizontalFrame       fInfo;
    TGLayoutHints         fCongLH;
    TGHorizontalFrame     fCongestion;
    RcuGui::ErrorBit      fFull;
    RcuGui::ErrorBit      fEmpty;
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ACTIVECHANNELS_H
#define RCUGUI_ACTIVECHANNELS_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <TGListView.h>
# include <TGButtonGroup.h>
# include <TObjArray.h>
# include <rcugui/rcu/Memory.h>
# include <rcuxx/rcu/RcuACL.h>
# include <vector>
# include <iterator>
# include <rcuxx/DebugGuard.h>


namespace RcuGui
{
  //==================================================================
  /** @class ActiveChannels
      @ingroup rcugui_low
      @brief Interface to active channel memory (ACL or READOUT LIST)
  */
  class ActiveChannels 
  {
  public:
    /** Make an address 
	@param board   Board # 
	@param altro   ALTRO # 
	@param channel ALTRO channel # 
	@return 12bit encoded address */
    static unsigned int MakeAddress(unsigned int board, 
				    unsigned int altro, 
				    unsigned int channel) 
    {
      return ((board & 0x1F) << 7) | ((altro & 0x7) << 4) | (channel & 0xF);
    }
    static void GetAddressParts(unsigned int addr, 
				unsigned int& board,
				unsigned int& altro, 
				unsigned int& channel)
    {
      board   = (addr >> 7) & 0x1F;
      altro   = (addr >> 4) & 0x7;
      channel = (addr >> 0) & 0xF;
    }

    //________________________________________________________________
    /** User data for entries */
    struct Data
    {
      /** Constructor */
      Data(unsigned int addr) : fAddr(addr) {}
      /** Constructor */
      Data(unsigned int b, unsigned int a, unsigned int c)
      {
	fAddr = ActiveChannels::MakeAddress(b, a, c);
      }
      /** @return Board number */
      unsigned int Board()   const { return (fAddr >> 7) & 0x1F; }
      /** @return ALTRO number */
      unsigned int Altro()   const { return (fAddr >> 4) & 0x7; }
      /** @return ALTRO channel number */
      unsigned int Channel() const { return (fAddr >> 0) & 0xF; }
      /** Full address */
      unsigned int fAddr;
    };    
    //________________________________________________________________
    struct Entry : public TGLVEntry
    {
      Entry(const TGLVContainer* p, unsigned int board, 
	    unsigned int altro, unsigned int channel)
	: TGLVEntry(p, "", "")
      {
	Data* d = new Data(board, altro, channel);
	Init(d);
      }
      Entry(const TGLVContainer* p, unsigned int addr)
	: TGLVEntry(p, "", "")
      {
	Data* d = new Data(addr);
	Init(d);
      }
      void Init(Data* d)
      {
	unsigned int b = d->Board();
	unsigned int a = d->Altro();
	unsigned int c = d->Channel();	
	SetItemName(Form("%02d/%1d/%02d", b, a, c));
	SetSubnames(Form("%02d", b), 
		    Form("%01d", a),
		    Form("%02d", c));
	SetUserData(d);
      }
      Data* GetAddress() const { return static_cast<Data*>(GetUserData()); }
    };

    //________________________________________________________________
    struct Element : public TGFrameElement 
    {
      Element(TGFrame* f, TGLayoutHints* l) 
	: TGFrameElement(f, l)
      {}
      bool IsSortable() const { return kTRUE; }
      Int_t Compare(const TObject* o) const 
      {
	const Element* e   = static_cast<const Element*>(o);
	if (!e) { 
	  std::cerr << "Other is not an Element, but a " 
		    << o->ClassName() << std::endl;
	  return 0;
	}	  
	const Entry* self  = dynamic_cast<const Entry*>(fFrame);
	const Entry* other = dynamic_cast<const Entry*>(e->fFrame);
	if (!self) {
	  std::cerr << "Own frame is not an Entry, but a " 
		    << fFrame->ClassName() << std::endl;
	  return 0;
	}
	if (!other) {
	  std::cerr << "Other frame is not an Entry, but a " 
		    << e->fFrame->ClassName() << std::endl;
	  return 0;
	}
	if (self->GetAddress()->fAddr < other->GetAddress()->fAddr) return -1;
	if (self->GetAddress()->fAddr > other->GetAddress()->fAddr) return +1;
	return 0;
      }
    };
	
    
    //________________________________________________________________
    struct Container : public TGLVContainer 
    {
      Container(TGListView* p, bool sorted=true) // TGCanvas* p)
	: TGLVContainer(p), fSorted(true)
	/// TGLVContainer(p, p->GetWidth(), p->GetHeight(), kSunkenFrame)
      {
	fMsgWindow = p;
      }
      void Setup(TGListView& lv)
      {
	SetMultipleSelection();
	lv.SetHeaders(4);
	lv.SetHeader("",        kTextCenterX, kTextRight, 0);
	lv.SetHeader("Board",   kTextCenterX, kTextRight, 1);
	lv.SetHeader("Chip",    kTextCenterX, kTextRight, 2);
	lv.SetHeader("Channel", kTextCenterX, kTextRight, 3);
	lv.AdjustHeaders();
	lv.SetContainer(this);
	lv.SetViewMode(kLVDetails);
	SetBackgroundColor(GetWhitePixel());
      }
      void AddFrame(TGFrame* f, TGLayoutHints* l) 
      {
	Element* e = new Element(f, l);
	fList->Add(e);
      }
      void AddUserItem(unsigned int b, unsigned int a, unsigned int c)
      {
	unsigned int addr = ActiveChannels::MakeAddress(b, a, c);
	AddUserItem(addr);
      }
      void AddUserItem(unsigned int addr) 
      {
	if (FindItem(addr)) { 
	  Warning("AddUserItem", "Entry for 0x%03x already exists", addr);
	  return;
	}
	Entry* e = new Entry(this, addr);
	AddItem(e);
	fEntries[addr] = e;
      }
      void RemoveUserItem(unsigned int b, unsigned int a, unsigned int c)
      {
	unsigned int addr = ActiveChannels::MakeAddress(b, a, c);
	RemoveUserItem(addr);
      }
      void RemoveUserItem(unsigned int addr) 
      {
	Entry* e = FindItem(addr);
	if (!e) return;
	Data* data  = e->GetAddress();
	RemoveItem(e);
	if (data) {
	  fEntries.erase(data->fAddr);
	  delete data;
	}
	// delete e;
      }
      /** Find an item */
      Entry* FindItem(unsigned int b, unsigned int a, unsigned int c)
      {
	unsigned int addr = ActiveChannels::MakeAddress(b, a, c);
	return FindItem(addr);
      }
      /** Find an item */
      Entry* FindItem(unsigned int addr)
      {
	EntryMap::const_iterator i = fEntries.find(addr);
	if (i == fEntries.end()) return 0;
	return i->second;
      }
      /** Move an item up or down */
      void MoveItem(TGLVEntry* entry, Int_t dir) { 
	if (!entry) return;
	TIter           next(fList);
	TGFrameElement* elem   = 0;
	while ((elem = static_cast<TGFrameElement*>(next()))) 
	  if (elem->fFrame == entry) break;
	if (!elem) return;
	TGFrameElement* before = 
	  static_cast<TGFrameElement*>(fList->Before(elem));
	TGFrameElement* after  = 
	  static_cast<TGFrameElement*>(fList->After(elem));
	fList->Remove(elem);
	if (dir < 0) { 
	  if (before) fList->AddBefore(before, elem);
	  else        fList->AddFirst(elem);
	}
	else { 
	  if (after) fList->AddAfter(after, elem);
	  else       fList->AddLast(elem);
	}
      }
      void Sort() 
      {
	if (!fSorted) return;
	fList->Sort();
	GetListView()->Layout();
      }
      void List() const
      {
	std::cout << "Content of container:" << std::endl;
	for (EntryMap::const_iterator i = fEntries.begin(); 
	     i != fEntries.end(); ++i) 
	  std::cout << std::hex << std::setfill('0') 
		    << "  0x" << std::setw(3) << i->first 
		    << " -> " << std::setfill(' ') << std::dec 
		    << i->second->GetItemName()->GetString() 
		    << std::endl;
      }
      typedef std::map<unsigned int,Entry*> EntryMap;
      EntryMap fEntries;
      /** Whether the view is sorted */
      Bool_t fSorted;
    };

    //________________________________________________________________
    /** Buttons */
    enum {
      /** Add an entry */
      kAdd = 1, 
      /** Remove an entry */
      kRemove, 
      /** Move an entry up */
      kUp,
      /** Move an entry down */
      kDown, 
      /** Write to hard ware */
      kCommit, 
      /** Read from hardware */
      kUpdate, 
      /** Print contents */
      kPrint 
    };
    //________________________________________________________________
    /** List of addresses */
    typedef std::vector<unsigned int> AddrList;

    /** Constructor */
    ActiveChannels(TGTab& tabs, Rcuxx::RcuACL& low, 
		   unsigned int fecmask, const unsigned int* chmask)
      : fLow(low),
	fName(low.Name().c_str()),
	fTop(tabs.AddTab(low.Name().c_str())),
	fCont(fTop, int(fCanvasW*.4), int(fCanvasH*.8)),
	fCanvasW(fTop->GetWidth()-6),
	fCanvasH(fTop->GetHeight()-6),
	fListHints(kLHintsExpandX|kLHintsExpandY, 2, 1, 3, 3),
	fMiddleHints(kLHintsExpandY, 2, 3, 3, 3),
	fLeft(&fCont, "Available"), 
	fAvailable(&fLeft, int(fCanvasW*.3), int(fCanvasH*.6)),
	fAvailableCont(&fAvailable, true), // kTRUE),
	fMiddle(&fCont, ""), 
	fAdd(&fMiddle, "Add", kAdd),
	fRemove(&fMiddle, "Remove", kRemove),
	fUp(&fMiddle, "Up", kUp),
	fDown(&fMiddle, "Down", kDown),
	fCommit(&fMiddle, "Commit", kCommit),
	fUpdate(&fMiddle, "Update", kUpdate),
	fPrint(&fMiddle, "Print", kPrint),
	fRight(&fCont, "Enabled"), 
	fEnabled(&fRight, int(fCanvasW*.3), int(fCanvasH*.6)),
	fEnabledCont(&fEnabled, false) // , kFALSE)
    {
      fAvailableCont.Setup(fAvailable);
      fAvailable.Connect("SelectionChanged()", "RcuGui::ActiveChannels", this,
			 "HandleAvailable()");

      fEnabledCont.Setup(fEnabled);
      fEnabled.Connect("SelectionChanged()", "RcuGui::ActiveChannels", this,
		       "HandleEnabled()");

      fMiddle.Connect("Clicked(Int_t)", "RcuGui::ActiveChannels", this, 
		      "HandleButtons(Int_t)");
      fAdd.SetEnabled(kFALSE);
      fRemove.SetEnabled(kFALSE);
      fUp.SetEnabled(kFALSE);
      fDown.SetEnabled(kFALSE);
    
      fTop->SetLayoutManager(new TGVerticalLayout(fTop));
      fTop->AddFrame(&fCont, 
		     new TGLayoutHints(kLHintsExpandX|kLHintsTop
				       /* |kLHintsExpandY */ ,
				       ISCALE(3), ISCALE(3), 
				       ISCALE(3),ISCALE(3)));
      fCont.AddFrame(&fLeft,      &fListHints);
      fLeft.AddFrame(&fAvailable, &fListHints);
      fCont.AddFrame(&fMiddle,    &fMiddleHints);
      fCont.AddFrame(&fRight,     &fListHints);
      fRight.AddFrame(&fEnabled,  &fListHints);

      for (size_t i = 0; i < 32; i++) { 
	if (fecmask & (1 << 0)) { 
	  for (size_t j = 0; j < 8; j++) { 
	    if (chmask[j] == 0) continue;
	    for (size_t k = 0; k < 16; k++) 
	      if (chmask[j] & (1 << k)) 
		fAvailableCont.AddUserItem(i, j, k);
	  }
	}
      }
      fAvailableCont.Sort();
      // fAvailableCont.List();
    }
    /** Get list of available channels */
    Container& Available() { return fAvailableCont; }
    /** Add an item */
    void AddItem(Container& l, unsigned int b, unsigned int a, unsigned int c) 
    {
      l.AddUserItem(b, a, c);
      // if (l.fSorted) l.Sort();
    }
    /** Add an item */
    void AddItem(Container& l, unsigned int addr) 
    {
      l.AddUserItem(addr);
      // if (l.fSorted) l.Sort();
    }
    /** 
     * Get selected entries 
     * 
     * @param c   Container to look for selection in 
     * @param a   On return, contains the selected items
     */
    void GetSelected(TGLVContainer& c, TObjArray& a)
    {
      void* next = 0;
      do { 
	const Entry* e = static_cast<const Entry*>(c.GetNextSelected(&next));
	if (!e) break;
	a.Add(const_cast<Entry*>(e));
      } while(true);
    }
    /** 
     * Get selected entries 
     * 
     * @param c   Container to look for selection in 
     * @param a   On return, contains the selected items
     * @param l   On return, contains the selected addresses
     */
    void GetSelected(TGLVContainer& c, TObjArray& a, AddrList& l)
    {
      void* next = 0;
      do { 
	const Entry* e = static_cast<const Entry*>(c.GetNextSelected(&next));
	if (!e) break;
	a.Add(const_cast<Entry*>(e));
	Data* v = e->GetAddress();
	l.push_back(v->fAddr);
      } while(true);
    }
    /** 
     * Get selected entries 
     * 
     * @param c   Container to look for selection in 
     * @param l   On return, contains the selected addresses
     */
    void GetSelectedAddress(TGLVContainer& c, AddrList& l)
    {
      void* next = 0;
      do { 
	const Entry* e =  static_cast<const Entry*>(c.GetNextSelected(&next));
	if (!e) break;
	Data* v = e->GetAddress();
	l.push_back(v->fAddr);
      } while(true);
    }
    /** 
     * Handle selections on the left 
     */
    void HandleAvailable()
    {
      // std::cout << "Available selection changed" << std::endl;
      AddrList a;
      GetSelectedAddress(fAvailableCont, a);
      // Print 
      // std::copy(a.begin(), a.end(), 
      // 		std::ostream_iterator<unsigned int>(std::cout, "\t"));
      Bool_t sel = a.size() != 0;
      fAdd.SetEnabled(sel);
    }
    /** 
     * Handle selections on the right 
     */
    void HandleEnabled()
    {
      // std::cout << "Enabled selection changed" << std::endl;
      AddrList a;
      GetSelectedAddress(fEnabledCont, a);
      Bool_t sel = a.size() != 0;
      fUp.SetEnabled(sel);
      fDown.SetEnabled(sel);
      fRemove.SetEnabled(sel);
      if (sel) fAvailableCont.UnSelectAll();
    }
    /** Handle buttons */
    void HandleButtons(Int_t id)
    {
      TGLVEntry* e = 0;
      TObjArray  a;
      AddrList   l;
      if      (id == kAdd)    FlipSelected(fAvailableCont, fEnabledCont);
      else if (id == kRemove) FlipSelected(fEnabledCont, fAvailableCont);
      else if (id == kDown) { 
	GetSelected(fEnabledCont, a, l);
	for (Int_t i = a.GetEntriesFast(); i > 0; i--) {
	  e = static_cast<TGLVEntry*>(a.At(i-1));
	  fEnabledCont.MoveItem(e,1);
	}
	fAvailable.Layout();
	fEnabled.Layout();
      }
      else if (id == kUp) { 
	GetSelected(fEnabledCont, a, l);
	TIter next(&a);
	while ((e = static_cast<TGLVEntry*>(next()))) { 
	  fEnabledCont.MoveItem(e,-1);
	}
	fAvailable.Layout();
	fEnabled.Layout();
      }
      else if (id == kCommit) Commit();
      else if (id == kUpdate) Update();
      else if (id == kPrint)  fLow.Print();
    }
    /** Enable a channel */
    Bool_t EnableChannel(unsigned int board, unsigned int altro, 
			 unsigned int channel)
    {
      return FlipChannel(fAvailableCont, fEnabledCont, 
			 MakeAddress(board, altro, channel));
    }
    /** Enable a channel */
    Bool_t EnableChannel(unsigned int addr)
    {
      return FlipChannel(fAvailableCont, fEnabledCont, addr);
      
    }
    /** Disable a channel */
    Bool_t DisableChannel(unsigned int board, 
			  unsigned int altro, 
			  unsigned int channel)
    {
      return FlipChannel(fEnabledCont, fAvailableCont, 
			 MakeAddress(board, altro, channel));
    }
    /** Check if a channel is enabled */ 
    Bool_t ChannelEnabled(unsigned int addr) 
    { 
      return fEnabledCont.FindItem(addr) != 0;
    }
    /** Check if a channel is enabled */ 
    Bool_t ChannelEnabled(unsigned int board, 
			  unsigned int altro, 
			  unsigned int channel)
    { 
      return ChannelEnabled(MakeAddress(board, altro, channel)) != 0;
    }
    /** Check if a channel is enabled */ 
    Bool_t ChannelAvailable(unsigned int addr) 
    { 
      return (fAvailableCont.FindItem(addr) != 0 || 
	      fEnabledCont.FindItem(addr) != 0);
    }
    /** Check if a channel is available */ 
    Bool_t ChannelAvailable(unsigned int board, 
			    unsigned int altro, 
			    unsigned int channel)
    { 
      return ChannelAvailable(MakeAddress(board, altro, channel));
    }
    /** Enable a chip */ 
    Bool_t EnableChip(unsigned int board, unsigned int chip, unsigned int mask)
    {
      // std::cout << "Enable chip" << std::endl;
      Bool_t ret = kTRUE;
      for (size_t i = 0 ; i < 16; i++) { 
	if (mask & (0x1 << i)) ret = EnableChannel(board, chip, i);
	else                   ret = DisableChannel(board, chip, i);
	if (!ret) return ret;
      }
      return ret;
    }
    
    /** Disable a chip */ 
    Bool_t DisableChip(unsigned int board, unsigned int chip)
    {
      // std::cout << "Disable chip" << std::endl;
      for (size_t i = 0 ; i < 16; i++) 
	if (!DisableChannel(board, chip, i)) return kFALSE;
      return kTRUE;
    }
    /** Enable a chip */ 
    Bool_t EnableBoard(unsigned int board, unsigned int* mask, 
		       unsigned int max)
    {
      // std::cout << "Enable board" << std::endl;
      for (unsigned int chip = 0; chip < max; chip++)
	if (!EnableChip(board, chip, mask[chip])) return kFALSE;
      return kTRUE;
    }
    /** Disable a chip */ 
    Bool_t DisableBoard(unsigned int board)
    {
      // std::cout << "Disalbe board" << std::endl;
      for (unsigned int chip = 0; chip < 8; chip++) 
	if (!DisableChip(board, chip)) return kFALSE;
      return kTRUE;
    }
    /** Enable all possible channels */
    void EnableAll()
    {
      // std::cout << "Enable all" << std::endl;
      FlipAll(fAvailableCont, fEnabledCont);
    }
    /** Disable all possible channels */
    void DisableAll()
    {
      // std::cout << "Disable all" << std::endl;
      FlipAll(fEnabledCont, fAvailableCont);
    }
    /** Decode into low-level values */
    void Get()
    {
      const Rcuxx::RcuACL::ChannelList& l = fLow.Channels();
      for (Rcuxx::RcuACL::ChannelList::const_iterator i = l.begin();
	   i != l.end(); ++i) {
	if (!EnableChannel(*i)) AddItem(fEnabledCont, *i);
      }
      fAvailable.Layout();
      fEnabled.Layout();
      // fAvailableCont.List();
      // WriteText();
    }
    /** Read from hardware */
    Bool_t Update() 
    {
      // std::cout << "Disabling all" << std::endl;
      DisableAll();
      UInt_t ret = fLow.Update();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      Main::Instance()->SetStatus(Form("Read %s", fName.Data()));
      // std::cout << "Getting from low-level" << std::endl;
      Get();
      return kTRUE;
    }
    /** Set low-level memory from GUI */
    void Set()
    {
      fLow.Zero();
      fEnabledCont.SelectAll();
      AddrList l;
      GetSelectedAddress(fEnabledCont, l);

      for (AddrList::const_iterator i = l.begin(); i != l.end(); ++i) {
	unsigned int addr = *i;
	unsigned int board, altro, channel;
	GetAddressParts(*i, board, altro, channel);
	fLow.EnableChannel(board, altro, channel); //
      }
      fEnabledCont.UnSelectAll();
      fAvailable.AdjustHeaders();
      fEnabled.AdjustHeaders();
      // fAvailable.Layout();
      // fEnabled.Layout();
    }
    /** Write to hardware */
    Bool_t Commit() 
    {
      Set();
      UInt_t ret = fLow.Commit();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      Update();
      Main::Instance()->SetStatus(Form("Wrote %s", fName.Data()));
      return kTRUE;
    }
  protected:
    /** Move all elements from src to dest */
    void FlipAll(Container& src, Container& dest)
    {
      src.SelectAll();
      FlipSelected(src, dest);
    }
    /** Move selected entries from src to dest */
    void FlipSelected(Container& src, Container& dest)
    {
      void* next = 0;
      bool enab = (&src == &fAvailableCont);
#if 0
      std::cout << "Flipping selected from " 
		<< (enab ? "Available" : "Enabled") << " to " 
		<< (enab ? "Enabled" : "Available") << std::endl;
#endif
      AddrList  addresses;
      GetSelectedAddress(src, addresses);
      
      for (size_t i = 0; i < addresses.size(); i++) {
	src.RemoveUserItem(addresses[i]);
	dest.AddUserItem(addresses[i]);
      }
      src.UnSelectAll();
      src.GetListView()->AdjustHeaders();
      dest.GetListView()->AdjustHeaders();
      src.Sort();
      dest.Sort();
      // src.GetListView()->Layout();
      // dest.GetListView()->Layout();
    }
    /** Move channel entry from src to dest */
    Bool_t FlipChannel(Container&   src, 
		       Container&   dest, 
		       unsigned int addr)
    {
      Entry* e = src.FindItem(addr);
      if (!e) return kFALSE;

      src.RemoveUserItem(addr);
      dest.AddUserItem(addr);
      src.GetListView()->AdjustHeaders();
      dest.GetListView()->AdjustHeaders();
      src.Sort();
      dest.Sort();
      // src.GetListView()->Layout();
      // dest.GetListView()->Layout();
      return kTRUE;
    }
    /** Pointer to low-level */ 
    Rcuxx::RcuACL& fLow;
    /** Name */
    TString fName;
    /** Top */
    TGCompositeFrame* fTop;
    /** Container  */
    TGHorizontalFrame fCont;
    /** Width */
    UInt_t fCanvasW;
    /** Height */
    UInt_t fCanvasH;

    TGLayoutHints fListHints;
    TGLayoutHints fMiddleHints;
    TGGroupFrame  fLeft;
    TGListView    fAvailable;
    Container     fAvailableCont;
    TGButtonGroup fMiddle;
    TGGroupFrame  fRight;
    TGListView    fEnabled;
    Container     fEnabledCont;
    TGTextButton  fUp;
    TGTextButton  fDown;
    TGTextButton  fRemove;
    TGTextButton  fAdd;
    TGTextButton  fCommit;
    TGTextButton  fUpdate;
    TGTextButton  fPrint;

    ClassDef(ActiveChannels,1)
  };
}

#endif
//
// EOF
//

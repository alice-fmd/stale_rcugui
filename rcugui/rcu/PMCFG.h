// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_PMCFG_H
#define RCUGUI_PMCFG_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuPMCFG.h>


namespace RcuGui
{
  //====================================================================
  /** @class PMCFG
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class PMCFG : public RcuGui::Register
  {
  private:
    Rcuxx::RcuPMCFG&  fLow;
    RcuGui::LabeledIntEntry fBegin;
    RcuGui::LabeledIntEntry fEnd;
  public:
    PMCFG(TGCompositeFrame& f, Rcuxx::RcuPMCFG& low) 
      : RcuGui::Register(f, low), 
	fLow(low),
	fBegin(fFields, "Begin", 0, 0x3ff),
	fEnd(fFields, "End", 0, 0x3ff)
    {
      fGroup.SetTitle("Block");
      Get();
    }
    void Get() 
    {
      fBegin.SetValue(fLow.Begin());
      fEnd.SetValue(fLow.End());
    }
    void Set()
    {
      fLow.SetBegin(fBegin.GetValue());
      fLow.SetEnd(fEnd.GetValue());
    }
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_CDH_H
#define RCUGUI_CDH_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <TGCanvas.h>
# include <rcugui/Scrollable.h>
# include <rcugui/LabeledNumber.h>
# include <rcuxx/rcu/RcuCDH.h>
# include <vector>

namespace RcuGui
{
  //==================================================================
  /** @class CDH
      @ingroup rcugui_low
   */
  class CDH // : public Scrollable 
  {
  public:
    /** Signals */
    enum {
      /** pdate */
      kUpdate,
      /** Print */
      kPrint
    };
    /** Create the register interface tab 
	@param tabs Where to put the tab 
	@param low Low-level interface  */
    CDH(TGTab& tabs, Rcuxx::RcuCDH& low);
    /** Handle buttons 
	@param id of button to handle */
    void Handle(Int_t id);
    /** Update (some of) the register displays */
    Bool_t Update();
    /** Update (some of) the register displays */
    void Print() const;
    /** Write the data buffer into the display */ 
    void WriteText();
  protected:
    /** Low */
    Rcuxx::RcuCDH& fLow;
    /** ELement */
    TGLayoutHints fButtonHints;
    /** ELement */
    TGLayoutHints fViewHints;
    /** ELement */
    TGLayoutHints fTableHints;
    /** ELement */
    TGLayoutHints fLineHints;
    
    /** Composite frame - owned by tabs */
    TGCompositeFrame* fTop;
    /** Container */
    TGHorizontalFrame fBottom;
    /** Canvas */
    TGCanvas fCanvas;
    /** Container */
    TGVerticalFrame fCont;
    /** Button group */
    TGButtonGroup fButtons;
    /** Update button */
    TGTextButton fUpdate;    
    /** Update button */
    TGTextButton fPrint;    
    /** FormatVersion */
    LabeledIntView fFormatVersion;
    /** L1Message */
    LabeledIntView fL1Message;
    /** BunchCrossing */
    LabeledIntView fBunchCrossing;
    /** OrbitNumber */
    LabeledIntView fOrbitNumber;
    /** RcuVersion */
    LabeledIntView fRcuVersion;
    /** DetectorMask */
    LabeledIntView fDetectorMask;
    /** StatusError */
    LabeledIntView fStatusError;
    /** MiniID */
    LabeledIntView fMiniID;
    /** TriggerMask */
    LabeledIntView fTriggerMask1;
    /** TriggerMask */
    LabeledIntView fTriggerMask2;
    /** RegionOfInterest */
    LabeledIntView fRegionOfInterest1;
    /** RegionOfInterest */
    LabeledIntView fRegionOfInterest2;
  };
}

//==================================================================
inline 
RcuGui::CDH::CDH(TGTab& tabs, Rcuxx::RcuCDH& low)
  : fLow(low), 
    fButtonHints(kLHintsExpandY,ISCALE(3), ISCALE(3), ISCALE(3), ISCALE(3)),
    fViewHints(kLHintsExpandX,0, 0, 0, 0),
    fTableHints(kLHintsExpandY|kLHintsExpandX, ISCALE(3), ISCALE(3), 
		ISCALE(3), ISCALE(3)),
    fLineHints(kLHintsExpandX),
    fTop(tabs.AddTab("CDH")),
    fBottom(fTop),
    fCanvas(&fBottom, ISCALE(fTop->GetWidth()-300), 
	    ISCALE(fTop->GetHeight()-100),  kChildFrame|kSunkenFrame),
    fCont(fCanvas.GetViewPort()),
    fButtons(&fBottom, "Operations", kChildFrame|kVerticalFrame),
    fUpdate(&fButtons, "Update", 0),
    fPrint(&fButtons, "Print", 1),
    fFormatVersion(fCont, "Format Version",   0, 0xFF,  kHorizontalFrame|kHex),
    fL1Message(fCont,     "L1 Message",       0, 0xFF,  kHorizontalFrame|kHex),
    fBunchCrossing(fCont, "Bunch Crossing",   0, 0xFFF, kHorizontalFrame),
    fOrbitNumber(fCont,   "Orbit Number",     0, 0xFFFFFF, kHorizontalFrame),
    fRcuVersion(fCont,    "RCU Data Version", 0, 0xFF,  kHorizontalFrame),
    fDetectorMask(fCont,  "Detector Mask",    0, 0xFFFFFF, 
		  kHorizontalFrame|kHex),
    fStatusError(fCont,   "Status & Error",   0, 0xFFFF,kHorizontalFrame|kHex),
    fMiniID(fCont,        "Mini ID",          0, 0xFFF, kHorizontalFrame),
    fTriggerMask1(fCont,   "Trigger Mask [31:0]",     0, 0xFFFFFFFF, 
		  kHorizontalFrame|kHex),
    fTriggerMask2(fCont,   "Trigger Mask [49:32]",     0, 0x3FFFF, 
		  kHorizontalFrame|kHex),
    fRegionOfInterest1(fCont, "Region Of Interest [35:4]", 0, 0xFFFFFFFF, 
		       kHorizontalFrame|kHex),
    fRegionOfInterest2(fCont, "Region Of Interest [0:3]", 0, 0xF, 
		       kHorizontalFrame|kHex)

{
  fTop->SetLayoutManager(new TGVerticalLayout(fTop));
  fTop->AddFrame(&fBottom, &fTableHints);
  fBottom.AddFrame(&fCanvas, &fViewHints);
  fBottom.AddFrame(&fButtons, &fButtonHints);
  fCanvas.SetContainer(&fCont);
  fButtons.Connect("Clicked(Int_t)", "RcuGui::CDH", this, "Handle(Int_t)");
  WriteText();
}

//____________________________________________________________________
inline void
RcuGui::CDH::Handle(Int_t oper) 
{
  switch (oper) {
  case kUpdate:  Update(); break;
  case kPrint:   Print();  break;
  }
}

//____________________________________________________________________
inline Bool_t 
RcuGui::CDH::Update() 
{
  UInt_t ret = fLow.Update();
  if (ret) {
    Main::Instance()->Error(ret);
    return kFALSE;
  }
  Main::Instance()->SetStatus(Form("Read CDH"));
  WriteText();
  return kTRUE;
}

//____________________________________________________________________
inline void 
RcuGui::CDH::Print() const
{
  fLow.Print();
}
//____________________________________________________________________
inline void 
RcuGui::CDH::WriteText()
{
    fFormatVersion.SetValue(fLow.FormatVersion());
    fL1Message.SetValue(fLow.L1Message());
    fBunchCrossing.SetValue(fLow.BunchCrossing());
    fOrbitNumber.SetValue(fLow.OrbitNumber());
    fRcuVersion.SetValue(fLow.RcuVersion());
    fDetectorMask.SetValue(fLow.DetectorMask());
    fStatusError.SetValue(fLow.StatusError());
    fMiniID.SetValue(fLow.MiniID());
    fTriggerMask1.SetValue((fLow.TriggerMask() >> 0)  & 0xFFFFFFFF);
    fTriggerMask2.SetValue((fLow.TriggerMask() >> 32) & 0x3FFFF);
    fRegionOfInterest1.SetValue((fLow.RegionOfInterest() >> 4) & 0xFFFFFFFF);
    fRegionOfInterest2.SetValue((fLow.RegionOfInterest() >> 0) & 0xF);
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_MEBSTCNT_H
#define RCUGUI_MEBSTCNT_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuMEBSTCNT.h>
# include <climits>


namespace RcuGui
{
  //====================================================================
  /** @class MEBSTCNT
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class MEBSTCNT : public Register
  {
  private:
    Rcuxx::RcuMEBSTCNT&  fLow;
    LabeledIntView fCounts;
    LabeledIntView fDataReady;
    ErrorBit       fDM_SEL1;
    ErrorBit       fDM_SEL2;
  public:
    MEBSTCNT(TGCompositeFrame& f, Rcuxx::RcuMEBSTCNT& low) 
      : Register(f, low), fLow(low),
	fCounts(fFields, "Counts", 0, 0xf), 
	fDataReady(fFields, "Data ready", 0, 0xf),
	fDM_SEL1(fFields,"Data memory 1", "Data memory 1 selected", 
		 0x00aa00, 0xc0c0c0),	
	fDM_SEL2(fFields,"Data memory 2", "Data memory 2 selected", 
		 0x00aa00, 0xc0c0c0)

    {
      fGroup.SetTitle("Multi-event buffer counter & debug");
      Get();
    }
    void Get() 
    {
      fCounts.SetValue(fLow.Counts());
      fDataReady.SetValue(fLow.DataReady());
      fDM_SEL1.SetState(fLow.DM1Selected());
      fDM_SEL2.SetState(fLow.DM2Selected());
    }
  };
}
#endif
//
// EOF
//

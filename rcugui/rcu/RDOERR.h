// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_RDOERR_H
#define RCUGUI_RDOERR_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuRDOERR.h>


namespace RcuGui
{
  //__________________________________________________________________
  /** @class RDOERR
      @brief Interface to the ERRor and STatus register
      @ingroup rcugui
  */
  class RDOERR : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    RDOERR(TGCompositeFrame& f, Rcuxx::RcuRDOERR& low) 
      : Register(f, low), 
	fLow(low),
	fSend(fFields, "Send", "Error during send command", 
	      0xdd0000, 0xc0c0c0),
	fAltroError(fFields, "AltroError", 
		    "Altro error during wait for transfer", 
		    0xdd0000, 0xc0c0c0),
	fNoTrsf(fFields, "NoTrsf", "No transfer strobe asserted", 
		0xdd0000, 0xc0c0c0),
	fMissDstb(fFields, "MissDstb", "Less than 4 data strobes", 
		  0xdd0000, 0xc0c0c0),
	fNoRelease(fFields, "NoRelease", "No release of the transfer line", 
		   0xdd0000, 0xc0c0c0),
	fSCEVL(fFields, "SCEVL", "Error during send of scan event length", 
	       0xdd0000, 0xc0c0c0),
	fRDRX(fFields, "RDRX", "RD_RX not (de)asserted", 
	      0xdd0000, 0xc0c0c0),
	fChannelAddress(fFields, "ChannelAddress", "Channel address mismatch", 
			0xdd0000, 0xc0c0c0),
	fBlockLength(fFields, "BlockLength", "Block length mismatch", 
		     0xdd0000, 0xc0c0c0),
	fWR_ST_B(fFields, "Write FSM branch B state ", 0, 0xf,
		 ISCALE(3), ISCALE(3), ISCALE(12), ISCALE(2)),
	fWR_ST_A(fFields, "Write FSM branch A state ", 0, 0xf,
		 ISCALE(3), ISCALE(3), ISCALE(12), ISCALE(2)),
	fScan(fFields, "Scan event length FSM state ", 0, 0xf,
	      ISCALE(3), ISCALE(3), ISCALE(12), ISCALE(2))
    {
      fGroup.SetTitle("Read-out errors");
      Get();
    }
    void Get() 
    {
      fSend.SetState(fLow.IsSend());
      fAltroError.SetState(fLow.IsAltroError());
      fNoTrsf.SetState(fLow.IsNoTrsf());
      fMissDstb.SetState(fLow.IsMissDstb());
      fNoRelease.SetState(fLow.IsNoRelease());
      fSCEVL.SetState(fLow.IsSCEVL());
      fRDRX.SetState(fLow.IsRDRX());
      fChannelAddress.SetState(fLow.IsChannelAddress());
      fBlockLength.SetState(fLow.IsBlockLength());
      fWR_ST_B.SetValue(fLow.WR_ST_B()); 
      fWR_ST_A.SetValue(fLow.WR_ST_A());
      fScan.SetValue(fLow.Scan());
    }
  private:
    Rcuxx::RcuRDOERR& fLow;
    /** Error during send command */
    ErrorBit fSend;
    /** Altro error during wait for transfer */
    ErrorBit fAltroError;
    /** No transfer strobe asserted */
    ErrorBit fNoTrsf;
    /** Less than 4 data strobes */
    ErrorBit fMissDstb;
    /** No release of the transfer line */
    ErrorBit fNoRelease;
    /** Error during send of scan event length */
    ErrorBit fSCEVL;
    /** RD_RX not (de)asserted */
    ErrorBit fRDRX;
    /** Channel address mismatch */
    ErrorBit fChannelAddress;
    /** Block length mismatch*/
    ErrorBit fBlockLength;
    /** Write FSM branch B state */
    LabeledIntView fWR_ST_B; 
    /** Write FSM branch A state */
    LabeledIntView fWR_ST_A;
    /** Scan event length FSM state */
    LabeledIntView fScan;
#if 0
    ErrorBit fChannelAddress;
    ErrorBit fBlockLength;
    ErrorBit fRDRX;
    LabeledIntView fScan;
#endif
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_RESULTMEMORY_H
#define RCUGUI_RESULTMEMORY_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <TGListView.h>
# include <TGButtonGroup.h>
# include <TObjArray.h>
# include <rcugui/rcu/Memory.h>
# include <rcuxx/rcu/RcuRMEM.h>
# include <vector>
# include <rcuxx/DebugGuard.h>

namespace RcuGui
{
  /** @class ResultMemory 
      @ingroup rcugui_low
      @brief Interface to result memory (RMEM)
  */
  class ResultMemory 
  {
  public:
    //________________________________________________________________
    /** Buttons */
    enum {
      /** Read from hardware */
      kUpdate, 
      /** Print contents */
      kPrint 
    };

    /** Constructor 
	@param tabs Reference to tab container 
	@param low  Low-level interface */
    ResultMemory(TGTab& tabs, Rcuxx::RcuRMEM& low)
      : fLow(low),
	fName(low.Name().c_str()),
	fTop(tabs.AddTab(low.Name().c_str())),
	fContHints(kLHintsExpandX|kLHintsExpandY, 3, 3, 3, 3),
	fCont(fTop),
	fCanvasW(fTop->GetWidth()-6),
	fCanvasH(fTop->GetHeight()-6),
	fContentHints(kLHintsExpandX, 3, 1, 3, 3),
	fButtonsHints(kLHintsExpandY, 2, 3, 3, 3),
	fContent(&fCont, int(fCanvasW*.7), int(fCanvasH*.8)),
	fContainer(&fContent), /* int(fCanvasW*.7), int(fCanvasH*.8),
				  kSunkenFrame, tabs.GetWhitePixel()), */
	fButtons(&fCont, ""), 
	fUpdate(&fButtons, "Update", kUpdate),
	fPrint(&fButtons, "Print", kPrint),
	fErrBig(0),
	fErrSmall(0),
	fOkBig(0),
	fOkSmall(0)
    {
      fContainer.SetMultipleSelection(kFALSE);
      fContent.SetHeaders(4);
      fContent.SetHeader("Number",  kTextCenterX, kTextRight, 0);
      fContent.SetHeader("Type",    kTextCenterX, kTextRight, 1);
      fContent.SetHeader("What",    kTextCenterX, kTextRight, 2);
      fContent.SetHeader("Data",    kTextCenterX, kTextRight, 3);
      fContent.AdjustHeaders();
      fContent.SetContainer(&fContainer);
      fContainer.SetBackgroundColor(fContent.GetWhitePixel());
      fContent.SetViewMode(kLVDetails);

      fButtons.Connect("Clicked(Int_t)", "RcuGui::ResultMemory", this, 
		       "HandleButtons(Int_t)");
    
      fTop->SetLayoutManager(new TGVerticalLayout(fTop));
      fTop->AddFrame(&fCont, &fContHints);
      fCont.AddFrame(&fContent, &fContentHints);
      fCont.AddFrame(&fButtons, &fButtonsHints);

      UInt_t small = 16;
      UInt_t big = 32;
      TGPicturePool* pool = gClient->GetPicturePool();
      fOkBig      = pool->GetPicture("mb_asterisk_s.xpm",    big,   big);
      fOkSmall    = pool->GetPicture("mb_asterisk_s.xpm",    small, small);
      fErrBig     = pool->GetPicture("mb_stop_s.xpm",        big,   big);
      fErrSmall   = pool->GetPicture("mb_stop_s.xpm",        small, small);
      fStopBig    = pool->GetPicture("stop_t.xpm",           big,   big);
      fStopSmall  = pool->GetPicture("stop_t.xpm",           small, small);
      fLoopBig    = pool->GetPicture("tb_refresh.xpm",       big,   big);
      fLoopSmall  = pool->GetPicture("tb_refresh.xpm",       small, small);
      fWaitBig    = pool->GetPicture("tb_refresh.xpm",       big,   big);
      fWaitSmall  = pool->GetPicture("tb_refresh.xpm",       small, small);
      fReadBig    = pool->GetPicture("tb_back.xpm",          big,   big);
      fReadSmall  = pool->GetPicture("tb_back.xpm",          small, small);
      fWriteBig   = pool->GetPicture("tb_forw.xpm",          big,   big);
      fWriteSmall = pool->GetPicture("tb_forw.xpm",          small, small);
      fCmdBig     = pool->GetPicture("tmacro_t.xpm",         big,   big);
      fCmdSmall   = pool->GetPicture("tmacro_t.xpm",         small, small);
      fQuitBig    = pool->GetPicture("quit.xpm",             big,   big);
      fQuitSmall  = pool->GetPicture("quit.xpm",             small, small);

      // Get();
    }
    /** Handle buttons 
	@param id Button to handle */
    void HandleButtons(Int_t id)
    {
      if      (id == kUpdate) Update();
      else if (id == kPrint)  fLow.Print();
    }
    /** Decode into low-level values */
    void Get()
    {
      bool   elab  = fLow.Max() > 0xfffff;
      size_t zeros = 0;
      const Rcuxx::RcuMemory::Cache_t& data = fLow.Data();
      for (size_t i = 0; i < data.size(); i++) { 
	TGLVEntry* e = 0;
	if (!elab) { 
	  if (data[i] == 0x0) zeros++;
	  e = new TGLVEntry(&fContainer, Form("%5d", i), "");
	  e->SetSubnames("", "", Form("0x%05x", data[i]));
	}
	else { 
	  if (data[i] >= 0xFFFFFF) zeros = 3;
	  unsigned int num  = i;
	  char         type = '?';
	  unsigned int res  = 0;
	  bool         err  = (data[i] >> 20) & 0x1;
	  unsigned int what = (data[i] & 0xfffff);
	  const TGPicture* bp = 0; // (err ? fErrBig   : fOkBig);
	  const TGPicture* sp = 0; // (err ? fErrSmall : fOkSmall);
	  switch (data[i] >> 21) { 
	  case 0x0: type = 'r'; res = (data[++i] & 0xfffff); 
	    bp = fReadBig; sp = fReadSmall; break;
	  case 0x1: type = 'c'; res = (data[++i] & 0xfffff); 
	    bp = fCmdBig; sp = fCmdSmall; break;
	  case 0x2: type = 'w'; res = (data[++i] & 0xfffff);
	    bp = fWriteBig; sp = fWriteSmall; break;
	  case 0x4: type = 'l'; bp = fLoopBig; sp = fLoopSmall; break;
	  case 0x5: type = 'h'; bp = fWaitBig; sp = fWaitSmall; break;
	  case 0x6: type = 'e'; bp = fOkBig;   sp = fOkSmall;   break;
	  case 0x7: type = 'm'; bp = fStopBig; sp = fStopSmall; break;
	  case 0xf: type = 'o'; bp = fQuitBig; sp = fQuitSmall; break;
	  }
	  if (err) { bp = fErrBig ; sp = fErrSmall; }
	  e = new TGLVEntry(&fContainer, Form("%5d", num), "");
	  const char* sres = (type == 'r' || type == 'c' || type == 'w' ? 
			     Form("0x%05x", res) : "n/a");
	  e->SetSubnames(Form("%c", type), 
			 Form("0x%05x", what), 
			 sres);
	  if (bp && sp) e->SetPictures(bp, sp);
	}
	if (e) fContainer.AddItem(e);
	if (zeros >= 3) break;
      }
      fContainer.UnSelectAll();
      fContent.AdjustHeaders();
      fContent.Layout();
    }
    /** Read from hardware */
    Bool_t Update() 
    {
      fContainer.RemoveAll();
      UInt_t ret = fLow.Update();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      Main::Instance()->SetStatus(Form("Read %s", fName.Data()));
      Get();
      return kTRUE;
    }
  protected:
    /** Pointer to low-level */ 
    Rcuxx::RcuRMEM& fLow;
    /** Name */
    TString fName;
    /** Top */
    TGCompositeFrame* fTop;
    /** Container  */
    TGLayoutHints fContHints;
    TGHorizontalFrame fCont;
    /** Width */
    UInt_t fCanvasW;
    /** Height */
    UInt_t fCanvasH;
    /** Contents hints */
    TGLayoutHints fContentHints;
    /** Content view */
    TGListView    fContent;
    /** Content Container */
    TGLVContainer fContainer;
    /** Button hints */
    TGLayoutHints fButtonsHints;
    /** Buttons */
    TGButtonGroup fButtons;
    /** Update button  */
    TGTextButton  fUpdate;
    /** Print button  */
    TGTextButton  fPrint;

    /** Error picture - big */
    const TGPicture* fErrBig;
    /** Error picture - small */
    const TGPicture* fErrSmall;
    /** Stop picture - big */
    const TGPicture* fStopBig;
    /** Stop picture - small */
    const TGPicture* fStopSmall;
    /** OK picture - big */
    const TGPicture* fOkBig;
    /** OK picture - small */
    const TGPicture* fOkSmall;

    /** WRITE picture - big */
    const TGPicture* fWriteBig;
    /** WRITE picture - small */
    const TGPicture* fWriteSmall;
    /** READ picture - big */
    const TGPicture* fReadBig;
    /** READ picture - small */
    const TGPicture* fReadSmall;
    /** CMD picture - big */
    const TGPicture* fCmdBig;
    /** CMD picture - small */
    const TGPicture* fCmdSmall;
    /** LOOP picture - big */
    const TGPicture* fLoopBig;
    /** LOOP picture - small */
    const TGPicture* fLoopSmall;
    /** WAIT picture - big */
    const TGPicture* fWaitBig;
    /** WAIT picture - small */
    const TGPicture* fWaitSmall;
    /** QUIT picture - big */
    const TGPicture* fQuitBig;
    /** QUIT picture - small */
    const TGPicture* fQuitSmall;


    ClassDef(ResultMemory,1)
  };
}
#endif
//
// EOF
//

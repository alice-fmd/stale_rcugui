// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_TRCFG2_H
#define RCUGUI_TRCFG2_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/rcu/Memory.h>
# include <rcuxx/rcu/RcuTRCFG2.h>

namespace RcuGui
{
  //____________________________________________________________________
  /** @class TRCFG2 
      @brief Interface to the TRigger ConFiG register 
      @ingroup rcugui
  */
  class TRCFG2 : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    TRCFG2(TGCompositeFrame& f, Rcuxx::RcuTRCFG2& low) 
      : RcuGui::Register(f, low), 
	fLow(low),
	fOpts(&fFields, "Options", kVerticalFrame),
	fHW(&fOpts, "Enable hardware trigger"),
	fPush(&fOpts, "ALTRO push mode")
    {
      fGroup.SetTitle("Trigger configuration 2");
      fFields.AddFrame(&fOpts);
      Get();
    }
    void Get()
    {
      fHW.SetDown(fLow.IsEnableHT());
      fPush.SetDown(fLow.Mode() == Rcuxx::RcuTRCFG2::kPush);
    }
    void Set()
    {
      fLow.SetEnableHT(fHW.GetState() == kButtonDown);
      fLow.SetMode((fPush.GetState() == kButtonDown ? 
		     Rcuxx::RcuTRCFG2::kPush : 
		     Rcuxx::RcuTRCFG2::kPop));
    }
  private:
    /** Parent frame for writeable bits */
    Rcuxx::RcuTRCFG2& fLow;
    TGButtonGroup    fOpts;
    TGCheckButton    fHW;
    TGCheckButton    fPush;
  };
}
#endif
//
// EOF
//

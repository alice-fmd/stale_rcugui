// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_PEDESTALCONFIG_H
#define RCUGUI_PEDESTALCONFIG_H
# include <TGFrame.h>
# include <TGButton.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/Scrollable.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuPMCFG.h>
# include <rcuxx/rcu/RcuIMEM.h>
# include <rcuxx/rcu/RcuERRST.h>
# include <sstream>

namespace RcuGui
{
  //====================================================================
  /** @class PMCFG
      @brief Interface to the Pedestal Memory ConFiGuration
      @ingroup rcugui_low
  */
  class PedestalConfig : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f 
	@param low 
	@param imem 
	@param errst 
	@return  */
    PedestalConfig(TGCompositeFrame& f, Rcuxx::RcuPMCFG& low, 
		   Rcuxx::RcuIMEM& imem, Rcuxx::RcuERRST& errst);
    /** Handle broadcast */
    void   HandleBroadcast();
    /** Handle block option */
    void   HandleBlock();
    /** Do it. 
	@param cmd Command */
    Bool_t Execute(unsigned int cmd); 
    /** Varify */
    Bool_t Verify();
    /** Commit */
    Bool_t Commit() { return Execute(0x380000); }
    /** Get values */
    void   Get();
    /** Set values */
    void   Set();
  private:
    /** Low-level interface */
    Rcuxx::RcuPMCFG&  fLow;
    /** Instruction memory */
    Rcuxx::RcuIMEM&   fIMEM;
    /** Error */ 
    Rcuxx::RcuERRST&  fERRST;
    /** Elements */
    TGLayoutHints           fBaseHints;
    /** Base */
    TGVerticalFrame         fBase;
    /** Elements */
    TGLayoutHints           fAddressHints;
    /** Elements */
    TGHorizontalFrame       fAddress;
    /** Elements */
    TGLayoutHints           fCheckHints;
    /** Elements */
    TGCheckButton           fBroadcast;
    /** Elements */
    RcuGui::LabeledIntEntry fBoard;
    /** Elements */
    RcuGui::LabeledIntEntry fChip;
    /** Elements */
    RcuGui::LabeledIntEntry fChannel;
    /** Elements */
    TGHorizontalFrame       fOptions;
    /** Elements */
    RcuGui::ErrorBit        fStatus;
    /** Elements */
    TGCheckButton           fBlock;
    /** Elements */
    RcuGui::LabeledIntEntry fBegin;
    /** Elements */
    RcuGui::LabeledIntEntry fEnd;
    /** Elements */
    TGTextButton            fVerify;
    /** Elements */
    unsigned int fAddr;
  };
}

//====================================================================
inline
RcuGui::PedestalConfig::PedestalConfig(TGCompositeFrame& f, 
				       Rcuxx::RcuPMCFG& low, 
				       Rcuxx::RcuIMEM&  imem, 
				       Rcuxx::RcuERRST& errst) 
  : RcuGui::Register(f, low), 
    fLow(low), 
    fIMEM(imem), 
    fERRST(errst),
    fBaseHints(kLHintsExpandX|kLHintsExpandY),
    fBase(&fFields),
    fAddressHints(kLHintsExpandX, 0, 0, ISCALE(3), ISCALE(3)),
    fAddress(&fBase),
    fCheckHints(kLHintsLeft, ISCALE(3), ISCALE(6), 0, 0),
    fBroadcast(&fAddress, "Broadcast"),
    fBoard(fAddress, "Board",   0, 0x1f),
    fChip(fAddress, "Chip",    0, 0x07),
    fChannel(fAddress, "Channel", 0, 0xff),
    fOptions(&fBase),
    fStatus(fGroup, "Verified", 
	    "Pattern memory of chip(s) didn't match PMEM"),
    fBlock(&fOptions, "Block"),
    fBegin(fOptions,"Begin", 0, 0x3ff),
    fEnd(fOptions,"End", 0, 0x3ff),
    fVerify(&fButtons, "Verify")
{
  fGroup.SetTitle("Pedestal block configuration");
  fFields.AddFrame(&fBase, &fBaseHints);
  fBase.AddFrame(&fAddress, &fAddressHints);
  fBroadcast.Connect("Clicked()", "RcuGui::PedestalConfig", this, 
		     "HandleBroadcast()");
  fAddress.AddFrame(&fBroadcast, &fCheckHints);
  
  fBase.AddFrame(&fOptions, &fAddressHints);
  fBlock.Connect("Clicked()", "RcuGui::PedestalConfig", this, "HandleBlock()");
  fOptions.AddFrame(&fBlock, &fCheckHints);

  fVerify.Connect("Clicked()", "RcuGui::PedestalConfig", this, "Verify()");
  fButtons.AddFrame(&fVerify, &fButtonHints);
  fBlock.SetState(kButtonUp);
  fBroadcast.SetState(kButtonUp);
  HandleBroadcast();
  HandleBlock();
  Get();
}
//____________________________________________________________________
inline void 
RcuGui::PedestalConfig::HandleBroadcast() 
{
  fBoard.SetEnabled(!fBroadcast.IsDown());
  fChip.SetEnabled(!fBroadcast.IsDown());
  fChannel.SetEnabled(!fBroadcast.IsDown());
  fVerify.SetEnabled(!fBroadcast.IsDown());
}
//____________________________________________________________________
inline void 
RcuGui::PedestalConfig::HandleBlock() 
{
  fBegin.SetEnabled(fBlock.IsDown());
  fEnd.SetEnabled(fBlock.IsDown());
}
//____________________________________________________________________
inline Bool_t 
RcuGui::PedestalConfig::Execute(unsigned int cmd) 
{
  Set();
  unsigned int ret = 0;
  static unsigned int idata[] = { 0x0, 0x390000 };
  static unsigned int store[2];
  try {
    unsigned int block = (fBlock.IsDown() ? 1 << 13 : 0);
    if (block > 0 && (ret = RcuGui::Register::Commit()) != 0) throw ret;
    
    fIMEM.Update(0, 2);
    fIMEM.Get(0, 2, store);
    idata[0]  = (cmd + fAddr + (block ? (1 << 13) : 0));
    fIMEM.Set(0, 2, idata);
    if ((ret = fIMEM.Commit(0, 2)))   throw ret;
    if ((ret = fIMEM.Exec(0))) throw ret;
  }
  catch (unsigned int& r) {
    RcuGui::Main::Instance()->Error(ret);
    return kFALSE;
  }
  fIMEM.Set(0, 2, store);
  fIMEM.Commit(0, 2);
  RcuGui::Main::Instance()->SetStatus(Form("Executed %s", 
					   fLow.Name().c_str()));
  return ret == 0;
}

//____________________________________________________________________
inline Bool_t 
RcuGui::PedestalConfig::Verify()
{
  fStatus.SetState(kTRUE);
  Execute(0x370000);
  if (!&fERRST) return true;
  fStatus.SetState(!fERRST.IsPattern());
  if (fERRST.IsPattern()) {
    std::stringstream s;
    s << "Error when verifying pattern memory of " 
      << "board " << fBoard.GetValue() << ", chip " << fChip.GetValue()
      << ", channel " << fChannel.GetValue() << std::endl;
    Main::Instance()->SetError(s.str().c_str());
    fERRST.Clear();
    return false;
  }
  return true;
}

//____________________________________________________________________
inline void 
RcuGui::PedestalConfig::Get() 
{
  fStatus.SetState(kTRUE);
  fBegin.SetValue(fLow.Begin());
  fEnd.SetValue(fLow.End());
}
//____________________________________________________________________
inline void 
RcuGui::PedestalConfig::Set()
{
  if (fBroadcast.IsDown()) 
    fAddr = 0x1000;
  else
    fAddr = (((fBoard.GetValue()   & 0x1f) << 7) + 
	     ((fChip.GetValue()    & 0x7)  << 4) + 
	     ((fChannel.GetValue() & 0xF)  << 0));
  fLow.SetBegin(fBegin.GetValue());
  fLow.SetEnd(fEnd.GetValue());
}
#endif
//
// EOF
//

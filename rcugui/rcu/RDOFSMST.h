// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_RDOFSMST_H
#define RCUGUI_RDOFSMST_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/rcu/StateDisplay.h>
# include <rcuxx/rcu/RcuRDOFSMST.h>
# include <climits>


namespace RcuGui
{
  //====================================================================
  /** @class RDOFSMST
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class RDOFSMST : public Register
  {
  private:
    Rcuxx::RcuRDOFSMST&  fLow;
    StateDisplay fSCEVLEN;
    StateDisplay fEVLEN;
    StateDisplay fEVRDO_CTRL;
    StateDisplay fSEL_BR_B;
    StateDisplay fSEL_BR_A;
  public:
    RDOFSMST(TGCompositeFrame& f, Rcuxx::RcuRDOFSMST& low) 
      : Register(f, low), fLow(low),
	fSCEVLEN(fFields,fLow.SCEVLEN()), 	
	fEVLEN(fFields,fLow.EVLEN()), 	
	fEVRDO_CTRL(fFields,fLow.EVRDO_CTRL()), 	
	fSEL_BR_B(fFields,fLow.SEL_BR_B()), 	
	fSEL_BR_A(fFields,fLow.SEL_BR_A())	
    {
      // "Arbitrator FSM"
      Get();
    }
    void Get() 
    {
      fSCEVLEN.Update();
      fEVLEN.Update();
      fEVRDO_CTRL.Update();
      fSEL_BR_B.Update();
      fSEL_BR_A.Update();
    }
  };
}
#endif
//
// EOF
//

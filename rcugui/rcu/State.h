// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_STATE_H
#define RCUGUI_STATE_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#include <rcuxx/rcu/RcuState_t.h>
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif
#ifndef ROOT_TMath
# include <TMath.h>
#endif
#ifndef TGNumberEntry
# include <TGNumberEntry.h>
#endif
#ifndef TString
# include <TString.h>
#endif

class TGLayoutHints;

namespace RcuGui 
{

  //====================================================================
  /** @struct StateDisplay 
      @brief Bit to be toggled on errors.   Cannot be set.
      @ingroup rcugui_basic
   */
  struct StateDisplay : public TGCompositeFrame
  {
    /** Constructor 
	@param p 
	@param name 
	@param desc 
	@param okColour 
	@param badColour  */
    StateDisplay(TGCompositeFrame& p, const Rcuxx::RcuState_t& low)
      : TGCompositeFrame(p, kHorizontalFrame), 
	fLow(low), 
	fHints(kLHintsExpandX, 2, 2, 1, 1),
	fName(this, low.Name()), 
	fDisplay(this, low.AsString())
    {
      AddFrame(&fName, &fHints);
      AddFrame(&fDisplay, &fHints);
      fDisplay.SetEnabled(kFALSE);
    }
    /** Destructor */
    virtual ~StateDisplay() {}
    
    /** Handle button */
    virtual void HandleButton();
    /** @param ok New state  */
    void Update() { 
      std::cout << "State of " << fLow.Name() << " is " << fLow.AsString()
		<< std::endl;
      fDiplay.SetText(fLow.AsString());
    }
  protected:
    const RcuState_t& fLow;
    /** Name Layout hints */
    TGLayoutHints    fHints;
    /** Name */
    TGLabel          fName;
    /** StateDisplay */
    TGTextEntry      fDisplay;
  };
  
}
#endif
//
// EOF
//

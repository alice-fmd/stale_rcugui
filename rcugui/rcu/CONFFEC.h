// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_CONFFEC_H
#define RCUGUI_CONFFEC_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuCommand.h>


namespace RcuGui
{
  //________________________________________________________________
  class CONFFEC  : public RcuGui::Command 
  { 
  public:
    CONFFEC(TGCompositeFrame& p, Rcuxx::RcuCommand& low)
      : Command(p,low,"Configure front-end card firmware"), 
	fBoard(p, "Board", 0, 0x1F)
    {}
    void Commit() 
    {
      Rcuxx::RcuCommand& c = static_cast<Rcuxx::RcuCommand&>(fLow);
      c.Commit(fBoard.GetValue() & 0x1F);
    }
  protected:
    LabeledIntEntry fBoard;
  };
}
#endif
//
// EOF
//

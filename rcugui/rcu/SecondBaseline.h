// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_SECONDBASELINE_H
#define RCUGUI_SECONDBASELINE_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/LabeledNumber.h>

namespace RcuGui
{
  struct SecondBaseline : public TGGroupFrame 
  {
    SecondBaseline(TGCompositeFrame& p) 
      : TGGroupFrame(&p, "2nd baseline", kVerticalFrame),
	fHints(kLHintsExpandX|kLHintsExpandY),
	fEnableHints(kLHintsExpandX|kLHintsTop),
	fEnable(this, "Enable", 0),
	fPre(*this, "Pre excl.", 0, 0x3, kHorizontalFrame),
	fPost(*this, "Post excl.", 0, 0xf, kHorizontalFrame)
    {
      p.AddFrame(this, &fHints);
      AddFrame(&fEnable, &fEnableHints);
      fPre.SetEnabled(kFALSE);
      fPost.SetEnabled(kFALSE);
      fEnable.Connect("Toggled(Bool_t)", "RcuGui::LabeledIntEntry", 
		      &fPre, "SetEnabled(Bool_t)");
      fEnable.Connect("Toggled(Bool_t)", "RcuGui::LabeledIntEntry", 
		      &fPost, "SetEnabled(Bool_t)");
    }
    virtual ~SecondBaseline()
    {
    }
    void Get(bool enabled, unsigned short pre, unsigned short post)
    {
      fEnable.SetState(enabled ? kButtonDown : kButtonUp);
      fPre.SetValue(pre);
      fPost.SetValue(post);
    }
    void Set(bool& enabled, unsigned short& pre, unsigned short& post)
    {
      enabled = fEnable.IsOn();
      pre     = fPre.GetValue();
      post    = fPost.GetValue();
    }
  protected:
    TGLayoutHints      fHints;
    TGLayoutHints      fEnableHints;
    TGCheckButton      fEnable;
    LabeledIntEntry    fPre;
    LabeledIntEntry    fPost;
  };
}
#endif
//
// EOF
//

    
      
      
      

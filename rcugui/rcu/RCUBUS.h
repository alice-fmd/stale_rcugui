// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_RCUBUS_H
#define RCUGUI_RCUBUS_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcuxx/rcu/RcuRCUBUS.h>


namespace RcuGui
{
  //====================================================================
  /** @class RCUBUS
      @brief Interface to the INTerrupt MODe
      @ingroup rcugui
  */
  class RCUBUS : public Register
  {
  private:
    Rcuxx::RcuRCUBUS&  fLow;
    TGLayoutHints fTips;
    TGCheckButton fDCS;
    TGCheckButton fSIU;
  public:
    RCUBUS(TGCompositeFrame& f, Rcuxx::RcuRCUBUS& low) 
      : Register(f, low), 
	fLow(low),
	fTips(kLHintsExpandX, ISCALE(2), ISCALE(2)),
	fDCS(&fFields, "DCS"),
	fSIU(&fFields, "SIU")
    {
      fGroup.SetTitle("Bus grant");
      fDCS.SetToolTipText("Bus granted to DCS");
      fSIU.SetToolTipText("Bus granted to SIU");
      fFields.AddFrame(&fDCS, &fTips);
      fFields.AddFrame(&fSIU, &fTips);
    }
    void Get() 
    {
      fDCS.SetDown(fLow.DCS());
      fSIU.SetDown(fLow.DDL());
    }
  };
}
#endif
//
// EOF
//

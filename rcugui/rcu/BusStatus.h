// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BusStatus_H
#define RCUGUI_BusStatus_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuBusStatus.h>


namespace RcuGui
{
  //__________________________________________________________________
  /** @class BusStatus
      @brief Interface to the ERRor and STatus register
      @ingroup rcugui
  */
  class BusStatus : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    BusStatus(const char* name, 
	      TGCompositeFrame& f, Rcuxx::RcuBusStatus& low) 
      : Register(f, low), 
	fLow(low),
	fCSTB_A(fFields,"CSTB_A","Control strobe on branch A asserted",
		 0x00aa00, 0xc0c0c0),
	fACK_A(fFields,"ACK_A","Acknowledge on branch A asserted", 
	       0x00aa00, 0xc0c0c0),
	fTRSF_A(fFields,"TRSF_A", "Transfer on branch A asserted",
		 0x00aa00, 0xc0c0c0),
	fCSTB_B(fFields,"CSTB_B","Control strobe on branch B asserted",
		 0x00aa00, 0xc0c0c0),
	fACK_B(fFields,"ACK_B","Acknowledge on branch B asserted", 
	       0x00aa00, 0xc0c0c0),
	fTRSF_B(fFields,"TRSF_B", "Transfer on branch B asserted",
		0x00aa00, 0xc0c0c0),
	fL1(fFields,"L1", "L1 asserted on bus", 0x00aa00, 0xc0c0c0),
	fL2(fFields,"L2", "L2 asserted on bus", 0x00aa00, 0xc0c0c0)
    {
      fGroup.SetTitle(name);
      Get();
    }
    void Get() 
    {
      fCSTB_A.SetState(fLow.CSTB_A());
      fACK_A.SetState(fLow.ACK_A());
      fTRSF_A.SetState(fLow.TRSF_A());
      fCSTB_B.SetState(fLow.CSTB_B());
      fACK_B.SetState(fLow.ACK_B());
      fTRSF_B.SetState(fLow.TRSF_B());
      fL1.SetState(fLow.L1());
      fL2.SetState(fLow.L2());
    }
  private:
    Rcuxx::RcuBusStatus& fLow;
    ErrorBit fCSTB_A;
    ErrorBit fACK_A;
    ErrorBit fTRSF_A;
    ErrorBit fCSTB_B;
    ErrorBit fACK_B;
    ErrorBit fTRSF_B;
    ErrorBit fL1;
    ErrorBit fL2;
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_RDOMOD_H
#define RCUGUI_RDOMOD_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuRDOMOD.h>


namespace RcuGui
{
  //__________________________________________________________________
  /** @class RDOMOD
      @brief Interface to the ERRor and STatus register
      @ingroup rcugui
  */
  class RDOMOD : public RcuGui::Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    RDOMOD(TGCompositeFrame& f, Rcuxx::RcuRDOMOD& low) 
      : Register(f, low), 
	fLow(low),
	fHints(kLHintsExpandX, 3, 3, 0, 0),
	fBuffers(&fFields, "# of buffers"), 
	f4Buffers(&fBuffers, "4"),
	f8Buffers(&fBuffers, "8"),
	fSparseReadout(&fFields, "Sparse readout"),
	fExecOnSOD(&fFields, "Execute IMEM on SOD"),
	fCheckRDYRX(&fFields, "Check RDYRX")
    {
      fGroup.SetTitle("Read-out mode");
      fFields.AddFrame(&fBuffers,        &fHints);
      fFields.AddFrame(&fSparseReadout,  &fHints);
      fFields.AddFrame(&fExecOnSOD,      &fHints);
      fFields.AddFrame(&fCheckRDYRX,     &fHints);
      Get();
    }
    void Get() 
    {
      f4Buffers.SetDown((fLow.NBuffers() == 4));
      fSparseReadout.SetDown(fLow.SparseReadout());
      fExecOnSOD.SetDown(fLow.ExecOnSOD());
      fCheckRDYRX.SetDown(fLow.CheckRDYRX());
    }
    void Set() 
    {
      fLow.SetBuffers(f4Buffers.IsDown() ? Rcuxx::RcuRDOMOD::k4Buffers :
		      Rcuxx::RcuRDOMOD::k8Buffers);
      fLow.SetSparseReadout(fSparseReadout.IsDown());
      fLow.SetExecOnSOD(fExecOnSOD.IsDown());
      fLow.SetCheckRDYRX(fCheckRDYRX.IsDown());
    }
  private:
    Rcuxx::RcuRDOMOD& fLow;
    TGLayoutHints fHints;
    TGVButtonGroup fBuffers;
    TGRadioButton f4Buffers;
    TGRadioButton f8Buffers;
    TGCheckButton fSparseReadout;
    TGCheckButton fExecOnSOD;
    TGCheckButton fCheckRDYRX;
  };
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTROCFG_H
#define RCUGUI_ALTROCFG_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuALTROCFG1.h>
# include <rcuxx/rcu/RcuALTROCFG2.h>


namespace RcuGui
{
  //====================================================================
  /** @class ALTROCFG1
      @brief Interface to the CHannel Address register 
      @ingroup rcugui
  */
  class ALTROCFG : public Register
  {
  private:
    Rcuxx::RcuALTROCFG1&  fLow1;
    Rcuxx::RcuALTROCFG2&  fLow2;
    LabeledIntEntry fValue1;
    LabeledIntEntry fValue2;
  public:
    ALTROCFG(TGCompositeFrame& f, Rcuxx::RcuALTROCFG1& low1,
	     Rcuxx::RcuALTROCFG2& low2) 
      : Register(f, low1), fLow1(low1), fLow2(low2),
	fValue1(fFields, "1st word", 0, 0xffff),
	fValue2(fFields, "2nd word", 0, 0xffff)
    {
      fGroup.SetTitle("ALTRO configuration");
      Get();
    }
    void Get() 
    {
      fValue1.SetValue(fLow1.Data());
      fValue2.SetValue(fLow2.Data());
    }
    void Set() 
    {
      fLow1.SetData(fValue1.GetValue());
      fLow2.SetData(fValue2.GetValue());
    }
    Bool_t Update() 
    {
      unsigned int ret = fLow2.Update();
      if (ret) {
	Main::Instance()->Error(ret);
	return kFALSE;
      }
      return Register::Update();
    }
    Bool_t Commit() 
    {
      Set();
      fLow2.Commit();
      return Register::Commit();
    }
  };
}
#endif
//
// EOF
//

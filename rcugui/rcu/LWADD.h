// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_LWADD_H
#define RCUGUI_LWADD_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGButton.h>
# include <rcugui/Register.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/rcu/RcuLWADD.h>

namespace RcuGui
{
  //====================================================================
  /** @class LWADD
      @brief Display the Last Written ADDress in the DMEM1 and DMEM2 
      @ingroup rcugui
  */
  class LWADD : public Register
  {
  public:
    /** Constructor 
	@param f The frame to put the interface in */
    LWADD(TGCompositeFrame& f, Rcuxx::RcuLWADD& low) 
      : Register(f, low), fLow(low),
	fBank1(fFields,"Bank 1", 0, 0xffff, kHorizontalFrame),
	fBank2(fFields,"Bank 2", 0, 0xffff, kHorizontalFrame)
    {
      fGroup.SetTitle("Last data word address");
      fBank1.SetToolTipText("# of 40bit words in DMEM1");
      fBank2.SetToolTipText("# of 40bit words in DMEM2");
      Get();
    }
    void Get()
    {
      fBank1.SetValue(fLow.Bank1());
      fBank2.SetValue(fLow.Bank2());
    }
  private:
    Rcuxx::RcuLWADD&   fLow;
    LabeledIntView fBank1;
    LabeledIntView fBank2;
  };
}
#endif
//
// EOF
//

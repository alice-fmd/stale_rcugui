//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/ConnectionDialog.h>

/* These are commonly used classes */
#include <TString.h>
#include <TGTableLayout.h>
#include <TGMsgBox.h>
#include <TGButton.h>
#include <TGButtonGroup.h>

/* These are used by the concrete connection types */
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>

/* These are for the U2F connection type  */
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/* The are just normal system headers */
#include <iostream>

//====================================================================
RcuGui::ConnectionType::ConnectionType(TGCompositeFrame& p, 
				       const char* title, int i) 
  : fSelectHints(0,1,i,i+1), 
    fSelect(&p, title, i)
{
  p.AddFrame(&fSelect, &fSelectHints);
  Connect("RcuGui::ConnectionType", this, "Handle()");
}

//____________________________________________________________________
RcuGui::ConnectionType::ConnectionType(const ConnectionType& o)
  : fSelectHints(o.fSelectHints.GetAttachLeft(), 
		 o.fSelectHints.GetAttachRight(), 
		 o.fSelectHints.GetAttachTop(), 
		 o.fSelectHints.GetAttachBottom(), 
		 o.fSelectHints.GetPadLeft(),
		 o.fSelectHints.GetPadRight(),
		 o.fSelectHints.GetPadTop(),
		 o.fSelectHints.GetPadBottom())
    // fSelect(o.fSelect)
{}


//____________________________________________________________________
void
RcuGui::ConnectionType::Connect(const char* rClass, void* r, 
				const char* rHandler) 
{
  fSelect.Connect("Clicked()", rClass, r, rHandler);
}

//====================================================================
/* Private implementation of connection types */
namespace 
{
  struct U2F : public RcuGui::ConnectionType
  {
    /** Enumeratio to configure this class */
    enum { kMajor = 195 };
  
    /** Constructor 
	@param p Parent frame 
	@param i Line number */
    U2F(TGCompositeFrame& p, int i) 
      : RcuGui::ConnectionType(p, "U2F", i), 
	fLabelHints(1,2,i,i+1,kLHintsLeft), 
	fDevicesHints(2,5,i,i+1,kLHintsRight),
	fLabel(&p,"Device:"), 
	fDevices(&p)
    {
      p.AddFrame(&fLabel,   &fLabelHints);
      p.AddFrame(&fDevices, &fDevicesHints);
      fDevices.Resize(200, 24);

      TString          pwd(gSystem->WorkingDirectory());
      TSystemDirectory dirDev("/dev", "/dev");
      TList*           files = dirDev.GetListOfFiles();
      files->Sort();
      TSystemFile*     file = 0;
      TIter            next(files);
      while ((file = static_cast<TSystemFile*>(next()))) {
	struct stat buf;
	int ret = stat(file->GetName(), &buf);
	if (ret != 0) continue;
	unsigned int major = (buf.st_rdev >> 8);
	unsigned int minor = (buf.st_rdev & 0xff);
      
	if (major == kMajor) {
	  TString path(gSystem->ConcatFileName(file->GetTitle(),
					       file->GetName()));
	  fDevices.AddEntry(path.Data(), minor);
	}
	
      }
      gSystem->ChangeDirectory(pwd.Data());
    }
    /** Enables this line */
    void Enable() 
    { 
      RcuGui::ConnectionType::Enable();
      // fDevices.SetEnabled();
      // fDevices.SetFocus();
    }
    /** Disables this line */
    void Disable() 
    { 
      RcuGui::ConnectionType::Disable();
      // fDevices.SetEnabled(kFALSE);
    }
    /** Get the URI that describes this connection. 
	@param url On return, the URI.  In case of failure, the empty
	string.  */
    void Url(TString& url)
    {
      if (!fDevices.GetSelectedEntry()) return;
      url = "u2f:";
      url.Append(fDevices.GetSelectedEntry()->GetTitle());
    }
  private:
    /** Layout hints from the label */
    TGTableLayoutHints fLabelHints;
    /** Layout hints from the device list */
    TGTableLayoutHints fDevicesHints;
    /** Label */
    TGLabel            fLabel;
    /** List of devices. */
    TGComboBox         fDevices;
  };
  //__________________________________________________________________
  /** structure that describes a connection via a front end server.
   */
  struct Fee : public RcuGui::ConnectionType
  {
    /** Constructor 
	@param p Parent frame
	@param i Line number */
    Fee(TGCompositeFrame& p, int i) 
      : RcuGui::ConnectionType(p, "FeeServer", i), 
	fDnsLabelHints    (1,2,i,i+1),
	fDnsHints 	(2,3,i,i+1),
	fServerLabelHints (3,4,i,i+1),
	fServerHints 	(4,5,i,i+1),
	fDnsLabel(&p, "DNS Node:"), 
	fDns(&p, "localhost"),
	fServerLabel(&p, "Server:"), 
	fServer(&p, "FMD1")
    {
      p.AddFrame(&fDnsLabel,    &fDnsLabelHints);    
      p.AddFrame(&fDns,         &fDnsHints);         
      p.AddFrame(&fServerLabel, &fServerLabelHints); 
      p.AddFrame(&fServer,      &fServerHints);      
      fDns.SetWidth(2*fDns.GetWidth());
      fDns.SetToolTipText("The host where the "
			  "DIM Domain Name Servere " 
			  "is runninng");
      fServer.SetWidth(2*fServer.GetWidth());
      fServer.SetToolTipText("The name of the FeeServer to connect to");
    }
    /** Enable this line */
    void Enable()
    {
      RcuGui::ConnectionType::Enable();
      fDns.SetEnabled();
      fServer.SetEnabled();
      fDns.SetFocus();
    }
    /** Disable this line */
    void Disable()
    {
      RcuGui::ConnectionType::Disable();
      fDns.SetEnabled(kFALSE);
      fServer.SetEnabled(kFALSE);
    }
    /** Get the URI that corresponds to this connection.
	@param url On return, the URI, or i case of failures, the
	empty string.  */
    void Url(TString& url) 
    {
      if (!fDns.GetText()    || fDns.GetText()[0] == '\0' || 
	  !fServer.GetText() || fServer.GetText()[0] == '\0') return;
      url = "fee://";
      url.Append(fDns.GetText());
      url.Append("/");
      url.Append(fServer.GetText());
    }
  private:
    /** Hints for the Dns Label */
    TGTableLayoutHints fDnsLabelHints;	 
    /** Hints for the Dns input widget */
    TGTableLayoutHints fDnsHints;	 
    /** Hints for the Server Label  */
    TGTableLayoutHints fServerLabelHints; 
    /** Hints for the Server input widget */
    TGTableLayoutHints fServerHints;      
    /** Label for Dns input widget */
    TGLabel            fDnsLabel;
    /** Dns input widget */
    TGTextEntry        fDns;
    /** Label for Server input widget */
    TGLabel            fServerLabel;
    /** Server input widget */
    TGTextEntry        fServer;
  };


  //____________________________________________________________________
  /** structure that describes a connection via a RORC card
   */
  struct Rorc : public RcuGui::ConnectionType
  {
    /** Constructor 
	@param p Parent frame
	@param i Line number */
    Rorc(TGCompositeFrame& p, int i) 
      : RcuGui::ConnectionType(p, "RORC", i), 
	fMinorLabelHints    (1,2,i,i+1),
	fMinorHints         (2,3,i,i+1),
	fChannelLabelHints  (3,4,i,i+1),
	fChannelHints       (4,5,i,i+1),
	fMinorLabel(&p, "Minor device #:"),
	fMinor(&p, 0, 5, -1, TGNumberFormat::kNESInteger, 
	       TGNumberFormat::kNEANonNegative, 
	       TGNumberFormat::kNELLimitMinMax, 0, 16),
	fChannelLabel(&p, "Channel #:"),
	fChannel(&p, 0, 5, -1, TGNumberFormat::kNESInteger, 
		 TGNumberFormat::kNEANonNegative, 
		 TGNumberFormat::kNELLimitMinMax, 0, 4)
    {
      p.AddFrame(&fMinorLabel,    &fMinorLabelHints);    
      p.AddFrame(&fMinor,         &fMinorHints);         
      p.AddFrame(&fChannelLabel,  &fChannelLabelHints);  
      p.AddFrame(&fChannel,       &fChannelHints);       
    }
    /** Enable this line */
    void Enable()
    {
      RcuGui::ConnectionType::Enable();
      fMinor.SetState(kTRUE);
      fChannel.SetState(kTRUE);
      // fMinor.SetFocus();
    }
    /** Disable this line */
    void Disable()
    {
      RcuGui::ConnectionType::Disable();
      fMinor.SetState(kFALSE);
      fChannel.SetState(kFALSE);
    }
    /** Get the URI that corresponds to this connection.
	@param url On return, the URI, or i case of failures, the
	empty string.  */
    void Url(TString& url) 
    {
      url = Form("rorc://%d:%d",fMinor.GetIntNumber(),fChannel.GetIntNumber());
    }
  private:
    /** Hints for the Minor Label  */
    TGTableLayoutHints fMinorLabelHints;
    /** Hints for the Minor input widget */
    TGTableLayoutHints fMinorHints;
    /** Hints for the Channel Label */
    TGTableLayoutHints fChannelLabelHints;
    /** Hints for the Channel input widget */
    TGTableLayoutHints fChannelHints;
    /** Label for Minor input widget */
    TGLabel            fMinorLabel;
    /** Minor input widget */
    TGNumberEntry      fMinor;
    /** Label for Channel input widget */
    TGLabel            fChannelLabel;
    /** Channel input widget */
    TGNumberEntry      fChannel;
  };

  //__________________________________________________________________
  /** structure that describes a connection via 2 pipes
   */
  struct Pipe : public RcuGui::ConnectionType
  {
    /** Constructor 
	@param p Parent frame
	@param i Line number */
    Pipe(TGCompositeFrame& p, int i) 
      : RcuGui::ConnectionType(p, "Pipes", i), 
	fInputLabelHints (1,2,i,i+1),
	fInputHints 	 (2,3,i,i+1),
	fOutputLabelHints(3,4,i,i+1),
	fOutputHints 	 (4,5,i,i+1),
	fInputLabel(&p, "Input:"), 
	fInput(&p, "/dev/stdin"),
	fOutputLabel(&p, "Output:"), 
	fOutput(&p, "/dev/stdout")
    {
      p.AddFrame(&fInputLabel,    &fInputLabelHints);    
      p.AddFrame(&fInput,         &fInputHints);         
      p.AddFrame(&fOutputLabel, &fOutputLabelHints); 
      p.AddFrame(&fOutput,      &fOutputHints);      
      fInput.SetWidth(2*fInput.GetWidth());
      fInput.SetToolTipText("Input pipe to read results from");
      fOutput.SetWidth(2*fOutput.GetWidth());
      fOutput.SetToolTipText("Output pipe to write to");
    }
    /** Enable this line */
    void Enable()
    {
      RcuGui::ConnectionType::Enable();
      fInput.SetEnabled();
      fOutput.SetEnabled();
      fInput.SetFocus();
    }
    /** Disable this line */
    void Disable()
    {
      RcuGui::ConnectionType::Disable();
      fInput.SetEnabled(kFALSE);
      fOutput.SetEnabled(kFALSE);
    }
    /** Get the URI that corresponds to this connection.
	@param url On return, the URI, or i case of failures, the
	empty string.  */
    void Url(TString& url) 
    {
      if (!fInput.GetText()    || fOutput.GetText()[0] == '\0') return;
      url = "pipe://";
      url.Append(fInput.GetText());
      url.Append("|");
      url.Append(fOutput.GetText());
    }
  private:
    /** Hints for the Input Label */
    TGTableLayoutHints fInputLabelHints;	 
    /** Hints for the Input Label */
    TGTableLayoutHints fInputHints;	 
    /** Hints for the Output Label */
    TGTableLayoutHints fOutputLabelHints;	 
    /** Hints for the Input input widget */
    TGTableLayoutHints fOutputHints;	 
    /** Label for Input input widget */
    TGLabel            fInputLabel;
    /** Input input widget */
    TGTextEntry        fInput;
    /** Label for Output input widget */
    TGLabel            fOutputLabel;
    /** Output input widget */
    TGTextEntry        fOutput;
  };
  
}
//====================================================================
RcuGui::ConnectionDialog::ConnectionDialog(const TGWindow* p, 
					   TString& ret, 
					   int flags)
  : TGTransientFrame(p, gClient->GetRoot(),1,1,kVerticalFrame), 
    fReturn(ret),
    fSelectHints(kLHintsExpandX|kLHintsExpandY,3,3,3,3),
    fSelect(this, "Connection URI"),
    fU2F(0), 
    fFee(0), 
    fRorc(0),
    fPipe(0),
    fButtonsHints(kLHintsExpandX,3,3,0,3),
    fButtonHints(kLHintsCenterY | kLHintsExpandX, 3, 3),
    fButtons(this), 
    fCancel(&fButtons, "Cancel", kCancel),
    fOK(&fButtons, "OK", kOk)
{
  AddFrame(&fSelect,&fSelectHints);
  int lines = 0;
  if ((flags & kU2F)  != 0) lines++;
  if ((flags & kFee)  != 0) lines++;
  if ((flags & kRorc) != 0) lines++;
  if ((flags & kPipe) != 0) lines++;
  fSelect.SetLayoutManager(new TGTableLayout(&fSelect,lines,5,kFALSE,3));

  int line = 0;
  if ((flags & kFee) != 0) {
    fFee = new Fee(fSelect, line);
    fFee->Connect("RcuGui::ConnectionDialog", this, "EnableFee()");
    EnableFee();
    line++;
  }
  if ((flags & kRorc) != 0) {
    fRorc = new Rorc(fSelect, line);
    fRorc->Connect("RcuGui::ConnectionDialog", this, "EnableRorc()");
    if (!fFee) EnableRorc();
    line++;
  }
  if ((flags & kU2F) != 0) {
    fU2F = new U2F(fSelect, line);
    fU2F->Connect("RcuGui::ConnectionDialog", this, "EnableU2F()");
    if (!fFee && !fRorc) EnableU2F();
    line++;
  }
  if ((flags & kPipe) != 0) {
    fPipe = new Pipe(fSelect, line);
    fPipe->Connect("RcuGui::ConnectionDialog", this, "EnablePipe()");
    if (!fU2F && !fFee && !fRorc) EnablePipe();
    line++;
  }
  
  
  AddFrame(&fButtons, &fButtonsHints);
  fButtons.Connect("Clicked(int)", "RcuGui::ConnectionDialog",
		   this,"HandleButtons(int)");
  fButtons.SetLayoutHints(&fButtonHints);

  // fOK.SetFocus();
  Layout();
  MapSubwindows();
  Resize(GetDefaultSize());
  CenterOnParent();
  Int_t w = GetWidth();
  Int_t h = GetHeight();
  SetWMSize(w, h);
  SetWMSizeHints(w, h, w, h, 0, 0);
  SetWindowName("Open RCU Connection ...");
  SetIconName("Open RCU Connection ...");
  SetClassHints("MsgBox", "MsgBox");
  SetMWMHints(kMWMDecorTitle,kMWMFuncMove,kMWMInputModeless);
  SetCleanup(kNoCleanup);
  MapRaised();
  fClient->WaitFor(this);
  // MapWindow();
}

//____________________________________________________________________
RcuGui::ConnectionDialog::~ConnectionDialog() 
{
  if (fU2F)  delete fU2F;
  if (fFee)  delete fFee;
  if (fRorc) delete fRorc;
  if (fPipe) delete fPipe;
}

//____________________________________________________________________
void
RcuGui::ConnectionDialog::HandleButtons(Int_t i) 
{
  bool ret = true;
  switch (i) {
  case kCancel: 
    break;
  case kOk:
    ret = false;
    if      (fU2F  && fU2F->IsEnabled())  fU2F->Url(fReturn);
    else if (fFee  && fFee->IsEnabled())  fFee->Url(fReturn);
    else if (fRorc && fRorc->IsEnabled()) fRorc->Url(fReturn);
    else if (fPipe && fPipe->IsEnabled()) fPipe->Url(fReturn);
    break;
  }
  DeleteWindow(); 
}
//____________________________________________________________________
void 
RcuGui::ConnectionDialog::EnableU2F() 
{
  if (fU2F)  fU2F->Enable();
  if (fFee)  fFee->Disable();
  if (fRorc) fRorc->Disable();
  if (fPipe) fPipe->Disable();
}

//____________________________________________________________________
void 
RcuGui::ConnectionDialog::EnableFee()
{
  if (fU2F)  fU2F->Disable();
  if (fFee)  fFee->Enable();
  if (fRorc) fRorc->Disable();
  if (fPipe) fPipe->Disable();
}

//____________________________________________________________________
void 
RcuGui::ConnectionDialog::EnableRorc()
{
  if (fU2F)  fU2F->Disable();
  if (fFee)  fFee->Disable();
  if (fRorc) fRorc->Enable();
  if (fPipe) fPipe->Disable();
}

//____________________________________________________________________
void 
RcuGui::ConnectionDialog::EnablePipe()
{
  if (fU2F)  fU2F->Disable();
  if (fFee)  fFee->Disable();
  if (fRorc) fRorc->Disable();
  if (fPipe) fPipe->Enable();
}

//____________________________________________________________________
//
// EOF
//

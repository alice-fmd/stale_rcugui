//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/Acquisition.h>
#include <rcugui/Main.h>
#include <rcuxx/DebugGuard.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <TGTableLayout.h>
#include <TGFileDialog.h>
#include <TString.h>
#include <TRegexp.h>
#include <TGMsgBox.h>
#include <TSystem.h>
#include <TGFSContainer.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TThread.h>
#include <TTree.h>
#include <TTimer.h>
#include <climits>


namespace 
{
  bool fDebug = false;

  void* 
  ThreadFunction(void* arg)
  {
    unsigned int ret = 0;
    try {
      RcuGui::Acquisition* acqgui = 
      static_cast<RcuGui::Acquisition*>(arg);
      Rcuxx::Acq* acq = acqgui->Low();
      ret = acq->Run();
      acqgui->Done(ret);
    }
    catch (std::exception& e) {
      std::cerr << "Exception in thread: " << e.what() << std::endl;
      return 0;
    }
    catch (...) {
      std::cerr << "Unknown Exception in thread " << std::endl;
      throw;
      return 0;
    }
    return (void*)(ret);
  }

  enum {
    kIDF_CDUP,
    kIDF_NEW_FOLDER,
    kIDF_LIST,
    kIDF_DETAILS,
    kIDF_FSLB,
    kIDF_FTYPESLB,
    kIDF_OK,
    kIDF_CANCEL
  };

  //____________________________________________________________________
  class DirectoryDialog : public TGFileDialog 
  {
  public:
    DirectoryDialog(const TGWindow* p, const TGWindow* main, 
		    TGFileInfo* file_info)
      : TGFileDialog(p, main, kFDSave, file_info)
    {}
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
    {
    if (GET_MSG(msg) == kC_COMMAND && GET_SUBMSG(msg) == kCM_BUTTON
	&& parm1  == kIDF_OK) {
      if (!gSystem->AccessPathName(fFc->GetDirectory(), kWritePermission)) {
	// std::cout << "Selected: " << fFc->GetDirectory() << std::endl;
	fFileInfo->fIniDir = Form("%s", fFc->GetDirectory());
	if (fFileInfo->fFilename) delete [] fFileInfo->fFilename;
	fFileInfo->fFilename = 0;
	DeleteWindow();
	return kTRUE;
      }
      else {
	new TGMsgBox(gClient->GetRoot(), fMain, "Invalid diretory", 
		     "Directory isn't writeable", kMBIconExclamation, kMBOk);
	return kFALSE;
      }
    }
    return TGFileDialog::ProcessMessage(msg, parm1, parm2);
    }
  };
}

//____________________________________________________________________
RcuGui::Acquisition::Acquisition(TGTab& tabs, Rcuxx::Acq& low) 
  : Scrollable("Acquisition", tabs, kVerticalFrame), 
    fLow(low), 
    fOptions(&fCont, "Options", kHorizontalFrame),
    fNEventsLabel(&fOptions, "Run options", kVerticalFrame),
    fRunNoInput(fNEventsLabel, "Run #", 0, INT_MAX),
    fNEventsInput(fNEventsLabel, "# of events",-1,INT_MAX),
    fTriggerButtons(&fOptions, "Trigger type"),
    fSoftwareTrigger(&fTriggerButtons, "Software", 
		     Rcuxx::Acq::kSoftwareTrigger),
    fExternalTrigger(&fTriggerButtons, "External", 
		     Rcuxx::Acq::kExternalTrigger),
    fTodo(&fOptions, "Things to commit"),
    fACTFEC(&fTodo, "Active FEC's", 0),
    fFECRST(&fTodo, "Reset FEC's", 1),
    fIMEM(&fTodo, "Instr. memory", 2),
    fPMEM(&fTodo, "Pedestal memory", 3),
    fACL(&fTodo, "Active channels", 4),
    fTRCFG1(&fTodo, "Trigger config.", 5),
    fAddr(fTodo,"Address", 0, 0xff),
    fOperations(&fCont, "Operations", kHorizontalFrame),
    fStart(&fOperations, "Start", 0),
    fStop(&fOperations, "Stop", 0),
    fRun(0), 
    fThread(0),
    fTree(0), 
    fTimer(0)
{
  // Options frame 
  fCont.AddFrame(&fOptions, new TGLayoutHints(kLHintsExpandX, 0, 0, 0, 3));

  // Event an  run 
  fOptions.AddFrame(&fNEventsLabel, 
		     new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,0,0,0,0));
  fRunNoInput.SetValue(fRun);
  fRunNoInput.SetHexFormat(kFALSE);
  fNEventsInput.SetHexFormat(kFALSE);

  // Triggers
  fTriggerButtons.SetExclusive(kTRUE);
  fTriggerButtons.SetLayoutHints(new TGLayoutHints(kLHintsNormal,6,0,0,0));
  fExternalTrigger.SetDown();
  fOptions.AddFrame(&fTriggerButtons, 
		    new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,10,0,0,0));

  // Todo 
  fOptions.AddFrame(&fTodo, 
		    new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,0,0,0,0));
  fIMEM.Connect("Toggled(Bool_t)", "RcuGui::LabeledIntEntry", &fAddr, 
		"SetEnabled(Bool_t)");
  fACTFEC.SetDown();
  fFECRST.SetDown(); 
  fIMEM.SetDown();   
  // fPMEM->SetDown();   
  fACL.SetDown();    
  fTRCFG1.SetDown();
  
  
  // Operations frame 
  fCont.AddFrame(&fOperations, new TGLayoutHints(kLHintsExpandX, 0, 0, 0, 0));
  fStart.Connect("Clicked()", "RcuGui::Acquisition", this, "HandleStart()");
  fOperations.AddFrame(&fStart, new TGLayoutHints(kLHintsExpandX, 3, 0, 0, 0));
  fStop.SetState(kButtonDisabled);
  fStop.Connect("Clicked()", "RcuGui::Acquisition", this, "HandleStop()");
  fOperations.AddFrame(&fStop, new TGLayoutHints(kLHintsExpandX, 3, 0, 0, 0));
}

//____________________________________________________________________
void 
RcuGui::Acquisition::HandleTimer()
{
  std::cerr << "HandleTimer" << std::endl;
  if (fTree) { 
    std::cerr << "Drawing data" << std::endl;
    fTree->Draw("channel.fData");
  }
}

//____________________________________________________________________
void 
RcuGui::Acquisition::Run()
{
  // Make thread
  if (!fThread) fThread = new TThread(&ThreadFunction, this);
  fStop.SetState(kButtonUp);

  // Get Number of events 
  Int_t nEvent = fNEventsInput.GetValue();
  // Get run number 
  fRun = fRunNoInput.GetValue();
  // Trigger type 
  Rcuxx::Acq::Trigger_t trig = (fSoftwareTrigger.IsDown() ? 
				Rcuxx::Acq::kSoftwareTrigger : 
				Rcuxx::Acq::kExternalTrigger);
  // Mask 
  UInt_t mask = 0;
  if (fACTFEC.IsDown()) mask |= Rcuxx::Acq::kACTFEC;
  if (fFECRST.IsDown()) mask |= Rcuxx::Acq::kFECRST;
  if (fTRCFG1.IsDown()) mask |= Rcuxx::Acq::kTRCFG1;
  if (fIMEM.IsDown())   mask |= Rcuxx::Acq::kIMEM;
  if (fPMEM.IsDown())   mask |= Rcuxx::Acq::kPMEM;
  if (fACL.IsDown())    mask |= Rcuxx::Acq::kACL;
  Main::Instance()->Set4Acq(mask);

  // Setup job
  if (fLow.Setup(fRun, nEvent, trig, mask, fAddr.GetValue())) return;
  fLow.SetDebug(fDebug);
  
  std::cout << "Starting run " << fRun << std::endl;
  fThread->Run();
  // usleep(50);
#if 0
  RcuData::RootAcq* rootAcq = dynamic_cast<RcuData::RootAcq*>(fLow);
  if (rootAcq) { 
    fTree = rootAcq->GetTree();
    std::cerr << "Got tree " << fTree << " tree from acq" << std::endl;
    std::cerr << "starting timer" << std::endl;
    // fTimer->Start(500, kFALSE);
  }
#endif
}

//____________________________________________________________________
void
RcuGui::Acquisition::HandleStart() 
{
  fStart.SetState(kButtonEngaged);
  fStart.SetEnabled(kFALSE);
  Run();
}

//____________________________________________________________________
void
RcuGui::Acquisition::HandleStop() 
{
  // if (fDAQ) fDAQ->Cancel();
  fLow.Stop();
}

//____________________________________________________________________
void
RcuGui::Acquisition::HandleIMEM() 
{
  // fAddr->SetEnabled(fIMEM->IsDown());
}

//____________________________________________________________________
void
RcuGui::Acquisition::Done(unsigned int ret) 
{
  if (ret) Main::Instance()->Error(ret);
  if (fTimer) fTimer->Stop();
  fStart.SetEnabled(kTRUE);
  fStop.SetState(kButtonDisabled);
  fStart.SetState(kButtonUp);
  fRun++;
  fRunNoInput.SetValue(fRun);
  
  fACTFEC.SetDown(kFALSE);
  fFECRST.SetDown(kFALSE); 
  // fIMEM->SetDown(kFALSE);   
  // fPMEM->SetDown();   
  fACL.SetDown(kFALSE);    
  // fTRCFG1->SetDown(kFALSE);
}

//____________________________________________________________________
void
RcuGui::Acquisition::SetDebug(Bool_t on)
{
  fDebug = on;
}



//____________________________________________________________________
//
// EOF
//

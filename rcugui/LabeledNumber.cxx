//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/LabeledNumber.h>
#include <TGMsgBox.h>
#include <iostream>
#include <unistd.h>

//====================================================================
RcuGui::LabeledNumber::LabeledNumber(TGCompositeFrame& f, 
				     const char*       label, 
				     Int_t             option,
				     Int_t             lmargin, 
				     Int_t             rmargin, 
				     Int_t             tmargin, 
				     Int_t             bmargin)
  : fLHFrame(kLHintsExpandX, lmargin, rmargin, tmargin, bmargin),
    fLHLabel(kLHintsLeft, 0, 3, 0, 0), // kLHintsExpandX
    fLHPrefix(kLHintsNormal, 0, 3, 0, 0),
    fLHView(kLHintsRight, 0, 0, 0, 0), // kLHintsExpandX
    fFrame(&f),    
    fPrefix(&fFrame, "  "),
    fLabel(&fFrame, label)
    
{
  fLabel.SetMinWidth(50);
  fLabel.SetMaxWidth(300);
  fFrame.SetLayoutManager(option & kHorizontalFrame 
			  ? new TGHorizontalLayout(&fFrame)
			  : new TGVerticalLayout(&fFrame));
  
  f.AddFrame(&fFrame, &fLHFrame);
  fLabel.SetTextJustify(option & kHorizontalFrame ? kTextRight : kTextCenterX);
  fFrame.AddFrame(&fLabel, &fLHLabel);

  fPrefix.SetTextJustify(kTextLeft);
  fFrame.AddFrame(&fPrefix, &fLHPrefix);
}


//____________________________________________________________________
void 
RcuGui::LabeledNumber::SetHexFormat(Bool_t ishex) 
{
  fPrefix.SetText((ishex ? "0x" : "  "));
}

//____________________________________________________________________
void 
RcuGui::LabeledNumber::AddView(TGFrame& view) 
{
  fFrame.AddFrame(&view, &fLHView);
  view.SetMinWidth(50);
  view.SetMaxWidth(300);
}

//====================================================================
RcuGui::LabeledIntView::LabeledIntView(TGCompositeFrame& f, 
				       const char*       label, 
				       Long_t            min, 
				       Long_t            max, 
				       Int_t             option,
				       Int_t             lmargin, 
				       Int_t             rmargin, 
				       Int_t             tmargin, 
				       Int_t             bmargin)
  : LabeledNumber(f, label, option, lmargin, rmargin, tmargin, bmargin), 
    fView(&fFrame, -1,0,TGNumberFormat::kNESInteger,
	  (min < 0 ? TGNumberFormat::kNEAAnyNumber : 
	   TGNumberFormat::kNEANonNegative),
	  TGNumberFormat::kNELLimitMinMax, min, max)
{
  Int_t logmax = Int_t(TMath::Log10(max)) + 1;
  fView.SetMaxLength((logmax > 1 ? logmax : 1));
  fView.SetEditDisabled();
  fView.SetEnabled(kFALSE);
  fView.SetNumber(min);
  AddView(fView);
  SetHexFormat(option & kHex);
  fView.Connect("TextChanged(const char*)", 
		"RcuGui::LabeledIntView", this, 
		"HandleChange()");
}

//____________________________________________________________________
void 
RcuGui::LabeledIntView::SetToolTipText(const char* t)
{
  fView.SetToolTipText(t);
}

//____________________________________________________________________
void 
RcuGui::LabeledIntView::SetHexFormat(Bool_t ishex) 
{
  LabeledNumber::SetHexFormat(ishex);
  fView.SetFormat((ishex ? 
		   TGNumberFormat::kNESHex : 
		   TGNumberFormat::kNESInteger), 
		  fView.GetNumAttr());
}

//____________________________________________________________________
void 
RcuGui::LabeledIntView::HandleChange() 
{
  static Int_t value = 0;
  if (GetValue() == value) return;
  value = GetValue();
  ValueChanged();
}


//====================================================================
RcuGui::LabeledIntEntry::LabeledIntEntry(TGCompositeFrame& f, 
					 const char*       label, 
					 Long_t            min, 
					 Long_t            max, 
					 Int_t             option,
					 Int_t             lmargin, 
					 Int_t             rmargin, 
					 Int_t             tmargin, 
					 Int_t             bmargin)
  : LabeledNumber(f, label, option, lmargin, rmargin, tmargin, bmargin), 
    fView(&fFrame, 0, (Int_t(TMath::Log10(max)) + 1 > 1 ? 
		       Int_t(TMath::Log10(max)) + 1 : 1), -1,
	  TGNumberFormat::kNESInteger, 
	  (min < 0 ? TGNumberFormat::kNEAAnyNumber :
	   TGNumberFormat::kNEANonNegative), TGNumberFormat::kNELLimitMinMax,
	  min, max)
{
  fView.SetNumber(min);
  AddView(fView);
  SetHexFormat(option & kHex);
  fView.GetNumberEntry()->Connect("TextChanged(const char*)", 
				  "RcuGui::LabeledIntEntry", this, 
				  "HandleChange()");
}

//____________________________________________________________________
void 
RcuGui::LabeledIntEntry::SetToolTipText(const char* t)
{
  fView.GetNumberEntry()->SetToolTipText(t);
}

//____________________________________________________________________
void 
RcuGui::LabeledIntEntry::SetHexFormat(Bool_t ishex) 
{
  LabeledNumber::SetHexFormat(ishex);
  fView.SetFormat((ishex ? TGNumberFormat::kNESHex : 
		   TGNumberFormat::kNESInteger), fView.GetNumAttr());
}

//____________________________________________________________________
void 
RcuGui::LabeledIntEntry::HandleChange() 
{
  static Int_t value = 0;
  if (GetValue() == value) return;
  value = GetValue();
  ValueChanged();
}
//====================================================================
RcuGui::LabeledFloatView::LabeledFloatView(TGCompositeFrame& f, 
					   const char*       label, 
					   Float_t           min, 
					   Float_t           max, 
					   Int_t             option,
					   Int_t             lmargin, 
					   Int_t             rmargin, 
					   Int_t             tmargin, 
					   Int_t             bmargin)
  : LabeledNumber(f, label, option, lmargin, rmargin, tmargin, bmargin),
    fView(&fFrame, -1, 0, TGNumberFormat::kNESReal, 
	  (min < 0 ? TGNumberFormat::kNEAAnyNumber :
	   TGNumberFormat::kNEANonNegative), TGNumberFormat::kNELLimitMinMax,
	  min, max)
{
  Int_t logmax = Int_t(TMath::Log10(max)) + 1;
  Int_t logmin = -5; // Int_t(TMath::Log10(min)) - 1;
  Int_t width  = logmax - logmin;
  fView.SetNumber(min);
  fView.SetMaxLength(width);
  fView.SetEditDisabled();
  fView.SetEnabled(kFALSE);
  fView.Connect("TextChanged(const char*)", "RcuGui::LabeledFloatView", this, 
		"HandleChange()");
  AddView(fView);
}

//____________________________________________________________________
void 
RcuGui::LabeledFloatView::SetToolTipText(const char* t)
{
  fView.SetToolTipText(t);
}

//____________________________________________________________________
void 
RcuGui::LabeledFloatView::HandleChange() 
{
  static Float_t value = 0;
  if (GetValue() == value) return;
  value = GetValue();
  ValueChanged();
}
//====================================================================
RcuGui::LabeledFloatEntry::LabeledFloatEntry(TGCompositeFrame& f, 
					     const char*       label, 
					     Float_t           min, 
					     Float_t           max, 
					     Int_t             option,
					     Int_t             lmargin, 
					     Int_t             rmargin, 
					     Int_t             tmargin, 
					     Int_t             bmargin)
  : LabeledNumber(f, label, option, lmargin, rmargin, tmargin, bmargin), 
    fView(&fFrame,0, Int_t(TMath::Log10(max)) + 6, -1,  
	  TGNumberFormat::kNESReal, (min < 0 ? 
				     TGNumberFormat::kNEAAnyNumber :
				     TGNumberFormat::kNEANonNegative),
	   TGNumberFormat::kNELLimitMinMax, min, max)
{
  fView.SetNumber(min);
  fView.GetNumberEntry()->Connect("TextChanged(const char*)", 
				  "RcuGui::LabeledFloatEntry", this, 
				  "HandleChange()");
  AddView(fView);
}

//____________________________________________________________________
void 
RcuGui::LabeledFloatEntry::SetToolTipText(const char* t)
{
  fView.GetNumberEntry()->SetToolTipText(t);
}

//____________________________________________________________________
void 
RcuGui::LabeledFloatEntry::HandleChange() 
{
  static Float_t value = 0;
  if (GetValue() == value) return;
  value = GetValue();
  ValueChanged();
}
//____________________________________________________________________
//
// EOF
//

//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/Main.h>
#include <rcugui/Rcu.h>
#include <rcugui/Altro.h>
#include <rcugui/Bc.h>
#include <rcugui/Acquisition.h>
#include <rcugui/Compiler.h>
#include <rcugui/NumberDialog.h>
#include <rcugui/rcu/InstructionMemory.h>
#include <rcugui/rcu/PatternMemory.h>
#include <rcugui/rcu/ActiveChannels.h>

#include <rcuxx/DebugGuard.h>
#include <TGTableLayout.h>
#include <TGMsgBox.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TTimer.h>
#include <TError.h>
#include <TGPicture.h>
#include <TGResourcePool.h>
#include <stdexcept>
#include <iostream>
#include <cstdarg>
#include <rcugui/rcuGui.xpm>

namespace 
{
  bool fDebug;
}
#define WIDTH  std::min(int(gClient->GetDisplayWidth() * .8),1200)
#define HEIGHT std::min(int(gClient->GetDisplayHeight() * .9),1000)
#define GSCALE  (float(WITDH) / 800)
#define IGSCALE(X) int(fFontScale * X)
//____________________________________________________________________
RcuGui::Main* RcuGui::Main::fgInstance = 0;

//____________________________________________________________________
RcuGui::Main*
RcuGui::Main::Instance() 
{
  if (!fgInstance) fgInstance = new Main;
  return fgInstance;
}


//____________________________________________________________________
RcuGui::Main::Main() 
  : fInteractive(kTRUE), 
    fFontScale(/*gClient->GetResourcePool()->GetDefaultFont()->TextHeight()*/15./15.),
    fMain(gClient->GetRoot(), WIDTH, HEIGHT, kMainFrame | kVerticalFrame),
    fMenuHints(kLHintsTop|kLHintsExpandX),
    fMenuBar(&fMain, 100, IGSCALE(15)),
    fMenuItemLayout(kLHintsTop|kLHintsLeft, 0, IGSCALE(4), 0, 0),
    fFileMenu(gClient->GetRoot(), IGSCALE(10), IGSCALE(10), kHorizontalFrame),
    fActionMenu(gClient->GetRoot(), IGSCALE(10), IGSCALE(10), kHorizontalFrame),
    fOptionMenu(gClient->GetRoot(), IGSCALE(10), IGSCALE(10), kHorizontalFrame),
    fTabHints(kLHintsExpandX|kLHintsExpandY,0,0,0,0),
    fTab(&fMain, WIDTH-20, HEIGHT-40),
    fStatusHints(kLHintsExpandX|kLHintsBottom, 
		 IGSCALE(3), IGSCALE(3), IGSCALE(3), IGSCALE(3)),
    fStatusBar(&fMain),
    fAcquisition(0), 
    fRcu(0),
    fBc(0),
    fAltro(0), 
    fCompiler(0)
{  
  fMain.Resize(int(fFontScale*fMain.GetWidth()), 
	       int(fFontScale*fMain.GetHeight()));
  fMain.Connect("CloseWindow()", "RcuGui::Main", this, "Close()");
  fMain.SetWindowName("RCU GUI");
  fMain.AddFrame(&fMenuBar, &fMenuHints);
  fMain.AddFrame(&fTab, &fTabHints);

  fFileMenu.AddEntry("Close", kClose);
  fFileMenu.AddEntry("&Quit", kQuit);
  fFileMenu.Connect("Activated(Int_t)", "RcuGui::Main", this, 
		    "HandleFile(Int_t)");
  fMenuBar.AddPopup("&File", &fFileMenu, &fMenuItemLayout);

  fActionMenu.AddEntry("Update all", kUpdate);
  fFileMenu.Connect("Activated(Int_t)", "RcuGui::Main", this, 
		    "HandleFile(Int_t)");
  fMenuBar.AddPopup("&Actions", &fActionMenu, &fMenuItemLayout);

  fOptionMenu.AddEntry("Debug &Gui", 3);
  fOptionMenu.Connect("Activated(Int_t)", "RcuGui::Main", this, 
		      "HandleOption(Int_t)");
  fMenuBar.AddPopup("&Options", &fOptionMenu, &fMenuItemLayout);

  fStatusBar.SetParts(2);
  // fStatusBar->SetText("No connection", 0);
  fMain.AddFrame(&fStatusBar, &fStatusHints);
}
  
//____________________________________________________________________
void 
RcuGui::Main::AddAcq(Rcuxx::Acq& acq) 
{
  if (fAcquisition) {
    std::cerr << "Already have an acquisitions tab" << std::endl;
    return;
  }
  fAcquisition  = new Acquisition(fTab, acq);
  fOptionMenu.AddEntry("Debug Ac&q", 4);
}

//____________________________________________________________________
void 
RcuGui::Main::AddRcu(Rcuxx::Rcu& rcu, Int_t maxFEC, 
		     const UInt_t* mask) 
{
  if (fRcu) {
    std::cerr << "Already have an RCU tab" << std::endl;
    return;
  }
  fRcu  = new Rcu(fTab, rcu, maxFEC, mask);
	
  fOptionMenu.AddEntry("Debug &RCU",      Rcuxx::Rcu::kRcu);
  fOptionMenu.AddEntry("Debug &Back-end", Rcuxx::Rcu::kBackend);
}

//____________________________________________________________________
void 
RcuGui::Main::AddBc(Rcuxx::Bc& bc) 
{
  if (fBc) {
    std::cerr << "Already have an BC tab" << std::endl;
    return;
  }
  fBc  = new Bc(fTab, bc);
  fOptionMenu.AddEntry("Debug Board &controller", Rcuxx::Rcu::kBc);
}

//____________________________________________________________________
void 
RcuGui::Main::AddAltro(Rcuxx::Altro& altro) 
{
  if (fAltro) {
    std::cerr << "Already have an ALTRO tab" << std::endl;
    return;
  }
  fAltro  = new Altro(fTab, altro);
  fOptionMenu.AddEntry("Debug &Altro", Rcuxx::Rcu::kAltro);
}

//____________________________________________________________________
void 
RcuGui::Main::AddCompiler() 
{
  if (fCompiler) {
    std::cerr << "Already have an Compiler tab" << std::endl;
    return;
  }
  fCompiler  = new Compiler(fTab);
  fActionMenu.AddEntry("Choose program ...", kChoose);
}

//____________________________________________________________________
void 
RcuGui::Main::Display() 
{
  fMain.MapSubwindows();
  fMain.Resize(fMain.GetDefaultSize());
  Int_t w = int(fMain.GetWidth());
  Int_t h = int(fMain.GetHeight());
  // fMain.Resize(w,h);
  fMain.SetWMSize(w, h);
  fMain.SetWMSizeHints(int(w*.5), int(h*.8*.5), 2*w, 2*h, 2, 2);
  fMain.SetWindowName(Form("RCU Controls: %s", 
			   fRcu->Low().GetUrl().Raw().c_str()));
  fMain.SetIconName("RCU Controls");
  fMain.SetClassHints("RcuControl", "RcuControl");
  const TGPicture* p = 
    gClient->GetPicturePool()->GetPicture("rcu_gui.xpm", 
					  const_cast<char**>(rcuGui_xpm));
  if (p) {
    Pixmap_t pic = p->GetPicture();
    gVirtualX->SetIconPixmap(fMain.GetId(), pic);
  }
  // fMain.SetIconPixmap("rcu_gui.xpm");
  fMain.SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
  fMain.MapWindow();
}

//____________________________________________________________________
void 
RcuGui::Main::Destroy() 
{
  fgInstance->Close();
  gSystem->Sleep(100);
  if (fRcu)         delete fRcu;
  if (fBc)          delete fBc;
  if (fAltro)       delete fAltro;
  if (fAcquisition) delete fAcquisition;
  if (fCompiler)    delete fCompiler;
  Bool_t inter = fInteractive;
  // delete fgInstance;
  fgInstance = 0;
  if (!inter) exit(0); // gApplication->Terminate();
}


//____________________________________________________________________
void
RcuGui::Main::Update()
{
  std::cout << "Updating everything ... " << std::flush;
  if (fRcu)   fRcu->Update();
  if (fAltro) fAltro->Update();
  if (fBc)    fBc->Update();
  std::cout << "done" << std::endl;
}

//____________________________________________________________________
void 
RcuGui::Main::Close() 
{
  Rcuxx::DebugGuard g(fDebug, "RcuGui::Main::~Close()");
  fMain.DontCallClose();
  fMain.UnmapWindow();
  if (fBc)    delete fBc;
  if (fAltro) delete fAltro;
  if (fRcu)   delete fRcu;
  if (!fInteractive) gApplication->Terminate();
}

//____________________________________________________________________
void 
RcuGui::Main::HandleOption(Int_t id) 
{
  int ret = 0;
  if (fOptionMenu.IsEntryChecked(id)) fOptionMenu.UnCheckEntry(id);
  else                                fOptionMenu.CheckEntry(id);
  bool on = fOptionMenu.IsEntryChecked(id);
  switch (id) {
  case Rcuxx::Rcu::kRcu:   if (fRcu) fRcu->SetDebug(on); break;
  case Rcuxx::Rcu::kBackend: 
    if (!fRcu) break;
    new NumberDialog(gClient->GetRoot(), &fMain, "Please give a number ...", 
		     "Debug level for backend", ret);
    fRcu->SetDebugBackend(ret);
    break;
  case Rcuxx::Rcu::kAltro: if (fAltro) fAltro->SetDebug(on); break;
  case Rcuxx::Rcu::kBc:    if (fBc)    fBc->SetDebug(on); break;
  case 3:
    fDebug = on;
    break;
  case 4: 
    if (fAcquisition) fAcquisition->SetDebug(on);
    break;
  }
}

      
//____________________________________________________________________
void 
RcuGui::Main::HandleFile(Int_t id) 
{
  UInt_t ret = 0;
  switch (id) {
  case kUpdate: 
    Update();
    break;
  case kChoose: 
    if (fCompiler) { 
      ret = fCompiler->Select();
      fRcu->IMEM()->SetAddr(ret);
    }
    break;
  case kClose: 
    // Close connection to RCU 
    Close();
    break;
  case kQuit: 
    // Close connection to RCU 
    Main::Destroy();
    break;
  }
  if (ret) {
    // Main::Instance()->U2FError(ret);
    return;
  }
}
//____________________________________________________________________
void 
RcuGui::Main::Set4Acq(Int_t mask) 
{
  std::cout << "Getting values from GUI: " << std::flush;
  if (mask & Rcuxx::Acq::kACTFEC) { 
    std::cout << "ACTFEC " << std::flush;
    fRcu->ACTFEC()->Set();
  }
  if (mask & Rcuxx::Acq::kFECRST) { 
    std::cout << "FECRST " << std::flush;
  }
  if (mask & Rcuxx::Acq::kPMEM) { 
    std::cout << "PMEM " << std::flush;
    fRcu->PMEM()->ReadText();
  }
  if (mask & Rcuxx::Acq::kIMEM) { 
    std::cout << "IMEM " << std::flush;
    fRcu->IMEM()->ReadText();
  }
  if (mask & Rcuxx::Acq::kACL) { 
    std::cout << "ACL " << std::flush;
    fRcu->ACL()->Set();
  }
  if (mask & Rcuxx::Acq::kTRCFG1) { 
    std::cout << "TRCFG1 " << std::flush;
    fRcu->TRCFG1()->Set();
  }
  std::cout << "done" << std::endl;
}

//____________________________________________________________________
void 
RcuGui::Main::SetStatus(const char* text, Bool_t noupdate) 
{ 
  fStatusBar.SetText(text, 0); 
  if (noupdate) return;
  if (fRcu && fRcu->ERRST()) fRcu->ERRST()->Update();
}

//____________________________________________________________________
void 
RcuGui::Main::SetError(const char* text, Bool_t abort, Bool_t altro) 
{ 
  fStatusBar.SetText(text, 1); 
  if (!text || text[0] == '\0') return;
  int ret = 0;
  int but = kMBIgnore|kMBRetry;
  if (abort) but |= kMBDismiss;
  new TGMsgBox(gClient->GetRoot(), &fMain, "Error from RCU", text, 
	       kMBIconExclamation, but, &ret);
  if (!fRcu || !fRcu->Low().RS_STATUS()) return;
  switch (ret) {
  case kMBDismiss: 
    if (abort) fRcu->Low().ABORT()->Commit();
    if (altro) fRcu->Low().FECRST()->Commit();
  case kMBRetry:   
    fRcu->Low().RS_STATUS()->Commit(); 
    fRcu->ERRST()->Update();
    break;
  case kMBIgnore: 
    break;
  default: 
    break;
  }
}

//____________________________________________________________________
void 
RcuGui::Main::DebugMsg(Int_t lvl, const Char_t* where, const char* format, ...)
{
  if (!fDebug) return;
  va_list ap;
  va_start(ap, format);
  ErrorHandler(kInfo, where, (format), ap);
  va_end(ap);
}

//____________________________________________________________________
bool
RcuGui::Main::IsDebug() const
{
  return fDebug;
}
//____________________________________________________________________
UInt_t
RcuGui::Main::Error(UInt_t errcode)
{
  std::string str = fRcu->Low().ErrorString(errcode);
  SetError(str.c_str());
  return errcode;
}



//____________________________________________________________________
//
// EOF
//

// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_COMPILER_H
#define RCUGUI_COMPILER_H
#ifndef RCUGUI_SCROLLABLE_H
# include <rcugui/Scrollable.h>
#endif
#ifndef ROOT_TString
# include <TString.h>
#endif
#ifndef ROOT_TGTextEdit
# include <TGTextEdit.h>
#endif
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef ROOT_TGTextView
# include <TGTextView.h>
#endif 
#ifndef ROOT_TGTextEdit
# include <TGTextEdit.h>
#endif 
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif
#ifndef ROOT_TGComboBox
# include <TGComboBox.h>
#endif
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif
#ifndef ROOT_TGNumberEntry
# include <TGNumberEntry.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

namespace RcuGui 
{
  /** Select a program 
      @ingroup rcugui_low 
  */
  class ProgramSelect : public TGTransientFrame
  {
  public:
    /** Buttons */
    TGButtonGroup*     fButtons;
    /** OK */
    TGTextButton*      fOK;
    /** Cancel */
    TGTextButton*      fCancel; 
    /** Top frame */
    TGGroupFrame*      fTop;
    /** Combobox */
    TGComboBox*        fCombo;
    /** Offsets */
    TGNumberEntry*     fOff;
    /** Which */
    Int_t              fWhich;
    /** First */
    Int_t              fFirst;
    /** Last */
    Int_t              fLast;
  public:
    /** Constructor 
	@param p 
	@return  */
    ProgramSelect(const TGWindow* p);
    /** @param id Button to handle */
    void  HandleButtons(Int_t id);
    /** @param id Entry to handle  */
    void  HandleEntry(Int_t id);
    /** Clear it */
    void  Clear();
    /** Add label and offset 
	@param lab 
	@param off */
    void  Add(const char* lab, size_t off);
    /** Show it */
    Int_t Show();
    ClassDef(ProgramSelect, 0);
  };
    
  /** @class Compiler 
      @brief Compiler interface 
      @ingroup rcugui_low 
  */
  class Compiler : public Scrollable
  {
  private:
    /** Current file name */
    TString          fCurrentFile;
    /** Container of code and program views */
    TGVerticalFrame  fViews;
    /** Code editor */
    TGTextEdit       fCodeView;
    /** File name display */
    TGLabel          fCurrent;
    /** Program view */
    TGTextEdit       fProgView;
    /** Button group */
    TGButtonGroup    fButtons;
    /** Compile button */
    TGTextButton     fCompile;
    /** Compile and load button */
    TGTextButton     fCommit;
    /** Load a file into the  code buffer button */
    TGTextButton     fLoad;
    /** Save code buffer to a file */
    TGTextButton     fSave;
    /** Clear view and editor */
    TGTextButton     fClear;
    /** Code buffer */
    TGText*          fCode;
    /** Program buffer */
    TGText*          fProgram;
    /** Clear view and editor */
    // TGTextButton     fExec;
    /** Program selection */
    ProgramSelect*   fProgramSelect;
  public:
    /** Constructor 
	@param tabs  */
    Compiler(TGTab& tabs);
    /** Compile */
    Bool_t Compile();
    /** Compile and load */
    void Commit();
    /** Load a file into the  code buffer button */
    void Load();
    /** Save code buffer to a file */
    void Save();
    /** Clear view and editor */
    void Clear();
    /** Changed view */
    void Changed();
    /** handle open */
    void Opened();
    /** Handle close */
    void Closed();
    /** Handle save */
    void Saved();
    /** Open, compile, and load */
    void OpenCompileLoad(const Char_t* file);
    /** Select program */
    Int_t Select() { return (fProgramSelect ? fProgramSelect->Show() : 0); }
    /** @param debug set dedbug mode on or off */
    void SetDebug(Bool_t debug);
  };
}

#endif
//
// EOF
//






// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BCCSR3_H
#define RCUGUI_BCCSR3_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcCSR3.h>


namespace RcuGui
{
  //====================================================================
  struct CSR3 : public RcuGui::Register 
  {
    Rcuxx::BcCSR3& fLow;
    TGCheckButton       fCnvEnd;
    RcuGui::LabeledIntEntry fWarnRatio;
    RcuGui::LabeledIntEntry fWatchDog;
  
    CSR3(TGCompositeFrame& f, Rcuxx::BcCSR3& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fCnvEnd(&fFields, "Conversion end"),
	fWarnRatio(fFields, "rclk/sclk warning ratio", 0, 0x7f, 
		   kHorizontalFrame),
	fWatchDog(fFields, "ALTRO master watch dog", 0, 0xff)
    {
      fFields.AddFrame(&fCnvEnd, new TGLayoutHints(0, ISCALE(10)));
      fWarnRatio.SetValue(0x22);
      fWatchDog.SetValue(0x20);
      Get();
    }
    void Get()
    {
      fWatchDog.SetValue(fLow.WatchDog());
      fWarnRatio.SetValue(fLow.WarnRatio());
      fCnvEnd.SetState(fLow.IsCnvEnd()  ? kButtonDown : kButtonUp);
    }
    void Set()
    {
      fLow.SetWatchDog(fWatchDog.GetValue());
      fLow.SetWarnRatio(fWarnRatio.GetValue());
      // fLow.SetCnvEnd(fCnvEnd.IsDown());
    }
  };
}

#endif
//
// EOF
//

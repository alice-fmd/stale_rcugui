// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BCDSTBCNT_H
#define RCUGUI_BCDSTBCNT_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcDSTBCNT.h>


namespace RcuGui
{
  //____________________________________________________________________
  struct DSTBCNT : public RcuGui::Register
  {
    RcuGui::LabeledIntView   fDSTBCNT;
    Rcuxx::BcDSTBCNT&          fLow;
    DSTBCNT(TGCompositeFrame& f, Rcuxx::BcDSTBCNT& low) 
      : Register(f, low),
	fDSTBCNT(fFields,"DSTB",0,0xffff,kHorizontalFrame),
	fLow(low)
    {}
    void Get() { fDSTBCNT.SetValue(fLow.Counts()); }
  };
}

#endif
//
// EOF
//

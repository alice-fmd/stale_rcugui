// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BC_ANALOGVOLTAGE_H
#define RCUGUI_BC_ANALOGVOLTAGE_H
#include <rcugui/bc/Monitored.h>
#include <rcuxx/bc/BcAV.h>
#include <rcuxx/bc/BcAV_TH.h>


namespace RcuGui
{  
  //__________________________________________________________________
  struct AnalogVoltage : public RcuGui::Monitored
  {
    AnalogVoltage(TGCompositeFrame& f, 
		  Rcuxx::BcAV&    low1, 
		  Rcuxx::BcAV_TH& low2)
      : Monitored(f, low1, low2, kHorizontalFrame, true) {}
  };
}
#endif
//
// EOF
//

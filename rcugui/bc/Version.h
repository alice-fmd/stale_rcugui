// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BC_Version_H
#define RCUGUI_BC_Version_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcVersion.h>


namespace RcuGui
{
  //____________________________________________________________________
  struct Version : public RcuGui::Register
  {
    Rcuxx::BcVersion& fLow;
    TGHorizontalFrame fFrame;
    TGLabel fLabel;
    TGTextEntry fVersion;
    TGLayoutHints fHints;
    // RcuGui::LabeledFloatView fVersion;
    Version(TGCompositeFrame& f, Rcuxx::BcVersion& low)
      : RcuGui::Register(f, low, false, 3, 3, 0, 3), 
	fLow(low),
	// fVersion(fFields,"Value",0,10,kHorizontalFrame)
	fFrame(&fFields),
	fLabel(&fFrame, "Firmware version number:"),
	fVersion(&fFrame, ""),
	fHints(kLHintsExpandX, 3, 3, 0, 0)
    {
      fFields.AddFrame(&fFrame, &fHints);
      fFrame.AddFrame(&fLabel, &fHints);
      fFrame.AddFrame(&fVersion, &fHints);
      // fVersion.SetValue(0);
      fVersion.SetToolTipText("Firmware version");
      fVersion.SetEnabled(kFALSE);
      fVersion.SetAlignment(kTextRight);
      // fVersion.SetFrameDrawn(kFALSE);
      Get();
    }
    void Get() 
    { 
      fVersion.SetText(Form("%d.%d", fLow.Major(), fLow.Minor()));
      // fVersion.SetValue(fLow.AsFloat()); 
    }
  };
}

#endif
//
// EOF
//

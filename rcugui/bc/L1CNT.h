// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BCL1CNT_H
#define RCUGUI_BCL1CNT_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcL1CNT.h>


namespace RcuGui
{
  //____________________________________________________________________
  struct L1CNT : public RcuGui::Register
  {
    RcuGui::LabeledIntView   fL1CNT;
    Rcuxx::BcL1CNT&          fLow;
    L1CNT(TGCompositeFrame& f, Rcuxx::BcL1CNT& low) 
      : Register(f, low),
	fL1CNT(fFields,"L1",0,0xffff,kHorizontalFrame),
	fLow(low)
    {}
    void Get() { fL1CNT.SetValue(fLow.Counts()); }
  };
}

#endif
//
// EOF
//

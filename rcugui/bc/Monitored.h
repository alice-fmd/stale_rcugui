// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BCMONITORED_H
#define RCUGUI_BCMONITORED_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcMonitored.h>
# include <rcuxx/bc/BcThreshold.h>


namespace RcuGui
{
  /** @struct Monitored 
      @brief Class that holds monitor registers 
   */
  struct Monitored : public Register
  {
    /** Constructor 
	@param f   Parent
	@param cur Register that holds current value
	@param thr Register that holds threshold 
	@param def Frame options. 
	@param under Whether we're check if it's under the threshold */
    Monitored(TGCompositeFrame& f, 
	      Rcuxx::BcMonitored& cur, 
	      Rcuxx::BcThreshold& thr,
	      unsigned int def=kVerticalFrame, 
	      bool under=false);
    /** Update current value  and threshold.  If the monitor check
	button is enabled, then also do conversion.
	@return @c false if the update failed, or timed out. */
    Bool_t Update();
    /** Decode the return.  */
    void Get();
    /** Set for commit */
    void Set() { fThr.SetThreshold(fThreshold.GetValue()); }
    /** Print information */
    void Print() const;
    /** Number of tries. */
    static UInt_t fgNTries;
  protected:
    /** Under is bad */
    Bool_t                     fUnder;
    /** Current value */
    Rcuxx::BcMonitored&         fCur;
    /** Threshold */
    Rcuxx::BcThreshold&         fThr;
    /** Fram */
    TGCompositeFrame           fFrame;
    /** Whether it's bad */
    // RcuGui::ErrorBit           fBad;
    /** Monitor enable */
    TGCheckButton              fMonitor;
    /** Current value */
    RcuGui::LabeledFloatView   fCurrent;
    /** Threshold */
    RcuGui::LabeledFloatEntry  fThreshold;
  };
}

//____________________________________________________________________
inline
RcuGui::Monitored::Monitored(TGCompositeFrame& f, 
			     Rcuxx::BcMonitored& cur, 
			     Rcuxx::BcThreshold& thr,
			     unsigned int def, 
			     bool under)
  : RcuGui::Register(f, thr, false),
    fUnder(under),
    fCur(cur), 
    fThr(thr),
    fFrame(&fFields, 0, 0, def),
    // fBad(fFields, "", Form("%s out of bounds", cur.Name().c_str())),
    fMonitor(&fFields, "Monitor", def),
    fCurrent(fFrame, Form("Value [%s]", cur.Unit()),0,
	     cur.Convert2Natural(0x3ff)),
    fThreshold(fFrame, Form("/ "),0, 
	       thr.Convert2Natural(0x3ff))
{
  fMonitor.SetToolTipText("If enabled, run acquisition of state");
  fGroup.SetTitle(cur.Name().c_str());
  fFields.AddFrame(&fMonitor, new TGLayoutHints(kLHintsCenterY, 
						ISCALE(3), ISCALE(3), 
						ISCALE(5), ISCALE(5)));
  fFields.AddFrame(&fFrame, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
  Get();
}
//____________________________________________________________________
inline
Bool_t 
RcuGui::Monitored::Update() 
{  
  // Rcuxx::DebugGuard g(fDebug, "RcuGui::Monitored::Update()");
  bool monret = true;
  RcuGui::Main* m = RcuGui::Main::Instance();
  if (fMonitor.IsOn()) { 
    // Rcuxx::DebugGuard::Message(fDebug, "Doing conversion (%d tries)",
    // fgNTries);
    monret = fCur.Monitor(fgNTries);
    if (!monret) 
      m->SetError(Form("Conversion didn't end in %d tries", fgNTries));
  }
  UInt_t ret = fCur.Update();
  if (ret) 	m->Error(ret);
  return RcuGui::Register::Update();
}
//____________________________________________________________________
inline
void
RcuGui::Monitored::Get()
{
  fThreshold.SetValue(fThr.ThresholdNatural());
  fCurrent.SetValue(fCur.CurrentNatural());
  Bool_t status = kTRUE;
  if (fUnder && fCur.CurrentNatural() < fThr.ThresholdNatural())
    status = kFALSE;
  else if (!fUnder && fCur.CurrentNatural() > fThr.ThresholdNatural())
    status = kFALSE;
  fCurrent.fView.ChangeBackground(status ? 0x00aa00 : 0xaa0000);
  // fBad.SetState(status);
}

//____________________________________________________________________
inline
void
RcuGui::Monitored::Print() const 
{
  fCur.Print();
  fThr.Print();
}
#endif
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BC_CSR01_H
#define RCUGUI_BC_CSR01_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcCSR0.h>
# include <rcuxx/bc/BcCSR1.h>


namespace RcuGui
{
  //____________________________________________________________________
  struct CSR01 : public RcuGui::Register
  {
    //____________________________________________________________________
    struct BcErrorBit : public RcuGui::ErrorBit 
    {
      TGLayoutHints fHints;
      TGCheckButton fEnable;
      BcErrorBit(TGCompositeFrame& p, const Char_t* name, const Char_t* desc)
	: RcuGui::ErrorBit(p, name, desc),
	  fHints(kLHintsCenterX),
	  fEnable(&fTop, "", 1)
      {
	fTop.AddFrame(&fEnable, &fHints);
	fEnable.SetState(kButtonDown);
      }
      void SetEnabled(Bool_t what) 
      {
	fEnable.SetState(what ? kButtonDown : kButtonUp);
      }
      Bool_t IsEnabled() const 
      {
	return fEnable.IsDown();
      }
    };
    //____________________________________________________________________
    struct PrivateCSR01 : public Rcuxx::Register 
    {
      Rcuxx::BcCSR0& fMask;
      Rcuxx::BcCSR1& fVal;
      PrivateCSR01(Rcuxx::BcCSR0& mask, Rcuxx::BcCSR1& val) 
	: Register("CSR0/1", "configuration/status registers 0 & 1", true), 
	  fMask(mask), 
	  fVal(val)
      {}
      /** Whether we got Clear */
      virtual bool IsClearable() const { return true; }
      virtual unsigned int Commit() { return fMask.Commit();  }
      virtual unsigned int Clear() { return fVal.Clear(); }
      virtual unsigned int Update() 
      { 
	unsigned int ret = 0;
	if ((ret = fMask.Update())) return  ret;
	if ((ret = fVal.Update())) return  ret;
	return ret; 
      }    
      virtual void Print() const 
      {
	fMask.Print();
	fVal.Print();
      }
    };

    PrivateCSR01*  fBoth;

    TGLayoutHints fHints;
    TGGroupFrame  fErrors;
    RcuGui::ErrorBit      fError;
    RcuGui::ErrorBit      fSlowControl;
    RcuGui::ErrorBit      fAltroError;
    BcErrorBit    fParity;
    BcErrorBit    fInstruction;

    TGGroupFrame  fInterrupts;
    RcuGui::ErrorBit      fInterrupt;
    BcErrorBit    fMissedSclk;
    BcErrorBit    fAlps;
    BcErrorBit    fPaps;
    BcErrorBit    fDcOverTh;
    BcErrorBit    fDvOverTh;
    BcErrorBit    fAcOverTh;
    BcErrorBit    fAvOverTh;
    BcErrorBit    fTempOverTh;
    TGLayoutHints  fCnvHints;
    TGCheckButton fCnv;

    // Rcuxx::BcCSR0* fMask;
    // Rcuxx::BcCSR1* fVal;
    CSR01(TGCompositeFrame& f, Rcuxx::BcCSR0& mask, Rcuxx::BcCSR1& val)
      : RcuGui::Register(f, *(fBoth = new PrivateCSR01(mask,val)), true),
	fHints(kLHintsExpandX),
	fErrors(&fFields, "Errors", kHorizontalFrame),
	fError(fErrors, "Error", "Error recvied"),
	fSlowControl(fErrors, "SC", "Slow control instruction error"),
	fAltroError(fErrors, "ALTRO", "ALTRO Error"),
	fParity(fErrors, "Parity", "Parity error"),
	fInstruction(fErrors, "Instr", "Instruction error"),
	fInterrupts(&fFields, "Interrupts", kHorizontalFrame),
	fInterrupt(fInterrupts, "Interrupt","Interrupt recvied"),
	fMissedSclk(fInterrupts, "SCLK", "Missed sample clocks"),
	fAlps(fInterrupts, "Alps", "ALTRO power supply error"),
	fPaps(fInterrupts, "Paps", "PASA power supply error"),
	fDcOverTh(fInterrupts, "DC", "Digital current over threshold"),
	fDvOverTh(fInterrupts, "DV", "Digital voltage over threshold"),
	fAcOverTh(fInterrupts, "AC", "Analog current over threshold"),
	fAvOverTh(fInterrupts, "AV", "Analog voltage over threshold"),
	fTempOverTh(fInterrupts, "T", "Temperture over threshold"),
	fCnvHints(kLHintsCenterY, 10),
	fCnv(&fFields, "Continues ADC conversion", 0)
    {
      fFields.AddFrame(&fErrors, &fHints);
      fFields.AddFrame(&fInterrupts, &fHints);
      fFields.AddFrame(&fCnv, &fCnvHints);
      Get();
    }
    //     Bool_t Update() 
    //     {
    //       //       UInt_t ret = fBoth->Update();
    //       //       if (ret) {
    //       // 	RcuGui::Main::Instance().Error(ret);
    //       // 	return kFALSE;
    //       //       }
    //       return RcuGui::Register::Update();
    //     }
    void Get()
    {
      fTempOverTh.SetEnabled(fBoth->fMask.IsTempOverTh());
      fAvOverTh.SetEnabled(fBoth->fMask.IsAvOverTh());
      fAcOverTh.SetEnabled(fBoth->fMask.IsAcOverTh());
      fDvOverTh.SetEnabled(fBoth->fMask.IsDvOverTh());
      fDcOverTh.SetEnabled(fBoth->fMask.IsDcOverTh());
      fPaps.SetEnabled(fBoth->fMask.IsPaps());
      fAlps.SetEnabled(fBoth->fMask.IsAlps());
      fMissedSclk.SetEnabled(fBoth->fMask.IsMissedSclk());
      fParity.SetEnabled(fBoth->fMask.IsParity());
      fInstruction.SetEnabled(fBoth->fMask.IsInstruction());
      fCnv.SetState(fBoth->fMask.IsCnv() ? kButtonDown : kButtonUp);
    
      fTempOverTh.SetState(!fBoth->fVal.IsTempOverTh());
      fAvOverTh.SetState(!fBoth->fVal.IsAvOverTh());
      fAcOverTh.SetState(!fBoth->fVal.IsAcOverTh());
      fDvOverTh.SetState(!fBoth->fVal.IsDvOverTh());
      fDcOverTh.SetState(!fBoth->fVal.IsDcOverTh());
      fPaps.SetState(!fBoth->fVal.IsPaps());
      fAlps.SetState(!fBoth->fVal.IsAlps());
      fMissedSclk.SetState(!fBoth->fVal.IsMissedSclk());
      fParity.SetState(!fBoth->fVal.IsParity());
      fInstruction.SetState(!fBoth->fVal.IsInstruction());
      fAltroError.SetState(!fBoth->fVal.IsAltroError());
      fSlowControl.SetState(!fBoth->fVal.IsSlowControl());
      fError.SetState(!fBoth->fVal.IsError());
      fInterrupt.SetState(!fBoth->fVal.IsInterrupt());
    }
    void Set()
    {
      fBoth->fMask.SetTempOverTh(fTempOverTh.IsEnabled());
      fBoth->fMask.SetAvOverTh(fAvOverTh.IsEnabled());
      fBoth->fMask.SetAcOverTh(fAcOverTh.IsEnabled());
      fBoth->fMask.SetDvOverTh(fDvOverTh.IsEnabled());
      fBoth->fMask.SetDcOverTh(fDcOverTh.IsEnabled());
      fBoth->fMask.SetPaps(fPaps.IsEnabled());
      fBoth->fMask.SetAlps(fAlps.IsEnabled());
      fBoth->fMask.SetMissedSclk(fMissedSclk.IsEnabled());
      fBoth->fMask.SetParity(fParity.IsEnabled());
      fBoth->fMask.SetInstruction(fInstruction.IsEnabled());
      fBoth->fMask.SetCnv(fCnv.IsDown());
    }
    void Clear() 
    {
      fBoth->Clear();
      RcuGui::Main::Instance()->SetStatus(Form("Cleared %s",
					       fBoth->Name().c_str()));
      Get();
    }    
  };
}

#endif
//
// EOF
//

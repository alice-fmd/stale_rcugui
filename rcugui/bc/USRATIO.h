// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BC_USRATIO_H
#define RCUGUI_BC_USRATIO_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcUSRATIO.h>


namespace RcuGui
{
  //____________________________________________________________________
  struct USRATIO : public RcuGui::Register
  {
    Rcuxx::BcUSRATIO& fLow;
    RcuGui::LabeledIntEntry fUSRATIO;
    USRATIO(TGCompositeFrame& f, Rcuxx::BcUSRATIO& low)
      : RcuGui::Register(f, low), fLow(low),
	fUSRATIO(fFields,"Value",0,0xffff,kHorizontalFrame)
    {
      fUSRATIO.SetValue(1);
      fUSRATIO.SetToolTipText("Under sampling ratio in test mode");
      Get();
    }
    void Get() { fUSRATIO.SetValue(fLow.Ratio()); }
    void Set() { fLow.SetRatio(fUSRATIO.GetValue()); }
  };
}

#endif
//
// EOF
//

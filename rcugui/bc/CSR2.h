// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BC_CSR2_H
#define RCUGUI_BC_CSR2_H
# include <rcugui/LabeledNumber.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/bc/BcCSR2.h>


namespace RcuGui
{
  //____________________________________________________________________
  struct CSR2 : public RcuGui::Register 
  {
    Rcuxx::BcCSR2& fLow;
    TGLayoutHints	fLH1;
    TGLayoutHints	fLH2;
    TGLayoutHints	fLH3;
    RcuGui::LabeledIntView   fHADD;
    TGGroupFrame        fTestMode;
    TGCheckButton       fCardIsolated;
    TGCheckButton       fContinousTSM;
    RcuGui::LabeledIntEntry  fALTROAddress;
    RcuGui::LabeledIntEntry  fADCAddress;
    TGGroupFrame        fClockEnables;
    TGCheckButton       fAdcClock;
    TGCheckButton       fRdoClock;
    TGGroupFrame        fPowerSupplyEnables;
    TGCheckButton       fPASASwitch;
    TGCheckButton       fALTROSwitch;
  
    CSR2(TGCompositeFrame& f, Rcuxx::BcCSR2& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fLH1(kLHintsExpandX, ISCALE(10)),
	fLH2(kLHintsExpandY|kLHintsExpandX, ISCALE(10)),
	fLH3(kLHintsExpandY, ISCALE(10)),
	fHADD(fFields,"HADD", 0, 0x1f),
	fTestMode(&fFields, "Test Mode", kVerticalFrame),
	fCardIsolated(&fTestMode, "Card Isolated"),
	fContinousTSM(&fTestMode, "Continues TSM"),
	fALTROAddress(fTestMode, "ALTRO Address",0, 0x7),
	fADCAddress(fTestMode, "ADC Address",0, 0x3),
	fClockEnables(&fFields, "ClockEnables", kVerticalFrame),
	fAdcClock(&fClockEnables, "ADC"),
	fRdoClock(&fClockEnables, "RDO"),
	fPowerSupplyEnables(&fFields, "Power supply enables", kVerticalFrame),
	fPASASwitch(&fPowerSupplyEnables, "PASA"),
	fALTROSwitch(&fPowerSupplyEnables, "ALTRO")
    {
      fFields.AddFrame(&fTestMode, &fLH2);
      fTestMode.AddFrame(&fCardIsolated, &fLH1);
      fTestMode.AddFrame(&fContinousTSM, &fLH1);
    
      fFields.AddFrame(&fClockEnables, &fLH3);
      fAdcClock.SetState(kButtonDown);
      fClockEnables.AddFrame(&fAdcClock, &fLH1);
      fRdoClock.SetState(kButtonDown);
      fClockEnables.AddFrame(&fRdoClock, &fLH1);
    
      fFields.AddFrame(&fPowerSupplyEnables, &fLH3);
      fPASASwitch.SetState(kButtonDown);
      fPowerSupplyEnables.AddFrame(&fPASASwitch, &fLH1);
      fALTROSwitch.SetState(kButtonDown);
      fPowerSupplyEnables.AddFrame(&fALTROSwitch, &fLH1);
      fHADD.SetToolTipText("Hardware address");
      fCardIsolated.SetToolTipText("Isolate card");
      
      Get();
    }
    void Get()
    {
      fALTROSwitch.SetState(fLow.IsALTROSwitch()  ? kButtonDown : kButtonUp);
      fPASASwitch.SetState(fLow.IsPASASwitch() ? kButtonDown : kButtonUp);
      fAdcClock.SetState(fLow.IsAdcClock() ? kButtonDown : kButtonUp);
      fRdoClock.SetState(fLow.IsRdoClock() ? kButtonDown : kButtonUp);
      fADCAddress.SetValue(fLow.ADCAddress());
      fALTROAddress.SetValue(fLow.ALTROAddress());
      fContinousTSM.SetState(fLow.IsContinousTSM()? kButtonDown : kButtonUp);
      fCardIsolated.SetState(fLow.IsCardIsolated()? kButtonDown : kButtonUp);
      fHADD.SetValue(fLow.HADD());
    }
    void Set()
    {
      fLow.SetALTROSwitch(fALTROSwitch.IsDown());
      fLow.SetPASASwitch(fPASASwitch.IsDown());
      fLow.SetAdcClock(fAdcClock.IsDown());
      fLow.SetRdoClock(fRdoClock.IsDown());
      fLow.SetADCAddress(fADCAddress.GetValue());
      fLow.SetALTROAddress(fALTROAddress.GetValue());
      fLow.SetContinousTSM(fContinousTSM.IsDown());
      fLow.SetCardIsolated(fCardIsolated.IsDown());
    }
  };
}

#endif
//
// EOF
//

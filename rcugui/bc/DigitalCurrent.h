// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_BC_DIGITIALCURRENT_H
#define RCUGUI_BC_DIGITIALCURRENT_H
#include <rcugui/bc/Monitored.h>
#include <rcuxx/bc/BcDC.h>
#include <rcuxx/bc/BcDC_TH.h>


namespace RcuGui
{  
  //__________________________________________________________________
  struct DigitialCurrent : public RcuGui::Monitored
  {
    DigitialCurrent(TGCompositeFrame& f, 
		    Rcuxx::BcDC&    low1, 
		    Rcuxx::BcDC_TH& low2)
      : Monitored(f, low1, low2, kHorizontalFrame) {}
  };
}
#endif
//
// EOF
//

//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/ErrorBit.h>
#include <TGMsgBox.h>
#include <iostream>

//====================================================================
RcuGui::ErrorBit::ErrorBit(TGCompositeFrame& p, 
			   const Char_t* name, const Char_t* desc,
			   Int_t okColour, Int_t badColour) 
  : fTopLH(kLHintsNormal, 3, 0, 0, 0),
    fNameLH(kLHintsCenterX),
    fStateLH(kLHintsCenterX|kLHintsExpandX),
    fTop(&p, 60), 
    fName(&fTop, name), 
    fState(&fTop, "  ", -1, TGButton::GetDefaultGC()(), 
	   TGTextButton::GetDefaultFontStruct(),  0),
    fDescription(desc), fOkColour(okColour), fBadColour(badColour)
{
  p.AddFrame(&fTop, &fTopLH);
  fTop.AddFrame(&fName, &fNameLH);
  fState.SetToolTipText(fDescription.Data());
  fState.SetEnabled(kFALSE);
  SetState(kTRUE);    
  fState.Connect("Clicked()", "RcuGui::ErrorBit", this, "HandleButton()");
  fTop.AddFrame(&fState, &fStateLH);
}
//____________________________________________________________________
void 
RcuGui::ErrorBit::HandleButton() 
{
  new TGMsgBox(gClient->GetRoot(), gClient->GetRoot(), fName.GetTitle(), 
	       fDescription.Data(), kMBIconQuestion);
  TString tmp;
  SetState(fGood, tmp);
}

//____________________________________________________________________
void 
RcuGui::ErrorBit::SetState(Bool_t ok) 
{
  fGood = ok;
  fState.ChangeBackground(ok ? fOkColour : fBadColour);
  gClient->NeedRedraw(&fState, kTRUE);
}

//____________________________________________________________________
void 
RcuGui::ErrorBit::SetState(Bool_t ok, TString& errstr) 
{
  fGood = ok;
  fState.ChangeBackground(ok ? fOkColour : fBadColour);
  if (!fGood) { 
    errstr.Append(fName.GetTitle());
    errstr.Append(" ");
  }
  gClient->NeedRedraw(&fState, kTRUE);
}

//____________________________________________________________________
//
// EOF
//

//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_KLCOEFFS_H
# define RCUGUI_ALTRO_KLCOEFFS_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/altro/AltroKLCoeffs.h>

namespace 
{
  bool fDebug = false;
  //====================================================================
  struct KLCoeffs : public RcuGui::Register 
  {
    Rcuxx::AltroKLCoeffs&        fLow1;
    Rcuxx::AltroKLCoeffs&        fLow2;
    Rcuxx::AltroKLCoeffs&        fLow3;
    RcuGui::LabeledIntEntry      fCoeff1;
    RcuGui::LabeledIntEntry      fCoeff2;
    RcuGui::LabeledIntEntry      fCoeff3;
    KLCoeffs(TGCompositeFrame& f, char w, 
	     Rcuxx::AltroKLCoeffs& low1,
	     Rcuxx::AltroKLCoeffs& low2,
	     Rcuxx::AltroKLCoeffs& low3)
      : RcuGui::Register(f, low1),
	fLow1(low1),
	fLow2(low2),
	fLow3(low3),
	fCoeff1(fFields, fLow1.Name().c_str(),0, 0xffff, kVerticalFrame),
	fCoeff2(fFields, fLow2.Name().c_str(),0, 0xffff, kVerticalFrame),
	fCoeff3(fFields, fLow3.Name().c_str(),0, 0xffff, kVerticalFrame)
    {
      fGroup.SetTitle(Form("Filter %c coefficents (channel specific)", 
			   fLow1.Name()[0]));
      Get();
    }
    Bool_t Update() 
    {
      UInt_t ret = 0;
      Set();
      try {
	if ((ret = fLow1.Update())) throw ret;
	if ((ret = fLow2.Update())) throw ret;
	if ((ret = fLow3.Update())) throw ret;
      }
      catch (UInt_t& ret) {
	RcuGui::Main::Instance()->Error(ret);
      }
      RcuGui::Main::Instance()->SetStatus(Form("Read %s,%s,%s",
					       fLow1.Name().c_str(),
					       fLow2.Name().c_str(),
					       fLow3.Name().c_str()));
      Get();
      return ret == 0;
    }
    void Get() 
    {
      fCoeff1.SetValue(fLow1.Value());
      fCoeff2.SetValue(fLow2.Value());
      fCoeff3.SetValue(fLow3.Value());
    }
    void Set()
    {
      fLow1.SetValue(unsigned(fCoeff1.GetValue()));
      fLow2.SetValue(unsigned(fCoeff2.GetValue()));
      fLow3.SetValue(unsigned(fCoeff3.GetValue()));
    }
    void Print() const
    {
      fLow1.Print();
      fLow2.Print();
      fLow3.Print();
    }
    Bool_t Commit() 
    {
      UInt_t ret = kTRUE;
      Set();
      try {
	if ((ret = fLow1.Commit())) throw ret;
	if ((ret = fLow2.Commit())) throw ret;
	if ((ret = fLow3.Commit())) throw ret;
      }
      catch (UInt_t& ret) {
	RcuGui::Main::Instance()->Error(ret);
      }
      RcuGui::Main::Instance()->SetStatus(Form("Wrote %s,%s,%s",
					       fLow1.Name().c_str(),
					       fLow2.Name().c_str(),
					       fLow3.Name().c_str()));
      return ret == 0;
    }
  };
}
#endif
//
// EOF
// 

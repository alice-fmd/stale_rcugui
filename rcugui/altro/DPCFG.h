//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_DPCFG_H
# define RCUGUI_ALTRO_DPCFG_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcugui/rcu/FirstBaseline.h>
#include <rcugui/rcu/SecondBaseline.h>
#include <rcugui/rcu/ZeroSuppression.h>
#include <rcuxx/altro/AltroDPCFG.h>

namespace RcuGui
{
  //====================================================================
  struct DPCFG : public RcuGui::Register 
  {
    Rcuxx::AltroDPCFG& fLow;
    FirstBaseline      f1stB;
    SecondBaseline     f2ndB;
    ZeroSuppression    fZS;
    
    DPCFG(TGCompositeFrame& f, Rcuxx::AltroDPCFG& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	f1stB(fFields, true), 
	f2ndB(fFields), 
	fZS(fFields)
    {
      // 
      Get();
    }
    void Get()
    {
      // f1stBMode.SetValue(fLow.FirstBMode());
      f1stB.Get(fLow.FirstBMode(), fLow.IsFirstBPol());
      f2ndB.Get(fLow.IsSecondBEnable(), fLow.SecondBPre(), fLow.SecondBPost());
      fZS.Get(fLow.IsZSEnable(), fLow.ZSPre(), fLow.ZSPost(), fLow.ZSGlitch());
    }
    void Set()
    {
      unsigned short mode, pre, post;
      bool enable;
      f1stB.Set(mode, enable);
      fLow.SetFirstBMode(mode);
      fLow.SetFirstBPol(enable);
      
      f2ndB.Set(enable, pre, post);
      fLow.SetSecondBEnable(enable);
      fLow.SetSecondBPre(pre);
      fLow.SetSecondBPost(post);

      fZS.Set(enable, pre, post, mode);
      fLow.SetZSEnable(enable);
      fLow.SetZSPre(pre);
      fLow.SetZSPost(post);
      fLow.SetZSGlitch(mode);
    }
  };
}
#endif
//
// EOF
// 

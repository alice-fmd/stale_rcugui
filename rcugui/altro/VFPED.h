//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_VFPED_H
# define RCUGUI_ALTRO_VFPED_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/altro/AltroVFPED.h>

namespace RcuGui
{
  //====================================================================
  struct VFPED : public RcuGui::Register 
  {
    Rcuxx::AltroVFPED& fLow;
    RcuGui::LabeledIntView  fVP;
    RcuGui::LabeledIntEntry fFP;
    VFPED(TGCompositeFrame& f, Rcuxx::AltroVFPED& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fVP(fFields,"Variable", 0, 0x3ff,kHorizontalFrame),
	fFP(fFields,"Fixed", 0, 0x3ff, kHorizontalFrame)
    {
      fGroup.SetTitle("Variable/Fixed pedestal (channel specific)");
      Get();
    }
    void Get()
    {
      fVP.SetValue(fLow.VP());
      fFP.SetValue(fLow.FP());
    }
    void Set() { fLow.SetFP((fFP.GetValue() & 0x3ff)); }
  };
}
#endif
//
// EOF
// 

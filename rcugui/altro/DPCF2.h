//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_DPCF2_H
# define RCUGUI_ALTRO_DPCF2_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/altro/AltroDPCF2.h>

namespace RcuGui
{
  //====================================================================
  struct DPCF2 : public RcuGui::Register 
  {
    Rcuxx::AltroDPCF2& fLow;

    TGLayoutHints      fNBUFHints;
    TGButtonGroup      fNBUF;
    TGRadioButton      fNBUF4;
    TGRadioButton      fNBUF8;

    TGLayoutHints      fOptsHints;
    TGButtonGroup      fOpts;
    TGCheckButton      fFLT_EN;
    TGCheckButton      fPWSV;
    RcuGui::LabeledIntEntry fPTRG;
  
    DPCF2(TGCompositeFrame& f, Rcuxx::AltroDPCF2& low)
      : RcuGui::Register(f, low, false, 3, 3, 0, 3), 
	fLow(low),
	fNBUFHints(kLHintsExpandY),
	fNBUF(&fFields, "# buffers", kVerticalFrame),
	fNBUF4(&fNBUF, "4", 0),
	fNBUF8(&fNBUF, "8", 1),
	fOptsHints(kLHintsExpandY, 10),
	fOpts(&fFields, "Options", kVerticalFrame),
	fFLT_EN(&fOpts, "Digital filter", 2),
	fPWSV(&fOpts, "Power save", 3),
	fPTRG(fFields,"Pre trigger",0,0xf,kHorizontalFrame, 3, 5, 12, 0)
    {
      // 
      fNBUF.SetExclusive();
      fFields.AddFrame(&fNBUF, &fNBUFHints);
      fNBUF8.SetState(fLow.BUF() == Rcuxx::AltroDPCF2::k8Buffers ? 
		       kButtonDown : kButtonUp);
      fFields.AddFrame(&fOpts, &fOptsHints);
      Get();
    }
    void Get()
    {
      fPTRG.SetValue(fLow.PTRG());
      fFLT_EN.SetState(fLow.IsFLT_EN() ? kButtonDown : kButtonUp);
      fPWSV.SetState(fLow.IsPWSV() ? kButtonDown : kButtonUp);
      fNBUF8.SetState(fLow.BUF() == Rcuxx::AltroDPCF2::k8Buffers ? 
		       kButtonDown : kButtonUp);
    }
    void Set()
    {
      fLow.SetPTRG(fPTRG.GetValue());
      fLow.SetBUF((fNBUF4.IsOn() ? Rcuxx::AltroDPCF2::k4Buffers : 
		   Rcuxx::AltroDPCF2::k8Buffers));
      fLow.SetFLT_EN(fFLT_EN.IsOn());
      fLow.SetPWSV(fPWSV.IsOn());
    }
  };
}
#endif
//
// EOF
// 

// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_ADEVL_H
# define RCUGUI_ALTRO_ADEVL_H
# include <rcugui/Main.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/Register.h>
# include <rcugui/ErrorBit.h>
# include <rcuxx/altro/AltroADEVL.h>

namespace RcuGui
{
  //====================================================================
  struct ADEVL : public RcuGui::Register 
  {
    Rcuxx::AltroADEVL& fLow;

    RcuGui::LabeledIntView fHADD;
    RcuGui::LabeledIntView fEVL;
  
    ADEVL(TGCompositeFrame& f, Rcuxx::AltroADEVL& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fHADD(fFields, "Hardware address",0,0xff),
	fEVL(fFields, "Last event length",0,0xff, kHorizontalFrame)
    {
      fGroup.SetTitle("Address/Event Length (channel specific)");
      Get();
    }
    void Get()
    {
      fHADD.SetValue(fLow.HADD());
      fEVL.SetValue(fLow.EVL());
    }
  };
}
#endif
//
// EOF
// 

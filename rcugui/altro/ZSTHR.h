//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_ZSTHR_H
# define RCUGUI_ALTRO_ZSTHR_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/altro/AltroZSTHR.h>

namespace RcuGui
{
  //====================================================================
  struct ZSTHR : public RcuGui::Register 
  {
    Rcuxx::AltroZSTHR& fLow;
    RcuGui::LabeledIntEntry fOffset;
    RcuGui::LabeledIntEntry fZS_THR;  
    ZSTHR(TGCompositeFrame& f, Rcuxx::AltroZSTHR& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fOffset(fFields,"Offset",0,0x3ff, kHorizontalFrame),
	fZS_THR(fFields,"Threshold",0,0x3ff, kHorizontalFrame)
    {
      Get();
    }
    void Get()
    {
      fOffset.SetValue(fLow.Offset());
      fZS_THR.SetValue(fLow.ZS_THR());
    }
    void Set()
    {
      fLow.SetOffset(fOffset.GetValue());
      fLow.SetZS_THR(fZS_THR.GetValue());      
    }
  };
}
#endif
//
// EOF
// 

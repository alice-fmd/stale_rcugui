//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_TRCFG_H
# define RCUGUI_ALTRO_TRCFG_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/altro/AltroTRCFG.h>

namespace RcuGui
{
  //====================================================================
  struct TRCFG : public RcuGui::Register 
  {
    Rcuxx::AltroTRCFG& fLow;
    RcuGui::LabeledIntEntry fACQ_START;
    RcuGui::LabeledIntEntry fACQ_END;
    TRCFG(TGCompositeFrame& f, Rcuxx::AltroTRCFG& low)
      : RcuGui::Register(f, low), fLow(low),
	fACQ_START(fFields,"Start",0,0x3f0,kHorizontalFrame),
	fACQ_END(fFields,"End",0,0x3f0,kHorizontalFrame)
    {}
    void Get()
    {
      fACQ_START.SetValue(fLow.ACQ_START());
      fACQ_END.SetValue(fLow.ACQ_END());
    }
    void Set()
    {
      fLow.SetACQ_START(fACQ_START.GetValue());
      fLow.SetACQ_END(fACQ_END.GetValue());
    }
  };
}
#endif
//
// EOF
// 

//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_ERSTR_H
# define RCUGUI_ALTRO_ERSTR_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/altro/AltroERSTR.h>

namespace RcuGui
{
  //====================================================================
  struct ERSTR : public RcuGui::Register 
  {
    Rcuxx::AltroERSTR& fLow;

    RcuGui::ErrorBit          fRdo;
    RcuGui::ErrorBit          fInt2Seu;
    RcuGui::ErrorBit          fInt1Seu;
    RcuGui::ErrorBit          fMMU2Seu;
    RcuGui::ErrorBit          fMMU1Seu;
    RcuGui::ErrorBit          fTrigger;
    RcuGui::ErrorBit          fInstruction;
    RcuGui::ErrorBit          fParity;
    RcuGui::ErrorBit          fEmpty;
    RcuGui::ErrorBit          fFull;
    TGLayoutHints     fCountersHints;
    TGVerticalFrame   fCounters;
    RcuGui::LabeledIntView fBuffers;
    RcuGui::LabeledIntView fWp;
    RcuGui::LabeledIntView fRp;
  

    ERSTR(TGCompositeFrame& f, Rcuxx::AltroERSTR& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fRdo(fFields,    "Readout",     "Nothing to readout"),
	fInt2Seu(fFields,"2 bus SEU",   "2 single event upsets in bus FSM"),
	fInt1Seu(fFields,"1 bus SEU",   "1 single event upset in bus FSM"),
	fMMU2Seu(fFields,"2 buffer SEU","2 single event upsets in buffer FSM"),
	fMMU1Seu(fFields,"1 buffer SEU","1 single event upset in buffer FSM"),
	fTrigger(fFields,"Trigger",     "Overlapping triggers"),
	fInstruction(fFields, "Instr.", "Illegal instruction"),
	fParity(fFields, "Parity",      "Error in parity of instruction"),
	fEmpty(fFields,"Empty", "The buffers are empty",0xc0c0c0, 0x00aa00),
	fFull(fFields, "Full", "All buffers full", 0xc0c0c0, 0xff0000),
	fCountersHints(kLHintsRight,0,3),
	fCounters(&fFields),
	fBuffers(fCounters, "Free buffers",0,0xf, kHorizontalFrame),
	fWp(fCounters, "Write pointer",0,0x7),
	fRp(fCounters, "Read pointer",	0,0x7)
    {
      fFields.AddFrame(&fCounters, &fCountersHints);
					
    }
    void Get()
    {
      fRp.SetValue(fLow.Rp());
      fWp.SetValue(fLow.Wp());
      fBuffers.SetValue(fLow.Buffers());

      fFull.SetState(fLow.IsFull());
      fEmpty.SetState(fLow.IsEmpty());
      fParity.SetState(fLow.IsParity());
      fInstruction.SetState(fLow.IsInstruction());
      fTrigger.SetState(fLow.IsTrigger());
      fMMU1Seu.SetState(fLow.IsMMU1Seu());
      fMMU2Seu.SetState(fLow.IsMMU2Seu());
      fInt1Seu.SetState(fLow.IsInt1Seu());
      fInt2Seu.SetState(fLow.IsInt2Seu());
      fRdo.SetState(fLow.IsRdo());
    }
  };
}
#endif
//
// EOF
// 

//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_BCTHR_H
# define RCUGUI_ALTRO_BCTHR_H
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/Register.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/altro/AltroBCTHR.h>

namespace RcuGui
{
  //====================================================================
  struct BCTHR : public RcuGui::Register 
  {
    Rcuxx::AltroBCTHR& fLow;
    RcuGui::LabeledIntEntry fTHR_HI;
    RcuGui::LabeledIntEntry fTHR_LO;
  
    BCTHR(TGCompositeFrame& f, Rcuxx::AltroBCTHR& low)
      : RcuGui::Register(f, low), 
	fLow(low),
	fTHR_HI(fFields,"High", 0, 0x3ff,kHorizontalFrame),
	fTHR_LO(fFields,"Low", 0, 0x3ff,kHorizontalFrame)
    {
      Get();
    }
    void Get()
    {
      fTHR_HI.SetValue(fLow.THR_HI());
      fTHR_LO.SetValue(fLow.THR_LO());
    }
    void Set()
    {
      fLow.SetTHR_HI(fTHR_HI.GetValue());
      fLow.SetTHR_LO(fTHR_LO.GetValue());
    }
  };
}
#endif
//
// EOF
// 

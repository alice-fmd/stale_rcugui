// -*- mode: C++ -*-
//____________________________________________________________________
//
// $Id: MonitorTimer.h,v 1.3 2009-02-09 23:11:57 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    MonitorTimer.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of MonitorTimer
*/
#ifndef RCUGUI_MONITORTIMER_H
#define RCUGUI_MONITORTIMER_H
// #ifndef RCUDATA_READER_H
// # include <rcudata/Reader.h>
// #endif
#ifndef ROOT_TTimer
# include <TTimer.h>
#endif
// namespace RcuData 
// {
//   class Reader;
// }

namespace RcuGui 
{
  //____________________________________________________________________
  /** @struct MonitorTimer 
      @brief Class to do periodic reads of data 
      @ingroup rcugui_read 
  */
  template <typename T>
  struct MonitorTimer : public TTimer 
  {
    /** Constructor */
    MonitorTimer(T* reader=0)
      : TTimer(0, kTRUE), 
	fReader(reader) 
    {}
    /** Called periodically 
	@return @c true on success, and @c false means stop the
	timer. */
    Bool_t Notify() 
    {
      if (!fReader) {
	TurnOff(); 
	return kFALSE; 
      }
      fReader->GetNextEvent();
      return kTRUE;
    }
    
    /** Set the reader to use.
	@param r Pointer to reader. */
    void   SetReader(T* r) { fReader = r; }
  protected:
    /** The current reader */
    T* fReader;
  };
}

#endif
//
// EOF
// 

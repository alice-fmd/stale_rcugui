// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ALTRO_H
#define RCUGUI_ALTRO_H
#ifndef RCUGUI_SCROLLABLE_H
# include <rcugui/Scrollable.h>
#endif
#ifndef RCUGUI_LABELEDNUMBER_H
# include <rcugui/LabeledNumber.h>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif 
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGNumberEntry
# include <TGNumberEntry.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef RCUGUI_REGSITER_H
# include <rcugui/Register.h>
#endif
#ifndef RCUXX_ALTRO_H
# include <rcuxx/Altro.h>
#endif

namespace RcuGui 
{
  
  //====================================================================
  /** @class Altro 
      @brief GUI interface to ALTRO registers 
      @ingroup rcugui_low 
  */
  class Altro : public Scrollable 
  {
  public:
    /** Constructor 
	@param tab 
	@param low 
	@return  */
    Altro(TGTab& tab, Rcuxx::Altro& low);
    /** Handle broadcas */
    void HandleBroadcast();
    /** Handle address */
    void HandleAddress();
    /** Handle all buttons */ 
    virtual void HandleAll(Int_t v);
    /** Update all */
    void Update();
    /** @param debug Set debug mode */
    void SetDebug(Bool_t debug);
  private:
    /** Low level */
    Rcuxx::Altro& fLow;

    /** Address group */
    TGLayoutHints fAddressHints;
    /** Group */
    TGGroupFrame  fAddressGroup;
    /** Broad cast option */
    TGLayoutHints fBroadcastHints;
    /** Broadcast */
    TGCheckButton fBroadcast;
    /** Board number */
    LabeledIntEntry fBoardAddress;
    /** Chip number */ 
    LabeledIntEntry fChipAddress;
    /** Channel number */
    LabeledIntEntry fChannelAddress;
    /** Layout hints for all buttons */
    TGLayoutHints fAllButtonsHints;
    /** Commit, or update all group */ 
    TGButtonGroup fAllButtons;
    /** Commit all button */ 
    TGTextButton fCommitAll;
    /** Update all button */ 
    TGTextButton fUpdateAll;

    /** Tabs */
    TGTab fTabs;
    /** Info */
    TGCompositeFrame* fInfo;
    /** Data path */
    TGCompositeFrame* fDataPath;
    /** Pedestals */
    TGCompositeFrame* fPed;
    /** Filters */
    TGCompositeFrame* fFilter;
    /** Commands */ 
    TGLayoutHints fCommandHints;
    /** Commands */
    TGGroupFrame  fCommandGroup;
    /** Write pointer increment */
    Command* fWPINC;
    /** Read pointer increment */
    Command* fRPINC;
    /** Read-out channel */
    Command* fCHRDO;
    /** Software (internal) trigger */
    Command* fSWTRG;
    /** Trigger counter clear */
    Command* fTRCLR;
    /** Error clear */
    Command* fERCLR;
  
    /** K register */
    Register* fKCoeffs;
    /** L register */
    Register* fLCoeffs;
    /** Veriable and Fixed pedestal register */
    Register* fVFPED;
    /** Pattern memory data */
    Register* fPMDTA;
    /** Zero suppression paramters */
    Register* fZSTHR;
    /** Base-line correction paramters */
    Register* fBCTHR;
    /** Trigger configuration*/
    Register* fTRCFG;
    /** Data path configuration 1 */
    Register* fDPCFG;
    /** Data path configuration 2 */
    Register* fDPCF2;
    /** Pattern memory address */
    Register* fPMADD;
    /** Error and status register */
    Register* fERSTR;
    /** Acquisition window */
    Register* fADEVL;
    /** Trigger counters */
    Register* fTRCNT;

  };
}

#endif
//____________________________________________________________________
//
// EOF
//

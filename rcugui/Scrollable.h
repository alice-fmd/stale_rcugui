// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_SCROLLABLE_H
#define RCUGUI_SCROLLABLE_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif 
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif
#ifndef ROOT_TString
# include <TString.h>
#endif

namespace RcuGui
{
  /** @defgroup rcugui_basic Some basic widgets and so on. 
      @ingroup rcugui 
  */
  
  /** @class Scrollable
      @brief Base class for frames that are scrollable 
      @ingroup rcugui_basic
  */
  class Scrollable 
  {
  public:
    /** Constructor 
	@param name NAme of the frame 
	@param tab Where to put the tab 
	@param options Options  */
    Scrollable(const char* name, TGTab& tab, UInt_t options=kVerticalFrame);
    // void HandleMouseWheel(Event_t* event);
    /** @return Get container  */
    TGCompositeFrame& Container() { return fCont; }
    /** Return tab frame */
    TGCompositeFrame& Tab() { return *fTop; }
  protected:
    /** Top frame - managed by tabs */
    TGCompositeFrame*  fTop;
    /** Canvas width */
    UInt_t             fCanvasW;
    /** Canvas height */
    UInt_t             fCanvasH;
    /** Canvas */
    TGCanvas           fCanvas;
    /** Container */
    TGCompositeFrame   fCont;
    /** Name of the frame */
    TString            fName;
  };
}

#endif
//
// EOF
//



// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: LinkDef.h,v 1.11 2009-02-09 23:11:57 hehi Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    rcugui/LinkDef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:57 2004
    @brief   Linkage specifications 
*/
#ifndef __CINT__
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace RcuGui;
#pragma link C++ class     RcuGui::NumberDialog;
#pragma link C++ class     RcuGui::BoardDialog;
#pragma link C++ class     RcuGui::Scrollable;
#pragma link C++ class     RcuGui::ErrorBit;
#pragma link C++ class     RcuGui::LabeledNumber;
#pragma link C++ class     RcuGui::LabeledIntEntry;
#pragma link C++ class     RcuGui::LabeledIntView;
#pragma link C++ class     RcuGui::LabeledFloatEntry;
#pragma link C++ class     RcuGui::LabeledFloatView;
#pragma link C++ class     RcuGui::ConnectionType;
#pragma link C++ class     RcuGui::ConnectionDialog;
#pragma link C++ class     RcuGui::SourceType;
#pragma link C++ class     RcuGui::SourceDialog;
#pragma link C++ class     RcuGui::LinkedTree;
#pragma link C++ class     RcuGui::Monitor;
#pragma link C++ class     RcuGui::MonitorFrame;
#pragma link C++ class     RcuGui::MonitorTree;
#pragma link C++ class     RcuGui::DisplayHists;

//____________________________________________________________________ 
//  
// EOF
//

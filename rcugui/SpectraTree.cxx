//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "SpectraTree.h"
#include <rcudata/Spectra.h>
#include <TDirectory.h>
#include <TClass.h>
#include <cmath>
#define  TOP_NAME      "top"
#define  RCU_TEMPL     "rcu %4d"
#define  BOARD_TEMPL   "board %2d"
#define  CHIP_TEMPL    "chip %d"
#define  CHANNEL_TEMPL "channel %2d"
#define  TIMEBIN_TEMPL "timebin %4d"
#define  RCU_PATH      "/" TOP_NAME  "/"  RCU_TEMPL 
#define  BOARD_PATH    RCU_PATH      "/"  BOARD_TEMPL 
#define  CHIP_PATH     BOARD_PATH    "/"  CHIP_TEMPL
#define  CHANNEL_PATH  CHIP_PATH     "/"  CHANNEL_TEMPL
#define  TIMEBIN_PATH  CHANNEL_PATH  "/"  TIMEBIN_TEMPL


//====================================================================
RcuGui::SpectraTree::SpectraTree(TGCompositeFrame& p, UInt_t w, UInt_t h)
  : MonitorTree(p, w, h, TOP_NAME), 
    fCurrent(0), 
    fCachedEntry(0)
{}


//____________________________________________________________________
void
RcuGui::SpectraTree::Clear()
{
  fTop.Clear();
  fCachedEntry = 0;
}

//____________________________________________________________________
void
RcuGui::SpectraTree::Reset()
{
  fTop.Reset();
}

//____________________________________________________________________
void
RcuGui::SpectraTree::HandleSelect()
{
  TObject* user = Current();
  TH1*     hist = 0;
  if (user && user->IsA()->InheritsFrom(TH1::Class())) 
    hist = static_cast<TH1*>(user);
  else 
    hist = fTop.GetSummary();
  // std::cout << "Will draw histogram " << hist << std::endl;
  if (!hist) return;
  TVirtualPad* pad = cd();
  pad->SetLogy(log10(hist->GetMaximum() - hist->GetMinimum()) > 2);
  hist->Draw(hist->GetDrawOption());
  UpdateCanvas();
}

//____________________________________________________________________
bool
RcuGui::SpectraTree::MakeChannel(unsigned int rcu, 
				 unsigned int board, 
				 unsigned int chip, 
				 unsigned int channel)
{
  using namespace RcuData;
  Spectra::Rcu*   rcuCache   = fTop.Get(rcu);
  Spectra::Board* boardCache = (rcuCache  ? rcuCache->Get(board)   : 0);
  Spectra::Chip*  chipCache  = (boardCache? boardCache->Get(chip)  : 0);
  Spectra::Chan*  chanCache  = (chipCache ? chipCache->Get(channel): 0);
  
  TGListTreeItem* rcuEntry   = 0;
  TGListTreeItem* boardEntry = 0;
  TGListTreeItem* chipEntry  = 0;
  TGListTreeItem* chanEntry  = 0;
  std::string name;
  if (!chanCache) {
    if (!chipCache) {
      if (!boardCache) {
	if (!rcuCache) {
	  name     = Form(RCU_TEMPL, rcu);
	  rcuCache = fTop.GetOrAdd(rcu);
	  rcuEntry = fList.AddItem(fTopEntry, name.c_str());
	  rcuEntry->SetUserData(rcuCache->GetSummary());
	}
	else { 
	  name     = Form(RCU_PATH, rcu);
	  rcuEntry = fList.FindItemByPathname(name.c_str());
	}
	name       = Form(BOARD_TEMPL, board);
	boardCache = rcuCache->GetOrAdd(board);
	boardEntry = fList.AddItem(rcuEntry, name.c_str());
	boardEntry->SetUserData(boardCache->GetSummary());
	fNeedSort.push_back(rcuEntry);
      }
      else {
	name       = Form(BOARD_PATH, rcu, board);
	boardEntry = fList.FindItemByPathname(name.c_str());
      }
      name      = Form(CHIP_TEMPL, chip);
      chipCache = boardCache->GetOrAdd(chip);
      chipEntry = fList.AddItem(boardEntry, name.c_str());
      chipEntry->SetUserData(chipCache->GetSummary());
      fNeedSort.push_back(boardEntry);
    }
    else {
      name      = Form(CHIP_PATH, rcu, board, chip);
      chipEntry = fList.FindItemByPathname(name.c_str());
    }
    name      = Form(CHANNEL_TEMPL, channel);
    chanCache = chipCache->GetOrAdd(channel);
    chanEntry = fList.AddItem(chipEntry, name.c_str());
    chanEntry->SetUserData(chanCache->GetSummary());
    fNeedSort.push_back(chipEntry);
    fNeedUpdate = true;
  }
  fCurrent     = chanCache;
  fCachedEntry = chanEntry;
  
  return true;
}

//____________________________________________________________________
bool
RcuGui::SpectraTree::MakeTimebin(unsigned int rcu, 
				 unsigned int board, 
				 unsigned int chip, 
				 unsigned int channel, 
				 unsigned int t)
{
#if 0
  std::cout << "Making time bin " << board << "/" << chip << "/" << channel 
	    << "/" << t << " (" << fCurrent << ")" << std::endl;
#endif
  if (!fCurrent) return false;
  TH1* spectrum = fCurrent->Get(t);
  if (!spectrum) {
    spectrum = fCurrent->GetOrAdd(t);
    
    std::string path;
    if (!fCachedEntry) {
      path          = Form(CHANNEL_PATH, rcu, board, chip, channel);
      fCachedEntry  = fList.FindItemByPathname(path.c_str());
    }
    path = Form(TIMEBIN_TEMPL, t);
    fList.AddItem(fCachedEntry, path.c_str(),
		  fHist1DIcon, fHist1DIcon)->SetUserData(spectrum);
    fNeedSort.push_back(fCachedEntry);
    fNeedUpdate = true;
  }
  return true;
}

//____________________________________________________________________
void
RcuGui::SpectraTree::Fill(unsigned int rcu, 
			  unsigned int board, 
			  unsigned int chip, 
			  unsigned int channel, 
			  unsigned int t, 
			  unsigned int adc)
{
#if 0
  std::cout << "Filling time bin " << board << "/" << chip << "/" << channel 
	    << "/" << t << " (" << fCurrent << ")" << std::endl;
#endif
  fTop.Fill(rcu, board, chip, channel, t, adc);
}

//____________________________________________________________________
//
//
// EOF
//

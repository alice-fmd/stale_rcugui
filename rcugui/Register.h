// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_REGISTER_H
#define RCUGUI_REGISTER_H
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef ROOT_TGCanvas
# include <TGCanvas.h>
#endif
#ifndef ROOT_TMath
# include <TMath.h>
#endif
#ifndef TGNumberEntry
# include <TGNumberEntry.h>
#endif
#ifndef TString
# include <TString.h>
#endif
#ifndef RCUXX_RCU_H
# include <rcuxx/Rcu.h>
#endif
#ifndef RCUXX_REGISTER_H
# include <rcuxx/Register.h>
#endif
#ifndef RCUXX_COMMAND_H
# include <rcuxx/Command.h>
#endif

class TGLayoutHints;

namespace RcuGui 
{
  //====================================================================
  /** @struct Command
      @brief A command button.
      @ingroup rcugui_low
   */
  class Command 
  {
  public:
    /** constructor 
	@param f Parent frame
	@param low Low-level interface
	@param tip Tool-tip text 
	@param lmargin 	Left margin  
	@param rmargin 	Right margin 
	@param tmargin 	Top margin   
	@param bmargin 	Bottom margin */
    Command(TGCompositeFrame& f, 
	    Rcuxx::Command&   low, 
	    const char*       tip=0,
	    Int_t             lmargin=4,
	    Int_t             rmargin=4,
	    Int_t             bmargin=0,
	    Int_t             tmargin=0);
    /** Destructor */
    virtual ~Command(){}
    /** Toggle enable 
	@param on If true, it's enabled */
    void SetEnabled(Bool_t on) { fOk.SetEnabled(on); }
    /** Send command to hardware */
    void Commit();
    /** Connect to clicked signal */
    void Connect(const char* classname, void* object, const char* method) 
    {
      fOk.Connect("Clicked()", classname, object, method);
    }
    /** Do not check error state afterwards */
    Bool_t fNoCheck;
  protected:
    /** Low-level interface */
    Rcuxx::Command&    fLow;
    /** Layout hints */
    TGLayoutHints      fHints;
    /** Button */
    TGTextButton       fOk;
  };
  
  //====================================================================
  /** @class Register 
      @brief Register interface 
      @ingroup rcugui_low 
  */
  class Register 
  {
  public:
    /** Constructor 
	@param cont Where to put the interface 
	@param low Low-level interface 
	@param com Force commit button
	@param lmargin 	Left margin  
	@param rmargin 	Right margin 
	@param tmargin 	Top margin   
	@param bmargin 	Bottom margin  
	@param get Whether to add a Get button
	@param set Whether to add a Set button */
    Register(TGCompositeFrame& cont, 
	     Rcuxx::Register&  low, 
	     Bool_t            com=false, 
	     Int_t             lmargin=3,
	     Int_t             rmargin=3,
	     Int_t             bmargin=0,
	     Int_t             tmargin=0,
	     Bool_t            set=false, 
	     Bool_t            get=false);
    /** Destructor */
    virtual ~Register();
    /** Update the display of the register */
    virtual Bool_t Update();
    /** Update the display of the register */
    virtual void Set() {}
    /** Update the display of the register */
    virtual void Get() {}
    /** Clear the register (optional) */
    virtual void Clear();
    /** Commit to the register (optional) */
    virtual Bool_t Commit();
    /** Handle an address change */
    virtual void HandleAddress(UShort_t board, UShort_t chip, UShort_t chan);
    /** Handle an address change */
    virtual void HandleBroadcast();
    /** Print the register (optional) */
    virtual void Print() const;
    /** Get width */
    UInt_t GetWidth() const { return fGroup.GetWidth(); }
    /** Get height */
    UInt_t GetHeight() const { return fGroup.GetHeight(); }
    /** Do layout */
    void Layout() { fGroup.Layout(); }
  protected:
    /** Low-level interface */
    Rcuxx::Register&    fLow;
    /** Group layout hints */
    TGLayoutHints     fGroupHints;
    /** Group layout hints */
    TGLayoutHints     fFieldHints;
    /** Layout hints */
    TGLayoutHints     fButtonHints;
    /** Layout hints */
    TGLayoutHints     fButtonsHints;
    /** Group frame */
    TGGroupFrame      fGroup;
    /** The fields */
    TGCompositeFrame  fFields;   
    /** Button group */
    TGCompositeFrame  fButtons;
    /** Update button */
    TGTextButton      fUpdate;   
    /** Clear button */
    TGTextButton*     fClear;    
    /** Commit button */
    TGTextButton*     fCommit;   
    /** Set values on low-level button */
    TGTextButton*     fSet;   
    /** Get values from low-level button */
    TGTextButton*     fGet;   
    /** Print button */
    TGTextButton      fPrint;   
  };
}

#endif
//____________________________________________________________________
//
// EOF
//

// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_ACQUISITION_H
#define RCUGUI_ACQUISITION_H
#ifndef RCUGUI_LABELEDNUMBER_H
# include <rcugui/LabeledNumber.h>
#endif
#ifndef RCUXXACQ_H
# include <rcuxx/Acq.h>
#endif
#ifndef ROOT_TGFrame
# include <TGFrame.h>
#endif 
#ifndef ROOT_TGTab
# include <TGTab.h>
#endif 
#ifndef ROOT_TGLabel
# include <TGLabel.h>
#endif 
#ifndef ROOT_TGNumberEntry
# include <TGNumberEntry.h>
#endif 
#ifndef ROOT_TGTextEntry
# include <TGTextEntry.h>
#endif 
#ifndef ROOT_TGButton
# include <TGButton.h>
#endif 
#ifndef ROOT_TGButtonGroup
# include <TGButtonGroup.h>
#endif 
#ifndef RCUGUI_SCROLLABLE_H
# include <rcugui/Scrollable.h>
#endif
#ifndef RCUGUI_REGISTER_H
# include <rcugui/Register.h>
#endif
#ifndef ROOT_TGComboBox
# include <TGComboBox.h>
#endif
// class ProgramSelect;
class TThread;
class TTree;


namespace RcuGui 
{
  /** @class Acquisition
      @brief Data Acquisition tab. 
      @ingroup rcugui_low
  */
  class Acquisition : public Scrollable
  {
  private:
    /** Low level object */
    Rcuxx::Acq& fLow;
    /** Options group */
    TGGroupFrame      fOptions;
    /** Number of event group */
    TGGroupFrame      fNEventsLabel;
    /** Number of event input field */
    // TGNumberEntry*     fRunNoInput;
    LabeledIntEntry  fRunNoInput;
    /** Number of event input field */
    // TGNumberEntry*     fNEventsInput;
    LabeledIntEntry  fNEventsInput;
    /** Trigger group */
    TGButtonGroup     fTriggerButtons;
    /** Software trigger radio button */
    TGRadioButton     fSoftwareTrigger;
    /** External trigger radio button */
    TGRadioButton     fExternalTrigger;
    /** Memories, registers, commands */ 
    TGGroupFrame        fTodo;
    TGCheckButton       fACTFEC;
    TGCheckButton       fFECRST;
    TGCheckButton       fIMEM;
    TGCheckButton       fPMEM;
    TGCheckButton       fACL;
    TGCheckButton       fTRCFG1;
    LabeledIntEntry  fAddr;
    
  
    /** Operations group */
    TGGroupFrame     fOperations;
    /** DAQ start button */
    TGTextButton     fStart;
    /** DAQ stop button */
    TGTextButton     fStop;

    /** Run number */ 
    Int_t fRun;
    /** Thread */
    TThread* fThread;
    /** Tree */
    TTree* fTree;
    /** Timer */
    TTimer* fTimer;
  public:
    /** Constructor 
	@param tabs Where to put the DAQ tab 
	@param low Low-level interface */
    Acquisition(TGTab& tabs, Rcuxx::Acq& low);
    /** Handle the start button */
    void   HandleStart();
    /** Handle the stop button */
    void   HandleStop();
    /** Handle output input field */
    void   HandleIMEM();
    /** Handle output input field */
    void   HandleTimer();
    /** Run the data aquisition */
    void   Run();
    /** When done with ACQ
	@param ret Return value of ACQ */
    void Done(unsigned int ret=0);
    /** @return Low-level driver */
    Rcuxx::Acq* Low() { return &fLow; }
    /** @param on Set debug mode on or off */
    void SetDebug(Bool_t on);
  };
}


#endif
//
// EOF
//

    
  

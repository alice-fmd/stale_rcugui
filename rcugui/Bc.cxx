//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/Bc.h>
#include <rcuxx/bc/BcCommand.h>
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/DebugGuard.h>
#include <iostream>
#include "bc/AnalogCurrent.h"
#include "bc/AnalogVoltage.h"
#include "bc/CSR01.h"
#include "bc/CSR2.h"
#include "bc/CSR3.h"
#include "bc/DigitalCurrent.h"
#include "bc/DigitalVoltage.h"
#include "bc/DSTBCNT.h"
#include "bc/L1CNT.h"
#include "bc/L2CNT.h"
#include "bc/Monitored.h"
#include "bc/SCLKCNT.h"
#include "bc/Temperature.h"
#include "bc/TSMWORD.h"
#include "bc/USRATIO.h"
#include "bc/Version.h"

namespace 
{
  bool fDebug = false;
}

//====================================================================
UInt_t RcuGui::Monitored::fgNTries = 20;


//____________________________________________________________________
RcuGui::Bc::Bc(TGTab& tabs, Rcuxx::Bc& low) 
  : Scrollable("BC", tabs, kVerticalFrame), 
    fLow(low),
    fHints(kLHintsExpandX,ISCALE(3),ISCALE(3),0,ISCALE(3)),
    fTab(&fCont),
    fAddressGroup(&fCont, "Address", kHorizontalFrame),
    fBroadcastHints(kLHintsLeft, ISCALE(5),ISCALE(3),ISCALE(5),ISCALE(5)),
    fBroadcast(&fAddressGroup, "Broadcast", 0),
    fBoardAddress(fAddressGroup, "Board", 0, 31), 
    fAllButtonsHints(kLHintsRight, ISCALE(5),ISCALE(3),ISCALE(5),ISCALE(5)),
    fAllButtons(&fAddressGroup, "", kHorizontalFrame), 
    fCommitAll(&fAllButtons, "Commit", 1),
    fUpdateAll(&fAllButtons, "Update", 2),
    fUseI2C(&fAllButtons, "Use I2C", 3),
    fConfigStatus(fTab.AddTab("Configuraton/Status")),
    fCSRCommands(fConfigStatus, "Commands", kHorizontalFrame),
    fALRST(0),
    fBCRST(0),    
    fCSR01(0),
    fCSR2(0),
    fCSR3(0),
    fVersion(0),
    fCounters(fTab.AddTab("Counters")),
    fCounterCommands(fCounters,  "Commands",  kHorizontalFrame),
    fCNTLAT(0),
    fCNTCLR(0),
    fL1CNT(0),
    fL2CNT(0),
    fDSTBCNT(0),
    fSCLKCNT(0),
    fTestMode(fTab.AddTab("Test mode")),
    fTSMCommands(fTestMode,  "Commands",  kHorizontalFrame),
    fSCEVL(0),
    fEVLRDO(0),
    fSTTSM(0),
    fACQRDO(0),
    fTSMWORD(0),
    fUSRATIO(0),
    fMonitors(fTab.AddTab("Monitors")),
    fMonitorCommands(fMonitors, "Commands",  kHorizontalFrame),
    fUpdateMons(&fMonitorCommands, "Update all", 0),
    fTries(*fMonitors, "Number of tries", 0, 0xFFFF, kHorizontalFrame,
	   ISCALE(3),ISCALE(3),ISCALE(3),ISCALE(3)),
    fLowerHints(kLHintsExpandX,0,0,0,ISCALE(3)),
    fMonLower(fMonitors),
    fMonLeft(&fMonLower),
    fMonRight(&fMonLower),
    fSTCNV(0),
    fTemp(0),
    fAV(0),
    fAC(0),
    fDV(0),
    fDC(0)
{
  fCont.AddFrame(&fAddressGroup, &fHints);
  fCont.AddFrame(&fTab, &fHints);
  fBroadcast.Connect("Clicked()", "RcuGui::Bc", this, "HandleBroadcast()");
  fAddressGroup.AddFrame(&fBroadcast, &fBroadcastHints),
  fAddressGroup.AddFrame(&fAllButtons, &fAllButtonsHints);
  fBoardAddress.Connect("ValueChanged()", "RcuGui::Bc",this,"HandleAddress()");
  fAllButtons.Connect("Clicked(Int_t)", "RcuGui::Bc", this,"HandleAll(Int_t)");

  // Configuration and status registers 
  fConfigStatus->AddFrame(&fCSRCommands, &fHints);
  if (fLow.ALRST()) fALRST = new Command(fCSRCommands, *fLow.ALRST());
  if (fLow.BCRST()) fBCRST = new Command(fCSRCommands, *fLow.BCRST());
  if (fLow.CSR0())  fCSR01 = new CSR01(*fConfigStatus,*fLow.CSR0(),
				       *fLow.CSR1());
  if (fLow.CSR2())  fCSR2  = new CSR2(*fConfigStatus, *fLow.CSR2());
  if (fLow.CSR3())  fCSR3  = new CSR3(*fConfigStatus, *fLow.CSR3());
  if (fLow.Version()) fVersion = new Version(*fConfigStatus, *fLow.Version());
  else std::cout << "No version " << std::endl;

  // Monitors 
  fMonitors->AddFrame(&fMonitorCommands, &fHints);
  if (fLow.STCNV()) fSTCNV = new Command(fMonitorCommands, *fLow.STCNV());
  fMonitorCommands.AddFrame(&fUpdateMons, &fHints);
  fUpdateMons.Connect("Clicked()", "RcuGui::Bc", this, "HandleUpdate()");
  // fMonitors->AddFrame(&fTries, &fHints);
  fTries.fView.Connect("ValueSet(Long_t)", "RcuGui::Bc", this, 
		       "HandleTries(Long_t)");
  if (fLow.TEMP())  fTemp  = new Temperature(*fMonitors,*fLow.TEMP(),
					     *fLow.TEMP_TH());  
  fMonitors->AddFrame(&fMonLower, &fLowerHints);
  fMonLower.AddFrame(&fMonLeft, &fHints);
  fMonLower.AddFrame(&fMonRight, &fHints);
  if (fLow.AV())    fAV    = new AnalogVoltage(fMonLeft,*fLow.AV(),
					       *fLow.AV_TH());
  if (fLow.AC())    fAC    = new AnalogCurrent(fMonLeft,*fLow.AC(),
					       *fLow.AC_TH());
  if (fLow.DV())    fDV    = new DigitialVoltage(fMonRight,*fLow.DV(),
						 *fLow.DV_TH());
  if (fLow.DC())    fDC    = new DigitialCurrent(fMonRight,*fLow.DC(),
						 *fLow.DC_TH());
  // Counters 
  fCounters->AddFrame(&fCounterCommands, &fHints);
  if (fLow.CNTLAT())   fCNTLAT  = new Command(fCounterCommands,*fLow.CNTLAT());
  if (fLow.CNTCLR())   fCNTCLR  = new Command(fCounterCommands,*fLow.CNTCLR());
  if (fLow.L1CNT())    fL1CNT   = new L1CNT(*fCounters, *fLow.L1CNT());
  if (fLow.L2CNT())    fL2CNT   = new L2CNT(*fCounters, *fLow.L2CNT());
  if (fLow.DSTBCNT())  fDSTBCNT = new DSTBCNT(*fCounters, *fLow.DSTBCNT());
  if (fLow.SCLKCNT())  fSCLKCNT = new SCLKCNT(*fCounters, *fLow.SCLKCNT());

  // Test mode
  fTestMode->AddFrame(&fTSMCommands, &fHints);
  if (fLow.SCEVL())   fSCEVL   = new Command(fTSMCommands, *fLow.SCEVL());
  if (fLow.EVLRDO())  fEVLRDO  = new Command(fTSMCommands, *fLow.EVLRDO());
  if (fLow.STTSM())   fSTTSM   = new Command(fTSMCommands, *fLow.STTSM());
  if (fLow.ACQRDO())  fACQRDO  = new Command(fTSMCommands, *fLow.ACQRDO());
  if (fLow.TSMWORD()) fTSMWORD = new TSMWORD(*fTestMode, *fLow.TSMWORD());
  if (fLow.USRATIO()) fUSRATIO = new USRATIO(*fTestMode, *fLow.USRATIO());

  // Set initial address 
  HandleAddress();
}

//____________________________________________________________________
RcuGui::Bc::~Bc()
{
  Rcuxx::DebugGuard g(fDebug, "RcuGui::Bc::~Bc()");
  if (fALRST)	delete fALRST;
  if (fBCRST)	delete fBCRST;
  if (fCSR01)	delete fCSR01;
  if (fCSR2)	delete fCSR2;
  if (fCSR3)	delete fCSR3;
  
  //if (fCounters)	delete fCounters;
  if (fCNTLAT)	delete fCNTLAT;
  if (fCNTCLR)	delete fCNTCLR;
  if (fL1CNT)	delete fL1CNT;
  if (fL2CNT)	delete fL2CNT;
  if (fDSTBCNT)	delete fDSTBCNT;
  if (fSCLKCNT)	delete fSCLKCNT;

  //if (fTestMode)	delete fTestMode;
  if (fSCEVL)	delete fSCEVL;
  if (fEVLRDO)	delete fEVLRDO;
  if (fSTTSM)	delete fSTTSM;
  if (fACQRDO)	delete fACQRDO;
  if (fTSMWORD)	delete fTSMWORD;
  if (fUSRATIO)	delete fUSRATIO;

  //if (fMonitors)	delete fMonitors;
  if (fSTCNV)	delete fSTCNV;
  if (fTemp)	delete fTemp;
  if (fAV)	delete fAV;
  if (fAC)	delete fAC;
  if (fDV)	delete fDV;
  if (fDC)	delete fDC;
}


//____________________________________________________________________
void 
RcuGui::Bc::HandleBroadcast() 
{
  Rcuxx::DebugGuard g(fDebug, "RcuGui::Bc::HandleBroadcast()");
  Bool_t what = !(fBroadcast.IsDown());
  if (what) 
    HandleAddress();
  else {
    fLow.SetBroadcast();
    fUpdateAll.SetEnabled(kFALSE);
    if (fTemp)		fTemp->HandleBroadcast();
    if (fAV)		fAV->HandleBroadcast();
    if (fAC)		fAC->HandleBroadcast();
    if (fDV)		fDV->HandleBroadcast();
    if (fDC)		fDC->HandleBroadcast();
    if (fL1CNT)		fL1CNT->HandleBroadcast();
    if (fL2CNT)		fL2CNT->HandleBroadcast();
    if (fDSTBCNT)	fDSTBCNT->HandleBroadcast();
    if (fSCLKCNT)	fSCLKCNT->HandleBroadcast();
    if (fTSMWORD)	fTSMWORD->HandleBroadcast();
    if (fUSRATIO)	fUSRATIO->HandleBroadcast();
    if (fCSR01)		fCSR01->HandleBroadcast();
    if (fCSR2)		fCSR2->HandleBroadcast();
    if (fCSR3)		fCSR3->HandleBroadcast();
  }
}

//____________________________________________________________________
void
RcuGui::Bc::HandleAddress() 
{
  Rcuxx::DebugGuard g(fDebug, "RcuGui::Bc::HandleAddress()");
  UInt_t board   = fBoardAddress.GetValue();
  Rcuxx::DebugGuard::Message(fDebug, "Address set to 0x%x", board);
  fLow.SetAddress(board);
  fUpdateAll.SetEnabled(kTRUE);
  if (fTemp)	fTemp->HandleAddress(board,0,0);
  if (fAV)	fAV->HandleAddress(board,0,0);
  if (fAC)	fAC->HandleAddress(board,0,0);
  if (fDV)	fDV->HandleAddress(board,0,0);
  if (fDC)	fDC->HandleAddress(board,0,0);
  if (fL1CNT)	fL1CNT->HandleAddress(board,0,0);
  if (fL2CNT)	fL2CNT->HandleAddress(board,0,0);
  if (fDSTBCNT)	fDSTBCNT->HandleAddress(board,0,0);
  if (fSCLKCNT)	fSCLKCNT->HandleAddress(board,0,0);
  if (fTSMWORD)	fTSMWORD->HandleAddress(board,0,0);
  if (fUSRATIO)	fUSRATIO->HandleAddress(board,0,0);
  if (fCSR01)	fCSR01->HandleAddress(board,0,0);
  if (fCSR2)	fCSR2->HandleAddress(board,0,0);
  if (fCSR3)	fCSR3->HandleAddress(board,0,0);
}

//____________________________________________________________________
void
RcuGui::Bc::HandleAll(Int_t which) 
{
  unsigned int ret = 0;
  switch (which) {
  case 1: ret = fLow.Commit(); break;
  case 2: ret = fLow.Update(); break;
  case 3: fLow.SetUseI2C(fUseI2C.IsDown()); break;
  }
  if (ret) Main::Instance()->Error(ret);
}

//____________________________________________________________________
void
RcuGui::Bc::HandleUpdate() 
{
  if (fTemp	&& !fTemp->Update())	return;
  if (fAV	&& !fAV->Update())	return;
  if (fAC	&& !fAC->Update())	return;
  if (fDV	&& !fDV->Update())	return;
  if (fDC	&& !fDC->Update())	return;
}
//____________________________________________________________________
void
RcuGui::Bc::HandleTries(Long_t) 
{
  Rcuxx::DebugGuard g(fDebug, "RcuGui::Bc::HandleTries()");
  UInt_t tries   = fTries.GetValue();
  Monitored::fgNTries = tries;
}
//____________________________________________________________________
void
RcuGui::Bc::Update() 
{
  Rcuxx::DebugGuard g(fDebug, "RcuGui::Bc::Update()");
  UInt_t ret = fLow.Update();
  if (ret) {
    Main::Instance()->Error(ret);
    return;
  }
}
//____________________________________________________________________
void
RcuGui::Bc::SetDebug(Bool_t on)
{
  fLow.SetDebug(on);
  fDebug = on;
}

//____________________________________________________________________
//
// EOF
//

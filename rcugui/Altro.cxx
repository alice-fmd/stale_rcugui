//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/Altro.h>
#include <rcugui/Main.h>
#include <rcugui/LabeledNumber.h>
#include <rcugui/ErrorBit.h>
#include <rcuxx/DebugGuard.h>
#include <rcuxx/altro/AltroCommand.h>
#include <iostream>
#include <iomanip>
#include <TGMsgBox.h>
#include "altro/ADEVL.h"
#include "altro/BCTHR.h"
#include "altro/DPCF2.h"
#include "altro/DPCFG.h"
#include "altro/ERSTR.h"
#include "altro/KLCoeffs.h"
#include "altro/PMADD.h"
#include "altro/PMDTA.h"
#include "altro/TRCFG.h"
#include "altro/TRCNT.h"
#include "altro/VFPED.h"
#include "altro/ZSTHR.h"      

//====================================================================
RcuGui::Altro::Altro(TGTab& tabs, Rcuxx::Altro& low) 
  : Scrollable("ALTRO", tabs, kVerticalFrame), 
    fLow(low),
    fAddressHints(kLHintsExpandX, 0, 0, 0, ISCALE(3)),
    fAddressGroup(&fCont, "Address", kHorizontalFrame),
    fBroadcastHints(kLHintsLeft, ISCALE(5), ISCALE(3), ISCALE(5), ISCALE(3)),
    fBroadcast(&fAddressGroup, "Broadcast", 0),
    fBoardAddress(fAddressGroup,"Board",0,31),
    fChipAddress(fAddressGroup,"Chip",0,7),
    fChannelAddress(fAddressGroup,"Channel",0,15),
    fAllButtonsHints(kLHintsRight, ISCALE(5),ISCALE(3),ISCALE(5),ISCALE(5)),
    fAllButtons(&fAddressGroup, "", kHorizontalFrame), 
    fCommitAll(&fAllButtons, "Commit", 1),
    fUpdateAll(&fAllButtons, "Update", 2),
    fTabs(&fCont),
    fInfo(fTabs.AddTab("Status")),
    fDataPath(fTabs.AddTab("Data Path")),
    fPed(fTabs.AddTab("Pedestals")),
    fFilter(fTabs.AddTab("Filter")),
    fCommandHints(kLHintsExpandX, ISCALE(3), ISCALE(3), 0, ISCALE(3)),
    fCommandGroup(fInfo, "Commands", kHorizontalFrame)
{
  // 
  fCont.AddFrame(&fAddressGroup, &fAddressHints);
  fBroadcast.Connect("Clicked()", "RcuGui::Altro", this, "HandleBroadcast()");
  fAllButtons.Connect("Clicked(Int_t)", "RcuGui::Altro", this, 
		      "HandleAll(Int_t)");
  fAddressGroup.AddFrame(&fBroadcast, &fBroadcastHints);
  fBoardAddress.Connect("ValueChanged()", "RcuGui::Altro", this,
			"HandleAddress()");
  fChipAddress.Connect("ValueChanged()", "RcuGui::Altro", this,
		       "HandleAddress()");
  fChannelAddress.Connect("ValueChanged()", "RcuGui::Altro", this,
			  "HandleAddress()");
  fAddressGroup.AddFrame(&fAllButtons, &fAllButtonsHints);
  fCont.AddFrame(&fTabs, &fAddressHints);

  if (fLow.ERSTR())  fERSTR   = new ERSTR(*fInfo, *fLow.ERSTR());
  if (fLow.ADEVL())  fADEVL   = new ADEVL(*fInfo, *fLow.ADEVL());
  if (fLow.TRCNT())  fTRCNT   = new TRCNT(*fInfo, *fLow.TRCNT());

  fInfo->AddFrame(&fCommandGroup, &fCommandHints);
  if (fLow.WPINC()) fWPINC = new Command(fCommandGroup, *fLow.WPINC());
  if (fLow.RPINC()) fRPINC = new Command(fCommandGroup, *fLow.RPINC());
  if (fLow.CHRDO()) fCHRDO = new Command(fCommandGroup, *fLow.CHRDO());
  if (fLow.SWTRG()) fSWTRG = new Command(fCommandGroup, *fLow.SWTRG());
  if (fLow.TRCLR()) fTRCLR = new Command(fCommandGroup, *fLow.TRCLR());
  if (fLow.ERCLR()) fERCLR = new Command(fCommandGroup, *fLow.ERCLR());
  
  if (fLow.TRCFG())  fTRCFG   = new TRCFG(*fDataPath, *fLow.TRCFG());
  if (fLow.DPCFG())  fDPCFG   = new DPCFG(*fDataPath, *fLow.DPCFG());
  if (fLow.DPCF2())  fDPCF2   = new DPCF2(*fDataPath, *fLow.DPCF2());

  if (fLow.VFPED())  fVFPED   = new VFPED(*fPed, *fLow.VFPED());
  if (fLow.PMDTA())  fPMDTA   = new PMDTA(*fPed, *fLow.PMDTA());
  if (fLow.PMADD())  fPMADD   = new PMADD(*fPed, *fLow.PMADD());

  if (fLow.ZSTHR())  fZSTHR   = new ZSTHR(*fFilter, *fLow.ZSTHR());
  if (fLow.BCTHR())  fBCTHR   = new BCTHR(*fFilter, *fLow.BCTHR());
  if (fLow.K1())     fKCoeffs = new KLCoeffs(*fFilter, 'K', *fLow.K1(), 
					      *fLow.K2(), *fLow.K3());
  if (fLow.L1())     fLCoeffs = new KLCoeffs(*fFilter, 'L', *fLow.L1(), 
					     *fLow.L2(), *fLow.L3());

  HandleAddress();
}

//____________________________________________________________________
void 
RcuGui::Altro::HandleBroadcast() 
{
  Bool_t what = !(fBroadcast.IsDown());
  if (what) 
    HandleAddress();
  else {
    fLow.SetBroadcast();
    fUpdateAll.SetEnabled(kFALSE);
    if (fKCoeffs)	fKCoeffs->HandleBroadcast();
    if (fLCoeffs)	fLCoeffs->HandleBroadcast();
    if (fVFPED)		fVFPED->HandleBroadcast();
    if (fPMDTA)		fPMDTA->HandleBroadcast();
    if (fZSTHR)		fZSTHR->HandleBroadcast();
    if (fBCTHR)		fBCTHR->HandleBroadcast();
    if (fTRCFG)		fTRCFG->HandleBroadcast();
    if (fDPCFG)		fDPCFG->HandleBroadcast();
    if (fDPCF2)		fDPCF2->HandleBroadcast();
    if (fPMADD)		fPMADD->HandleBroadcast();
    if (fERSTR)		fERSTR->HandleBroadcast();
    if (fADEVL)		fADEVL->HandleBroadcast();
    if (fTRCNT)		fTRCNT->HandleBroadcast();
  }
}

//____________________________________________________________________
void
RcuGui::Altro::HandleAddress() 
{
  UInt_t board   = fBoardAddress.GetValue();
  UInt_t chip    = fChipAddress.GetValue();
  UInt_t channel = fChannelAddress.GetValue();
  fLow.SetAddress(board, chip, channel);
  fUpdateAll.SetEnabled(kTRUE);
  if (fKCoeffs)	fKCoeffs->HandleAddress(board, chip, channel);
  if (fLCoeffs)	fLCoeffs->HandleAddress(board, chip, channel);
  if (fVFPED)	fVFPED->HandleAddress(board, chip, channel);
  if (fPMDTA)	fPMDTA->HandleAddress(board, chip, channel);
  if (fZSTHR)	fZSTHR->HandleAddress(board, chip, channel);
  if (fBCTHR)	fBCTHR->HandleAddress(board, chip, channel);
  if (fTRCFG)	fTRCFG->HandleAddress(board, chip, channel);
  if (fDPCFG)	fDPCFG->HandleAddress(board, chip, channel);
  if (fDPCF2)	fDPCF2->HandleAddress(board, chip, channel);
  if (fPMADD)	fPMADD->HandleAddress(board, chip, channel);
  if (fERSTR)	fERSTR->HandleAddress(board, chip, channel);
  if (fADEVL)	fADEVL->HandleAddress(board, chip, channel);
  if (fTRCNT)	fTRCNT->HandleAddress(board, chip, channel);
}


//____________________________________________________________________
void
RcuGui::Altro::HandleAll(Int_t which) 
{
  unsigned int ret = 0;
  switch (which) {
  case 1: ret = fLow.Commit(); break;
  case 2: ret = fLow.Update(); break;
  }
  if (ret) Main::Instance()->Error(ret);
}

//____________________________________________________________________
void 
RcuGui::Altro::Update()
{
  UInt_t ret = fLow.Update();
  if (ret) Main::Instance()->Error(ret);
}

//____________________________________________________________________
void
RcuGui::Altro::SetDebug(Bool_t on)
{
  fLow.SetDebug(on);
  fDebug = on;
}

  
  
  
//____________________________________________________________________
//
// EOF
//

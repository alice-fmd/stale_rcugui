//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Register.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul 22 07:39:19 2006
    @brief   
*/
#include <rcugui/Register.h>
#include <rcugui/Main.h>
#include <TGMsgBox.h>
#include <rcuxx/DebugGuard.h>
#include <iostream>
#include <unistd.h>

//====================================================================
RcuGui::Command::Command(TGCompositeFrame& f, 
			 Rcuxx::Command&   low, 
			 const char*       tip,
			 Int_t             lmargin, 
			 Int_t             rmargin, 
			 Int_t             tmargin, 
			 Int_t             bmargin) 
  : fNoCheck(kFALSE), 
    fLow(low),
    fHints(kLHintsExpandX, lmargin, rmargin, tmargin, bmargin),
    fOk(&f, fLow.Name().c_str(), 0)
{
  f.AddFrame(&fOk, &fHints);
  fOk.Connect("Clicked()", "RcuGui::Command", this, "Commit()");
  if (tip) fOk.SetToolTipText(tip);
}

//____________________________________________________________________
void 
RcuGui::Command::Commit() 
{
  fOk.SetState(kButtonDown);
  unsigned int ret = fLow.Commit();
  fOk.SetState(kButtonUp);    
  if (ret) RcuGui::Main::Instance()->Error(ret);
  RcuGui::Main::Instance()->SetStatus(Form("Executed %s",
					   fLow.Name().c_str()), 
				      fNoCheck);
}

//====================================================================
RcuGui::Register::Register(TGCompositeFrame& cont, 
			   Rcuxx::Register&  low,
			   Bool_t            com,
			   Int_t             lmargin, 
			   Int_t             rmargin, 
			   Int_t             tmargin, 
			   Int_t             bmargin,
			   Bool_t            set, 
			   Bool_t            get) 
  : fLow(low),
    fGroupHints(kLHintsExpandX|kLHintsTop, lmargin, rmargin, tmargin, bmargin),
    fFieldHints(kLHintsExpandX, 1, 1, 0, 0),
    fButtonHints(kLHintsExpandX,0,0,0,0),
    fButtonsHints(kLHintsRight|kLHintsBottom,1, 1, 3, 0),
    fGroup(&cont, low.Name().c_str(), kHorizontalFrame),
    fFields(&fGroup, 0, 0, kHorizontalFrame),
    fButtons(&fGroup),
    fUpdate(&fButtons, "Update", 0),
    fClear(0), 
    fCommit(0), 
    fSet(0),
    fGet(0), 
    fPrint(&fButtons, "Print", 5)
{
  cont.AddFrame(&fGroup, &fGroupHints); 
  fGroup.SetLayoutManager(new TGHorizontalLayout(&fGroup));
  fGroup.AddFrame(&fFields, &fFieldHints);
  fUpdate.SetToolTipText("Update from hardware");
  fUpdate.Connect("Clicked()", "RcuGui::Register", this, "Update()");
  fButtons.AddFrame(&fUpdate, &fButtonHints);
    
  if (low.IsClearable()) {
    fClear = new TGTextButton(&fButtons, "Clear", 1);
    fClear->SetToolTipText("Clear register in hardware");
    fClear->Connect("Clicked()", "RcuGui::Register", this, "Clear()");
    fButtons.AddFrame(fClear, &fButtonHints);
  }    
  if (low.IsSubmitable() || com) {
    fCommit = new TGTextButton(&fButtons, "Commit", 2);
    fCommit->SetToolTipText("Commit values to hardware");
    fCommit->Connect("Clicked()", "RcuGui::Register", this, "Commit()");
    fButtons.AddFrame(fCommit, &fButtonHints);
  }
  if (set) {
    fSet = new TGTextButton(&fButtons, "Set", 3);
    fSet->SetToolTipText("Set cached value");
    fSet->Connect("Clicked()", "RcuGui::Register", this, "Set()");
    fButtons.AddFrame(fSet, &fButtonHints);
  }
  if (get) {
    fGet = new TGTextButton(&fButtons, "Get", 4);
    fSet->SetToolTipText("Get cached value");
    fGet->Connect("Clicked()", "RcuGui::Register", this, "Get()");
    fButtons.AddFrame(fGet, &fButtonHints);
  }
  fPrint.SetToolTipText("Print contents of register to standard out");
  fPrint.Connect("Clicked()", "RcuGui::Register", this, "Print()");
  fButtons.AddFrame(&fPrint, &fButtonHints);
  
  fGroup.AddFrame(&fButtons,  &fButtonsHints);
  fButtons.Resize(fButtons.GetDefaultSize());
}

//____________________________________________________________________
RcuGui::Register::~Register()
{
  if (fCommit)  delete fCommit;
  if (fClear)   delete fClear;
  if (fSet)     delete fSet;
  if (fGet)     delete fGet;
}

//____________________________________________________________________
Bool_t
RcuGui::Register::Commit()
{
  if (!fCommit) return kTRUE; 
  Rcuxx::DebugGuard g(Main::Instance()->IsDebug(), "Register::Commit() to %s", 
		      fLow.Name().c_str());
  // Set Values 
  Set();
  UInt_t ret = fLow.Commit();
  if (ret) {
    Rcuxx::DebugGuard::Message(Main::Instance()->IsDebug(), 
			       " failed with code %d", ret);
    RcuGui::Main::Instance()->Error(ret);
    return kFALSE;
  }
  usleep(100);
  if (!Update()) return kFALSE;
  RcuGui::Main::Instance()->SetStatus(Form("Wrote %s", fLow.Name().c_str()));
  return kTRUE;
}

//____________________________________________________________________
Bool_t
RcuGui::Register::Update()
{
  UInt_t ret = fLow.Update();
  if (ret == Rcuxx::Rcu::kCannotBroadcast) return kTRUE;
  if (ret) {
    RcuGui::Main::Instance()->Error(ret);
    return kFALSE;
  }
  RcuGui::Main::Instance()->SetStatus(Form("Read %s",fLow.Name().c_str()));
  Get();
  return kTRUE;
}
  
//____________________________________________________________________
void
RcuGui::Register::Clear() 
{
  if (!fClear) return;
  UInt_t ret = fLow.Clear();
  if (ret) {
    RcuGui::Main::Instance()->Error(ret);
    return;
  }
  Get();
  RcuGui::Main::Instance()->SetStatus(Form("Cleared %s",fLow.Name().c_str()));
}  

//____________________________________________________________________
void
RcuGui::Register::HandleBroadcast()
{
  fUpdate.SetEnabled(kFALSE);
  fPrint.SetEnabled(kFALSE);
  if (fGet)    fGet->SetEnabled(kFALSE);
#if 0
  if (!fLow->IsBroadcastable()) {
    if (fCommit) fCommit->SetEnabled(kFALSE);
    if (fSet)    fSet->SetEnabled(kFALSE);
  }
#endif
}

//____________________________________________________________________
void
RcuGui::Register::HandleAddress(UShort_t board, UShort_t chip, UShort_t chan)
{
  fUpdate.SetEnabled(kTRUE);
  fPrint.SetEnabled(kTRUE);
  if (fGet)    fGet->SetEnabled(kTRUE);
  if (fCommit) fCommit->SetEnabled(kTRUE);
  if (fSet)    fSet->SetEnabled(kTRUE);
}

//____________________________________________________________________
void
RcuGui::Register::Print() const
{
  fLow.Print();
  RcuGui::Main::Instance()->SetStatus(Form("Printed %s",fLow.Name().c_str()));
}  

//____________________________________________________________________
//
// EOF
//

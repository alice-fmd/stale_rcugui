//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include "BoardDialog.h"
#ifndef ROOT_TGTableLayout
# include <TGTableLayout.h>
#endif 
#include <iostream>
#include <iomanip>

//======================================================================
RcuGui::BoardDialog::~BoardDialog()
{}

  

RcuGui::BoardDialog::BoardDialog(const TGWindow *p, Char_t branch, Int_t board,
				 const unsigned int* mask, unsigned int* data, 
				 Bool_t* any, Bool_t* cancel) 
  : TGTransientFrame(gClient->GetRoot(), p, 0, 0, kVerticalFrame), 
    fMask(mask),
    fData(data), 
    fAny(any), 
    fCanceled(cancel),
    fLH1(kLHintsExpandX),
    fLH2(kLHintsExpandX|kLHintsExpandY),
    fTitle(this, Form("Branch %c, Board 0x%x",branch,board)),
    fTable(this),
    fChannel(&fTable, "Channel"),
    fChip(&fTable, "Chip"),
    fButtons(this, "", kHorizontalFrame),
    fOk(&fButtons, "OK", 0),
    fCancel(&fButtons, "Cancel", 1),
    fAll(&fButtons, "All", 2),
    fNone(&fButtons, "None", 3)
{
  *fAny = kFALSE;
  *fCanceled = kFALSE;
  AddFrame(&fTitle, &fLH1);
  fTable.SetLayoutManager(new TGTableLayout(&fTable, 11, 19, kFALSE, 1));
  AddFrame(&fTable, &fLH2);

  fTable.AddFrame(&fChannel,new TGTableLayoutHints(1,18,0,1,kLHintsCenterX));
  fTable.AddFrame(&fChip, new TGTableLayoutHints(0,1,1,10,kLHintsCenterY,0,5));

  for (size_t i = 0; i < 16; i++) {
    fChannelAxis[i] = new TGLabel(&fTable, Form("%2d", i));
    fTable.AddFrame(fChannelAxis[i], new TGTableLayoutHints(i+2, i+3, 1, 2));
    fAllChannel[i]  = new TGCheckButton(&fTable, "", i);
    fTable.AddFrame(fAllChannel[i],new TGTableLayoutHints(i+2,i+3,10,11,1,2));
    fAllChannel[i]->Connect("Clicked()", "RcuGui::BoardDialog", this, 
			    "HandleChannel()");
#if 1
    UInt_t row  = 0;
    UInt_t mrow = 0;
    for (size_t j = 0; j < 8; j++) {
      row  |= (fData[j] & (1 << i)) ? (1 << j) : 0;
      mrow |= (fMask[j] & (1 << i)) ? (1 << j) : 0;
    }
    fAllChannel[i]->SetDown((row & mrow) == mrow);
    if (mrow==0) fAllChannel[i]->SetState(kButtonDisabled,kFALSE);
#endif
  }
  fChannelAxis[16] = new TGLabel(&fTable, "All");
  fTable.AddFrame(fChannelAxis[16], new TGTableLayoutHints(18, 19, 1, 2));

  for (size_t i = 0; i < 8; i++) {
    fChipAxis[i] = new TGLabel(&fTable, Form("%d", i));
    fTable.AddFrame(fChipAxis[i], new TGTableLayoutHints(1,2,i+2,i+3,3,3));
    fAllChip[i]  = new TGCheckButton(&fTable, "", i);
    fTable.AddFrame(fAllChip[i], new TGTableLayoutHints(18,19,i+2,i+3,3,3));
    fAllChip[i]->Connect("Clicked()","RcuGui::BoardDialog",
			 this,"HandleChip()");
    // std::cout << std::hex << fData[i] << "==" << fMask[i] << std::endl;
    fAllChip[i]->SetDown((fData[i]&fMask[i])==fMask[i]);
    if (fMask[i]==0) fAllChip[i]->SetState(kButtonDisabled,kFALSE);
  }
  fChipAxis[8] = new TGLabel(&fTable, "All");
  fTable.AddFrame(fChipAxis[8], new TGTableLayoutHints(1,2,10,11,3,3));

  for (size_t i = 0; i < 8; i++) {
    for (size_t j = 0; j < 16; j++) {
      size_t id = i * 16 + j;
      fEnable[id] = new TGCheckButton(&fTable, "");
      fTable.AddFrame(fEnable[id],new TGTableLayoutHints(j+2,j+3,i+2,i+3));
      fEnable[id]->SetDown(TESTBIT(fData[i], j));
      if (TESTBIT(fData[i], j)) *fAny = kTRUE;
      if (!TESTBIT(fMask[i], j)) {
	fEnable[id]->SetState(kButtonDisabled,kFALSE);
	fEnable[id]->SetEditDisabled();
      }
    }
  }
  AddFrame(&fButtons, &fLH1);
  fButtons.Connect("Clicked(Int_t)", "RcuGui::BoardDialog", this, 
		    "HandleBut(Int_t)");
  fTable.Layout();
  MapSubwindows();
  Resize(GetDefaultSize());
  CenterOnParent();
  SetWindowName(Form("Active channel configuration for branch %c, board 0x%x",
		     branch, board));
  SetClassHints("MsgBox", "MsgBox");
  MapRaised();
  fClient->WaitFor(this);
}

//____________________________________________________________________
void 
RcuGui::BoardDialog::HandleChip() 
{
  TGButton *btn = (TGButton*)gTQSender;
  size_t i = btn->WidgetId();
    
  bool val = fAllChip[i]->IsDown();
  for (size_t j = 0; j < 16; j++) {
    bool   on  = val && TESTBIT(fMask[i], j);
    size_t bid = i * 16 + j;
    if (on) SETBIT(fData[i], j);
    else    CLRBIT(fData[i], j);
    fEnable[bid]->SetDown(on);
  }
}

//____________________________________________________________________
void 
RcuGui::BoardDialog::HandleChannel() 
{
  TGButton *btn = (TGButton*)gTQSender;
  size_t j = btn->WidgetId();

  bool val = fAllChannel[j]->IsDown();
  for (size_t i = 0; i < 8; i++) {
    size_t bid = i * 16 + j;
    bool   on  = val && TESTBIT(fMask[i], j);
    if (on) SETBIT(fData[i], j);
    else    CLRBIT(fData[i], j);
    fEnable[bid]->SetDown(on);
  }
}

//____________________________________________________________________
void 
RcuGui::BoardDialog::HandleBut(Int_t id) 
{
  unsigned col = 0;
  switch (id) {
  case 0: 
    // Loop over chips 
    *fAny = kFALSE;
    for (size_t i = 0; i < 8; i++) {
      // Loop over channels
      for (size_t j = 0; j < 16; j++) {
	size_t bid = i * 16 + j;
	bool   on  = TESTBIT(fMask[i], j) && fEnable[bid]->IsDown();
	if (on) {
	  SETBIT(fData[i], j);
	  *fAny = kTRUE;
	}
	else           
	  CLRBIT(fData[i], j);
      }
    }
    DeleteWindow();
    break;
  case 1:
    *fCanceled = kTRUE;
    DeleteWindow();
    break;
  case 2:
    // Loop over chips 
    for (size_t i = 0; i < 8;      i++) {
      fAllChip[i]->SetDown(fMask[i] != 0);
      fData[i] = fMask[i] & 0xFFFF;
      for (size_t j = 0; j < 16; j++) {
	col |= TESTBIT(fMask[i], j) ? (1 << j) : 0;
	size_t bid = i * 16 + j;
	fEnable[bid]->SetDown(TESTBIT(fData[i], j));
      }
    }
    for (size_t j = 0; j < 16; j++) 
      if (TESTBIT(col,j)) fAllChannel[j]->SetState(kButtonDown);
    *fAny = kTRUE;
    break;
  case 3:
    // Loop over chips 
    for (size_t i = 0; i < 8;      i++) {
      fAllChip[i]->SetDown(!(fMask[i] != 0));
      fData[i] = fMask[i] & 0x0000;
      for (size_t j = 0; j < 16; j++) {
	col |= TESTBIT(fMask[i], j) ? (1 << j) : 0;
	size_t bid = i * 16 + j;
	fEnable[bid]->SetDown(TESTBIT(fData[i], j));
      }
    }
    for (size_t j = 0; j < 16; j++) 
      if (TESTBIT(col,j)) fAllChannel[j]->SetState(kButtonUp);
    *fAny = kFALSE;
    break;
  }
  gClient->NeedRedraw(this, kTRUE);
}

//____________________________________________________________________
//
// EOF
//

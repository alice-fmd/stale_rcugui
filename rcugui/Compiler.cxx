//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUGUI_COMPILER_H
# include <rcugui/Compiler.h>
#endif
#include <rcuxx/DebugGuard.h>
#ifndef ROOT_TGText
# include <TGText.h>
#endif
#ifndef ROOT_TGFileDialog
# include <TGFileDialog.h>
#endif
#ifndef ROOT_TSystem
# include <TSystem.h>
#endif
#ifndef ROOT_TGMsgBox
# include <TGMsgBox.h>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __CSTDIO__
# include <cstdio>
#endif
#ifndef __CSTRING__
# include <cstring>
#endif
#ifdef HAVE_ALTROCC_COMPILER_H
# include <altrocc/compiler.h>
#endif

#ifdef HAVE_ALTROCC_COMPILER_H
static const struct RCUC_set* program = 0;
#endif

namespace {
  bool fDebug = false;
}

//____________________________________________________________________
RcuGui::ProgramSelect::ProgramSelect(const TGWindow* p) 
  : TGTransientFrame(gClient->GetRoot(), const_cast<TGWindow*>(p), 
		     150, 80, kVerticalFrame), 
    fWhich(0)
{
  fTop = new TGGroupFrame(this, "Program", kHorizontalFrame);
  AddFrame(fTop, new TGLayoutHints(kLHintsExpandX|kLHintsExpandY,0,0,0,0));
  Connect("CloseWindow()", "RcuGui::ProgramSelect", this, 
	  "HandleButtons(Int_t)");
  fCombo = new TGComboBox(fTop);
  fCombo->Resize(80, 24);
  fTop->AddFrame(fCombo, new TGLayoutHints(kLHintsExpandX,3,0,0,0));
  fOff = new TGNumberEntry(fTop, 0, 3, -1, TGNumberFormat::kNESInteger, 
			   TGNumberFormat::kNEANonNegative, 
			   TGNumberFormat::kNELLimitMin, 0);
  fTop->AddFrame(fOff, new TGLayoutHints(kLHintsRight,3,0,0,0));
  fCombo->Connect("Selected(Int_t)","TGNumberEntry",
		  fOff,"SetIntNumber(Int_t)");
  fOff->Connect("ValueSet(Long_t)","RcuGui::ProgramSelect",this,
		"HandleEntry(Int_t)");
  
  fButtons = new TGHButtonGroup(this);
  AddFrame(fButtons, new TGLayoutHints(kLHintsExpandX,3,0,0,0));
  fOK     = new TGTextButton(fButtons, "OK",     0);
  fCancel = new TGTextButton(fButtons, "Cancel", 1);
  fButtons->Connect("Clicked(Int_t)", "RcuGui::ProgramSelect", this, 
		    "HandleButtons(Int_t)");
  Clear();
}

Int_t  
RcuGui::ProgramSelect::Show()
{
  MapSubwindows();
  // Resize(GetDefaultSize());
  Resize(180, 100);
  CenterOnParent();
  SetWindowName("Choose IMEM offset or label");
  SetIconName("Choose IMEM offset or label");
  SetClassHints("MsgBox", "MsgBox");
  MapRaised();
  fClient->WaitForUnmap(this);
  // MapWindow();
  return fWhich;
}

//____________________________________________________________________
void 
RcuGui::ProgramSelect::Clear()
{
  fCombo->RemoveEntries(fFirst, fLast);
  // fCombo->Layout();
  fFirst = 0; // 100000;
  fLast  = 0;
  Add("top", 0);
  fCombo->Select(0);
}

//____________________________________________________________________
void 
RcuGui::ProgramSelect::Add(const char* lab, size_t off)
{
  fCombo->AddEntry(lab, off);
  if (off < size_t(fFirst)) fFirst = off;
  if (off > size_t(fLast))  fLast  = off;
  // fCombo->Layout();
}

//____________________________________________________________________
void 
RcuGui::ProgramSelect::HandleEntry(Int_t id) 
{
  std::cout << "Handle Entry " << id << std::endl;
  fCombo->Select(fOff->GetIntNumber());
  fCombo->Layout();
}

//____________________________________________________________________
void 
RcuGui::ProgramSelect::HandleButtons(Int_t id) 
{
  fWhich = id;
  if (id == 1) fWhich = -1;
  else         fWhich = fOff->GetIntNumber();
  // fMain->DeleteWindow();
  // fMain->CloseWindow();
  DontCallClose();
  UnmapWindow();
}

//____________________________________________________________________
RcuGui::Compiler::Compiler(TGTab& tabs)
  : Scrollable("Compiler", tabs, kHorizontalFrame),
    fViews(&fCont),
    fCodeView(&fViews, Int_t(.7 * (fCanvasW-6)), Int_t(.70 * (fCanvasH-6))),
    fCurrent(&fViews, "- no file loaded -"),
    fProgView(&fViews, Int_t(.7 * (fCanvasW-6)), Int_t(.27 * (fCanvasH-6))),
    fButtons(&fCont, "Operations"),
    fCompile(&fButtons, "Compile ...", 0),
    fCommit(&fButtons, "Compile and load", 1),
    fLoad(&fButtons, "Load from file ...", 2),
    fSave(&fButtons, "Save to file ...", 3),
    fClear(&fButtons, "Clear", 4), 
    fProgramSelect(0)
{
  // Int_t w  = fCanvasW;
  // Int_t h  = fCanvasH;
  fCurrentFile = "";
  
  fCont.AddFrame(&fViews, new TGLayoutHints(kLHintsLeft|kLHintsExpandX, 
					    0, 0, 0, 0));
  
  fCode     = new TGText;
  fCodeView.SetText(fCode);
  fCodeView.Connect("DataChanged()", "RcuGui::Compiler", this, "Changed()");
  fCodeView.Connect("Opened()", "RcuGui::Compiler", this, "Opened()");
  fCodeView.Connect("Closed()", "RcuGui::Compiler", this, "Closed()");
  fCodeView.Connect("Saved()", "RcuGui::Compiler", this, "Saved()");
  fCodeView.Connect("SavedAs()", "RcuGui::Compiler", this, "Saved()");
  fViews.AddFrame(&fCodeView, new TGLayoutHints(kLHintsExpandX, 0, 0, 0, 0));

  fCurrent.SetTextJustify(kTextLeft);
  fViews.AddFrame(&fCurrent, new TGLayoutHints(kLHintsLeft|kLHintsExpandX, 
					       0, 0, 0, 0));
  fProgram  = new TGText;
  fProgView.SetText(fProgram);
  fViews.AddFrame(&fProgView, new TGLayoutHints(kLHintsExpandX, 
						0, 0, 0, 0));
  
  fCompile.Connect("Clicked()", "RcuGui::Compiler", this, "Compile()");
  fCommit.Connect("Clicked()", "RcuGui::Compiler", this, "Commit()");
  fLoad.Connect("Clicked()", "RcuGui::Compiler", this, "Load()");
  fSave.Connect("Clicked()", "RcuGui::Compiler", this, "Save()");
  fClear.Connect("Clicked()", "RcuGui::Compiler", this, "Clear()");
  fCont.AddFrame(&fButtons, new TGLayoutHints(kLHintsExpandY,
					      3, 0, 0, 0));
  fProgramSelect = new ProgramSelect(gClient->GetRoot());
}

//____________________________________________________________________
void 
RcuGui::Compiler::Changed() 
{
  if (fCode->IsSaved())  return;
  fSave.SetState(kButtonUp, kFALSE);
  fCurrent.SetText(Form("%s (changed)", 
			gSystem->BaseName(fCurrentFile.Data())));
}

//____________________________________________________________________
void 
RcuGui::Compiler::Opened() 
{
  fCurrentFile = fCode->GetFileName();
  fCurrent.SetText(Form("%s", gSystem->BaseName(fCurrentFile.Data())));
  
}

//____________________________________________________________________
void 
RcuGui::Compiler::Closed() 
{
  fCurrentFile = "";
  fCurrent.SetText("- no file loaded -");
}

//____________________________________________________________________
void 
RcuGui::Compiler::Saved() 
{
  fCurrentFile = fCode->GetFileName();
  fSave.SetState(kButtonDisabled, kFALSE);
  fCurrent.SetText(Form("%s", gSystem->BaseName(fCurrentFile.Data())));
}

//____________________________________________________________________
Bool_t
RcuGui::Compiler::Compile() 
{
  fCurrentFile = fCode->GetFileName();
  if (!fCode->IsSaved())  { 
    Int_t ret;
    new TGMsgBox(gClient->GetRoot(), fTop,  "Save file?",
		 Form("%s is changed.\nDo you want to save it?", 
		      gSystem->BaseName(fCurrentFile.Data())),
		 kMBIconQuestion, kMBYes|kMBNo|kMBCancel, &ret);
    switch (ret) {
    case kMBYes: Save(); break;
    case kMBNo:  break;
    case kMBCancel: return kFALSE;
    default: return kFALSE;
    }
  }
#if 1
#ifdef HAVE_ALTROCC_COMPILER_H
  // RCUC_free(program);
  fProgramSelect->Clear();
  fProgView.Clear();
  program = 0;
  // std::cout << "Compiling " << fCurrentFile.Data() << std::endl;
  program = RCUC_compile(fCurrentFile.Data(), 0, 0);
  if (!program) {
    new TGMsgBox(gClient->GetRoot(), fTop,  "Compile failed",
		 Form("Compilation of %s failed\n%s", 
		      fCurrentFile.Data(),
		      RCUC_get_error()), 
		 kMBIconExclamation, kMBOk);
    return kFALSE;
  }
  for (Int_t i = 0; i < program->size; i++) {
    char* lab = program->labels[i];
    if (lab) fProgramSelect->Add(lab, i);
    TGLongPosition p(0, i);
    fProgView.GetText()->InsText(p, Form("%3d 0x%08x %s\n", 
					 i, program->data[i], 
					 (lab ? lab : " ")));
  }
  fProgView.Update();
  return kTRUE;
#else 
  new TGMsgBox(gClient->GetRoot(), fTop,  "Not supported",
	       "The RCU GUI was compiled without RCU compier support", 
	       kMBIconExclamation, kMBOk);
  return kFALSE;
#endif
#endif
}

//____________________________________________________________________
void 
RcuGui::Compiler::Commit() 
{
  if (!Compile()) return;
#ifdef HAVE_ALTROCC_COMPILER_H
  if (!program) {
    new TGMsgBox(gClient->GetRoot(), fTop,  "No program",
		 "No program loaded", kMBIconExclamation, kMBOk);
    return;
  }
  Int_t n = program->size;
  std::vector<UInt_t> data(n);
  for (Int_t i = 0; i < n; i++) {
    /// std::cout << "IMEM dat from program: " << i << " " 
    ///           << std::hex << program->data[i] << std::dec << std::endl;
    data[i] = program->data[i];
    // if (program->labels[i]) 
    // Main::Instance()->AddProgram(program->labels[i], i);
  }
#endif
}


//____________________________________________________________________
void 
RcuGui::Compiler::OpenCompileLoad(const Char_t* file) 
{
  if (!file || file[0] == '\0') return;
  if (!fCodeView.LoadFile(file)) return;
  Commit();
}

  

//____________________________________________________________________
void 
RcuGui::Compiler::Load() 
{
  fCodeView.SendMessage(&fCodeView, MK_MSG(kC_COMMAND, kCM_MENU), 
			TGTextEdit::kM_FILE_OPEN, 0);
  fProgram->Clear();
  fProgView.Clear();
}

//____________________________________________________________________
void 
RcuGui::Compiler::Save() 
{
  fCodeView.SendMessage(&fCodeView, MK_MSG(kC_COMMAND, kCM_MENU), 
			 TGTextEdit::kM_FILE_SAVE, 0);
}

//____________________________________________________________________
void 
RcuGui::Compiler::Clear()
{
  fCodeView.SendMessage(&fCodeView, MK_MSG(kC_COMMAND, kCM_MENU), 
			 TGTextEdit::kM_FILE_NEW, 0);
  fCurrentFile = fCode->GetFileName();
  fProgram->Clear();
  fProgView.Update();
}
//____________________________________________________________________
void
RcuGui::Compiler::SetDebug(Bool_t on)
{
  fDebug = on;
}

//____________________________________________________________________
//
// EOF
// 

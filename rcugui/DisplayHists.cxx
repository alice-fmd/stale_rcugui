//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    DisplayHists.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup rcugui
*/
#include "rcugui/DisplayHists.h"
#include <rcudata/ProgressMeter.h>
#include <TROOT.h>
#include <TClass.h>
#include <TKey.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TGraph.h>
#include <TF1.h>
#include <TApplication.h>
#include <TGFileDialog.h>
#include <TGClient.h>
#include <TLegend.h>
#include <iostream>

//____________________________________________________________________
namespace 
{
  RcuData::ProgressMeter fgMeter;
}


//____________________________________________________________________
RcuGui::DisplayHists::DisplayHists(const char* file) 
  : TGMainFrame(gClient->GetRoot(),10,10,kMainFrame | kVerticalFrame), 
    fMenuHints(kLHintsExpandX|kLHintsTop),
    fMenu(this),
    fFileMenu(fMenu.AddPopup("&File")),
    fViewHints(kLHintsExpandX|kLHintsExpandY),
    fView(*this),
    fStatusHints(kLHintsExpandX|kLHintsBottom), 
    fStatus(this),
    fFile(0)
{
  // Add the frames 
  AddFrame(&fMenu,   &fMenuHints);
  AddFrame(&fView,   &fViewHints);
  AddFrame(&fStatus, &fStatusHints);
  
  // Add items to the menu 
  fFileMenu->AddEntry("&Open ...",    kMenuOpen);
  fFileMenu->AddEntry("Save &As ...", kMenuSaveAs);
  // fFileMenu->AddEntry("&Close", kMenuClose);
  fFileMenu->AddEntry("&Quit",        kMenuQuit);
  
  // Connect signals to slots
  Connect("CloseWindow()", "RcuGui::DisplayHists", this, "HandleClose()");
  fView.Connect("SelectionChanged()", "RcuGui::DisplayHists", this, 
		"HandleDraw()");
  fFileMenu->Connect("Activated(Int_t)", "RcuGui::DisplayHists", this, 
		     "HandleMenu(Int_t)");

  // Setup status 
  fStatus.SetParts(2);

  // Layout, set window attributes, and show
  MapSubwindows();
  Resize(GetDefaultSize());
  Int_t w = GetWidth();
  Int_t h = GetHeight();
  SetWMSize(w, h);
  SetWMSizeHints(w, h, 2*w, 2*h, 2, 2);
  SetWindowName("Histogram Display");
  SetIconName("Histogram Display");
  SetClassHints("DisplayHists", "DisplayHists");
  SetMWMHints(kMWMDecorAll      | 
		    kMWMDecorResizeH  | 
		    kMWMDecorMaximize |
		    kMWMDecorMinimize | 
		    kMWMDecorMenu,
		    kMWMFuncAll      | 
		    kMWMFuncResize   | 
		    kMWMFuncMaximize |
		    kMWMFuncMinimize, 
		    kMWMInputModeless);
#if 0
  const TGPicture* p = 
    gClient->GetPicturePool()->GetPicture("rcu_monitor.xpm", rcuMonitor_xpm);
  if (p) {
    Pixmap_t pic = p->GetPicture();
    gVirtualX->SetIconPixmap(GetId(), pic);
  }
#endif
  MapWindow();

  // Open the file 
  Open(file);
}

//____________________________________________________________________
Bool_t
RcuGui::DisplayHists::Open(const char* file) 
{
  if (!file || file[0] == '\0') return kFALSE;
  if (fFile) {
    fFile->Close();
    delete fFile;
    fFile = 0;
  }
  fFile = TFile::Open(file, "READ");
  if (!fFile) {
    std::cerr << "Failed to open file " << file << std::endl;
    fFileMenu->EnableEntry(kMenuOpen);
    return kFALSE;
  }
  fFileMenu->DisableEntry(kMenuOpen);
  fStatus.SetText(file, 0);
  Bool_t ret = fView.ScanDirectory(fFile);
  std::cout << std::endl;
  return ret;
}

//____________________________________________________________________
void
RcuGui::DisplayHists::HandleClose() 
{
  DontCallClose();
  UnmapWindow();
  // if (fFrame) delete fFrame;
  gApplication->Terminate();
}
	      
//____________________________________________________________________
void
RcuGui::DisplayHists::HandleMenu(Int_t id) 
{
  static const char *saveAsTypes[] = { "PostScript",   "*.ps",
				       "Encapsulated PostScript", "*.eps",
				       "PDF",          "*.pdf",
				       "SVG",          "*.svg",
				       "GIF",          "*.gif",
				       "ROOT macros",  "*.C",
				       "ROOT files",   "*.root",
				       "XML",          "*.xml",
				       "PNG",          "*.png",
				       "XPM",          "*.xpm",
				       "JPEG",         "*.jpg",
				       "TIFF",         "*.tiff",
				       "XCF",          "*.xcf",
				       0,              0 };

 switch (id) {
  case kMenuOpen: {
    TGFileInfo info;
    new TGFileDialog(gClient->GetRoot(), this, kFDOpen, &info);
    if (!info.fFilename) return;
    Open(info.fFilename);
  }
    break;
  case kMenuClose:
    if (fFile) {
      fFile->Close();
      delete fFile;
      fFile = 0;
    }
    break;
  case kMenuSaveAs: {
    TGFileInfo info;
    if (fView.Current()) 
      info.fFilename = Form("%s.C", fView.Current()->GetName());
    info.fFileTypes    = saveAsTypes;
    info.fFileTypeIdx  = 5;
    new TGFileDialog(gClient->GetRoot(), this, kFDSave, &info);
    if (!info.fFilename) return;
    fView.Canvas().SaveAs(info.fFilename);
  }
    break;
  case kMenuQuit: 
    HandleClose(); 
    break;
  }
}

//____________________________________________________________________
void
RcuGui::DisplayHists::HandleDraw()
{
  TObject* user = fView.Current();
  if (!user) return;
  fView.cd();
  // user->Draw();
  TGraph* graph = 0;
  TH1*    hist  = 0;
  TString opt("");
  if (user->IsA()->InheritsFrom(TH3::Class())) 
    hist = static_cast<TH3*>(user);
  else if (user->IsA()->InheritsFrom(TH2::Class())) {
    hist = static_cast<TH2*>(user);
    opt  = "COLZ";
  }
  else if (user->IsA()->InheritsFrom(TH1::Class())) {
    hist = static_cast<TH1*>(user);
    opt  = "HIST E";
  }
  else if (user->IsA()->InheritsFrom(TGraph::Class())) {
    graph = static_cast<TGraph*>(user);
    opt   = "APL";
  }
  else {
    std::cout << user->GetName() << " is not a histogram or graph" 
	      << std::endl;
    return;
  }
  TList* funcs = 0;
  if (hist)  { 
    hist->Draw(opt.Data());
    funcs = hist->GetListOfFunctions();
    fStatus.SetText(hist->GetName(), 1);
  }
  if (graph) { 
    fView.ClearCanvas();
    graph->Draw(opt.Data());
    funcs = graph->GetListOfFunctions();
    fStatus.SetText(graph->GetName(), 1);
  }
  if (funcs && funcs->GetEntries() > 0) {
    TIter next(funcs);
    TF1* func = 0;
    while ((func = static_cast<TF1*>(next()))) 
      func->Draw("same");
    // TLegend* l = fView.Canvas().BuildLegend(.6, .11, .945, .30);
    // l->SetBorderSize(0);
    // l->SetFillColor(0);
    // l->SetFillStyle(4000);
  }
  
  fView.UpdateCanvas();
}

//____________________________________________________________________
//
// EOF
//

//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugui/SourceDialog.h>

/* These are commonly used classes */
#include <TString.h>
#include <TGTableLayout.h>
#include <TGButton.h>
#include <TGButtonGroup.h>

/* These are used by the concrete source types */
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGLabel.h>

/* These are for the U2F source type  */
#include <TGFileDialog.h>

/* The are just normal system headers */
#include <iostream>
#include <climits>

//====================================================================
RcuGui::SourceType::SourceType(TGCompositeFrame& p, const char* title, int i) 
  : fSelectHints(0,1,i,i+1), 
    fSelect(&p, title, i)
{
  p.AddFrame(&fSelect, &fSelectHints);
  Connect("RcuGui::SourceType", this, "Handle()");
}

//____________________________________________________________________
RcuGui::SourceType::SourceType(const SourceType& o)
  : fSelectHints(o.fSelectHints.GetAttachLeft(), 
		 o.fSelectHints.GetAttachRight(), 
		 o.fSelectHints.GetAttachTop(), 
		 o.fSelectHints.GetAttachBottom(), 
		 o.fSelectHints.GetPadLeft(),
		 o.fSelectHints.GetPadRight(),
		 o.fSelectHints.GetPadTop(),
		 o.fSelectHints.GetPadBottom())
    // fSelect(o.fSelect)
{}


//____________________________________________________________________
void
RcuGui::SourceType::Connect(const char* rClass, void* r, const char* rHandler) 
{
  fSelect.Connect("Clicked()", rClass, r, rHandler);
}

//====================================================================
namespace 
{
  //__________________________________________________________________
  struct OnlineSource : public RcuGui::SourceType 
  {
    OnlineSource(TGCompositeFrame& p, int i) 
      : RcuGui::SourceType(p, "On-line", i), 
	fHostLabelHints(1,2,i,i+1,kLHintsLeft), 
	fHostEntryHints(2,3,i,i+1,kLHintsLeft), 
	fRelayLabelHints(3,4,i,i+1,kLHintsLeft), 
	fRelayEntryHints(4,6,i,i+1,kLHintsLeft), 
	fHostLabel(&p, "Host"), 
	fHostEntry(&p, ""),
	fRelayLabel(&p, "via"), 
	fRelayEntry(&p, "")
    {
      p.AddFrame(&fHostLabel, &fHostLabelHints);
      p.AddFrame(&fHostEntry, &fHostEntryHints);
      p.AddFrame(&fRelayLabel, &fRelayLabelHints);
      p.AddFrame(&fRelayEntry, &fRelayEntryHints);
      fHostEntry.SetWidth(100);
      fHostEntry.SetToolTipText("Optional host name to monitor from");
      fRelayEntry.SetWidth(100);
      fRelayEntry.SetToolTipText("Optional relay host name");
    }
    void Enable() 
    {
      RcuGui::SourceType::Enable();
      fHostEntry.SetEnabled();
      fRelayEntry.SetEnabled();
      fHostEntry.SetFocus();
    }
    void Disable() 
    {
      RcuGui::SourceType::Disable();
      fHostEntry.SetEnabled(kFALSE);
      fRelayEntry.SetEnabled(kFALSE);
    }
    
    void Url(TString& url) const 
    {
      url = "date://";
      TString host(fHostEntry.GetText());
      TString shost(host.Strip(TString::kBoth));
      if (!shost.IsNull()) {
	TString relay(fRelayEntry.GetText());
	TString srelay(relay.Strip(TString::kBoth));
	if (!srelay.IsNull()) {
	  url.Append(srelay);
	  url.Append("|");
	}
	url.Append(shost);
	url.Append("/");
      }
    }
  protected:
    TGTableLayoutHints fHostLabelHints;
    TGTableLayoutHints fHostEntryHints;
    TGTableLayoutHints fRelayLabelHints;
    TGTableLayoutHints fRelayEntryHints;
    TGLabel            fHostLabel;
    TGTextEntry        fHostEntry;
    TGLabel            fRelayLabel;
    TGTextEntry        fRelayEntry;
    
  };

  //__________________________________________________________________
  struct OfflineSource : public RcuGui::SourceType 
  {
    OfflineSource(TGCompositeFrame& p, int i) 
      : RcuGui::SourceType(p, "Off-line", i), 
	fLabelHints(1,2,i,i+1,kLHintsLeft), 
	fFileEntryHints(2,5,i,i+1,kLHintsLeft), 
	fBrowseHints(5,6,i,i+1,kLHintsLeft), 
	fLabel(&p, "File"), 
	fFileEntry(&p, ""),
	fBrowse(&p, "Browse ...")
    {
      p.AddFrame(&fLabel, &fLabelHints);
      p.AddFrame(&fFileEntry, &fFileEntryHints);
      p.AddFrame(&fBrowse, &fBrowseHints);
      fFileEntry.SetWidth(200);
      fFileEntry.SetToolTipText("File to read from");
      fBrowse.Connect("Clicked()", "RcuGui::SourceType",this,"HandleBrowse()");
    }
    void Enable() 
    {
      RcuGui::SourceType::Enable();
      fFileEntry.SetEnabled();
      fBrowse.SetEnabled();
      fFileEntry.SetFocus();
    }
    void Disable() 
    {
      RcuGui::SourceType::Disable();
      fFileEntry.SetEnabled(kFALSE);
      fBrowse.SetEnabled(kFALSE);
    }
    void HandleBrowse() 
    {
      static const char *types[] = {
	"ROOT files",   "*.root",
	"Data files",   "*.dat",
	"All files",    "*",
	0,              0
      };
      static TGFileInfo fi;
      fi.fFileTypes = types;
      new TGFileDialog(gClient->GetRoot(), gClient->GetRoot(), kFDOpen, &fi);
      fFileEntry.SetText(fi.fFilename);
    }
    void Url(TString& url) const 
    { 
      url = fFileEntry.GetText(); 
    }
  protected:
    TGTableLayoutHints fLabelHints;
    TGTableLayoutHints fFileEntryHints;
    TGTableLayoutHints fBrowseHints;
    TGLabel            fLabel;
    TGTextEntry        fFileEntry;
    TGTextButton       fBrowse;
    
  };


  
}

  
//====================================================================
RcuGui::SourceDialog::SourceDialog(const TGWindow* p, 
				   TString& ret, 
				   TString& out, 
				   long&    n,
				   size_t&  skip,
				   bool&    tree, 
				   bool&    all,
				   int      flags)
  : TGTransientFrame(p, gClient->GetRoot(),1,1,kVerticalFrame), 
    fReturn(ret),
    fOut(out), 
    fN(n), 
    fSkip(skip),
    fAll(all),
    fTree(tree), 
    fSelectHints(kLHintsExpandX|kLHintsExpandY,3,3,3,3),
    fSelect(this, "Source URI"),
    fOptionsHints(kLHintsExpandX, 0, 3, 0, 3),
    fOptionHints(kLHintsExpandX, 3, 0, 0, 0),
    fOptions(this, "Options", kVerticalFrame), 
    fOutputFrame(&fOptions),
    fOutputLabel(&fOutputFrame,  "Output file:"),
    fOutputEntry(&fOutputFrame,  ""), 
    fOutputBrowse(&fOutputFrame, "Browse ...", kBrowse), 
    fMaxEvents(fOptions, "Max events:", -1, LONG_MAX, kHorizontalFrame),
    fSkipEvents(fOptions, "Skip events:", 0, LONG_MAX, kHorizontalFrame),
    fMiscFrame(&fOptions),
    fTreeOpt(&fMiscFrame, "Output tree", kTree), 
    fAllOpt(&fMiscFrame, "All events", kAll),
    fButtonsHints(kLHintsExpandX,3,3,0,3),
    fButtonHints(kLHintsCenterY | kLHintsExpandX, 3, 3),
    fButtons(this), 
    fCancel(&fButtons, "Cancel", kCancel),
    fOK(&fButtons, "OK", kOk), 
    fOnline(0), 
    fOffline(0)
{
  AddFrame(&fSelect,&fSelectHints);
  int lines = 0;
  if ((flags & kOnline)  != 0) lines++;
  if ((flags & kOffline) != 0) lines++;
  fSelect.SetLayoutManager(new TGTableLayout(&fSelect,lines,6,kFALSE,3));

  int line = 0;
  if ((flags & kOnline) != 0) {
    fOnline = new OnlineSource(fSelect, line);
    fOnline->Connect("RcuGui::SourceDialog", this, "EnableOnline()");
    line++;
  }
  if ((flags & kOffline) != 0) {
    fOffline = new OfflineSource(fSelect, line);
    fOffline->Connect("RcuGui::SourceDialog", this, "EnableOffline()");
    line++;
  }
  if (fOnline) EnableOnline();
  else         EnableOffline();
  
  AddFrame(&fOptions, &fOptionsHints);
  fOutputEntry.SetWidth(200);
  fOutputBrowse.Connect("Clicked()", "RcuGui::SourceDialog", this, 
			"HandleBrowse()");
  fMaxEvents.SetValue(n);
  fSkipEvents.SetValue(skip);
  fOutputEntry.SetText(out.Data());
  fAllOpt.SetDown(all);
  fTreeOpt.SetDown(tree);
  
  fOptions.AddFrame(&fOutputFrame,      &fOptionHints);
  fOutputFrame.AddFrame(&fOutputLabel,  &fOptionHints);
  fOutputFrame.AddFrame(&fOutputEntry,  &fOptionHints);
  fOutputFrame.AddFrame(&fOutputBrowse, &fOptionHints);
  fOptions.AddFrame(&fMiscFrame,        &fOptionHints);
  fMiscFrame.AddFrame(&fTreeOpt,        &fOptionHints);
  fMiscFrame.AddFrame(&fAllOpt,         &fOptionHints);
  
  AddFrame(&fButtons, &fButtonsHints);
  fButtons.Connect("Clicked(int)", "RcuGui::SourceDialog",
		   this,"HandleButtons(int)");
  fButtons.SetLayoutHints(&fButtonHints);

  Layout();
  MapSubwindows();
  Resize(GetDefaultSize());
  CenterOnParent();
  Int_t w = GetWidth();
  Int_t h = GetHeight();
  SetWMSize(w, h);
  SetWMSizeHints(w, h, w, h, 0, 0);
  SetWindowName("Open data Source ...");
  SetIconName("Open data Source ...");
  SetClassHints("MsgBox", "MsgBox");
  SetMWMHints(kMWMDecorTitle,kMWMFuncMove,kMWMInputModeless);
  SetCleanup(kNoCleanup);
  MapRaised();
  fClient->WaitFor(this);
  // MapWindow();
}

//____________________________________________________________________
RcuGui::SourceDialog::~SourceDialog() 
{
  if (fOnline)  delete fOnline;
  if (fOffline) delete fOffline;
}

//____________________________________________________________________
void
RcuGui::SourceDialog::HandleBrowse()
{
  static const char *types[] = {
    "ROOT files",   "*.root",
    "All files",    "*",
    0,              0
  };
  static TGFileInfo fi;
  fi.fFileTypes = types;
  new TGFileDialog(gClient->GetRoot(), gClient->GetRoot(), kFDSave, &fi);
  fOutputEntry.SetText(fi.fFilename);
}

//____________________________________________________________________
void
RcuGui::SourceDialog::HandleButtons(Int_t i) 
{
  switch (i) {
  case kCancel: 
    break;
  case kOk:
    if      (fOnline  && fOnline->IsEnabled())  fOnline->Url(fReturn);
    else if (fOffline && fOffline->IsEnabled()) fOffline->Url(fReturn);
    fOut  = fOutputEntry.GetText();
    fN    = fMaxEvents.GetValue();
    fSkip = fSkipEvents.GetValue();
    fTree = fTreeOpt.IsDown();
    fAll  = fAllOpt.IsDown();
    break;
  }
  DeleteWindow(); 
}

//____________________________________________________________________
void 
RcuGui::SourceDialog::EnableOnline()
{
  if (fOnline)  fOnline->Enable();
  if (fOffline) fOffline->Disable();
}

//____________________________________________________________________
void 
RcuGui::SourceDialog::EnableOffline()
{
  if (fOffline) fOffline->Enable();
  if (fOnline)  fOnline->Disable();
}

//____________________________________________________________________
//
// EOF
//

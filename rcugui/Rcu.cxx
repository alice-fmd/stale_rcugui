//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Rcu.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul 22 07:46:42 2006
    @brief   
    
*/
#include <rcugui/Rcu.h>
#include <rcugui/Main.h>
#include <rcuxx/DebugGuard.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <TSystem.h>
#include <RQ_OBJECT.h>
#include "rcu/ABDFSMST.h"
#include "rcu/ACTFEC.h"
#include "rcu/ActiveBit.h"
#include "rcu/ActiveChannels.h"
#include "rcu/ALTROCFG1.h"
#include "rcu/ALTROCFG2.h"
#include "rcu/ALTROIF.h"
#include "rcu/BPVERS.h"
#include "rcu/CDH.h"
#include "rcu/CHADD.h"
#include "rcu/Counter.h"
#include "rcu/Counters.h"
#include "rcu/ERRREG.h"
#include "rcu/ERRST.h"
#include "rcu/EVMNGST.h"
#include "rcu/EVWORD.h"
#include "rcu/FECRST.h"
#include "rcu/CONFFEC.h"
#include "rcu/FECERR.h"
#include "rcu/FMIREG.h"
#include "rcu/FWVERS.h"
#include "rcu/InstructionMemory.h"
#include "rcu/INSSEQST.h"
#include "rcu/INTMOD.h"
#include "rcu/INTREG.h"
#include "rcu/L1Timeout.h"
#include "rcu/L2Timeout.h"
#include "rcu/L1MsgTimeout.h"
#include "rcu/LWADD.h"
#include "rcu/Memory.h"
#include "rcu/PatternMemory.h"
#include "rcu/ResultMemory.h"
#include "rcu/PedestalConfig.h"
#include "rcu/PMCFG.h"
#include "rcu/RCUBUS.h"
#include "rcu/RDOERR.h"
#include "rcu/BusStatus.h"
#include "rcu/RDOFEC.h"
#include "rcu/RDOFSMST.h"
#include "rcu/RDOMOD.h"
#include "rcu/RESREG.h"
#include "rcu/MEBSTCNT.h"
#include "rcu/ROIConfig.h"
#include "rcu/SCCLK.h"
#include "rcu/SCINT.h"
#include "rcu/StatusEntry.h"
#include "rcu/Status.h"
#include "rcu/TTCControl.h"
#include "rcu/TTCEventInfo.h"
#include "rcu/TTCEventError.h"
#include "rcu/TRCFG1.h"
#include "rcu/TRCFG2.h"
#include "rcu/TRGCONF.h"
#include "rcu/TRCNT.h"
#include <rcuxx/rcu/RcuRMEM.h>
#include <rcuxx/rcu/RcuDMEM.h>
#include <rcuxx/rcu/RcuHEADER.h>
#include <rcuxx/rcu/RcuHITLIST.h>
#include <rcuxx/rcu/RcuCDH.h>

namespace
{
  bool fDebug = false;
}
// #define SCALE(X) (RcuGui::Main::Instance()->Scale()*(X))
// #define ISCALE(X) int(RcuGui::Main::Instance()->Scale()*(X))


//====================================================================
RcuGui::Rcu::Rcu(TGTab& tabs, Rcuxx::Rcu& rcu, UInt_t maxFEC, 
		 const UInt_t* mask) 
  : Scrollable("RCU", tabs, kVerticalFrame), 
    fLow(rcu),
    fTab(&fCont, tabs.GetWidth(), tabs.GetHeight()-ISCALE(35)),
    fFront(fTab.AddTab("Commands and Registers")),
    fFrontCmd(rcu.GLB_RESET() || rcu.GLB_RESET() ? 
	      new TGGroupFrame(fFront, "Commands", kHorizontalFrame) : 0),
    fEvent(fTab.AddTab("Event")),
    fEventCmd(rcu.CLR_EVTAG() || 
	      rcu.L1_CMD() || 
	      rcu.L1_I2C() || 
	      rcu.L1_TTC() || 
	      rcu.RDABORT() || 
	      rcu.SWTRG()   || 
	      rcu.TRG_CLR() ? 
	      new TGGroupFrame(fEvent, "Commands", kHorizontalFrame) : 0),
    fBus(fTab.AddTab("Bus")),
    fBusCmd(rcu.DCS_ON() || rcu.DDL_ON() || rcu.FECRST() ? 
	    new TGGroupFrame(fBus, "Commands", kHorizontalFrame) : 0),
    fTTC(rcu.L1Timeout() ? fTab.AddTab("TTC") : 0),
    fTTCCmd(rcu.TTCReset()  ? 
	    new TGGroupFrame(fTTC, "Commands", kHorizontalFrame) : 0),
    fALTRO(rcu.ALTROCFG1() ? fTab.AddTab("ALTRO") : 0),
    fALTROCmd(false ? 
	      new TGGroupFrame(fALTRO, "Commands", kHorizontalFrame) : 0),
    fMonitor(fTab.AddTab("Monitor")),
    fMonitorCmd(rcu.SCCOMMAND() ? 
		new TGGroupFrame(fMonitor, "Commands", kHorizontalFrame) : 0),
    fFW(rcu.FMIREG() ? fTab.AddTab("Firmware") : 0),
    fFWCmd(rcu.RDFM() || rcu.WRFM() ? 
	   new TGGroupFrame(fFW, "Commands", kHorizontalFrame) : 0),
    fCommandHints(kLHintsExpandX, ISCALE(3), ISCALE(3), ISCALE(3), ISCALE(3)),
    fCLR_EVTAG(0), // Command: CLeaR EVent TAG command
    fDCS_ON(0),	   // Command: DCS OwN bus command
    fDDL_ON(0),	   // Command: DDL OwN bus command
    fFECRST(0),	   // Command: Front-End Card ReSeT command
    fCONFFEC(0),   // Command: CONFigure Front-End Card firmware command
    fGLB_RESET(0), // Command: GLoBal RESET command
    fL1_CMD(0),	   // Command: L1-enable via CoMmanD command
    fL1_I2C(0),	   // Command: L1-enable via I2C command
    fL1_TTC(0),	   // Command: L1-enable via TTC command
    fRCU_RESET(0), // Command: RCU RESET command
    fRDABORT(0),   // Command: ReaD-out ABORT command
    fRDFM(0),	   // Command: FirMware command
    fSCCOMMAND(0), // Command: Slow Control COMMAND command
    fARBITERIRQ(0),//Command: Request DCS interrupt
    fSWTRG(0),	   // Command: SoftWare TRiGger command
    fTRG_CLR(0),   // Command: TRiGger CLeaR command
    fWRFM(0),	   // Command: WRite FirMware command
    fERRST(0),	   // Register: Interface to the ERRor and STatus register.  
    fTRCNT(0),	   // Register: Cache of the TRigger CouNTers.  
    fLWADD(0),	   // Register: Last Written ADDress in the DMEM1 and MEM2.  
    fIRADD(0),	   // Register: Last executed ALTRO InstRuction ADDress.  
    fEVWORD(0),	   // Register: EVent WORD register.  
    fACTFEC(0),	   // Register: ACTive FrontEnd Card.  
    fRDOFEC(0),	   // Register: ReaDOut FrontEnd Card register.  
    fTRCFG1(0),	   // Register: TRigger ConFiG register.  
    fTRCFG2(0),	   // Register: ReaD-Out MODe register.  
    fFMIREG(0),	   // Register: FirMware Input ReGister.
    fFMOREG(0),	   // Register: FirMware Output ReGister 
    fPMCFG(0),	   // Register: Pedestal Memory ConFiGuration
    fCHADD(0),	   // Register: CHannel Address register 
    fINTREG(0),	   // Register: INTerrupt REGister 
    fSCCLK(0),     // Register: Slow Control interface 
    fSCINT(0),     // Register: Slow Control interface 
    fRESREG(0),	   // Register: RESult REGister 
    fERRREG(0),	   // Register: ERRor REGister 
    fINTMOD(0),	   // Register: INTerrupt MODe register 
    fFWVERS(0),    // Register: RCU FirmWare VERSion
    fABDFSMST(0),  //
    fRDOFSMST(0), 
    fINSSEQST(0), 
    fEVMNGST(0),

    fBPVERS(0),	   //
    fALTROCFG1(0), //
    fALTROCFG2(0), //
    fTRGCONF(0),   //
    fALTROIF(0),   //
    fRCUBUS(0),	   //
    fRDOMOD(0),	   //
    fRDOERR(0),	   //
    fALTBUS_STATUS(0),
    fALTBUS_TRSF(0),
    fFECERRA(0),
    fFECERRB(0),
    fMEBSTCNT(0),
    fCounters(0), // 

    fTTCControl(0),
    fTTCReset(0),
    fTTCResetCnt(0),
    fTTCTestMode(0),
    fROIConfig(0),
    fL1Timeout(0),
    fL2Timeout(0),
    fL1MsgTimeout(0),
    fROITimeout(0),
#if 1
    fTTCCounters(0),
#else
    fPrePulseCnt(0),
    fBCId(0),
    fL0CNT(0),
    fL1CNT(0),
    fL1MsgCNT(0),
    fL2ACNT(0),
    fL2RCNT(0),
    fROICNT(0),
    fTTCHammingCnt(0),
    fTTCDecodeCnt(0),
    fBufferedCnt(0),
#endif
    fTTCEventInfo(0),
    fTTCEventError(0),

    fIMEM(0),	   // Memory: Pointer to IMEM interface
    fRMEM(0),	   // Memory: Pointer to RMEM interface
    fPMEM(0),	   // Memory: Pointer to PMEM interface
    fDM1(0),	   // Memory: Pointer to DM1 interface
    fDM2(0),	   // Memory: Pointer to DM2 interface
    fACL(0),	   // Memory: Pointer to ACL interface
    fHITLIST(0),   // Memory: Pointr to HITLIST interface 
    fHEADER(0),	   // Memory: Pointer to HEADER interface
    fCDH(0)
{
  static UInt_t def_mask[] = { 0xFFFFFFFF, 
			       0xFFFFFFFF, 
			       0xFFFFFFFF, 
			       0xFFFFFFFF, 
			       0xFFFFFFFF, 
			       0xFFFFFFFF, 
			       0xFFFFFFFF, 
			       0xFFFFFFFF };
  fCont.AddFrame(&fTab, new TGLayoutHints(kLHintsExpandX));

  if (fFrontCmd)   fFront->AddFrame(fFrontCmd, &fCommandHints);
  if (fBusCmd)     fBus->AddFrame(fBusCmd, &fCommandHints);
  if (fTTCCmd)     fTTC->AddFrame(fTTCCmd, &fCommandHints);
  if (fMonitorCmd) fMonitor->AddFrame(fMonitorCmd, &fCommandHints);
  if (fEventCmd)   fEvent->AddFrame(fEventCmd, &fCommandHints);
  if (fFWCmd)      fFW->AddFrame(fFWCmd, &fCommandHints);
  if (fALTROCmd)   fALTRO->AddFrame(fALTROCmd, &fCommandHints);

  if (fLow.IMEM())   fIMEM   = new InstructionMemory(fTab, *fLow.IMEM(),
						     *fLow.IRADD(), 
						     *fLow.IRDAT(),
						     *fLow.EXEC(), 
						     *fLow.ABORT());
  if (fLow.RMEM())   fRMEM   = new ResultMemory(fTab, *fLow.RMEM());
  if (fLow.PMEM() && fLow.PMCFG() && fLow.IMEM())
    fPMEM   = new PatternMemory(fTab, *fLow.PMEM(), *fLow.PMCFG(), 
				*fLow.IMEM(), *fLow.ERRST(), 0);
  if (fLow.DM1())    fDM1    = new Memory(fTab, *fLow.DM1());
  if (fLow.DM2())    fDM2    = new Memory(fTab, *fLow.DM2());
  if (fLow.ACL())    fACL    = new ActiveChannels(fTab, *fLow.ACL(), 
						  maxFEC, 
						  (mask ? mask : def_mask));
  if (fLow.HITLIST())fHITLIST= new Memory(fTab, *fLow.HITLIST());
  if (fLow.HEADER()) fHEADER = new Memory(fTab, *fLow.HEADER());
  if (fLow.STATUS()) fSTATUS = new Status(fTab, *fLow.STATUS());
  if (fLow.CDH())    fCDH    = new CDH(fTab, 
				       *static_cast<Rcuxx::RcuCDH*>(fLow.CDH()));
  if (fIMEM) fIMEM->WriteText();
  // if (fACL) fACL->WriteText();

  SetupCommands(*fFront);
  SetupRegisters(*fFront, maxFEC, (mask ? mask : def_mask));

}

//====================================================================
RcuGui::Rcu::~Rcu()
{
  Rcuxx::DebugGuard g(fDebug, "RcuGui::Rcu::~Rcu()");
}

//____________________________________________________________________
void 
RcuGui::Rcu::SetupCommands(TGCompositeFrame& cont)
{
  if (fLow.GLB_RESET())
    fGLB_RESET = new Command(*fFrontCmd, *fLow.GLB_RESET(), "GLoBal RESET");
  if (fLow.RCU_RESET())
    fRCU_RESET = new Command(*fFrontCmd, *fLow.RCU_RESET(), "RCU RESET");

  if (fLow.DCS_ON())
    fDCS_ON = new Command(*fBusCmd, *fLow.DCS_ON(), "DCS OwN bus");
  if (fLow.DDL_ON())
    fDDL_ON = new Command(*fBusCmd, *fLow.DDL_ON(), "DDL OwN bus");
  if (fLow.FECRST())
    fFECRST = new Command(*fBusCmd, *fLow.FECRST(), "Front-End Card ReSeT");
  if (fLow.CONFFEC())
    fCONFFEC = new CONFFEC(*fBusCmd, *fLow.CONFFEC());

  if (fLow.CLR_EVTAG()) 
    fCLR_EVTAG = new Command(*fEventCmd, *fLow.CLR_EVTAG(),"CLeaR EVent TAG");
  if (fLow.L1_CMD())
    fL1_CMD = new Command(*fEventCmd, *fLow.L1_CMD(), "L1-enable via CoMmanD");
  if (fLow.L1_I2C())
    fL1_I2C = new Command(*fEventCmd, *fLow.L1_I2C(), 
			  "Interface L1-enable via I2C");
  if (fLow.L1_TTC()) {
    fL1_TTC = new Command(*fEventCmd, *fLow.L1_TTC(), "L1-enable via TTC");
    fL1_TTC->fNoCheck = kTRUE;
  } 

  // TTC 
  if (fLow.TTCReset()) 
    fTTCReset = new Command(*fTTCCmd, *fLow.TTCReset(), "Reset TTC module");
  if (fLow.TTCResetCnt()) 
    fTTCResetCnt = new Command(*fTTCCmd, *fLow.TTCResetCnt(), 
			       "Reset TTC counters");
  // if (fLow.TTCTestMode()) 
  // fTTCTestMode = new Command(*fTTCCmd, *fLow.TTCTestMode(), "Test mode");


  // Event  
  if (fLow.RDABORT())
    fRDABORT = new Command(*fEventCmd, *fLow.RDABORT(), "ReaD-out ABORT");
  if (fLow.SWTRG())
    fSWTRG = new Command(*fEventCmd, *fLow.SWTRG(), "SoftWare TRiGger");
  if (fLow.TRG_CLR())
    fTRG_CLR = new Command(*fEventCmd, *fLow.TRG_CLR(), "TRiGger CLeaR");

  if (fLow.RDFM())
    fRDFM = new Command(*fFWCmd, *fLow.RDFM(), "ReaD FirMware");
  if (fLow.WRFM())
    fWRFM = new Command(*fFWCmd, *fLow.WRFM(), "WRite FirMware");

  if (fLow.SCCOMMAND())
    fSCCOMMAND = new Command(*fMonitorCmd, *fLow.SCCOMMAND(), 
			     "Slow Control COMMAND");
  if (fLow.ARBITERIRQ()) fARBITERIRQ = new Command(*fMonitorCmd, 
						   *fLow.ARBITERIRQ(), 
						   "DCS Interrupt");
}

//____________________________________________________________________
void 
RcuGui::Rcu::SetupRegisters(TGCompositeFrame& cont, 
			    UInt_t maxFEC, const UInt_t* mask)
{
  // Front 
  if (fLow.ERRST())   fERRST   = new struct ERRST(*fFront, *fLow.ERRST());
  if (fLow.FWVERS())  fFWVERS  = new struct FWVERS(*fFront, *fLow.FWVERS());
  if (fLow.ABDFSMST())fABDFSMST= new struct ABDFSMST(*fFront,*fLow.ABDFSMST());
  if (fLow.RDOFSMST())fRDOFSMST= new struct RDOFSMST(*fFront,*fLow.RDOFSMST());
  if (fLow.INSSEQST())fINSSEQST= new struct INSSEQST(*fFront,*fLow.INSSEQST());
  if (fLow.EVMNGST()) fEVMNGST = new struct EVMNGST(*fFront,*fLow.EVMNGST());


  // Bus 
  if (fLow.ACTFEC())  fACTFEC  = new struct ACTFEC(*fBus, *fLow.ACTFEC(), 
						  *fACL, maxFEC, mask);
  if (fLow.ALTROIF()) fALTROIF = new struct ALTROIF(*fBus, *fLow.ALTROIF());
  if (fLow.RCUBUS())  fRCUBUS  = new struct RCUBUS(*fBus, *fLow.RCUBUS());
  if (fLow.BPVERS())  fBPVERS  = new struct BPVERS(*fBus, *fLow.BPVERS());
  if (fLow.FECERRA()) fFECERRA = new struct FECERR(*fBus, *fLow.FECERRA());
  if (fLow.FECERRB()) fFECERRB = new struct FECERR(*fBus, *fLow.FECERRB());
  if (fLow.ALTBUS_STATUS()) 
    fALTBUS_STATUS = new BusStatus("ALTRO Bus status", 
				   *fBus, *fLow.ALTBUS_STATUS());
  if (fLow.ALTBUS_TRSF()) 
    fALTBUS_STATUS = new BusStatus("ALTRO Bus status during transfer", 
				   *fBus, *fLow.ALTBUS_TRSF());
  

  // ALTRO 
  if (fLow.ALTROCFG1()) fALTROCFG1 = new struct ALTROCFG1(*fALTRO, 
							  *fLow.ALTROCFG1());
  if (fLow.ALTROCFG2()) fALTROCFG2 = new struct ALTROCFG2(*fALTRO, 
							  *fLow.ALTROCFG2());


  // Event 
  if (fLow.TRCFG1())  fTRCFG1  = new struct TRCFG1(*fEvent, *fLow.TRCFG1());
  if (fLow.TRCFG2())  fTRCFG2  = new struct TRCFG2(*fEvent, *fLow.TRCFG2());
  if (fLow.TRGCONF()) fTRGCONF = new struct TRGCONF(*fEvent, *fLow.TRGCONF());
  if (fLow.CHADD())   fCHADD   = new struct CHADD(*fEvent, *fLow.CHADD());
  if (fLow.TRCNT())   fTRCNT   = new struct TRCNT(*fEvent, *fLow.TRCNT());
  if (fLow.LWADD())   fLWADD   = new struct LWADD(*fEvent, *fLow.LWADD());
  if (fLow.EVWORD())  fEVWORD  = new struct EVWORD(*fEvent, *fLow.EVWORD());
  if (fLow.RDOMOD())  fRDOMOD  = new struct RDOMOD(*fEvent, *fLow.RDOMOD());
  if (fLow.RDOFEC())  fRDOFEC  = new struct RDOFEC(*fEvent, *fLow.RDOFEC());
  if (fLow.RDOERR())  fRDOERR  = new struct RDOERR(*fEvent, *fLow.RDOERR());
  if (fLow.MEBSTCNT())fMEBSTCNT= new struct MEBSTCNT(*fEvent,*fLow.MEBSTCNT());

  if (fLow.SWTRGCNT()) { 
    Rcuxx::RcuCounter* cnts[] = { fLow.SWTRGCNT(),
				  fLow.AUXTRGCNT(),
				  fLow.TTCL2ACNT(),
				  fLow.TTCL2RCNT(),
				  fLow.DSTBACNT(),
				  fLow.DSTBBCNT(),
				  fLow.TRSFACNT(),
				  fLow.TRSFBCNT(),
				  fLow.ACKACNT(),
				  fLow.ACKBCNT(),
				  fLow.CSTBACNT(),
				  fLow.CSTBBCNT(),
				  fLow.DSTBNUMA(),
				  fLow.DSTBNUMB(),
				  0 };
    const char* nams[] = { // "Multi-event buffer status counter", 
			   "Software trigger counter", 
			   "Aux trigger counter", 
			   "TTC L2 accept counter", 
			   "TTC L2 reject counter", 
			   "Data strobe counter for branch A",
			   "Data strobe counter for branch B",
			   "Transfer assert counter for branch A",
			   "Transfer assert counter for branch B",
			   "Acknowledge assert counter for branch A",
			   "Acknowledge assert counter for branch B",
			   "Control strobe counter for branch A",
			   "Control strobe counter for branch B",
			   "Last transfer data strobe counter for branch A",
			   "Last transfer data strobe counter for branch B",
			   0 };
    fCounters = new Counters(*fEvent, cnts, nams);
  }

  // TTC
  if (fLow.TTCControl())    
    fTTCControl = new TTCControl(*fTTC,*fLow.TTCControl());
  if (fLow.ROIConfig1() && fLow.ROIConfig2()) 
    fROIConfig = new ROIConfig(*fTTC, *fLow.ROIConfig1(), *fLow.ROIConfig2());
  if (fLow.L1Timeout())    
    fL1Timeout = new L1Timeout(*fTTC,*fLow.L1Timeout());
  if (fLow.L1MsgTimeout()) 
    fL1MsgTimeout = new L1MsgTimeout(*fTTC, *fLow.L1MsgTimeout());
  if (fLow.L2Timeout()) 
    fL2Timeout = new L2Timeout(*fTTC, *fLow.L2Timeout());
  if (fLow.TTCEventInfo())    
    fTTCEventInfo = new TTCEventInfo(*fTTC,*fLow.TTCEventInfo());
  if (fLow.TTCEventError())    
    fTTCEventError = new TTCEventError(*fTTC,*fLow.TTCEventError());

  if (fLow.PrePulseCnt()) {
    Rcuxx::RcuCounter* cnts[] = { fLow.PrePulseCnt(),		
				  fLow.BCId(),			
				  fLow.L0CNT(),			
				  fLow.L1CNT(),			
				  fLow.L1MsgCNT(),		
				  fLow.L2ACNT(),		
				  fLow.L2RCNT(),		
				  fLow.ROICNT(),		
				  fLow.TTCHammingCnt(),		
				  fLow.TTCDecodeCnt(),		
				  fLow.BufferedCnt(),		
				  0 };
    const char* nams[] = { "Prepulser counter",	   
			   "Bunch-crossing counter",  
			   "L0 counter",		   
			   "L1 counter",		   
			   "L1 message counter",	   
			   "L2A counter",		   
			   "L2R counter",		   
			   "ROI counter",		   
			   "# Hamming coding errors", 
			   "# Decode errors",	   
			   "# of buffered events",    
			   0 };
    fTTCCounters = new Counters(*fTTC, cnts, nams);
  }

  // Monitor
  if (fLow.INTREG()) fINTREG = new struct INTREG(*fMonitor, *fLow.INTREG());
  if (fLow.SCCLK())  fSCCLK  = new struct SCCLK(*fMonitor, *fLow.SCCLK());
  if (fLow.SCDAT())  fSCINT  = new struct SCINT(*fMonitor,  
						*fLow.SCADD(),
						*fLow.SCDAT(),
						*fLow.SCEXEC());
  if (fLow.RESREG()) fRESREG = new struct RESREG(*fMonitor, *fLow.RESREG());
  if (fLow.ERRREG()) fERRREG = new struct ERRREG(*fMonitor, *fLow.ERRREG());
  if (fLow.INTMOD()) fINTMOD = new struct INTMOD(*fMonitor, *fLow.INTMOD());
  
  if (fLow.FMIREG() && fLow.FMOREG())
    fFMIREG = new struct FMIREG(*fFW, *fLow.FMIREG(), *fLow.FMOREG());

}


//____________________________________________________________________
void 
RcuGui::Rcu::Update() 
{
  unsigned int ret = 0;
  try {    
    if (fERRST	 && (ret = fERRST->Update()))	 throw ret;
    if (fTRCNT	 && (ret = fTRCNT->Update()))	 throw ret;
    if (fLWADD	 && (ret = fLWADD->Update()))	 throw ret;
    if (fIRADD	 && (ret = fIRADD->Update()))	 throw ret;
    if (fIRDAT	 && (ret = fIRDAT->Update()))	 throw ret;
    if (fEVWORD	 && (ret = fEVWORD->Update()))	 throw ret;
    //if (fACTFEC	 && (ret = fACTFEC->Update()))	 throw ret;
    if (fRDOFEC	 && (ret = fRDOFEC->Update()))	 throw ret;
    //if (fTRCFG1	 && (ret = fTRCFG1->Update()))	 throw ret;
    if (fTRCFG2	 && (ret = fTRCFG2->Update()))	 throw ret;
    if (fFMIREG	 && (ret = fFMIREG->Update()))	 throw ret;
    if (fFMOREG	 && (ret = fFMOREG->Update()))	 throw ret;
    if (fPMCFG	 && (ret = fPMCFG->Update()))	 throw ret;
    if (fCHADD	 && (ret = fCHADD->Update()))	 throw ret;
    if (fINTREG	 && (ret = fINTREG->Update()))	 throw ret;
    if (fRESREG	 && (ret = fRESREG->Update()))	 throw ret;
    if (fERRREG	 && (ret = fERRREG->Update()))	 throw ret;
    if (fINTMOD	 && (ret = fINTMOD->Update()))	 throw ret;
    if (fIMEM	 && (ret = fIMEM->Update()))	 throw ret;
    if (fPMEM	 && (ret = fPMEM->Update()))	 throw ret;
    if (fRMEM	 && (ret = fRMEM->Update()))	 throw ret;
    if (fDM1	 && (ret = fDM1->Update()))	 throw ret;
    if (fDM2	 && (ret = fDM2->Update()))	 throw ret;
    if (fACL	 && (ret = fACL->Update()))	 throw ret;
    if (fHITLIST && (ret = fHITLIST->Update()))	 throw ret;
    if (fHEADER	 && (ret = fHEADER->Update()))	 throw ret;
    if (fSTATUS	 && (ret = fSTATUS->Update()))	 throw ret;
  }
  catch (unsigned int ret) {
    return;
  }   
}

//____________________________________________________________________
void 
RcuGui::Rcu::SetDebug(bool on) 
{
  fDebug = on;
}

void
RcuGui::Rcu::SetDebugBackend(int lvl)
{
  fLow.SetDebug(Rcuxx::Rcu::kRcu, lvl);
}

//____________________________________________________________________
//
// EOF
//

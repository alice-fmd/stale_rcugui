// -*- mode: C++ -*-
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCU_H
#define RCU_H
# include <TGFrame.h>
# include <TGTab.h>
# include <TGTextEdit.h>
# include <TGCanvas.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/Scrollable.h>
# include <rcugui/ErrorBit.h>
# include <rcugui/Register.h>
# include <rcuxx/Rcu.h>
# include <rcugui/LinkedView.h>
# include <rcugui/LabeledNumber.h>
# include <rcugui/Register.h>


namespace RcuGui
{
  class PedestalConfig;
  class StatusEntry;
  class Memory;
  class InstructionMemory;
  class PatternMemory;
  class Status;
  class ActiveBit;
  class ActiveChannels;
  class ResultMemory;
  class CDH;

  //____________________________________________________________________  
  /** @class Rcu 
      @brief Interface to the RCU 
      @ingroup rcugui_low
  */
  class Rcu : public Scrollable
  {
  public:
    /** Create the register interface tab 
	@param tabs Where to put the tab 
	@param low Low-level interface  
	@param maxFEC Bit mask of allowed active FECs.
	@param mask   of valid ALTRO channels per FEC. */
    Rcu(TGTab& tabs, Rcuxx::Rcu& low, 
	UInt_t maxFEC=0xFFFFFFFF, 
	const UInt_t* mask=0);
    /** Destructor */
    ~Rcu();
    /** Update (some of) the register displays */
    void Update();
    /** Handle change of address */
    void SetDebug(bool on);
    /** @param lvl Debug backend at level @a lvl */
    void SetDebugBackend(int lvl);
    /** @return Reset front end cards */
    Command* FECRST() const { return fFECRST; }
    /** @return Software trigger */
    Command* SWTRG() const { return fSWTRG; }
    /** @return Read-out abort */
    Command* RDABORT() const { return fRDABORT; }
    /** @return Pointer to the ERRST interface */
    Register* ERRST() const { return fERRST; }
    /** @return Pointer to the TRCFG interface */
    Register* TRCFG1() const { return fTRCFG1; }
    /** @return Pointer to the TRCFG2 interface */
    Register* TRCFG2() const { return fTRCFG2; }
    /** @return Pointer to the TRCNT interface */
    Register* TRCNT() const { return fTRCNT; }
    /** @return Pointer to the LWADD interface */
    Register* LWADD() const { return fLWADD; }
    /** @return Pointer to the IRADD interface */
    Register* IRADD() const { return fIRADD; }
    /** @return Pointer to the EVWORD interface */
    Register* EVWORD() const { return fEVWORD; }
    /** @return Pointer to the ACTFEC interface */
    Register* ACTFEC() const { return fACTFEC; }
    /** @return Pointer to IMEM interface */
    InstructionMemory* IMEM() const { return fIMEM; }
    /** @return Pointer to RMEM interface */
    ResultMemory* RMEM() const { return fRMEM; }
    /** @return Pointer to PMEM interface */
    PatternMemory* PMEM() const { return fPMEM; }
    /** @return Pointer to DM1 interface */
    Memory* DM1() const { return fDM1; }
    /** @return Pointer to DM2 interface */
    Memory* DM2() const { return fDM2; }
    /** @return Pointer to ACL interface */
    ActiveChannels* ACL() const { return fACL; }
    /** Get reference to low-level interface */
    Rcuxx::Rcu& Low() { return fLow; }
  protected:
    /** @param cont Setup commands in @a cont */
    void SetupCommands(TGCompositeFrame& cont);
    /** Setup regsters 
	@param cont     Containenr 
	@param maxFEC   Maximum FECs
	@param maxALTRO Maximum ALTROs/ FEC    */
    void SetupRegisters(TGCompositeFrame& cont, 
			UInt_t maxFEC, 
			const UInt_t* mask);
    
    /** Pointer to low-level control object */
    Rcuxx::Rcu& fLow;
    /** Tabs */ 
    TGTab fTab;
    /** Commands and registers - managed by tabs */
    TGCompositeFrame* fFront;
    /** Front commands */ 
    TGGroupFrame* fFrontCmd;
    /** Element */
    TGCompositeFrame* fEvent;
    /** Front commands */ 
    TGGroupFrame* fEventCmd;
    /** Element */ 
    TGCompositeFrame* fBus;
    /** Front commands */ 
    TGGroupFrame* fBusCmd;
    /** Element */
    TGCompositeFrame* fTTC;
    /** Front commands */ 
    TGGroupFrame* fTTCCmd;
    /** Element */ 
    TGCompositeFrame* fALTRO;
    /** Front commands */ 
    TGGroupFrame* fALTROCmd;
    /** Element */
    TGCompositeFrame* fMonitor;
    /** Front commands */ 
    TGGroupFrame* fMonitorCmd;
    /** Element */
    TGCompositeFrame* fFW;
    /** Front commands */ 
    TGGroupFrame* fFWCmd;
    
    /** @{
	@name Access to the commands */
    /** Frame of commands */
    TGLayoutHints fCommandHints;
    /** Interface to CLeaR EVent TAG command */
    Command* fCLR_EVTAG;
    /** Interface to DCS OwN bus command */
    Command* fDCS_ON;
    /** Interface to DDL OwN bus command */
    Command* fDDL_ON;
    /** Interface to Front-End Card ReSeT command */
    Command* fFECRST;
    /** Interface to CONFigure Front-End Card command */
    Command* fCONFFEC;
    /** Interface to GLoBal RESET command */
    Command* fGLB_RESET;
    /** Intefaceto L1-enable via CoMmanD command */
    Command* fL1_CMD;
    /** Interface L1-enable via I2C command */
    Command* fL1_I2C;
    /** Interface to L1-enable via TTC command */
    Command* fL1_TTC;
    /** Interace to RCU RESET command */
    Command* fRCU_RESET;
    /** Interface to ReaD-out ABORT command */
    Command* fRDABORT;
    /** Interface ReaD FirMware command */
    Command* fRDFM;
    /** Interface to Slow Control COMMAND command */
    Command* fSCCOMMAND;
    /** Interface SoftWare TRiGger command */
    Command* fSWTRG;
    /** Interface to TRiGger CLeaR command */
    Command* fTRG_CLR;
    /** Interface to WRite FirMware command */
    Command* fWRFM;
    /** @} */


    /** @{
	@name Access to registers */
    /** Interface to the ERRor and STatus register.   */
    Register* fERRST;
    /** Cache of the TRigger CouNTers.   */
    Register* fTRCNT;
    /** Last Written ADDress in the RcuDMEM1 and RcuDMEM2.   */
    Register* fLWADD;
    /** Cache the last executed ALTRO InstRuction ADDress.   */
    Register* fIRADD;
    /** Cache the last executed ALTRO InstRuction DATa. */
    Register* fIRDAT;
    /** Interface to the EVent WORD register.   */
    Register* fEVWORD;
    /** Interface to the ACTive FrontEnd Card.   */
    Register* fACTFEC;
    /** Interface to the ReaDOut FrontEnd Card register.   */
    Register* fRDOFEC;
    /** Interface to the TRigger ConFiG register.   */
    Register* fTRCFG1;
    /** Interface to the ReaD-Out MODe register.   */
    Register* fTRCFG2;
    /** Interface to the FirMware Input ReGister. */
    Register* fFMIREG;
    /** Interface to the FirMware Output ReGister  */
    Register* fFMOREG;
    /** Interface to the Pedestal Memory ConFiGuration */
    Register* fPMCFG;
    /** Interface to the CHannel Address register  */
    Register* fCHADD;
    /** Interface to the INTerrupt REGister  */
    Register* fINTREG;
    /** Interface to slow control commands */ 
    Register* fSCCLK;
    /** Interface to slow control commands */ 
    Register* fSCINT;
    /** Interface to the RESult REGister  */
    Register* fRESREG;
    /** Interface to the ERRor REGister  */
    Register* fERRREG;
    /** Interface to the INTerrupt MODe register  */
    Register* fINTMOD;
    /** Firmware version  */
    Register* fFWVERS;
    /** */
    Register* fABDFSMST;
    Register* fRDOFSMST;
    Register* fINSSEQST;
    Register* fEVMNGST;
    /** @} */
    

    Register* fBPVERS;
    Register* fALTROCFG1;
    Register* fALTROCFG2;
    Register* fTRGCONF;
    Register* fALTROIF;
    Register* fRCUBUS;
    Register* fRDOMOD;
    Register* fRDOERR;
    Register* fALTBUS_STATUS;
    Register* fALTBUS_TRSF;
    Register* fFECERRA;
    Register* fFECERRB;
    Command*  fARBITERIRQ;
    Register* fMEBSTCNT;
    Register* fCounters;

    Register* fTTCControl;
    Command*  fTTCReset;
    Command*  fTTCResetCnt;
    Command*  fTTCTestMode;
    Register* fROIConfig;
    Register* fL1Timeout;
    Register* fL2Timeout;
    Register* fL1MsgTimeout;
    Register* fROITimeout;

    Register* fTTCCounters;
    Register* fTTCEventInfo;
    Register* fTTCEventError;
    
    
    /** Pointer to IMEM interface */
    InstructionMemory* fIMEM;
    /** Pointer to RMEM interface */
    ResultMemory* fRMEM;
    /** Pointer to PMEM interface */
    PatternMemory* fPMEM;
    /** Pointer to DM1 interface */
    Memory* fDM1;
    /** Pointer to DM2 interface */
    Memory* fDM2;
    /** Pointer to ACL interface */
    ActiveChannels* fACL;
    /** Pointer to ACL interface */
    Memory* fHITLIST;
    /** Pointer to ACL interface */
    Memory* fHEADER;
    /** Status Memory */
    Status* fSTATUS;
    /** Status Memory */
    CDH* fCDH;
  };
}

#endif
//____________________________________________________________________
//
// EOF
//

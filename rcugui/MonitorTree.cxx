//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "unistd.h"
#include "MonitorTree.h"
#ifdef HAVE_LOWLEVEL
# include "rcuxx/Rcu.h"
# include "rcuxx/rcu/RcuACTFEC.h"
# include "rcuxx/rcu/RcuACL.h"
# include "rcuxx/Altro.h"
# include "rcuxx/altro/AltroTRCFG.h"
#endif
#include <rcudata/ProgressMeter.h>
#include <iostream>
#include <algorithm>

//====================================================================
RcuGui::MonitorTree::MonitorTree(TGCompositeFrame& p, UInt_t w, UInt_t h,
				 const std::string& topName)
  : LinkedTree(p, w, h, kHorizontalFrame), 
    fNeedUpdate(kFALSE), 
    fTopName(topName)
{
  fTopEntry     = fList.AddItem(0, fTopName.c_str());
  fCurrentEntry = fTopEntry;
  Connect("SelectionChanged()", "RcuGui::MonitorTree", this, "HandleSelect()");
}

//____________________________________________________________________
void
RcuGui::MonitorTree::UpdateList()
{
  if (!fNeedUpdate) return;
  TGListTreeItem* save = fCurrentEntry;
  for (SortList::iterator i = fNeedSort.begin(); i != fNeedSort.end(); ++i)
    fList.SortChildren(*i);
  LinkedTree::UpdateList();
  fNeedSort.clear();
  fNeedUpdate = kFALSE;
  fCurrentEntry = save;
}

//____________________________________________________________________
void
RcuGui::MonitorTree::PreMakeHistograms(const char* url) 
{
#ifdef HAVE_LOWLEVEL
  // Get low-level interface 
  Rcuxx::Rcu* rcu = Rcuxx::Rcu::Open(url);
  if (!rcu) {
    std::cerr << "\nCouldn't connect to Rcu at " << url << std::endl;
    return;
  }
  Rcuxx::Altro altro(*rcu);
  sleep(1);
  unsigned int ddl = 0;
  
  // update top-level registers 
  Int_t ret;
  if ((ret = rcu->ACTFEC()->Update())) {
    std::cerr << "Error from RCU when reading ACTFEC: " 
	      << rcu->ErrorString(ret) << std::endl;
    return;
  }
  if ((ret = rcu->ACL()->Update())) {
    std::cerr << "Error from RCU when reading ACL: " 
	      << rcu->ErrorString(ret) << std::endl;
    return;
  }

  // Make sure we sync to ACL. 
  rcu->ACTFEC()->SyncToACL(*rcu->ACL());
  // const Rcuxx::RcuMemory::Cache_t& acl = rcu->ACL()->Data();

  // Reset progress meter. 
  RcuData::ProgressMeter pm(-1, 1);
  TString path;

  // Say that we need to update list 
  fNeedUpdate = kTRUE;
  for (size_t board = 0; board < 32; board++) {
    // Check if board is on. 
    if (!rcu->ACTFEC()->IsOn(board)) continue;
    
    // Call user routine for board stuff. 
    if (!MakeBoard(ddl,board)) continue;

    for (size_t chip = 0; chip < 8; chip++) {
      // Get the timebin range of this chip
      unsigned int mask = rcu->ACL()->CheckChip(board, chip);
      if (mask == 0) continue;
      altro.SetAddress(board, chip);
      if ((ret = altro.TRCFG()->Update())) {
	std::cerr << "Failed to acquisition window from ALTRO at " 
		  << board << "," << chip << std::endl;
	return;
      }
      size_t min = altro.TRCFG()->ACQ_START() + 14;
      size_t max = altro.TRCFG()->ACQ_END()   + 14;

      // Loop over channels 
      for (size_t chan = 0; chan < 16; chan++) {
	// Check if channel is on. 
	if ((mask >> chan & 0x1) != 0x1) continue;

	// Call user routine to do channel stuff. 
	if (!MakeChannel(ddl,board, chip, chan)) continue;

	// Loop over timebins 
	for (size_t t = min; t < max; t++) 
	  if (!MakeTimebin(ddl,board, chip, chan, t)) break;

	// Sort children.
	// path = Form(CHANNEL_PATH, board, chip, chan);
	// fList.SortChildren(fList.FindItemByPathname(path.Data()));
      } // For chan 
      // path = Form(CHIP_PATH, board, chip);
      // fList.SortChildren(fList.FindItemByPathname(path.Data()));
    } // For chip 
    // path = Form(BOARD_PATH, board);
    // fList.Sort(fList.FindItemByPathname(path.Data()));
  } // For board

  // Update the display
  UpdateList();

  // Close connection
  delete rcu;
#else
  std::cerr << "Low-level RCU connectability not compiled in" << std::endl;
#endif
}

//____________________________________________________________________
//
// EOF
//


  




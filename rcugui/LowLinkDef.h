// -*- mode: c++ -*-
//____________________________________________________________________ 
//  
// $Id: LowLinkDef.h,v 1.9 2010-07-07 10:44:37 hehi Exp $ 
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    rcugui/LowLinkDef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:57 2004
    @brief   Linkage specifications 
*/
#ifndef __CINT__
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace RcuGui;
#pragma link C++ class     RcuGui::Main;
#pragma link C++ class     RcuGui::Register;
#pragma link C++ class     RcuGui::Acquisition;
#pragma link C++ class     RcuGui::Altro;
#pragma link C++ class     RcuGui::Bc;
#pragma link C++ class     RcuGui::Rcu;
#pragma link C++ class     RcuGui::Memory;
#pragma link C++ class     RcuGui::PatternMemory;
#pragma link C++ class     RcuGui::InstructionMemory;
#pragma link C++ class     RcuGui::PedestalConfig;
#pragma link C++ class     RcuGui::Status;
#pragma link C++ class     RcuGui::Compiler;
#pragma link C++ class     RcuGui::ProgramSelect;
#pragma link C++ class     RcuGui::Command;
#pragma link C++ class     RcuGui::ActiveBit;
#pragma link C++ class     RcuGui::ActiveChannels+;
#pragma link C++ class     RcuGui::ResultMemory;
#pragma link C++ class     RcuGui::CDH;

//____________________________________________________________________ 
//  
// EOF
//

//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Scrollable.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul 22 07:39:01 2006
    @brief   
*/
#ifndef RCUGUI_SCROLLABLE_H
# include <rcugui/Scrollable.h>
#endif

RcuGui::Scrollable::Scrollable(const char* name, TGTab& tabs, UInt_t options) 
  : fTop(tabs.AddTab(name)), 
    fCanvasW(fTop->GetWidth() - 6),
    fCanvasH(fTop->GetHeight() - 6),
    fCanvas(fTop, fCanvasW, fCanvasH, kChildFrame),
    fCont(fCanvas.GetViewPort()),
    fName(name)
{
  fCanvas.SetContainer(&fCont);
  fCanvas.GetViewPort()->SetContainer(&fCont);
  fCont.SetLayoutManager(options == kHorizontalFrame 
			 ? new TGHorizontalLayout(&fCont) 
			 : new TGVerticalLayout(&fCont));
  fTop->AddFrame(&fCanvas, 
		 new TGLayoutHints(kLHintsExpandX|kLHintsExpandY|kLHintsBottom,
				   3, 3, 3, 3));
}

  
//
// EOF
//

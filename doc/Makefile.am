#
# $Id: Makefile.am,v 1.5 2009-02-09 23:11:57 hehi Exp $
#
#  Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation; either version 2.1
#  of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free
#  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
#  02111-1307 USA
#
PNGS			= favicon.png			\
			  rcugui_actfec.png		\
			  rcugui_altro.png		\
			  rcugui_altroclk.png		\
			  rcugui_bc.png			\
			  rcugui_bcfull.png		\
			  rcugui_begin.png		\
			  rcugui_compile.png		\
			  rcugui_fecrst.png		\
			  rcugui_fmd.png		\
			  rcugui_fmdcmd.png		\
			  rcugui_pulser.png		\
			  rcugui_rcu.png		\
			  rcugui_regcmd.png		\
			  rcugui_setup.png		\
			  rcugui_setup_puls.png		\
			  rcugui_start.png

DOCS			= GUI.h				\
			  Mainpage.h			

EXTRA_DIST		= header.html.in 	\
			  footer.html 		\
			  style.css 		\
			  doxyconfig.in 	\
			  $(PNGS) $(DOCS)

DISTCLEANFILES		= doxygen.log $(PACKAGE).tags header.html
if HAVE_DOXYGEN
HTML			= html/index.html 
TAGS			= $(PACKAGE).tags
else 
HTML			=
TAGS			= 
endif
INSTALLDOX_FLAGS	= $(RCUDATA_INSTDOX) $(RCUXX_INSTDOX)
RCUDATA_INSTDOX		= -l rcudata.tags@$(RCUDATA_URL)/html 
RCUXX_INSTDOX		= -l rcuxx.tags@$(RCUXX_URL)/html 
docdir			= $(datadir)/doc/$(PACKAGE)
doc_DATA		= $(TAGS)

%.png:%.fig
	fig2dev -Lpng $< 

all-local:$(HTML)

install-data-local:
if HAVE_DOXYGEN
	if test -x html/installdox ; then \
	  (cd html && ./installdox -q $(INSTALLDOX_FLAGS)) ; fi
	$(mkinstalldirs) $(DESTDIR)$(docdir)/html 
	$(INSTALL_DATA) $(wildcard html/*) $(DESTDIR)$(docdir)/html 
endif

uninstall-local:
	rm -rf $(DESTDIR)$(docdir)/html 

clean-local: 
	rm -rf *~ html latex man

html/index.html $(PACKAGE).tags: doxyconfig	\
				 header.html 	\
				 footer.html 	\
				 style.css 	\
				 $(PNGS) $(DOCS)
	$(DOXYGEN) $<  
	if test -f @srcdir@/favicon.png ; then \
		cp -f @srcdir@/favicon.png html ; fi
	if test -x html/installdox ; then \
	  (cd html && ./installdox -q $(INSTALLDOX_FLAGS)) ; fi

tar-ball:$(HTML)
	if test -f html/index.html ; then \
	  mv html $(PACKAGE) ;	\
	  cp -f $(TAGS) $(PACKAGE) ; \
	  tar -czvf ../$(PACKAGE)-$(VERSION)-doc.tar.gz $(PACKAGE) ; \
	  mv $(PACKAGE) html ; \
	  rm -f html/$(notdir $(TAGS)) ; fi 


#
# EOF
#

/*
 * $Id: GUI.h,v 1.2 2009-02-09 23:11:57 hehi Exp $
 *
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   GUI.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  GUI documentation. */
/** @page gui Graphical User Interface  

   @section login Login, load the driver and start the GUI
    <OL>
    <LI> Log in 
    <LI> Open  terminal by right clicking the background, and choose
      @b Open @b Terminal.
    <LI> Next, you need to load the driver for the U2F board.   Do
    @verbatim 
    prompt% sudo ~/sbin/altro restart
    Password:
    @endverbatim

    The password is the same as for the login.

    <LI>Next,you need to start the GUI:
    @verbatim 
    prompt% rcugui
    @endverbatim 
    
    This gives you the well known GUI:
    
    @image html rcugui_begin.png 
    </OL>


    The options for the program are 
    @verbatim
    Usage: rcugui [OPTIONS] [FILE]
    
    Options:
            -e      Emulation mode
            -f      Use FMD
            -d      Debug RCU++
            -b      Debug Backend
            -a      Debug ALTRO
            -I URL  Device URL to talk to
            -h      This help

    @endverbatim
    Using the FMD means that theh FMD tab is visible, and that
    RcuData::FmdAcg data aquisition class is used rather than the
    normal RcuData::RootAcq class. 

    @section setup Setting up to do an acquisition
    <OL>
    <LI> Now, go back o the @b Acquisition tab,and select the number
      of events and trigger type (should be @b External). If the
      number of events is negative, the DAQ takes an infinite number
      of events.  

      @image html rcugui_setup.png 


    <LI> And now, press @b Start to start the run.

      @image html rcugui_start.png


    <LI> Check in the terminal that the program is really taking data.
      you should see a progress bar, that tells you how many events
      has been taken so far, how much time is left, and a throbber
      that tells you that the aquisition program is running.    If
      not, then you need to shut down the GUI, press reset on the U2F
      board, and go back to point 1 in this guide.
    </ol>


    @section pulser Setting up to use the calibration pulser

    <OL>
      <LI> The first thing to do, so to go to the @b RCU tab and the
        sub tab @b Registers @b and @b commands.

	@image html rcugui_regcmd.png 

      <LI> There, you need to press @b Commit to the @b ACTFEC
	register, and press the @b Reset @b FEC button (once or
	twice)  

	@image html rcugui_actfec.png
	@image html rcugui_fecrst.png

      <LI> @b Optional: Now, go to the @b FMD tab, and make sure
	that the @b ALTRO clock  phase is half of the divisor. 

      <LI> @b Turn on the pulser, set a DAC value (in the range from
        @c 0x00 to @c 0xFF), press @b Commit, and then press @b
	Change @b DACs 
	  
	@image html rcugui_pulser.png 

      <LI> Next, you should go back to the @b Acquisition tab and
        de-select @b Active @b FEC's and @b Reset @b FEC's.
	 
	@image html rcugui_setup_puls.png

      <LI> Next, follow he guidelines for a normal acquisition as
        given iin @ref setup

    </OL>

*/

#error Not for compilation
//
// EOF
//
